module miter ( a, b, c, p1, p2 );

 input wire  [7:0] a, b, c;

 wire [15:0] s1;
 wire [15:0] t1, t2;
 output wire [15:0] p1, p2;

 assign s1 = a + b;
 assign p1 = s1 * c;

 assign t1 = a * c;
 assign t2 = b * c;
 assign p2 = t1 + t2;


endmodule
