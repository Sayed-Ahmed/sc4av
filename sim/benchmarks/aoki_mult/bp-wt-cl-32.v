/*----------------------------------------------------------------------------
  Copyright (c) 2004 Aoki laboratory. All rights reserved.

  Top module: Multiplier_31_0_3000

  Number system: Unsigned binary
  Multiplicand length: 32
  Multiplier length: 32
  Partial product generation: PPG with Radix-4 modified Booth recoding
  Partial product accumulation: Wallace tree
  Final stage addition: Carry look-ahead adder
----------------------------------------------------------------------------*/

module NUBZero_32_32(O);
  output [32:32] O;
  assign O[32] = 0;
endmodule

module R4BEEL_0_2(O_ds, O_d1, O_d0, I2, I1);
  output O_ds, O_d1, O_d0;
  input I1;
  input I2;
  assign O_d0 = I1;
  assign O_d1 = I2 & ( ~ I1 );
  assign O_ds = I2;
endmodule

module R4BEE_1(O_ds, O_d1, O_d0, I2, I1, I0);
  output O_ds, O_d1, O_d0;
  input I0;
  input I1;
  input I2;
  assign O_d0 = I1 ^ I0;
  assign O_d1 = ( I2 ^ I1 ) & ( ~ ( I1 ^ I0 ) );
  assign O_ds = I2;
endmodule

module R4BEE_2(O_ds, O_d1, O_d0, I2, I1, I0);
  output O_ds, O_d1, O_d0;
  input I0;
  input I1;
  input I2;
  assign O_d0 = I1 ^ I0;
  assign O_d1 = ( I2 ^ I1 ) & ( ~ ( I1 ^ I0 ) );
  assign O_ds = I2;
endmodule

module R4BEE_3(O_ds, O_d1, O_d0, I2, I1, I0);
  output O_ds, O_d1, O_d0;
  input I0;
  input I1;
  input I2;
  assign O_d0 = I1 ^ I0;
  assign O_d1 = ( I2 ^ I1 ) & ( ~ ( I1 ^ I0 ) );
  assign O_ds = I2;
endmodule

module R4BEE_4(O_ds, O_d1, O_d0, I2, I1, I0);
  output O_ds, O_d1, O_d0;
  input I0;
  input I1;
  input I2;
  assign O_d0 = I1 ^ I0;
  assign O_d1 = ( I2 ^ I1 ) & ( ~ ( I1 ^ I0 ) );
  assign O_ds = I2;
endmodule

module R4BEE_5(O_ds, O_d1, O_d0, I2, I1, I0);
  output O_ds, O_d1, O_d0;
  input I0;
  input I1;
  input I2;
  assign O_d0 = I1 ^ I0;
  assign O_d1 = ( I2 ^ I1 ) & ( ~ ( I1 ^ I0 ) );
  assign O_ds = I2;
endmodule

module R4BEE_6(O_ds, O_d1, O_d0, I2, I1, I0);
  output O_ds, O_d1, O_d0;
  input I0;
  input I1;
  input I2;
  assign O_d0 = I1 ^ I0;
  assign O_d1 = ( I2 ^ I1 ) & ( ~ ( I1 ^ I0 ) );
  assign O_ds = I2;
endmodule

module R4BEE_7(O_ds, O_d1, O_d0, I2, I1, I0);
  output O_ds, O_d1, O_d0;
  input I0;
  input I1;
  input I2;
  assign O_d0 = I1 ^ I0;
  assign O_d1 = ( I2 ^ I1 ) & ( ~ ( I1 ^ I0 ) );
  assign O_ds = I2;
endmodule

module R4BEE_8(O_ds, O_d1, O_d0, I2, I1, I0);
  output O_ds, O_d1, O_d0;
  input I0;
  input I1;
  input I2;
  assign O_d0 = I1 ^ I0;
  assign O_d1 = ( I2 ^ I1 ) & ( ~ ( I1 ^ I0 ) );
  assign O_ds = I2;
endmodule

module R4BEE_9(O_ds, O_d1, O_d0, I2, I1, I0);
  output O_ds, O_d1, O_d0;
  input I0;
  input I1;
  input I2;
  assign O_d0 = I1 ^ I0;
  assign O_d1 = ( I2 ^ I1 ) & ( ~ ( I1 ^ I0 ) );
  assign O_ds = I2;
endmodule

module R4BEE_10(O_ds, O_d1, O_d0, I2, I1, I0);
  output O_ds, O_d1, O_d0;
  input I0;
  input I1;
  input I2;
  assign O_d0 = I1 ^ I0;
  assign O_d1 = ( I2 ^ I1 ) & ( ~ ( I1 ^ I0 ) );
  assign O_ds = I2;
endmodule

module R4BEE_11(O_ds, O_d1, O_d0, I2, I1, I0);
  output O_ds, O_d1, O_d0;
  input I0;
  input I1;
  input I2;
  assign O_d0 = I1 ^ I0;
  assign O_d1 = ( I2 ^ I1 ) & ( ~ ( I1 ^ I0 ) );
  assign O_ds = I2;
endmodule

module R4BEE_12(O_ds, O_d1, O_d0, I2, I1, I0);
  output O_ds, O_d1, O_d0;
  input I0;
  input I1;
  input I2;
  assign O_d0 = I1 ^ I0;
  assign O_d1 = ( I2 ^ I1 ) & ( ~ ( I1 ^ I0 ) );
  assign O_ds = I2;
endmodule

module R4BEE_13(O_ds, O_d1, O_d0, I2, I1, I0);
  output O_ds, O_d1, O_d0;
  input I0;
  input I1;
  input I2;
  assign O_d0 = I1 ^ I0;
  assign O_d1 = ( I2 ^ I1 ) & ( ~ ( I1 ^ I0 ) );
  assign O_ds = I2;
endmodule

module R4BEE_14(O_ds, O_d1, O_d0, I2, I1, I0);
  output O_ds, O_d1, O_d0;
  input I0;
  input I1;
  input I2;
  assign O_d0 = I1 ^ I0;
  assign O_d1 = ( I2 ^ I1 ) & ( ~ ( I1 ^ I0 ) );
  assign O_ds = I2;
endmodule

module R4BEE_15(O_ds, O_d1, O_d0, I2, I1, I0);
  output O_ds, O_d1, O_d0;
  input I0;
  input I1;
  input I2;
  assign O_d0 = I1 ^ I0;
  assign O_d1 = ( I2 ^ I1 ) & ( ~ ( I1 ^ I0 ) );
  assign O_ds = I2;
endmodule

module R4BEEH_16_2(O_ds, O_d1, O_d0, I1, I0);
  output O_ds, O_d1, O_d0;
  input I0;
  input I1;
  assign O_d0 = I1 ^ I0;
  assign O_d1 = 0;
  assign O_ds = I1;
endmodule

module SD41DDECON_0(S, U_d1, U_d0, I_ds, I_d1, I_d0);
  output S;
  output U_d1, U_d0;
  input I_ds, I_d1, I_d0;
  assign S = I_ds;
  assign U_d0 = I_d0;
  assign U_d1 = I_d1;
endmodule

module U4DPPGL_0_0(Po, O, I, U_d1, U_d0);
  output O;
  output Po;
  input I;
  input U_d1, U_d0;
  assign O = I & U_d0;
  assign Po = I & U_d1;
endmodule

module U4DPPG_1_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_2_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_3_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_4_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_5_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_6_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_7_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_8_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_9_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_10_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_11_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_12_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_13_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_14_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_15_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_16_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_17_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_18_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_19_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_20_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_21_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_22_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_23_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_24_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_25_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_26_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_27_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_28_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_29_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_30_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_31_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPGH_32_0(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & ( U_d1 | U_d0 );
endmodule

module BWCPP_0(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_1(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_2(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_3(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_4(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_5(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_6(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_7(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_8(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_9(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_10(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_11(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_12(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_13(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_14(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_15(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_16(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_17(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_18(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_19(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_20(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_21(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_22(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_23(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_24(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_25(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_26(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_27(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_28(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_29(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_30(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_31(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_32(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCNP_33(O, I, S);
  output O;
  input I;
  input S;
  assign O = ( ~ S ) ^ I;
endmodule

module UBBBG_0(O, S);
  output O;
  input S;
  assign O = S;
endmodule

module SD41DDECON_1(S, U_d1, U_d0, I_ds, I_d1, I_d0);
  output S;
  output U_d1, U_d0;
  input I_ds, I_d1, I_d0;
  assign S = I_ds;
  assign U_d0 = I_d0;
  assign U_d1 = I_d1;
endmodule

module U4DPPGL_0_1(Po, O, I, U_d1, U_d0);
  output O;
  output Po;
  input I;
  input U_d1, U_d0;
  assign O = I & U_d0;
  assign Po = I & U_d1;
endmodule

module U4DPPG_1_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_2_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_3_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_4_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_5_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_6_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_7_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_8_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_9_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_10_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_11_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_12_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_13_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_14_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_15_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_16_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_17_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_18_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_19_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_20_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_21_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_22_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_23_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_24_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_25_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_26_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_27_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_28_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_29_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_30_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_31_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPGH_32_1(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & ( U_d1 | U_d0 );
endmodule

module BWCPP_33(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_34(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCNP_35(O, I, S);
  output O;
  input I;
  input S;
  assign O = ( ~ S ) ^ I;
endmodule

module UBBBG_2(O, S);
  output O;
  input S;
  assign O = S;
endmodule

module SD41DDECON_2(S, U_d1, U_d0, I_ds, I_d1, I_d0);
  output S;
  output U_d1, U_d0;
  input I_ds, I_d1, I_d0;
  assign S = I_ds;
  assign U_d0 = I_d0;
  assign U_d1 = I_d1;
endmodule

module U4DPPGL_0_2(Po, O, I, U_d1, U_d0);
  output O;
  output Po;
  input I;
  input U_d1, U_d0;
  assign O = I & U_d0;
  assign Po = I & U_d1;
endmodule

module U4DPPG_1_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_2_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_3_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_4_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_5_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_6_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_7_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_8_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_9_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_10_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_11_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_12_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_13_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_14_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_15_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_16_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_17_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_18_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_19_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_20_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_21_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_22_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_23_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_24_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_25_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_26_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_27_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_28_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_29_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_30_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_31_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPGH_32_2(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & ( U_d1 | U_d0 );
endmodule

module BWCPP_35(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_36(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCNP_37(O, I, S);
  output O;
  input I;
  input S;
  assign O = ( ~ S ) ^ I;
endmodule

module UBBBG_4(O, S);
  output O;
  input S;
  assign O = S;
endmodule

module SD41DDECON_3(S, U_d1, U_d0, I_ds, I_d1, I_d0);
  output S;
  output U_d1, U_d0;
  input I_ds, I_d1, I_d0;
  assign S = I_ds;
  assign U_d0 = I_d0;
  assign U_d1 = I_d1;
endmodule

module U4DPPGL_0_3(Po, O, I, U_d1, U_d0);
  output O;
  output Po;
  input I;
  input U_d1, U_d0;
  assign O = I & U_d0;
  assign Po = I & U_d1;
endmodule

module U4DPPG_1_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_2_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_3_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_4_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_5_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_6_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_7_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_8_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_9_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_10_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_11_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_12_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_13_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_14_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_15_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_16_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_17_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_18_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_19_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_20_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_21_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_22_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_23_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_24_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_25_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_26_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_27_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_28_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_29_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_30_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_31_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPGH_32_3(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & ( U_d1 | U_d0 );
endmodule

module BWCPP_37(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_38(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCNP_39(O, I, S);
  output O;
  input I;
  input S;
  assign O = ( ~ S ) ^ I;
endmodule

module UBBBG_6(O, S);
  output O;
  input S;
  assign O = S;
endmodule

module SD41DDECON_4(S, U_d1, U_d0, I_ds, I_d1, I_d0);
  output S;
  output U_d1, U_d0;
  input I_ds, I_d1, I_d0;
  assign S = I_ds;
  assign U_d0 = I_d0;
  assign U_d1 = I_d1;
endmodule

module U4DPPGL_0_4(Po, O, I, U_d1, U_d0);
  output O;
  output Po;
  input I;
  input U_d1, U_d0;
  assign O = I & U_d0;
  assign Po = I & U_d1;
endmodule

module U4DPPG_1_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_2_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_3_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_4_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_5_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_6_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_7_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_8_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_9_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_10_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_11_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_12_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_13_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_14_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_15_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_16_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_17_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_18_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_19_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_20_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_21_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_22_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_23_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_24_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_25_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_26_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_27_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_28_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_29_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_30_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_31_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPGH_32_4(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & ( U_d1 | U_d0 );
endmodule

module BWCPP_39(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_40(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCNP_41(O, I, S);
  output O;
  input I;
  input S;
  assign O = ( ~ S ) ^ I;
endmodule

module UBBBG_8(O, S);
  output O;
  input S;
  assign O = S;
endmodule

module SD41DDECON_5(S, U_d1, U_d0, I_ds, I_d1, I_d0);
  output S;
  output U_d1, U_d0;
  input I_ds, I_d1, I_d0;
  assign S = I_ds;
  assign U_d0 = I_d0;
  assign U_d1 = I_d1;
endmodule

module U4DPPGL_0_5(Po, O, I, U_d1, U_d0);
  output O;
  output Po;
  input I;
  input U_d1, U_d0;
  assign O = I & U_d0;
  assign Po = I & U_d1;
endmodule

module U4DPPG_1_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_2_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_3_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_4_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_5_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_6_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_7_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_8_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_9_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_10_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_11_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_12_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_13_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_14_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_15_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_16_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_17_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_18_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_19_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_20_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_21_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_22_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_23_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_24_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_25_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_26_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_27_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_28_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_29_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_30_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_31_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPGH_32_5(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & ( U_d1 | U_d0 );
endmodule

module BWCPP_41(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_42(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCNP_43(O, I, S);
  output O;
  input I;
  input S;
  assign O = ( ~ S ) ^ I;
endmodule

module UBBBG_10(O, S);
  output O;
  input S;
  assign O = S;
endmodule

module SD41DDECON_6(S, U_d1, U_d0, I_ds, I_d1, I_d0);
  output S;
  output U_d1, U_d0;
  input I_ds, I_d1, I_d0;
  assign S = I_ds;
  assign U_d0 = I_d0;
  assign U_d1 = I_d1;
endmodule

module U4DPPGL_0_6(Po, O, I, U_d1, U_d0);
  output O;
  output Po;
  input I;
  input U_d1, U_d0;
  assign O = I & U_d0;
  assign Po = I & U_d1;
endmodule

module U4DPPG_1_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_2_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_3_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_4_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_5_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_6_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_7_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_8_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_9_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_10_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_11_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_12_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_13_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_14_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_15_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_16_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_17_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_18_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_19_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_20_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_21_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_22_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_23_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_24_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_25_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_26_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_27_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_28_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_29_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_30_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_31_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPGH_32_6(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & ( U_d1 | U_d0 );
endmodule

module BWCPP_43(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_44(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCNP_45(O, I, S);
  output O;
  input I;
  input S;
  assign O = ( ~ S ) ^ I;
endmodule

module UBBBG_12(O, S);
  output O;
  input S;
  assign O = S;
endmodule

module SD41DDECON_7(S, U_d1, U_d0, I_ds, I_d1, I_d0);
  output S;
  output U_d1, U_d0;
  input I_ds, I_d1, I_d0;
  assign S = I_ds;
  assign U_d0 = I_d0;
  assign U_d1 = I_d1;
endmodule

module U4DPPGL_0_7(Po, O, I, U_d1, U_d0);
  output O;
  output Po;
  input I;
  input U_d1, U_d0;
  assign O = I & U_d0;
  assign Po = I & U_d1;
endmodule

module U4DPPG_1_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_2_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_3_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_4_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_5_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_6_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_7_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_8_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_9_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_10_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_11_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_12_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_13_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_14_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_15_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_16_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_17_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_18_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_19_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_20_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_21_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_22_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_23_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_24_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_25_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_26_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_27_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_28_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_29_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_30_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_31_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPGH_32_7(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & ( U_d1 | U_d0 );
endmodule

module BWCPP_45(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_46(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCNP_47(O, I, S);
  output O;
  input I;
  input S;
  assign O = ( ~ S ) ^ I;
endmodule

module UBBBG_14(O, S);
  output O;
  input S;
  assign O = S;
endmodule

module SD41DDECON_8(S, U_d1, U_d0, I_ds, I_d1, I_d0);
  output S;
  output U_d1, U_d0;
  input I_ds, I_d1, I_d0;
  assign S = I_ds;
  assign U_d0 = I_d0;
  assign U_d1 = I_d1;
endmodule

module U4DPPGL_0_8(Po, O, I, U_d1, U_d0);
  output O;
  output Po;
  input I;
  input U_d1, U_d0;
  assign O = I & U_d0;
  assign Po = I & U_d1;
endmodule

module U4DPPG_1_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_2_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_3_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_4_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_5_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_6_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_7_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_8_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_9_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_10_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_11_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_12_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_13_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_14_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_15_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_16_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_17_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_18_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_19_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_20_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_21_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_22_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_23_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_24_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_25_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_26_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_27_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_28_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_29_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_30_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_31_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPGH_32_8(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & ( U_d1 | U_d0 );
endmodule

module BWCPP_47(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_48(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCNP_49(O, I, S);
  output O;
  input I;
  input S;
  assign O = ( ~ S ) ^ I;
endmodule

module UBBBG_16(O, S);
  output O;
  input S;
  assign O = S;
endmodule

module SD41DDECON_9(S, U_d1, U_d0, I_ds, I_d1, I_d0);
  output S;
  output U_d1, U_d0;
  input I_ds, I_d1, I_d0;
  assign S = I_ds;
  assign U_d0 = I_d0;
  assign U_d1 = I_d1;
endmodule

module U4DPPGL_0_9(Po, O, I, U_d1, U_d0);
  output O;
  output Po;
  input I;
  input U_d1, U_d0;
  assign O = I & U_d0;
  assign Po = I & U_d1;
endmodule

module U4DPPG_1_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_2_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_3_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_4_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_5_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_6_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_7_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_8_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_9_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_10_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_11_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_12_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_13_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_14_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_15_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_16_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_17_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_18_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_19_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_20_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_21_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_22_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_23_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_24_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_25_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_26_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_27_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_28_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_29_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_30_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_31_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPGH_32_9(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & ( U_d1 | U_d0 );
endmodule

module BWCPP_49(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_50(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCNP_51(O, I, S);
  output O;
  input I;
  input S;
  assign O = ( ~ S ) ^ I;
endmodule

module UBBBG_18(O, S);
  output O;
  input S;
  assign O = S;
endmodule

module SD41DDECON_10(S, U_d1, U_d0, I_ds, I_d1, I_d0);
  output S;
  output U_d1, U_d0;
  input I_ds, I_d1, I_d0;
  assign S = I_ds;
  assign U_d0 = I_d0;
  assign U_d1 = I_d1;
endmodule

module U4DPPGL_0_10(Po, O, I, U_d1, U_d0);
  output O;
  output Po;
  input I;
  input U_d1, U_d0;
  assign O = I & U_d0;
  assign Po = I & U_d1;
endmodule

module U4DPPG_1_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_2_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_3_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_4_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_5_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_6_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_7_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_8_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_9_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_10_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_11_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_12_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_13_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_14_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_15_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_16_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_17_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_18_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_19_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_20_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_21_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_22_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_23_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_24_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_25_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_26_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_27_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_28_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_29_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_30_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_31_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPGH_32_10(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & ( U_d1 | U_d0 );
endmodule

module BWCPP_51(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_52(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCNP_53(O, I, S);
  output O;
  input I;
  input S;
  assign O = ( ~ S ) ^ I;
endmodule

module UBBBG_20(O, S);
  output O;
  input S;
  assign O = S;
endmodule

module SD41DDECON_11(S, U_d1, U_d0, I_ds, I_d1, I_d0);
  output S;
  output U_d1, U_d0;
  input I_ds, I_d1, I_d0;
  assign S = I_ds;
  assign U_d0 = I_d0;
  assign U_d1 = I_d1;
endmodule

module U4DPPGL_0_11(Po, O, I, U_d1, U_d0);
  output O;
  output Po;
  input I;
  input U_d1, U_d0;
  assign O = I & U_d0;
  assign Po = I & U_d1;
endmodule

module U4DPPG_1_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_2_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_3_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_4_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_5_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_6_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_7_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_8_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_9_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_10_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_11_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_12_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_13_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_14_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_15_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_16_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_17_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_18_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_19_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_20_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_21_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_22_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_23_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_24_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_25_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_26_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_27_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_28_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_29_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_30_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_31_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPGH_32_11(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & ( U_d1 | U_d0 );
endmodule

module BWCPP_53(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_54(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCNP_55(O, I, S);
  output O;
  input I;
  input S;
  assign O = ( ~ S ) ^ I;
endmodule

module UBBBG_22(O, S);
  output O;
  input S;
  assign O = S;
endmodule

module SD41DDECON_12(S, U_d1, U_d0, I_ds, I_d1, I_d0);
  output S;
  output U_d1, U_d0;
  input I_ds, I_d1, I_d0;
  assign S = I_ds;
  assign U_d0 = I_d0;
  assign U_d1 = I_d1;
endmodule

module U4DPPGL_0_12(Po, O, I, U_d1, U_d0);
  output O;
  output Po;
  input I;
  input U_d1, U_d0;
  assign O = I & U_d0;
  assign Po = I & U_d1;
endmodule

module U4DPPG_1_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_2_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_3_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_4_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_5_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_6_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_7_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_8_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_9_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_10_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_11_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_12_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_13_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_14_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_15_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_16_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_17_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_18_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_19_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_20_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_21_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_22_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_23_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_24_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_25_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_26_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_27_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_28_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_29_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_30_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_31_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPGH_32_12(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & ( U_d1 | U_d0 );
endmodule

module BWCPP_55(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_56(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCNP_57(O, I, S);
  output O;
  input I;
  input S;
  assign O = ( ~ S ) ^ I;
endmodule

module UBBBG_24(O, S);
  output O;
  input S;
  assign O = S;
endmodule

module SD41DDECON_13(S, U_d1, U_d0, I_ds, I_d1, I_d0);
  output S;
  output U_d1, U_d0;
  input I_ds, I_d1, I_d0;
  assign S = I_ds;
  assign U_d0 = I_d0;
  assign U_d1 = I_d1;
endmodule

module U4DPPGL_0_13(Po, O, I, U_d1, U_d0);
  output O;
  output Po;
  input I;
  input U_d1, U_d0;
  assign O = I & U_d0;
  assign Po = I & U_d1;
endmodule

module U4DPPG_1_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_2_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_3_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_4_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_5_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_6_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_7_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_8_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_9_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_10_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_11_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_12_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_13_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_14_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_15_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_16_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_17_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_18_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_19_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_20_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_21_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_22_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_23_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_24_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_25_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_26_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_27_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_28_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_29_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_30_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_31_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPGH_32_13(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & ( U_d1 | U_d0 );
endmodule

module BWCPP_57(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_58(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCNP_59(O, I, S);
  output O;
  input I;
  input S;
  assign O = ( ~ S ) ^ I;
endmodule

module UBBBG_26(O, S);
  output O;
  input S;
  assign O = S;
endmodule

module SD41DDECON_14(S, U_d1, U_d0, I_ds, I_d1, I_d0);
  output S;
  output U_d1, U_d0;
  input I_ds, I_d1, I_d0;
  assign S = I_ds;
  assign U_d0 = I_d0;
  assign U_d1 = I_d1;
endmodule

module U4DPPGL_0_14(Po, O, I, U_d1, U_d0);
  output O;
  output Po;
  input I;
  input U_d1, U_d0;
  assign O = I & U_d0;
  assign Po = I & U_d1;
endmodule

module U4DPPG_1_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_2_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_3_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_4_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_5_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_6_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_7_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_8_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_9_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_10_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_11_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_12_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_13_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_14_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_15_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_16_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_17_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_18_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_19_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_20_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_21_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_22_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_23_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_24_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_25_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_26_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_27_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_28_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_29_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_30_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_31_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPGH_32_14(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & ( U_d1 | U_d0 );
endmodule

module BWCPP_59(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_60(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCNP_61(O, I, S);
  output O;
  input I;
  input S;
  assign O = ( ~ S ) ^ I;
endmodule

module UBBBG_28(O, S);
  output O;
  input S;
  assign O = S;
endmodule

module SD41DDECON_15(S, U_d1, U_d0, I_ds, I_d1, I_d0);
  output S;
  output U_d1, U_d0;
  input I_ds, I_d1, I_d0;
  assign S = I_ds;
  assign U_d0 = I_d0;
  assign U_d1 = I_d1;
endmodule

module U4DPPGL_0_15(Po, O, I, U_d1, U_d0);
  output O;
  output Po;
  input I;
  input U_d1, U_d0;
  assign O = I & U_d0;
  assign Po = I & U_d1;
endmodule

module U4DPPG_1_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_2_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_3_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_4_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_5_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_6_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_7_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_8_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_9_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_10_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_11_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_12_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_13_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_14_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_15_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_16_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_17_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_18_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_19_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_20_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_21_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_22_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_23_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_24_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_25_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_26_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_27_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_28_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_29_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_30_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_31_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPGH_32_15(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & ( U_d1 | U_d0 );
endmodule

module BWCPP_61(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_62(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCNP_63(O, I, S);
  output O;
  input I;
  input S;
  assign O = ( ~ S ) ^ I;
endmodule

module UBBBG_30(O, S);
  output O;
  input S;
  assign O = S;
endmodule

module SD41DDECON_16(S, U_d1, U_d0, I_ds, I_d1, I_d0);
  output S;
  output U_d1, U_d0;
  input I_ds, I_d1, I_d0;
  assign S = I_ds;
  assign U_d0 = I_d0;
  assign U_d1 = I_d1;
endmodule

module U4DPPGL_0_16(Po, O, I, U_d1, U_d0);
  output O;
  output Po;
  input I;
  input U_d1, U_d0;
  assign O = I & U_d0;
  assign Po = I & U_d1;
endmodule

module U4DPPG_1_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_2_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_3_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_4_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_5_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_6_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_7_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_8_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_9_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_10_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_11_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_12_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_13_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_14_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_15_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_16_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_17_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_18_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_19_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_20_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_21_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_22_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_23_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_24_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_25_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_26_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_27_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_28_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_29_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_30_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPG_31_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & U_d1;
endmodule

module U4DPPGH_32_16(Po, O, I, U_d1, U_d0, Pi);
  output O;
  output Po;
  input I;
  input Pi;
  input U_d1, U_d0;
  assign O = ( I & U_d0 ) | Pi;
  assign Po = I & ( U_d1 | U_d0 );
endmodule

module BWCPP_63(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCPP_64(O, I, S);
  output O;
  input I;
  input S;
  assign O = S ^ I;
endmodule

module BWCNP_65(O, I, S);
  output O;
  input I;
  input S;
  assign O = ( ~ S ) ^ I;
endmodule

module UBBBG_32(O, S);
  output O;
  input S;
  assign O = S;
endmodule

module UBOne_34(O);
  output O;
  assign O = 1;
endmodule

module UB1DCON_34(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_0(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_1(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_2(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_3(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_4(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_5(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_6(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_7(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_8(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_9(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_10(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_11(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_12(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_13(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_14(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_15(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_16(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_17(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_18(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_19(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_20(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_21(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_22(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_23(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_24(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_25(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_26(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_27(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_28(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_29(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_30(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_31(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_32(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_33(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBOne_36(O);
  output O;
  assign O = 1;
endmodule

module UB1DCON_36(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_35(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBZero_1_1(O);
  output [1:1] O;
  assign O[1] = 0;
endmodule

module UBOne_38(O);
  output O;
  assign O = 1;
endmodule

module UB1DCON_38(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_37(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBZero_3_3(O);
  output [3:3] O;
  assign O[3] = 0;
endmodule

module UBOne_40(O);
  output O;
  assign O = 1;
endmodule

module UB1DCON_40(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_39(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBZero_5_5(O);
  output [5:5] O;
  assign O[5] = 0;
endmodule

module UBOne_42(O);
  output O;
  assign O = 1;
endmodule

module UB1DCON_42(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_41(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBZero_7_7(O);
  output [7:7] O;
  assign O[7] = 0;
endmodule

module UBOne_44(O);
  output O;
  assign O = 1;
endmodule

module UB1DCON_44(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_43(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBZero_9_9(O);
  output [9:9] O;
  assign O[9] = 0;
endmodule

module UBOne_46(O);
  output O;
  assign O = 1;
endmodule

module UB1DCON_46(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_45(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBZero_11_11(O);
  output [11:11] O;
  assign O[11] = 0;
endmodule

module UBOne_48(O);
  output O;
  assign O = 1;
endmodule

module UB1DCON_48(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_47(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBZero_13_13(O);
  output [13:13] O;
  assign O[13] = 0;
endmodule

module UBOne_50(O);
  output O;
  assign O = 1;
endmodule

module UB1DCON_50(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_49(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBZero_15_15(O);
  output [15:15] O;
  assign O[15] = 0;
endmodule

module UBOne_52(O);
  output O;
  assign O = 1;
endmodule

module UB1DCON_52(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_51(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBZero_17_17(O);
  output [17:17] O;
  assign O[17] = 0;
endmodule

module UBOne_54(O);
  output O;
  assign O = 1;
endmodule

module UB1DCON_54(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_53(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBZero_19_19(O);
  output [19:19] O;
  assign O[19] = 0;
endmodule

module UBOne_56(O);
  output O;
  assign O = 1;
endmodule

module UB1DCON_56(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_55(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBZero_21_21(O);
  output [21:21] O;
  assign O[21] = 0;
endmodule

module UBOne_58(O);
  output O;
  assign O = 1;
endmodule

module UB1DCON_58(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_57(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBZero_23_23(O);
  output [23:23] O;
  assign O[23] = 0;
endmodule

module UBOne_60(O);
  output O;
  assign O = 1;
endmodule

module UB1DCON_60(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_59(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBZero_25_25(O);
  output [25:25] O;
  assign O[25] = 0;
endmodule

module UBOne_62(O);
  output O;
  assign O = 1;
endmodule

module UB1DCON_62(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_61(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBZero_27_27(O);
  output [27:27] O;
  assign O[27] = 0;
endmodule

module UBOne_64(O);
  output O;
  assign O = 1;
endmodule

module UB1DCON_64(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UB1DCON_63(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBZero_29_29(O);
  output [29:29] O;
  assign O[29] = 0;
endmodule

module UB1DCON_65(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBZero_31_31(O);
  output [31:31] O;
  assign O[31] = 0;
endmodule

module UBOne_33(O);
  output O;
  assign O = 1;
endmodule

module UBHA_0(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_1(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBFA_2(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_3(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_4(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_5(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_6(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_7(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_8(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_9(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_10(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_11(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_12(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_13(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_14(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_15(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_16(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_17(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_18(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_19(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_20(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_21(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_22(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_23(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_24(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_25(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_26(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_27(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_28(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_29(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_30(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_31(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_32(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_33(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_34(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBHA_35(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_36(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_6(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_7(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBFA_35(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_36(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_37(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_38(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_39(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_40(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBHA_41(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_42(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_12(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_13(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBFA_41(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_42(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_43(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_44(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_45(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_46(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBHA_47(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_48(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_18(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_19(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBFA_47(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_48(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_49(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_50(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_51(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_52(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBHA_53(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_54(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_24(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_25(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBFA_53(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_54(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_55(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_56(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_57(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_58(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBHA_59(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_60(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_2(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_3(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_38(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_10(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_11(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_44(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_45(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_46(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_49(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_20(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_21(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_56(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_28(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_29(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBFA_59(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_60(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_61(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBHA_62(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_63(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_64(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_4(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_5(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_40(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_43(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_16(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_17(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_51(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_52(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_55(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_57(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_30(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_31(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_34(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_37(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_39(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_50(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_58(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_61(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_65(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_8(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_9(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_26(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_27(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UB1DCON_66(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBHA_14(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_15(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_22(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBHA_23(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBFA_62(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBFA_63(C, S, X, Y, Z);
  output C;
  output S;
  input X;
  input Y;
  input Z;
  assign C = ( X & Y ) | ( Y & Z ) | ( Z & X );
  assign S = X ^ Y ^ Z;
endmodule

module UBHA_66(C, S, X, Y);
  output C;
  output S;
  input X;
  input Y;
  assign C = X & Y;
  assign S = X ^ Y;
endmodule

module UBZero_67_67(O);
  output [67:67] O;
  assign O[67] = 0;
endmodule

module GPGenerator(Go, Po, A, B);
  output Go;
  output Po;
  input A;
  input B;
  assign Go = A & B;
  assign Po = A ^ B;
endmodule

module UBPriCLA_67_6(S, X, Y, Cin);
  output [68:6] S;
  input Cin;
  input [67:6] X;
  input [67:6] Y;
  wire [68:7] Ct;
  wire [67:6] Gt;
  wire [67:6] Pt;
  wire [62:1] C;
  wire [61:0] G;
  wire [61:0] P;
  assign P=Pt;
  assign G=Gt;
  assign Ct=C; 
  assign S[6] = Cin ^ Pt[6];
  assign S[7] = Ct[7] ^ Pt[7];
  assign S[8] = Ct[8] ^ Pt[8];
  assign S[9] = Ct[9] ^ Pt[9];
  assign S[10] = Ct[10] ^ Pt[10];
  assign S[11] = Ct[11] ^ Pt[11];
  assign S[12] = Ct[12] ^ Pt[12];
  assign S[13] = Ct[13] ^ Pt[13];
  assign S[14] = Ct[14] ^ Pt[14];
  assign S[15] = Ct[15] ^ Pt[15];
  assign S[16] = Ct[16] ^ Pt[16];
  assign S[17] = Ct[17] ^ Pt[17];
  assign S[18] = Ct[18] ^ Pt[18];
  assign S[19] = Ct[19] ^ Pt[19];
  assign S[20] = Ct[20] ^ Pt[20];
  assign S[21] = Ct[21] ^ Pt[21];
  assign S[22] = Ct[22] ^ Pt[22];
  assign S[23] = Ct[23] ^ Pt[23];
  assign S[24] = Ct[24] ^ Pt[24];
  assign S[25] = Ct[25] ^ Pt[25];
  assign S[26] = Ct[26] ^ Pt[26];
  assign S[27] = Ct[27] ^ Pt[27];
  assign S[28] = Ct[28] ^ Pt[28];
  assign S[29] = Ct[29] ^ Pt[29];
  assign S[30] = Ct[30] ^ Pt[30];
  assign S[31] = Ct[31] ^ Pt[31];
  assign S[32] = Ct[32] ^ Pt[32];
  assign S[33] = Ct[33] ^ Pt[33];
  assign S[34] = Ct[34] ^ Pt[34];
  assign S[35] = Ct[35] ^ Pt[35];
  assign S[36] = Ct[36] ^ Pt[36];
  assign S[37] = Ct[37] ^ Pt[37];
  assign S[38] = Ct[38] ^ Pt[38];
  assign S[39] = Ct[39] ^ Pt[39];
  assign S[40] = Ct[40] ^ Pt[40];
  assign S[41] = Ct[41] ^ Pt[41];
  assign S[42] = Ct[42] ^ Pt[42];
  assign S[43] = Ct[43] ^ Pt[43];
  assign S[44] = Ct[44] ^ Pt[44];
  assign S[45] = Ct[45] ^ Pt[45];
  assign S[46] = Ct[46] ^ Pt[46];
  assign S[47] = Ct[47] ^ Pt[47];
  assign S[48] = Ct[48] ^ Pt[48];
  assign S[49] = Ct[49] ^ Pt[49];
  assign S[50] = Ct[50] ^ Pt[50];
  assign S[51] = Ct[51] ^ Pt[51];
  assign S[52] = Ct[52] ^ Pt[52];
  assign S[53] = Ct[53] ^ Pt[53];
  assign S[54] = Ct[54] ^ Pt[54];
  assign S[55] = Ct[55] ^ Pt[55];
  assign S[56] = Ct[56] ^ Pt[56];
  assign S[57] = Ct[57] ^ Pt[57];
  assign S[58] = Ct[58] ^ Pt[58];
  assign S[59] = Ct[59] ^ Pt[59];
  assign S[60] = Ct[60] ^ Pt[60];
  assign S[61] = Ct[61] ^ Pt[61];
  assign S[62] = Ct[62] ^ Pt[62];
  assign S[63] = Ct[63] ^ Pt[63];
  assign S[64] = Ct[64] ^ Pt[64];
  assign S[65] = Ct[65] ^ Pt[65];
  assign S[66] = Ct[66] ^ Pt[66];
  assign S[67] = Ct[67] ^ Pt[67];
  assign S[68] = Ct[68];
  GPGenerator U0 (Gt[6], Pt[6], X[6], Y[6]);
  GPGenerator U1 (Gt[7], Pt[7], X[7], Y[7]);
  GPGenerator U2 (Gt[8], Pt[8], X[8], Y[8]);
  GPGenerator U3 (Gt[9], Pt[9], X[9], Y[9]);
  GPGenerator U4 (Gt[10], Pt[10], X[10], Y[10]);
  GPGenerator U5 (Gt[11], Pt[11], X[11], Y[11]);
  GPGenerator U6 (Gt[12], Pt[12], X[12], Y[12]);
  GPGenerator U7 (Gt[13], Pt[13], X[13], Y[13]);
  GPGenerator U8 (Gt[14], Pt[14], X[14], Y[14]);
  GPGenerator U9 (Gt[15], Pt[15], X[15], Y[15]);
  GPGenerator U10 (Gt[16], Pt[16], X[16], Y[16]);
  GPGenerator U11 (Gt[17], Pt[17], X[17], Y[17]);
  GPGenerator U12 (Gt[18], Pt[18], X[18], Y[18]);
  GPGenerator U13 (Gt[19], Pt[19], X[19], Y[19]);
  GPGenerator U14 (Gt[20], Pt[20], X[20], Y[20]);
  GPGenerator U15 (Gt[21], Pt[21], X[21], Y[21]);
  GPGenerator U16 (Gt[22], Pt[22], X[22], Y[22]);
  GPGenerator U17 (Gt[23], Pt[23], X[23], Y[23]);
  GPGenerator U18 (Gt[24], Pt[24], X[24], Y[24]);
  GPGenerator U19 (Gt[25], Pt[25], X[25], Y[25]);
  GPGenerator U20 (Gt[26], Pt[26], X[26], Y[26]);
  GPGenerator U21 (Gt[27], Pt[27], X[27], Y[27]);
  GPGenerator U22 (Gt[28], Pt[28], X[28], Y[28]);
  GPGenerator U23 (Gt[29], Pt[29], X[29], Y[29]);
  GPGenerator U24 (Gt[30], Pt[30], X[30], Y[30]);
  GPGenerator U25 (Gt[31], Pt[31], X[31], Y[31]);
  GPGenerator U26 (Gt[32], Pt[32], X[32], Y[32]);
  GPGenerator U27 (Gt[33], Pt[33], X[33], Y[33]);
  GPGenerator U28 (Gt[34], Pt[34], X[34], Y[34]);
  GPGenerator U29 (Gt[35], Pt[35], X[35], Y[35]);
  GPGenerator U30 (Gt[36], Pt[36], X[36], Y[36]);
  GPGenerator U31 (Gt[37], Pt[37], X[37], Y[37]);
  GPGenerator U32 (Gt[38], Pt[38], X[38], Y[38]);
  GPGenerator U33 (Gt[39], Pt[39], X[39], Y[39]);
  GPGenerator U34 (Gt[40], Pt[40], X[40], Y[40]);
  GPGenerator U35 (Gt[41], Pt[41], X[41], Y[41]);
  GPGenerator U36 (Gt[42], Pt[42], X[42], Y[42]);
  GPGenerator U37 (Gt[43], Pt[43], X[43], Y[43]);
  GPGenerator U38 (Gt[44], Pt[44], X[44], Y[44]);
  GPGenerator U39 (Gt[45], Pt[45], X[45], Y[45]);
  GPGenerator U40 (Gt[46], Pt[46], X[46], Y[46]);
  GPGenerator U41 (Gt[47], Pt[47], X[47], Y[47]);
  GPGenerator U42 (Gt[48], Pt[48], X[48], Y[48]);
  GPGenerator U43 (Gt[49], Pt[49], X[49], Y[49]);
  GPGenerator U44 (Gt[50], Pt[50], X[50], Y[50]);
  GPGenerator U45 (Gt[51], Pt[51], X[51], Y[51]);
  GPGenerator U46 (Gt[52], Pt[52], X[52], Y[52]);
  GPGenerator U47 (Gt[53], Pt[53], X[53], Y[53]);
  GPGenerator U48 (Gt[54], Pt[54], X[54], Y[54]);
  GPGenerator U49 (Gt[55], Pt[55], X[55], Y[55]);
  GPGenerator U50 (Gt[56], Pt[56], X[56], Y[56]);
  GPGenerator U51 (Gt[57], Pt[57], X[57], Y[57]);
  GPGenerator U52 (Gt[58], Pt[58], X[58], Y[58]);
  GPGenerator U53 (Gt[59], Pt[59], X[59], Y[59]);
  GPGenerator U54 (Gt[60], Pt[60], X[60], Y[60]);
  GPGenerator U55 (Gt[61], Pt[61], X[61], Y[61]);
  GPGenerator U56 (Gt[62], Pt[62], X[62], Y[62]);
  GPGenerator U57 (Gt[63], Pt[63], X[63], Y[63]);
  GPGenerator U58 (Gt[64], Pt[64], X[64], Y[64]);
  GPGenerator U59 (Gt[65], Pt[65], X[65], Y[65]);
  GPGenerator U60 (Gt[66], Pt[66], X[66], Y[66]);
  GPGenerator U61 (Gt[67], Pt[67], X[67], Y[67]);

  assign C[1] = G[0] | ( P[0] & Cin );
  assign C[2] = G[1] | ( P[1] & G[0] ) | ( P[1] & P[0] & Cin );
  assign C[3] = G[2] | ( P[2] & G[1] ) | ( P[2] & P[1] & G[0] ) | ( P[2] & P[1] & P[0] & Cin );
  assign C[4] = G[3] | ( P[3] & G[2] ) | ( P[3] & P[2] & G[1] ) | ( P[3] & P[2] & P[1] & G[0] ) | ( P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[5] = G[4] | ( P[4] & G[3] ) | ( P[4] & P[3] & G[2] ) | ( P[4] & P[3] & P[2] & G[1] ) | ( P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[6] = G[5] | ( P[5] & G[4] ) | ( P[5] & P[4] & G[3] ) | ( P[5] & P[4] & P[3] & G[2] ) | ( P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[5] & P[4] & P[3]
 & P[2] & P[1] & P[0] & Cin );
  assign C[7] = G[6] | ( P[6] & G[5] ) | ( P[6] & P[5] & G[4] ) | ( P[6] & P[5] & P[4] & G[3] ) | ( P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[6] & P[5] & P[4]
 & P[3] & P[2] & P[1] & G[0] ) | ( P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[8] = G[7] | ( P[7] & G[6] ) | ( P[7] & P[6] & G[5] ) | ( P[7] & P[6] & P[5] & G[4] ) | ( P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[7] & P[6] & P[5]
 & P[4] & P[3] & P[2] & G[1] ) | ( P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[9] = G[8] | ( P[8] & G[7] ) | ( P[8] & P[7] & G[6] ) | ( P[8] & P[7] & P[6] & G[5] ) | ( P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[8] & P[7] & P[6]
 & P[5] & P[4] & P[3] & G[2] ) | ( P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[8] & P[7] & P[6] & P[5] & P[4] & P[3]
 & P[2] & P[1] & P[0] & Cin );
  assign C[10] = G[9] | ( P[9] & G[8] ) | ( P[9] & P[8] & G[7] ) | ( P[9] & P[8] & P[7] & G[6] ) | ( P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[9] & P[8] & P[7]
 & P[6] & P[5] & P[4] & G[3] ) | ( P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[9] & P[8] & P[7] & P[6] & P[5] & P[4]
 & P[3] & P[2] & P[1] & G[0] ) | ( P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[11] = G[10] | ( P[10] & G[9] ) | ( P[10] & P[9] & G[8] ) | ( P[10] & P[9] & P[8] & G[7] ) | ( P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[10] & P[9]
 & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[10] & P[9] & P[8] & P[7] & P[6]
 & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] &
 Cin );
  assign C[12] = G[11] | ( P[11] & G[10] ) | ( P[11] & P[10] & G[9] ) | ( P[11] & P[10] & P[9] & G[8] ) | ( P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[11]
 & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[11] & P[10] & P[9]
 & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3]
 & P[2] & P[1] & G[0] ) | ( P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[13] = G[12] | ( P[12] & G[11] ) | ( P[12] & P[11] & G[10] ) | ( P[12] & P[11] & P[10] & G[9] ) | ( P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[12]
 & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[12] & P[11] & P[10]
 & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4]
 & P[3] & P[2] & G[1] ) | ( P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2]
 & P[1] & P[0] & Cin );
  assign C[14] = G[13] | ( P[13] & G[12] ) | ( P[13] & P[12] & G[11] ) | ( P[13] & P[12] & P[11] & G[10] ) | ( P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) |
 ( P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[13] &
 P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[13] & P[12] & P[11] & P[10] & P[9] & P[8] &
 P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6]
 & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[15] = G[14] | ( P[14] & G[13] ) | ( P[14] & P[13] & G[12] ) | ( P[14] & P[13] & P[12] & G[11] ) | ( P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[14] & P[13] & P[12] & P[11] & P[10] & G[9] )
 | ( P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[14]
 & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[14] & P[13] & P[12] & P[11] & P[10] & P[9]
 & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] &
 P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[14] & P[13] & P[12] & P[11] & P[10]
 & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[16] = G[15] | ( P[15] & G[14] ) | ( P[15] & P[14] & G[13] ) | ( P[15] & P[14] & P[13] & G[12] ) | ( P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[15] & P[14] & P[13] & P[12] & P[11] & G[10] )
 | ( P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | (
 P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[15] & P[14] & P[13] & P[12] & P[11]
 & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[15] & P[14] & P[13] & P[12] & P[11] & P[10]
 & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[15] & P[14] & P[13] &
 P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] &
 P[1] & P[0] & Cin );
  assign C[17] = G[16] | ( P[16] & G[15] ) | ( P[16] & P[15] & G[14] ) | ( P[16] & P[15] & P[14] & G[13] ) | ( P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[16] & P[15] & P[14] & P[13] & P[12] & G[11] )
 | ( P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] )
 | ( P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[16] & P[15] & P[14] & P[13]
 & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[16] & P[15] & P[14] & P[13] & P[12]
 & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[16] & P[15]
 & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4]
 & P[3] & P[2] & P[1] & G[0] ) | ( P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[18] = G[17] | ( P[17] & G[16] ) | ( P[17] & P[16] & G[15] ) | ( P[17] & P[16] & P[15] & G[14] ) | ( P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[17] & P[16] & P[15] & P[14] & P[13] & G[12] )
 | ( P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9]
 ) | ( P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[17] & P[16] & P[15] &
 P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[17] & P[16] & P[15] & P[14]
 & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[17]
 & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7]
 & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[17] & P[16] &
 P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[19] = G[18] | ( P[18] & G[17] ) | ( P[18] & P[17] & G[16] ) | ( P[18] & P[17] & P[16] & G[15] ) | ( P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[18] & P[17] & P[16] & P[15] & P[14] & G[13] )
 | ( P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10]
 ) | ( P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[18] & P[17] & P[16]
 & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[18] & P[17] & P[16]
 & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4]
 ) | ( P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10]
 & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | (
 P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] &
 P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[20] = G[19] | ( P[19] & G[18] ) | ( P[19] & P[18] & G[17] ) | ( P[19] & P[18] & P[17] & G[16] ) | ( P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[19] & P[18] & P[17] & P[16] & P[15] & G[14] )
 | ( P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11]
 ) | ( P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[19] & P[18] & P[17]
 & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[19] & P[18] & P[17]
 & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5]
 ) | ( P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11]
 & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) |
 ( P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13]
 & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] &
 P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[21] = G[20] | ( P[20] & G[19] ) | ( P[20] & P[19] & G[18] ) | ( P[20] & P[19] & P[18] & G[17] ) | ( P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[20] & P[19] & P[18] & P[17] & P[16] & G[15] )
 | ( P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12]
 ) | ( P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[20] & P[19] & P[18]
 & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[20] & P[19] & P[18]
 & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6]
 ) | ( P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12]
 & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] )
 | ( P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14]
 & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8]
 & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2]
 & P[1] & P[0] & Cin );
  assign C[22] = G[21] | ( P[21] & G[20] ) | ( P[21] & P[20] & G[19] ) | ( P[21] & P[20] & P[19] & G[18] ) | ( P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[21] & P[20] & P[19] & P[18] & P[17] & G[16] )
 | ( P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13]
 ) | ( P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[21] & P[20] & P[19]
 & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[21] & P[20] &
 P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] &
 P[8] & G[7] ) | ( P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] &
 P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6]
 & P[5] & G[4] ) | ( P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[21] & P[20] & P[19] & P[18] &
 P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12]
 & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] &
 P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2]
 & P[1] & P[0] & Cin );
  assign C[23] = G[22] | ( P[22] & G[21] ) | ( P[22] & P[21] & G[20] ) | ( P[22] & P[21] & P[20] & G[19] ) | ( P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[22] & P[21] & P[20] & P[19] & P[18] & G[17] )
 | ( P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14]
 ) | ( P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[22] & P[21] & P[20]
 & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[22] & P[21] &
 P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10]
 & P[9] & G[8] ) | ( P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16]
 & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7]
 & P[6] & G[5] ) | ( P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[22] & P[21] & P[20] & P[19] &
 P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13]
 & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8]
 & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] &
 P[3] & P[2] & P[1] & G[0] ) | ( P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0]
 & Cin );
  assign C[24] = G[23] | ( P[23] & G[22] ) | ( P[23] & P[22] & G[21] ) | ( P[23] & P[22] & P[21] & G[20] ) | ( P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[23] & P[22] & P[21] & P[20] & P[19] & G[18] )
 | ( P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15]
 ) | ( P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[23] & P[22] & P[21]
 & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[23] & P[22] &
 P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11]
 & P[10] & G[9] ) | ( P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17]
 & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] &
 P[8] & P[7] & G[6] ) | ( P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[23] & P[22] & P[21] & P[20]
 & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14]
 & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9]
 & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] &
 P[4] & P[3] & P[2] & G[1] ) | ( P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1]
 & G[0] ) | ( P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[25] = G[24] | ( P[24] & G[23] ) | ( P[24] & P[23] & G[22] ) | ( P[24] & P[23] & P[22] & G[21] ) | ( P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[24] & P[23] & P[22] & P[21] & P[20] & G[19] )
 | ( P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16]
 ) | ( P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[24] & P[23] & P[22]
 & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[24] & P[23] &
 P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12]
 & P[11] & G[10] ) | ( P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[24] & P[23] & P[22] & P[21] & P[20] & P[19] &
 P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] &
 P[10] & P[9] & P[8] & G[7] ) | ( P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[24] & P[23] &
 P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17]
 & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12]
 & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8]
 & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5]
 & P[4] & P[3] & P[2] & G[1] ) | ( P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3]
 & P[2] & P[1] & G[0] ) | ( P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2]
 & P[1] & P[0] & Cin );
  assign C[26] = G[25] | ( P[25] & G[24] ) | ( P[25] & P[24] & G[23] ) | ( P[25] & P[24] & P[23] & G[22] ) | ( P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[25] & P[24] & P[23] & P[22] & P[21] & G[20] )
 | ( P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17]
 ) | ( P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[25] & P[24] & P[23]
 & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[25] & P[24] &
 P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13]
 & P[12] & G[11] ) | ( P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[25] & P[24] & P[23] & P[22] & P[21] & P[20] &
 P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12]
 & P[11] & P[10] & P[9] & G[8] ) | ( P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] &
 P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14]
 & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10]
 & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7]
 & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5]
 & P[4] & P[3] & P[2] & G[1] ) | ( P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4]
 & P[3] & P[2] & P[1] & G[0] ) | ( P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4]
 & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[27] = G[26] | ( P[26] & G[25] ) | ( P[26] & P[25] & G[24] ) | ( P[26] & P[25] & P[24] & G[23] ) | ( P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[26] & P[25] & P[24] & P[23] & P[22] & G[21] )
 | ( P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18]
 ) | ( P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[26] & P[25] &
 P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14]
 & P[13] & G[12] ) | ( P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[26] & P[25] & P[24] & P[23] & P[22] & P[21] &
 P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13]
 & P[12] & P[11] & P[10] & G[9] ) | ( P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20]
 & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] &
 P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12]
 & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9]
 & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7]
 & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6]
 & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6]
 & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7]
 & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[28] = G[27] | ( P[27] & G[26] ) | ( P[27] & P[26] & G[25] ) | ( P[27] & P[26] & P[25] & G[24] ) | ( P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[27] & P[26] & P[25] & P[24] & P[23] & G[22] )
 | ( P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19]
 ) | ( P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[27] & P[26] &
 P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15]
 & P[14] & G[13] ) | ( P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[27] & P[26] & P[25] & P[24] & P[23] & P[22] &
 P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14]
 & P[13] & P[12] & P[11] & G[10] ) | ( P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[27] &
 P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[27] & P[26] & P[25] & P[24] & P[23] & P[22] &
 P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] &
 P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14]
 & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] &
 P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10]
 & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9]
 & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9]
 & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10]
 & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12]
 & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[29] = G[28] | ( P[28] & G[27] ) | ( P[28] & P[27] & G[26] ) | ( P[28] & P[27] & P[26] & G[25] ) | ( P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[28] & P[27] & P[26] & P[25] & P[24] & G[23] )
 | ( P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20]
 ) | ( P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[28] & P[27] & P[26]
 & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[28] & P[27] &
 P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16]
 & P[15] & G[14] ) | ( P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[28] & P[27] & P[26] & P[25] & P[24] & P[23] &
 P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15]
 & P[14] & P[13] & P[12] & G[11] ) | ( P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[28] &
 P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[28] & P[27] & P[26] & P[25] & P[24] & P[23]
 & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19]
 & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16]
 & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] &
 P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12]
 & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11]
 & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11]
 & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12]
 & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14]
 & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17]
 & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[30] = G[29] | ( P[29] & G[28] ) | ( P[29] & P[28] & G[27] ) | ( P[29] & P[28] & P[27] & G[26] ) | ( P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & G[24] )
 | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21]
 ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[29] & P[28] & P[27]
 & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[29] & P[28] &
 P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17]
 & P[16] & G[15] ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24] &
 P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16]
 & P[15] & P[14] & P[13] & G[12] ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[29] &
 P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20]
 & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17]
 & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15]
 & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] &
 P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13]
 & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13]
 & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14]
 & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16]
 & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19]
 & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23]
 & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[31] = G[30] | ( P[30] & G[29] ) | ( P[30] & P[29] & G[28] ) | ( P[30] & P[29] & P[28] & G[27] ) | ( P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & G[25] )
 | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22]
 ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[30] & P[29] & P[28]
 & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[30] & P[29] &
 P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18]
 & P[17] & G[16] ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25] &
 P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17]
 & P[16] & P[15] & P[14] & G[13] ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[30] &
 P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21]
 & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18]
 & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16]
 & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15]
 & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] &
 P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15]
 & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16]
 & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18]
 & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21]
 & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4]
 & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[32] = G[31] | ( P[31] & G[30] ) | ( P[31] & P[30] & G[29] ) | ( P[31] & P[30] & P[29] & G[28] ) | ( P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & G[26] )
 | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23]
 ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[31] & P[30] &
 P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19]
 & P[18] & G[17] ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] &
 P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18]
 & P[17] & P[16] & P[15] & G[14] ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[31] &
 P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26]
 & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22]
 & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19]
 & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17]
 & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16]
 & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16]
 & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] &
 P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18]
 & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20]
 & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23]
 & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[31] & P[30] & P[29] & P[28] & P[27]
 & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] )
 | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] &
 P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13]
 & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[33] = G[32] | ( P[32] & G[31] ) | ( P[32] & P[31] & G[30] ) | ( P[32] & P[31] & P[30] & G[29] ) | ( P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & G[27] )
 | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24]
 ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[32] & P[31] &
 P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20]
 & P[19] & G[18] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] &
 P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19]
 & P[18] & P[17] & P[16] & G[15] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[32] &
 P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27]
 & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23]
 & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20]
 & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18]
 & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17]
 & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17]
 & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18]
 & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] &
 P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22]
 & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3]
 & G[2] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9]
 & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15]
 & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22]
 & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[34] = G[33] | ( P[33] & G[32] ) | ( P[33] & P[32] & G[31] ) | ( P[33] & P[32] & P[31] & G[30] ) | ( P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & G[28] )
 | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25]
 ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[33] & P[32] &
 P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21]
 & P[20] & G[19] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] &
 P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20]
 & P[19] & P[18] & P[17] & G[16] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[33] &
 P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28]
 & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21]
 & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19]
 & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18]
 & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18]
 & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19]
 & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21]
 & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] &
 P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27]
 & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5]
 & P[4] & G[3] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11]
 & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17]
 & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6]
 & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[35] = G[34] | ( P[34] & G[33] ) | ( P[34] & P[33] & G[32] ) | ( P[34] & P[33] & P[32] & G[31] ) | ( P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & G[29] )
 | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26]
 ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[34] & P[33] &
 P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22]
 & P[21] & G[20] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] &
 P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21]
 & P[20] & P[19] & P[18] & G[17] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[34] &
 P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22]
 & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20]
 & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19]
 & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19]
 & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20]
 & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22]
 & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] &
 P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7]
 & P[6] & P[5] & G[4] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13]
 & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19]
 & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26]
 & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] &
 P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18]
 & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[36] = G[35] | ( P[35] & G[34] ) | ( P[35] & P[34] & G[33] ) | ( P[35] & P[34] & P[33] & G[32] ) | ( P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & G[30] )
 | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27]
 ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[35] & P[34] &
 P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23]
 & P[22] & G[21] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] &
 P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22]
 & P[21] & P[20] & P[19] & G[18] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[35] &
 P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26]
 & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23]
 & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21]
 & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20]
 & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20]
 & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21]
 & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23]
 & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26]
 & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[35] &
 P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9]
 & P[8] & P[7] & P[6] & G[5] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] &
 P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21]
 & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28]
 & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] )
 | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11]
 & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20]
 & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4]
 & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[37] = G[36] | ( P[36] & G[35] ) | ( P[36] & P[35] & G[34] ) | ( P[36] & P[35] & P[34] & G[33] ) | ( P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & G[31] )
 | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28]
 ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[36] & P[35] &
 P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & G[22] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] &
 P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23]
 & P[22] & P[21] & P[20] & G[19] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[36] &
 P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27]
 & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22]
 & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21]
 & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21]
 & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22]
 & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27]
 & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] &
 P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16]
 & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] &
 P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4]
 & G[3] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13]
 & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22]
 & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6]
 & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17]
 & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[38] = G[37] | ( P[37] & G[36] ) | ( P[37] & P[36] & G[35] ) | ( P[37] & P[36] & P[35] & G[34] ) | ( P[37] & P[36] & P[35] & P[34] & G[33] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & G[32] )
 | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29]
 ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[37] & P[36] &
 P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & G[23] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] &
 P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & G[20] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[37] &
 P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28]
 & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23]
 & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22]
 & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22]
 & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23]
 & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28]
 & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] &
 P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] &
 P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] &
 P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6]
 & P[5] & G[4] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15]
 & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] &
 P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20]
 & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6]
 & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[39] = G[38] | ( P[38] & G[37] ) | ( P[38] & P[37] & G[36] ) | ( P[38] & P[37] & P[36] & G[35] ) | ( P[38] & P[37] & P[36] & P[35] & G[34] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & G[33] )
 | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30]
 ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[38] & P[37] &
 P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26]
 & P[25] & G[24] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] &
 P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & G[21] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[38] &
 P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26]
 & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23]
 & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23]
 & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26]
 & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] &
 P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] &
 P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] &
 P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7]
 & P[6] & G[5] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16]
 & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] &
 P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21]
 & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7]
 & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20]
 & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[40] = G[39] | ( P[39] & G[38] ) | ( P[39] & P[38] & G[37] ) | ( P[39] & P[38] & P[37] & G[36] ) | ( P[39] & P[38] & P[37] & P[36] & G[35] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & G[34] )
 | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & G[33] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31]
 ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[39] & P[38] &
 P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27]
 & P[26] & G[25] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] &
 P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26]
 & P[25] & P[24] & P[23] & G[22] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[39] &
 P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27]
 & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27]
 & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] &
 P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20]
 & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27]
 & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] &
 P[9] & P[8] & P[7] & G[6] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18]
 & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] &
 P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] &
 P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] &
 P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] &
 P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[41] = G[40] | ( P[40] & G[39] ) | ( P[40] & P[39] & G[38] ) | ( P[40] & P[39] & P[38] & G[37] ) | ( P[40] & P[39] & P[38] & P[37] & G[36] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & G[35] )
 | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & G[34] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & G[33] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32]
 ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[40] & P[39] &
 P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28]
 & P[27] & G[26] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] &
 P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27]
 & P[26] & P[25] & P[24] & G[23] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[40] &
 P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28]
 & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26]
 & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26]
 & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28]
 & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] &
 P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21]
 & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28]
 & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] &
 P[10] & P[9] & P[8] & G[7] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] &
 P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[40] &
 P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14]
 & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] &
 P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] &
 P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] &
 P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] &
 P[2] & P[1] & P[0] & Cin );
  assign C[42] = G[41] | ( P[41] & G[40] ) | ( P[41] & P[40] & G[39] ) | ( P[41] & P[40] & P[39] & G[38] ) | ( P[41] & P[40] & P[39] & P[38] & G[37] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & G[36] )
 | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & G[35] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & G[34] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & G[33]
 ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[41] & P[40] &
 P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & G[27] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] &
 P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28]
 & P[27] & P[26] & P[25] & G[24] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[41] &
 P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27]
 & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26]
 & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26]
 & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27]
 & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] &
 P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22]
 & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] &
 P[11] & P[10] & P[9] & G[8] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] &
 P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] &
 P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] &
 P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27]
 & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] &
 P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27]
 & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] &
 P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5]
 & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] &
 P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[43] = G[42] | ( P[42] & G[41] ) | ( P[42] & P[41] & G[40] ) | ( P[42] & P[41] & P[40] & G[39] ) | ( P[42] & P[41] & P[40] & P[39] & G[38] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & G[37] )
 | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & G[36] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & G[35] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & G[34]
 ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & G[33] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[42] & P[41] &
 P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & G[28] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] &
 P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & G[25] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[42] &
 P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28]
 & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27]
 & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27]
 & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28]
 & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] &
 P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23]
 & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] &
 P[12] & P[11] & P[10] & G[9] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22]
 & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) |
 ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] &
 P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[42] &
 P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16]
 & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3]
 ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18]
 & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7]
 & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23]
 & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] &
 P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[44] = G[43] | ( P[43] & G[42] ) | ( P[43] & P[42] & G[41] ) | ( P[43] & P[42] & P[41] & G[40] ) | ( P[43] & P[42] & P[41] & P[40] & G[39] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & G[38] )
 | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & G[37] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & G[36] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & G[35]
 ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & G[34] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & G[33] ) | ( P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[43] & P[42] &
 P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & G[29] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] &
 P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & G[26] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[43] &
 P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28]
 & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28]
 & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] &
 P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] &
 P[13] & P[12] & P[11] & G[10] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23]
 & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) |
 ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] &
 P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] &
 P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] &
 P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5]
 & G[4] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20]
 & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] &
 P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26]
 & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] &
 P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] &
 P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10]
 & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[45] = G[44] | ( P[44] & G[43] ) | ( P[44] & P[43] & G[42] ) | ( P[44] & P[43] & P[42] & G[41] ) | ( P[44] & P[43] & P[42] & P[41] & G[40] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & G[39] )
 | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & G[38] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & G[37] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & G[36]
 ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & G[35] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & G[34] ) | ( P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & G[33] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[44] & P[43] &
 P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & G[30] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] &
 P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & G[27] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[44] &
 P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] &
 P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] &
 P[14] & P[13] & P[12] & G[11] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] )
 | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20]
 & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) |
 ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] &
 P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7]
 & P[6] & G[5] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22]
 & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] &
 P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28]
 & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] )
 | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20]
 & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] &
 P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] &
 P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6]
 & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[46] = G[45] | ( P[45] & G[44] ) | ( P[45] & P[44] & G[43] ) | ( P[45] & P[44] & P[43] & G[42] ) | ( P[45] & P[44] & P[43] & P[42] & G[41] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & G[40] )
 | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & G[39] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & G[38] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & G[37]
 ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & G[36] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & G[35] ) | ( P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & G[34] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & G[33] ) | ( P[45] & P[44] &
 P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & G[31] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] &
 P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & G[28] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[45] &
 P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] &
 P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26]
 & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] &
 P[15] & P[14] & P[13] & G[12] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] )
 | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21]
 & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) |
 ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] &
 P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] &
 P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9]
 & P[8] & P[7] & G[6] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] &
 P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14]
 & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4]
 & G[3] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22]
 & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] &
 P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] &
 P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3]
 & P[2] & P[1] & P[0] & Cin );
  assign C[47] = G[46] | ( P[46] & G[45] ) | ( P[46] & P[45] & G[44] ) | ( P[46] & P[45] & P[44] & G[43] ) | ( P[46] & P[45] & P[44] & P[43] & G[42] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & G[41] )
 | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & G[40] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & G[39] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & G[38]
 ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & G[37] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & G[36] ) | ( P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & G[35] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & G[34] ) | ( P[46] & P[45] &
 P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & G[33] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & G[32] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] &
 P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & G[29] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[46] &
 P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] &
 P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27]
 & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] &
 P[16] & P[15] & P[14] & G[13] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26]
 & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] )
 | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22]
 & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] )
 | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22]
 & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] &
 P[10] & P[9] & P[8] & G[7] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] &
 P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] &
 P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6]
 & P[5] & G[4] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] &
 P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] &
 P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5]
 & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] &
 P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0]
 & Cin );
  assign C[48] = G[47] | ( P[47] & G[46] ) | ( P[47] & P[46] & G[45] ) | ( P[47] & P[46] & P[45] & G[44] ) | ( P[47] & P[46] & P[45] & P[44] & G[43] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & G[42] )
 | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & G[41] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & G[40] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & G[39]
 ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & G[38] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & G[37] ) | ( P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & G[36] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & G[35] ) | ( P[47] & P[46] &
 P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & G[34] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & G[33] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] &
 P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & G[30] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[47] &
 P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] &
 P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28]
 & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] &
 P[17] & P[16] & P[15] & G[14] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27]
 & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] )
 | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23]
 & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] )
 | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23]
 & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] &
 P[11] & P[10] & P[9] & G[8] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] &
 P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] &
 P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17]
 & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] &
 P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8]
 & P[7] & P[6] & G[5] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26]
 & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] &
 P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] &
 P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7]
 & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28]
 & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] &
 P[1] & G[0] ) | ( P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] &
 P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[49] = G[48] | ( P[48] & G[47] ) | ( P[48] & P[47] & G[46] ) | ( P[48] & P[47] & P[46] & G[45] ) | ( P[48] & P[47] & P[46] & P[45] & G[44] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & G[43] )
 | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & G[42] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & G[41] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & G[40]
 ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & G[39] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & G[38] ) | ( P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & G[37] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & G[36] ) | ( P[48] & P[47] &
 P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & G[35] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & G[34] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & G[33] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] &
 P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & G[31] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[48] &
 P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[48]
 & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] &
 P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] &
 P[18] & P[17] & P[16] & G[15] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28]
 & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] )
 | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] )
 | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] &
 P[12] & P[11] & P[10] & G[9] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28]
 & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] &
 P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] &
 P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10]
 & P[9] & P[8] & P[7] & G[6] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] &
 P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] &
 P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] &
 P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] &
 P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5]
 & P[4] & P[3] & P[2] & G[1] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] &
 P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1]
 & G[0] ) | ( P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[50] = G[49] | ( P[49] & G[48] ) | ( P[49] & P[48] & G[47] ) | ( P[49] & P[48] & P[47] & G[46] ) | ( P[49] & P[48] & P[47] & P[46] & G[45] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & G[44] )
 | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & G[43] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & G[42] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & G[41]
 ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & G[40] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & G[39] ) | ( P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & G[38] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & G[37] ) | ( P[49] & P[48] &
 P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & G[36] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & G[35] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & G[34] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] &
 P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & G[33] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & G[32] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[49] &
 P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[49]
 & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] &
 P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] &
 P[19] & P[18] & P[17] & G[16] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] )
 | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] )
 | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] &
 P[13] & P[12] & P[11] & G[10] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] &
 P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] &
 P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11]
 & P[10] & P[9] & P[8] & G[7] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[49] & P[48] &
 P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22]
 & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] &
 P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16]
 & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] &
 P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6]
 & P[5] & P[4] & P[3] & G[2] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] &
 P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] &
 P[2] & G[1] ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] &
 P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0]
 ) | ( P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[51] = G[50] | ( P[50] & G[49] ) | ( P[50] & P[49] & G[48] ) | ( P[50] & P[49] & P[48] & G[47] ) | ( P[50] & P[49] & P[48] & P[47] & G[46] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & G[45] )
 | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & G[44] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & G[43] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & G[42]
 ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & G[41] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & G[40] ) | ( P[50] & P[49] & P[48]
 & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & G[39] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & G[38] ) | ( P[50] & P[49] &
 P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & G[37] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & G[36] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & G[35] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] &
 P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & G[34] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & G[33] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[50] &
 P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[50]
 & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] &
 P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] &
 P[20] & P[19] & P[18] & G[17] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] )
 | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26]
 & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] )
 | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26]
 & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] &
 P[14] & P[13] & P[12] & G[11] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] &
 P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] &
 P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] &
 P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[50] &
 P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24]
 & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] &
 P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18]
 & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] &
 P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13]
 & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] &
 P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5]
 & P[4] & P[3] & G[2] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3]
 & P[2] & G[1] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28]
 & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] &
 P[1] & G[0] ) | ( P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] &
 P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1]
 & P[0] & Cin );
  assign C[52] = G[51] | ( P[51] & G[50] ) | ( P[51] & P[50] & G[49] ) | ( P[51] & P[50] & P[49] & G[48] ) | ( P[51] & P[50] & P[49] & P[48] & G[47] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & G[46] )
 | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & G[45] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & G[44] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & G[43]
 ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & G[42] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & G[41] ) | ( P[51] & P[50] & P[49]
 & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & G[40] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & G[39] ) | ( P[51] & P[50] &
 P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & G[38] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & G[37] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & G[36] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] &
 P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & G[35] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & G[34] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & G[33] ) | ( P[51] &
 P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[51]
 & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] &
 P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] &
 P[21] & P[20] & P[19] & G[18] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] )
 | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27]
 & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] )
 | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27]
 & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] &
 P[15] & P[14] & P[13] & G[12] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] &
 P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] &
 P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[51]
 & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] &
 P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] &
 P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20]
 & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] &
 P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15]
 & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] &
 P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11]
 & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7]
 & P[6] & P[5] & P[4] & G[3] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] &
 P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5]
 & P[4] & P[3] & G[2] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4]
 & P[3] & P[2] & G[1] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4]
 & P[3] & P[2] & P[1] & G[0] ) | ( P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] &
 P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5]
 & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[53] = G[52] | ( P[52] & G[51] ) | ( P[52] & P[51] & G[50] ) | ( P[52] & P[51] & P[50] & G[49] ) | ( P[52] & P[51] & P[50] & P[49] & G[48] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & G[47] )
 | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & G[46] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & G[45] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & G[44]
 ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & G[43] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & G[42] ) | ( P[52] & P[51] & P[50]
 & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & G[41] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & G[40] ) | ( P[52] & P[51] &
 P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & G[39] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & G[38] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & G[37] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] &
 P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & G[36] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & G[35] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & G[34] ) | ( P[52] &
 P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & G[33] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[52]
 & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] &
 P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[52] & P[51] & P[50] & P[49] & P[48]
 & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] &
 P[22] & P[21] & P[20] & G[19] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] )
 | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28]
 & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] )
 | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28]
 & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] &
 P[16] & P[15] & P[14] & G[13] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[52] & P[51] & P[50] & P[49] & P[48]
 & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] &
 P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] &
 P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[52]
 & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] &
 P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] &
 P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21]
 & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] &
 P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] &
 P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] &
 P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7]
 & P[6] & P[5] & P[4] & G[3] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] &
 P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6]
 & P[5] & P[4] & P[3] & G[2] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] &
 P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6]
 & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] &
 P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7]
 & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] &
 P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[54] = G[53] | ( P[53] & G[52] ) | ( P[53] & P[52] & G[51] ) | ( P[53] & P[52] & P[51] & G[50] ) | ( P[53] & P[52] & P[51] & P[50] & G[49] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & G[48] )
 | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & G[47] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & G[46] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & G[45]
 ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & G[44] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & G[43] ) | ( P[53] & P[52] & P[51]
 & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & G[42] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & G[41] ) | ( P[53] & P[52] &
 P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & G[40] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & G[39] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & G[38] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] &
 P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & G[37] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & G[36] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & G[35] ) | ( P[53] &
 P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & G[34] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48]
 & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & G[33] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48]
 & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[53]
 & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] &
 P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[53] & P[52] & P[51] & P[50] & P[49]
 & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] &
 P[23] & P[22] & P[21] & G[20] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] )
 | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] )
 | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] &
 P[17] & P[16] & P[15] & G[14] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[53] & P[52] & P[51] & P[50] & P[49]
 & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] &
 P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] &
 P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[53]
 & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] &
 P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48]
 & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] &
 P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] &
 P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18]
 & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] &
 P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] &
 P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] &
 P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] &
 P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] &
 P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] &
 P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] &
 P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] &
 P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14]
 & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[55] = G[54] | ( P[54] & G[53] ) | ( P[54] & P[53] & G[52] ) | ( P[54] & P[53] & P[52] & G[51] ) | ( P[54] & P[53] & P[52] & P[51] & G[50] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & G[49] )
 | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & G[48] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & G[47] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & G[46]
 ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & G[45] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & G[44] ) | ( P[54] & P[53] & P[52]
 & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & G[43] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & G[42] ) | ( P[54] & P[53] &
 P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & G[41] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & G[40] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & G[39] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] &
 P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & G[38] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & G[37] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & G[36] ) | ( P[54] &
 P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & G[35] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49]
 & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & G[34] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & G[33] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49]
 & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[54]
 & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] &
 P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[54] & P[53] & P[52] & P[51] & P[50]
 & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] &
 P[24] & P[23] & P[22] & G[21] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] )
 | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] )
 | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] &
 P[18] & P[17] & P[16] & G[15] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[54] & P[53] & P[52] & P[51] & P[50]
 & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] &
 P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] &
 P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[54]
 & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] &
 P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49]
 & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] &
 P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] &
 P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] &
 P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16]
 & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] &
 P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] &
 P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] &
 P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] &
 P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] &
 P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] &
 P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] &
 P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] &
 P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20]
 & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[56] = G[55] | ( P[55] & G[54] ) | ( P[55] & P[54] & G[53] ) | ( P[55] & P[54] & P[53] & G[52] ) | ( P[55] & P[54] & P[53] & P[52] & G[51] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & G[50] )
 | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & G[49] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & G[48] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & G[47]
 ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & G[46] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & G[45] ) | ( P[55] & P[54] & P[53]
 & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & G[44] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & G[43] ) | ( P[55] & P[54] &
 P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & G[42] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & G[41] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & G[40] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] &
 P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & G[39] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & G[38] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & G[37] ) | ( P[55] &
 P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & G[36] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50]
 & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & G[35] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & G[34] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & G[33] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50]
 & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[55]
 & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] &
 P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[55] & P[54] & P[53] & P[52] & P[51]
 & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] &
 P[25] & P[24] & P[23] & G[22] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] )
 | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] )
 | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31]
 & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] &
 P[19] & P[18] & P[17] & G[16] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[55] & P[54] & P[53] & P[52] & P[51]
 & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] &
 P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] &
 P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[55]
 & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] &
 P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50]
 & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] &
 P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] &
 P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] &
 P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] &
 P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15]
 & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] &
 P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] &
 P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] &
 P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] &
 P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] &
 P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] &
 P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48]
 & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] &
 P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[55] & P[54] & P[53] &
 P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27]
 & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] &
 P[0] & Cin );
  assign C[57] = G[56] | ( P[56] & G[55] ) | ( P[56] & P[55] & G[54] ) | ( P[56] & P[55] & P[54] & G[53] ) | ( P[56] & P[55] & P[54] & P[53] & G[52] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & G[51] )
 | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & G[50] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & G[49] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & G[48]
 ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & G[47] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & G[46] ) | ( P[56] & P[55] & P[54]
 & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & G[45] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & G[44] ) | ( P[56] & P[55] &
 P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & G[43] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & G[42] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & G[41] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] &
 P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & G[40] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & G[39] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & G[38] ) | ( P[56] &
 P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & G[37] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51]
 & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & G[36] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & G[35] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & G[34] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & G[33] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51]
 & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[56]
 & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] &
 P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[56] & P[55] & P[54] & P[53] & P[52]
 & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] &
 P[26] & P[25] & P[24] & G[23] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] )
 | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] )
 | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32]
 & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] &
 P[20] & P[19] & P[18] & G[17] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[56] & P[55] & P[54] & P[53] & P[52]
 & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] &
 P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] &
 P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[56]
 & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] &
 P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51]
 & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] &
 P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] &
 P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] &
 P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] &
 P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] &
 P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15]
 & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] &
 P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] &
 P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] &
 P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] &
 P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] &
 P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50]
 & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] &
 P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[56] & P[55]
 & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] &
 P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] &
 P[3] & P[2] & P[1] & G[0] ) | ( P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] &
 P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[58] = G[57] | ( P[57] & G[56] ) | ( P[57] & P[56] & G[55] ) | ( P[57] & P[56] & P[55] & G[54] ) | ( P[57] & P[56] & P[55] & P[54] & G[53] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & G[52] )
 | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & G[51] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & G[50] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & G[49]
 ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & G[48] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & G[47] ) | ( P[57] & P[56] & P[55]
 & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & G[46] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & G[45] ) | ( P[57] & P[56] &
 P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & G[44] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & G[43] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & G[42] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] &
 P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & G[41] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & G[40] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & G[39] ) | ( P[57] &
 P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & G[38] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52]
 & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & G[37] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48]
 & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & G[36] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & G[35] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & G[34] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & G[33] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48]
 & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52]
 & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[57]
 & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] &
 P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[57] & P[56] & P[55] & P[54] & P[53]
 & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] &
 P[27] & P[26] & P[25] & G[24] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] )
 | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] )
 | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33]
 & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] &
 P[21] & P[20] & P[19] & G[18] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[57] & P[56] & P[55] & P[54] & P[53]
 & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] &
 P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] &
 P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[57]
 & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] &
 P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52]
 & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] &
 P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48]
 & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] &
 P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] &
 P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] &
 P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] &
 P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] &
 P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16]
 & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] &
 P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] &
 P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] &
 P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48]
 & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] &
 P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52]
 & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] &
 P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[57]
 & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] &
 P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6]
 & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] &
 P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12]
 & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] &
 P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[59] = G[58] | ( P[58] & G[57] ) | ( P[58] & P[57] & G[56] ) | ( P[58] & P[57] & P[56] & G[55] ) | ( P[58] & P[57] & P[56] & P[55] & G[54] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & G[53] )
 | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & G[52] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & G[51] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & G[50]
 ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & G[49] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & G[48] ) | ( P[58] & P[57] & P[56]
 & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & G[47] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & G[46] ) | ( P[58] & P[57] &
 P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & G[45] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & G[44] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & G[43] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] &
 P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & G[42] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & G[41] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & G[40] ) | ( P[58] &
 P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & G[39] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53]
 & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & G[38] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49]
 & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & G[37] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & G[36] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & G[35] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & G[34] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & G[33] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49]
 & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53]
 & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[58]
 & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] &
 P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[58] & P[57] & P[56] & P[55] & P[54]
 & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] &
 P[28] & P[27] & P[26] & G[25] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48]
 & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] )
 | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] )
 | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48]
 & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] &
 P[22] & P[21] & P[20] & G[19] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[58] & P[57] & P[56] & P[55] & P[54]
 & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] &
 P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] &
 P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[58]
 & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] &
 P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53]
 & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] &
 P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49]
 & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] &
 P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] &
 P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] &
 P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] &
 P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43]
 & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] &
 P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] &
 P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18]
 & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] &
 P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] &
 P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50]
 & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] &
 P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[58] & P[57] & P[56] & P[55] & P[54]
 & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] &
 P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] )
 | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34]
 & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] &
 P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] &
 P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] &
 P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[58] & P[57] & P[56] & P[55] &
 P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29]
 & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3]
 & P[2] & P[1] & P[0] & Cin );
  assign C[60] = G[59] | ( P[59] & G[58] ) | ( P[59] & P[58] & G[57] ) | ( P[59] & P[58] & P[57] & G[56] ) | ( P[59] & P[58] & P[57] & P[56] & G[55] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & G[54] )
 | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & G[53] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & G[52] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & G[51]
 ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & G[50] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & G[49] ) | ( P[59] & P[58] & P[57]
 & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & G[48] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & G[47] ) | ( P[59] & P[58] &
 P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & G[46] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & G[45] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & G[44] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] &
 P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & G[43] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & G[42] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & G[41] ) | ( P[59] &
 P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & G[40] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54]
 & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & G[39] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50]
 & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & G[38] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & G[37] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & G[36] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & G[35] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & G[34] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & G[33] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50]
 & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54]
 & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[59]
 & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] &
 P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[59] & P[58] & P[57] & P[56] & P[55]
 & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] &
 P[29] & P[28] & P[27] & G[26] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49]
 & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] )
 | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] )
 | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35]
 & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49]
 & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] &
 P[23] & P[22] & P[21] & G[20] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39]
 & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[59] & P[58] & P[57] & P[56] & P[55]
 & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] &
 P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] &
 P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[59]
 & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] &
 P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54]
 & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] &
 P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50]
 & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] &
 P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] &
 P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] &
 P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] &
 P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] &
 P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] &
 P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] &
 P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21]
 & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49]
 & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] &
 P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52]
 & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] &
 P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[59] & P[58] & P[57] & P[56]
 & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] &
 P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5]
 & G[4] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] &
 P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] &
 P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49]
 & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] &
 P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[59] & P[58] & P[57]
 & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] &
 P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6]
 & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] &
 P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
  assign C[61] = G[60] | ( P[60] & G[59] ) | ( P[60] & P[59] & G[58] ) | ( P[60] & P[59] & P[58] & G[57] ) | ( P[60] & P[59] & P[58] & P[57] & G[56] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & G[55] )
 | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & G[54] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & G[53] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & G[52]
 ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & G[51] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & G[50] ) | ( P[60] & P[59] & P[58]
 & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & G[49] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & G[48] ) | ( P[60] & P[59] &
 P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & G[47] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48]
 & P[47] & G[46] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & G[45] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] &
 P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & G[44] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & G[43] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & G[42] ) | ( P[60] &
 P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & G[41] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55]
 & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & G[40] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51]
 & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & G[39] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48]
 & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & G[38] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & G[37] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & G[36] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & G[35] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & G[34] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48]
 & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & G[33] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51]
 & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55]
 & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[60]
 & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] &
 P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48]
 & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & G[28] ) | ( P[60] & P[59] & P[58] & P[57] & P[56]
 & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] &
 P[30] & P[29] & P[28] & G[27] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50]
 & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] )
 | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48]
 & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] )
 | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36]
 & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & G[22] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50]
 & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] &
 P[24] & P[23] & P[22] & G[21] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[60] & P[59] & P[58] & P[57] & P[56]
 & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] &
 P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48]
 & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] &
 P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[60]
 & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] &
 P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55]
 & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] &
 P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51]
 & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] &
 P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48]
 & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] &
 P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] &
 P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] &
 P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45]
 & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] &
 P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] &
 P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48]
 & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] &
 P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] &
 P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25]
 & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54]
 & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] &
 P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & G[6] ) | ( P[60] & P[59] & P[58]
 & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] &
 P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7]
 & P[6] & G[5] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38]
 & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] &
 P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] &
 P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51]
 & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] &
 P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[60] & P[59]
 & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] &
 P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8]
 & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] &
 P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & G[0] ) | ( P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] &
 P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26]
 & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin
 );
  assign C[62] = G[61] | ( P[61] & G[60] ) | ( P[61] & P[60] & G[59] ) | ( P[61] & P[60] & P[59] & G[58] ) | ( P[61] & P[60] & P[59] & P[58] & G[57] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & G[56] )
 | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & G[55] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & G[54] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & G[53]
 ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & G[52] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & G[51] ) | ( P[61] & P[60] & P[59]
 & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & G[50] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & G[49] ) | ( P[61] & P[60] &
 P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & G[48] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49]
 & P[48] & G[47] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & G[46] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] &
 P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & G[45] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48]
 & P[47] & P[46] & P[45] & G[44] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & G[43] ) | ( P[61] &
 P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & G[42] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56]
 & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & G[41] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52]
 & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & G[40] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49]
 & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & G[39] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & G[38] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & G[37] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & G[36] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & G[35] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49]
 & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & G[34] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52]
 & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & G[33] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56]
 & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & G[32] ) | ( P[61]
 & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] &
 P[35] & P[34] & P[33] & P[32] & G[31] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & G[30] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49]
 & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & G[29] ) | ( P[61] & P[60] & P[59] & P[58] & P[57]
 & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] &
 P[31] & P[30] & P[29] & G[28] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & G[27] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51]
 & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & G[26] )
 | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & G[25] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49]
 & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & G[24] )
 | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37]
 & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & G[23] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51]
 & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] &
 P[25] & P[24] & P[23] & G[22] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41]
 & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & G[21] ) | ( P[61] & P[60] & P[59] & P[58] & P[57]
 & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] &
 P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & G[20] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49]
 & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] &
 P[23] & P[22] & P[21] & P[20] & G[19] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42]
 & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & G[18] ) | ( P[61]
 & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] &
 P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & G[17] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56]
 & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] &
 P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & G[16] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52]
 & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] &
 P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & G[15] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49]
 & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] &
 P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & G[14] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] &
 P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & G[13] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] &
 P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & G[12] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] &
 P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & G[11] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47]
 & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] &
 P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & G[10] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49]
 & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] &
 P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & G[9] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52]
 & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] &
 P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & G[8] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] &
 P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30]
 & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & G[7] ) | ( P[61] & P[60]
 & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] &
 P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9]
 & P[8] & P[7] & G[6] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40]
 & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] &
 P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & G[5] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46]
 & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] &
 P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & G[4] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53]
 & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] &
 P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & G[3] ) | ( P[61]
 & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] &
 P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10]
 & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & G[2] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44]
 & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] &
 P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & G[1] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54]
 & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] & P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] &
 P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14] & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] &
 P[2] & P[1] & G[0] ) | ( P[61] & P[60] & P[59] & P[58] & P[57] & P[56] & P[55] & P[54] & P[53] & P[52] & P[51] & P[50] & P[49] & P[48] & P[47] & P[46] & P[45] & P[44] & P[43] & P[42] & P[41] & P[40] &
 P[39] & P[38] & P[37] & P[36] & P[35] & P[34] & P[33] & P[32] & P[31] & P[30] & P[29] & P[28] & P[27] & P[26] & P[25] & P[24] & P[23] & P[22] & P[21] & P[20] & P[19] & P[18] & P[17] & P[16] & P[15] & P[14]
 & P[13] & P[12] & P[11] & P[10] & P[9] & P[8] & P[7] & P[6] & P[5] & P[4] & P[3] & P[2] & P[1] & P[0] & Cin );
endmodule

module UBZero_6_6(O);
  output [6:6] O;
  assign O[6] = 0;
endmodule

module UBTC1CON69_0(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_1(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_2(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_3(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_4(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_5(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_6(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_7(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_8(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_9(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_10(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_11(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_12(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_13(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_14(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_15(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_16(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_17(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_18(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_19(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_20(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_21(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_22(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_23(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_24(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_25(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_26(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_27(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_28(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_29(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_30(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_31(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_32(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_33(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_34(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_35(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_36(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_37(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_38(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_39(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_40(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_41(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_42(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_43(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_44(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_45(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_46(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_47(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_48(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_49(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_50(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_51(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_52(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_53(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_54(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_55(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_56(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_57(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_58(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_59(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_60(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_61(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_62(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_63(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_64(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTC1CON69_65(O, I);
  output O;
  input I;
  assign O = I;
endmodule

module UBTCTCONV_68_66(O, I);
  output [69:66] O;
  input [68:66] I;
  assign O[66] = ~ I[66];
  assign O[67] = ~ I[67] ^ ( I[66] );
  assign O[68] = ~ I[68] ^ ( I[67] | I[66] );
  assign O[69] = ~ ( I[68] | I[67] | I[66] );
endmodule

module Multiplier_31_0_3000(P, IN1, IN2);
  output [63:0] P;
  input [31:0] IN1;
  input [31:0] IN2;
  wire [69:0] W;
  assign P[0] = W[0];
  assign P[1] = W[1];
  assign P[2] = W[2];
  assign P[3] = W[3];
  assign P[4] = W[4];
  assign P[5] = W[5];
  assign P[6] = W[6];
  assign P[7] = W[7];
  assign P[8] = W[8];
  assign P[9] = W[9];
  assign P[10] = W[10];
  assign P[11] = W[11];
  assign P[12] = W[12];
  assign P[13] = W[13];
  assign P[14] = W[14];
  assign P[15] = W[15];
  assign P[16] = W[16];
  assign P[17] = W[17];
  assign P[18] = W[18];
  assign P[19] = W[19];
  assign P[20] = W[20];
  assign P[21] = W[21];
  assign P[22] = W[22];
  assign P[23] = W[23];
  assign P[24] = W[24];
  assign P[25] = W[25];
  assign P[26] = W[26];
  assign P[27] = W[27];
  assign P[28] = W[28];
  assign P[29] = W[29];
  assign P[30] = W[30];
  assign P[31] = W[31];
  assign P[32] = W[32];
  assign P[33] = W[33];
  assign P[34] = W[34];
  assign P[35] = W[35];
  assign P[36] = W[36];
  assign P[37] = W[37];
  assign P[38] = W[38];
  assign P[39] = W[39];
  assign P[40] = W[40];
  assign P[41] = W[41];
  assign P[42] = W[42];
  assign P[43] = W[43];
  assign P[44] = W[44];
  assign P[45] = W[45];
  assign P[46] = W[46];
  assign P[47] = W[47];
  assign P[48] = W[48];
  assign P[49] = W[49];
  assign P[50] = W[50];
  assign P[51] = W[51];
  assign P[52] = W[52];
  assign P[53] = W[53];
  assign P[54] = W[54];
  assign P[55] = W[55];
  assign P[56] = W[56];
  assign P[57] = W[57];
  assign P[58] = W[58];
  assign P[59] = W[59];
  assign P[60] = W[60];
  assign P[61] = W[61];
  assign P[62] = W[62];
  assign P[63] = W[63];
  MultUB_R4B_WAL_CL000 U0 (W, IN1, IN2);
endmodule

module CSA_34_0_36_0_38_000 (C, S, X, Y, Z);
  output [37:1] C;
  output [38:0] S;
  input [34:0] X;
  input [36:0] Y;
  input [38:2] Z;
  PureCSHA_1_0 U0 (C[2:1], S[1:0], Y[1:0], X[1:0]);
  PureCSA_34_2 U1 (C[35:3], S[34:2], Z[34:2], Y[34:2], X[34:2]);
  PureCSHA_36_35 U2 (C[37:36], S[36:35], Z[36:35], Y[36:35]);
  UBCON_38_37 U3 (S[38:37], Z[38:37]);
endmodule

module CSA_38_0_37_1_44_000 (C, S, X, Y, Z);
  output [39:2] C;
  output [44:0] S;
  input [38:0] X;
  input [37:1] Y;
  input [44:4] Z;
  UB1DCON_0 U0 (S[0], X[0]);
  PureCSHA_3_1 U1 (C[4:2], S[3:1], Y[3:1], X[3:1]);
  PureCSA_37_4 U2 (C[38:5], S[37:4], Z[37:4], Y[37:4], X[37:4]);
  UBHA_38 U3 (C[39], S[38], Z[38], X[38]);
  UBCON_44_39 U4 (S[44:39], Z[44:39]);
endmodule

module CSA_40_4_42_6_44_000 (C, S, X, Y, Z);
  output [43:7] C;
  output [44:4] S;
  input [40:4] X;
  input [42:6] Y;
  input [44:8] Z;
  UBCON_5_4 U0 (S[5:4], X[5:4]);
  PureCSHA_7_6 U1 (C[8:7], S[7:6], Y[7:6], X[7:6]);
  PureCSA_40_8 U2 (C[41:9], S[40:8], Z[40:8], Y[40:8], X[40:8]);
  PureCSHA_42_41 U3 (C[43:42], S[42:41], Z[42:41], Y[42:41]);
  UBCON_44_43 U4 (S[44:43], Z[44:43]);
endmodule

module CSA_43_7_50_10_49000 (C, S, X, Y, Z);
  output [50:11] C;
  output [50:7] S;
  input [43:7] X;
  input [50:10] Y;
  input [49:13] Z;
  UBCON_9_7 U0 (S[9:7], X[9:7]);
  PureCSHA_12_10 U1 (C[13:11], S[12:10], Y[12:10], X[12:10]);
  PureCSA_43_13 U2 (C[44:14], S[43:13], Z[43:13], Y[43:13], X[43:13]);
  PureCSHA_49_44 U3 (C[50:45], S[49:44], Y[49:44], Z[49:44]);
  UB1DCON_50 U4 (S[50], Y[50]);
endmodule

module CSA_44_0_39_2_50_000 (C, S, X, Y, Z);
  output [45:3] C;
  output [50:0] S;
  input [44:0] X;
  input [39:2] Y;
  input [50:7] Z;
  UBCON_1_0 U0 (S[1:0], X[1:0]);
  PureCSHA_6_2 U1 (C[7:3], S[6:2], Y[6:2], X[6:2]);
  PureCSA_39_7 U2 (C[40:8], S[39:7], Z[39:7], Y[39:7], X[39:7]);
  PureCSHA_44_40 U3 (C[45:41], S[44:40], Z[44:40], X[44:40]);
  UBCON_50_45 U4 (S[50:45], Z[50:45]);
endmodule

module CSA_46_10_48_12_5000 (C, S, X, Y, Z);
  output [49:13] C;
  output [50:10] S;
  input [46:10] X;
  input [48:12] Y;
  input [50:14] Z;
  UBCON_11_10 U0 (S[11:10], X[11:10]);
  PureCSHA_13_12 U1 (C[14:13], S[13:12], Y[13:12], X[13:12]);
  PureCSA_46_14 U2 (C[47:15], S[46:14], Z[46:14], Y[46:14], X[46:14]);
  PureCSHA_48_47 U3 (C[49:48], S[48:47], Z[48:47], Y[48:47]);
  UBCON_50_49 U4 (S[50:49], Z[50:49]);
endmodule

module CSA_50_0_45_3_62_000 (C, S, X, Y, Z);
  output [51:4] C;
  output [62:0] S;
  input [50:0] X;
  input [45:3] Y;
  input [62:11] Z;
  UBCON_2_0 U0 (S[2:0], X[2:0]);
  PureCSHA_10_3 U1 (C[11:4], S[10:3], Y[10:3], X[10:3]);
  PureCSA_45_11 U2 (C[46:12], S[45:11], Z[45:11], Y[45:11], X[45:11]);
  PureCSHA_50_46 U3 (C[51:47], S[50:46], Z[50:46], X[50:46]);
  UBCON_62_51 U4 (S[62:51], Z[62:51]);
endmodule

module CSA_50_11_62_16_5000 (C, S, X, Y, Z);
  output [58:17] C;
  output [62:11] S;
  input [50:11] X;
  input [62:16] Y;
  input [57:20] Z;
  UBCON_15_11 U0 (S[15:11], X[15:11]);
  PureCSHA_19_16 U1 (C[20:17], S[19:16], Y[19:16], X[19:16]);
  PureCSA_50_20 U2 (C[51:21], S[50:20], Z[50:20], Y[50:20], X[50:20]);
  PureCSHA_57_51 U3 (C[58:52], S[57:51], Y[57:51], Z[57:51]);
  UBCON_62_58 U4 (S[62:58], Y[62:58]);
endmodule

module CSA_52_16_54_18_5000 (C, S, X, Y, Z);
  output [55:19] C;
  output [56:16] S;
  input [52:16] X;
  input [54:18] Y;
  input [56:20] Z;
  UBCON_17_16 U0 (S[17:16], X[17:16]);
  PureCSHA_19_18 U1 (C[20:19], S[19:18], Y[19:18], X[19:18]);
  PureCSA_52_20 U2 (C[53:21], S[52:20], Z[52:20], Y[52:20], X[52:20]);
  PureCSHA_54_53 U3 (C[55:54], S[54:53], Z[54:53], Y[54:53]);
  UBCON_56_55 U4 (S[56:55], Z[56:55]);
endmodule

module CSA_56_16_55_19_6000 (C, S, X, Y, Z);
  output [57:20] C;
  output [62:16] S;
  input [56:16] X;
  input [55:19] Y;
  input [62:22] Z;
  UBCON_18_16 U0 (S[18:16], X[18:16]);
  PureCSHA_21_19 U1 (C[22:20], S[21:19], Y[21:19], X[21:19]);
  PureCSA_55_22 U2 (C[56:23], S[55:22], Z[55:22], Y[55:22], X[55:22]);
  UBHA_56 U3 (C[57], S[56], Z[56], X[56]);
  UBCON_62_57 U4 (S[62:57], Z[62:57]);
endmodule

module CSA_58_17_65_25_6000 (C, S, X, Y, Z);
  output [66:26] C;
  output [66:17] S;
  input [58:17] X;
  input [65:25] Y;
  input [66:30] Z;
  UBCON_24_17 U0 (S[24:17], X[24:17]);
  PureCSHA_29_25 U1 (C[30:26], S[29:25], Y[29:25], X[29:25]);
  PureCSA_58_30 U2 (C[59:31], S[58:30], Z[58:30], Y[58:30], X[58:30]);
  PureCSHA_65_59 U3 (C[66:60], S[65:59], Z[65:59], Y[65:59]);
  UB1DCON_66 U4 (S[66], Z[66]);
endmodule

module CSA_58_22_60_24_6000 (C, S, X, Y, Z);
  output [61:25] C;
  output [62:22] S;
  input [58:22] X;
  input [60:24] Y;
  input [62:26] Z;
  UBCON_23_22 U0 (S[23:22], X[23:22]);
  PureCSHA_25_24 U1 (C[26:25], S[25:24], Y[25:24], X[25:24]);
  PureCSA_58_26 U2 (C[59:27], S[58:26], Z[58:26], Y[58:26], X[58:26]);
  PureCSHA_60_59 U3 (C[61:60], S[60:59], Z[60:59], Y[60:59]);
  UBCON_62_61 U4 (S[62:61], Z[62:61]);
endmodule

module CSA_61_25_64_28_6000 (C, S, X, Y, Z);
  output [65:29] C;
  output [65:25] S;
  input [61:25] X;
  input [64:28] Y;
  input [65:30] Z;
  UBCON_27_25 U0 (S[27:25], X[27:25]);
  PureCSHA_29_28 U1 (C[30:29], S[29:28], Y[29:28], X[29:28]);
  PureCSA_61_30 U2 (C[62:31], S[61:30], Z[61:30], Y[61:30], X[61:30]);
  PureCSHA_64_62 U3 (C[65:63], S[64:62], Z[64:62], Y[64:62]);
  UB1DCON_65 U4 (S[65], Z[65]);
endmodule

module CSA_62_0_51_4_66_000 (C, S, X, Y, Z);
  output [63:5] C;
  output [66:0] S;
  input [62:0] X;
  input [51:4] Y;
  input [66:17] Z;
  UBCON_3_0 U0 (S[3:0], X[3:0]);
  PureCSHA_16_4 U1 (C[17:5], S[16:4], Y[16:4], X[16:4]);
  PureCSA_51_17 U2 (C[52:18], S[51:17], Z[51:17], Y[51:17], X[51:17]);
  PureCSHA_62_52 U3 (C[63:53], S[62:52], Z[62:52], X[62:52]);
  UBCON_66_63 U4 (S[66:63], Z[66:63]);
endmodule

module CSA_65_25_65_29_3000 (C, S, X, Y, Z);
  output [66:30] C;
  output [65:25] S;
  input [65:25] X;
  input [65:29] Y;
  input [33:32] Z;
  UBCON_28_25 U0 (S[28:25], X[28:25]);
  PureCSHA_31_29 U1 (C[32:30], S[31:29], Y[31:29], X[31:29]);
  PureCSA_33_32 U2 (C[34:33], S[33:32], Z[33:32], Y[33:32], X[33:32]);
  PureCSHA_65_34 U3 (C[66:35], S[65:34], X[65:34], Y[65:34]);
endmodule

module CSA_66_0_63_5_66_000 (C, S, X, Y, Z);
  output [67:6] C;
  output [66:0] S;
  input [66:0] X;
  input [63:5] Y;
  input [66:26] Z;
  UBCON_4_0 U0 (S[4:0], X[4:0]);
  PureCSHA_25_5 U1 (C[26:6], S[25:5], Y[25:5], X[25:5]);
  PureCSA_63_26 U2 (C[64:27], S[63:26], Z[63:26], Y[63:26], X[63:26]);
  PureCSHA_66_64 U3 (C[67:65], S[66:64], Z[66:64], X[66:64]);
endmodule

module MultUB_R4B_WAL_CL000 (P, IN1, IN2);
  output [69:0] P;
  input [31:0] IN1;
  input [31:0] IN2;
  wire [34:0] PP0;
  wire [36:0] PP1;
  wire [54:18] PP10;
  wire [56:20] PP11;
  wire [58:22] PP12;
  wire [60:24] PP13;
  wire [62:26] PP14;
  wire [64:28] PP15;
  wire [65:30] PP16;
  wire [33:32] PP17;
  wire [38:2] PP2;
  wire [40:4] PP3;
  wire [42:6] PP4;
  wire [44:8] PP5;
  wire [46:10] PP6;
  wire [48:12] PP7;
  wire [50:14] PP8;
  wire [52:16] PP9;
  wire [67:6] S1;
  wire [66:0] S2;
  wire [68:0] UP;
  UBR4BPPG_31_0_31_000 U0 (PP0, PP1, PP2, PP3, PP4, PP5, PP6, PP7, PP8, PP9, PP10, PP11, PP12, PP13, PP14, PP15, PP16, PP17, IN1, IN2);
  WLCTR_34_0_36_0_3000 U1 (S1, S2, PP0, PP1, PP2, PP3, PP4, PP5, PP6, PP7, PP8, PP9, PP10, PP11, PP12, PP13, PP14, PP15, PP16, PP17);
  UBCLA_67_6_66_0 U2 (UP, S1, S2);
  UBTCCONV66_68_0 U3 (P, UP);
endmodule

module PureCSA_33_32 (C, S, X, Y, Z);
  output [34:33] C;
  output [33:32] S;
  input [33:32] X;
  input [33:32] Y;
  input [33:32] Z;
  UBFA_32 U0 (C[33], S[32], X[32], Y[32], Z[32]);
  UBFA_33 U1 (C[34], S[33], X[33], Y[33], Z[33]);
endmodule

module PureCSA_34_2 (C, S, X, Y, Z);
  output [35:3] C;
  output [34:2] S;
  input [34:2] X;
  input [34:2] Y;
  input [34:2] Z;
  UBFA_2 U0 (C[3], S[2], X[2], Y[2], Z[2]);
  UBFA_3 U1 (C[4], S[3], X[3], Y[3], Z[3]);
  UBFA_4 U2 (C[5], S[4], X[4], Y[4], Z[4]);
  UBFA_5 U3 (C[6], S[5], X[5], Y[5], Z[5]);
  UBFA_6 U4 (C[7], S[6], X[6], Y[6], Z[6]);
  UBFA_7 U5 (C[8], S[7], X[7], Y[7], Z[7]);
  UBFA_8 U6 (C[9], S[8], X[8], Y[8], Z[8]);
  UBFA_9 U7 (C[10], S[9], X[9], Y[9], Z[9]);
  UBFA_10 U8 (C[11], S[10], X[10], Y[10], Z[10]);
  UBFA_11 U9 (C[12], S[11], X[11], Y[11], Z[11]);
  UBFA_12 U10 (C[13], S[12], X[12], Y[12], Z[12]);
  UBFA_13 U11 (C[14], S[13], X[13], Y[13], Z[13]);
  UBFA_14 U12 (C[15], S[14], X[14], Y[14], Z[14]);
  UBFA_15 U13 (C[16], S[15], X[15], Y[15], Z[15]);
  UBFA_16 U14 (C[17], S[16], X[16], Y[16], Z[16]);
  UBFA_17 U15 (C[18], S[17], X[17], Y[17], Z[17]);
  UBFA_18 U16 (C[19], S[18], X[18], Y[18], Z[18]);
  UBFA_19 U17 (C[20], S[19], X[19], Y[19], Z[19]);
  UBFA_20 U18 (C[21], S[20], X[20], Y[20], Z[20]);
  UBFA_21 U19 (C[22], S[21], X[21], Y[21], Z[21]);
  UBFA_22 U20 (C[23], S[22], X[22], Y[22], Z[22]);
  UBFA_23 U21 (C[24], S[23], X[23], Y[23], Z[23]);
  UBFA_24 U22 (C[25], S[24], X[24], Y[24], Z[24]);
  UBFA_25 U23 (C[26], S[25], X[25], Y[25], Z[25]);
  UBFA_26 U24 (C[27], S[26], X[26], Y[26], Z[26]);
  UBFA_27 U25 (C[28], S[27], X[27], Y[27], Z[27]);
  UBFA_28 U26 (C[29], S[28], X[28], Y[28], Z[28]);
  UBFA_29 U27 (C[30], S[29], X[29], Y[29], Z[29]);
  UBFA_30 U28 (C[31], S[30], X[30], Y[30], Z[30]);
  UBFA_31 U29 (C[32], S[31], X[31], Y[31], Z[31]);
  UBFA_32 U30 (C[33], S[32], X[32], Y[32], Z[32]);
  UBFA_33 U31 (C[34], S[33], X[33], Y[33], Z[33]);
  UBFA_34 U32 (C[35], S[34], X[34], Y[34], Z[34]);
endmodule

module PureCSA_37_4 (C, S, X, Y, Z);
  output [38:5] C;
  output [37:4] S;
  input [37:4] X;
  input [37:4] Y;
  input [37:4] Z;
  UBFA_4 U0 (C[5], S[4], X[4], Y[4], Z[4]);
  UBFA_5 U1 (C[6], S[5], X[5], Y[5], Z[5]);
  UBFA_6 U2 (C[7], S[6], X[6], Y[6], Z[6]);
  UBFA_7 U3 (C[8], S[7], X[7], Y[7], Z[7]);
  UBFA_8 U4 (C[9], S[8], X[8], Y[8], Z[8]);
  UBFA_9 U5 (C[10], S[9], X[9], Y[9], Z[9]);
  UBFA_10 U6 (C[11], S[10], X[10], Y[10], Z[10]);
  UBFA_11 U7 (C[12], S[11], X[11], Y[11], Z[11]);
  UBFA_12 U8 (C[13], S[12], X[12], Y[12], Z[12]);
  UBFA_13 U9 (C[14], S[13], X[13], Y[13], Z[13]);
  UBFA_14 U10 (C[15], S[14], X[14], Y[14], Z[14]);
  UBFA_15 U11 (C[16], S[15], X[15], Y[15], Z[15]);
  UBFA_16 U12 (C[17], S[16], X[16], Y[16], Z[16]);
  UBFA_17 U13 (C[18], S[17], X[17], Y[17], Z[17]);
  UBFA_18 U14 (C[19], S[18], X[18], Y[18], Z[18]);
  UBFA_19 U15 (C[20], S[19], X[19], Y[19], Z[19]);
  UBFA_20 U16 (C[21], S[20], X[20], Y[20], Z[20]);
  UBFA_21 U17 (C[22], S[21], X[21], Y[21], Z[21]);
  UBFA_22 U18 (C[23], S[22], X[22], Y[22], Z[22]);
  UBFA_23 U19 (C[24], S[23], X[23], Y[23], Z[23]);
  UBFA_24 U20 (C[25], S[24], X[24], Y[24], Z[24]);
  UBFA_25 U21 (C[26], S[25], X[25], Y[25], Z[25]);
  UBFA_26 U22 (C[27], S[26], X[26], Y[26], Z[26]);
  UBFA_27 U23 (C[28], S[27], X[27], Y[27], Z[27]);
  UBFA_28 U24 (C[29], S[28], X[28], Y[28], Z[28]);
  UBFA_29 U25 (C[30], S[29], X[29], Y[29], Z[29]);
  UBFA_30 U26 (C[31], S[30], X[30], Y[30], Z[30]);
  UBFA_31 U27 (C[32], S[31], X[31], Y[31], Z[31]);
  UBFA_32 U28 (C[33], S[32], X[32], Y[32], Z[32]);
  UBFA_33 U29 (C[34], S[33], X[33], Y[33], Z[33]);
  UBFA_34 U30 (C[35], S[34], X[34], Y[34], Z[34]);
  UBFA_35 U31 (C[36], S[35], X[35], Y[35], Z[35]);
  UBFA_36 U32 (C[37], S[36], X[36], Y[36], Z[36]);
  UBFA_37 U33 (C[38], S[37], X[37], Y[37], Z[37]);
endmodule

module PureCSA_39_7 (C, S, X, Y, Z);
  output [40:8] C;
  output [39:7] S;
  input [39:7] X;
  input [39:7] Y;
  input [39:7] Z;
  UBFA_7 U0 (C[8], S[7], X[7], Y[7], Z[7]);
  UBFA_8 U1 (C[9], S[8], X[8], Y[8], Z[8]);
  UBFA_9 U2 (C[10], S[9], X[9], Y[9], Z[9]);
  UBFA_10 U3 (C[11], S[10], X[10], Y[10], Z[10]);
  UBFA_11 U4 (C[12], S[11], X[11], Y[11], Z[11]);
  UBFA_12 U5 (C[13], S[12], X[12], Y[12], Z[12]);
  UBFA_13 U6 (C[14], S[13], X[13], Y[13], Z[13]);
  UBFA_14 U7 (C[15], S[14], X[14], Y[14], Z[14]);
  UBFA_15 U8 (C[16], S[15], X[15], Y[15], Z[15]);
  UBFA_16 U9 (C[17], S[16], X[16], Y[16], Z[16]);
  UBFA_17 U10 (C[18], S[17], X[17], Y[17], Z[17]);
  UBFA_18 U11 (C[19], S[18], X[18], Y[18], Z[18]);
  UBFA_19 U12 (C[20], S[19], X[19], Y[19], Z[19]);
  UBFA_20 U13 (C[21], S[20], X[20], Y[20], Z[20]);
  UBFA_21 U14 (C[22], S[21], X[21], Y[21], Z[21]);
  UBFA_22 U15 (C[23], S[22], X[22], Y[22], Z[22]);
  UBFA_23 U16 (C[24], S[23], X[23], Y[23], Z[23]);
  UBFA_24 U17 (C[25], S[24], X[24], Y[24], Z[24]);
  UBFA_25 U18 (C[26], S[25], X[25], Y[25], Z[25]);
  UBFA_26 U19 (C[27], S[26], X[26], Y[26], Z[26]);
  UBFA_27 U20 (C[28], S[27], X[27], Y[27], Z[27]);
  UBFA_28 U21 (C[29], S[28], X[28], Y[28], Z[28]);
  UBFA_29 U22 (C[30], S[29], X[29], Y[29], Z[29]);
  UBFA_30 U23 (C[31], S[30], X[30], Y[30], Z[30]);
  UBFA_31 U24 (C[32], S[31], X[31], Y[31], Z[31]);
  UBFA_32 U25 (C[33], S[32], X[32], Y[32], Z[32]);
  UBFA_33 U26 (C[34], S[33], X[33], Y[33], Z[33]);
  UBFA_34 U27 (C[35], S[34], X[34], Y[34], Z[34]);
  UBFA_35 U28 (C[36], S[35], X[35], Y[35], Z[35]);
  UBFA_36 U29 (C[37], S[36], X[36], Y[36], Z[36]);
  UBFA_37 U30 (C[38], S[37], X[37], Y[37], Z[37]);
  UBFA_38 U31 (C[39], S[38], X[38], Y[38], Z[38]);
  UBFA_39 U32 (C[40], S[39], X[39], Y[39], Z[39]);
endmodule

module PureCSA_40_8 (C, S, X, Y, Z);
  output [41:9] C;
  output [40:8] S;
  input [40:8] X;
  input [40:8] Y;
  input [40:8] Z;
  UBFA_8 U0 (C[9], S[8], X[8], Y[8], Z[8]);
  UBFA_9 U1 (C[10], S[9], X[9], Y[9], Z[9]);
  UBFA_10 U2 (C[11], S[10], X[10], Y[10], Z[10]);
  UBFA_11 U3 (C[12], S[11], X[11], Y[11], Z[11]);
  UBFA_12 U4 (C[13], S[12], X[12], Y[12], Z[12]);
  UBFA_13 U5 (C[14], S[13], X[13], Y[13], Z[13]);
  UBFA_14 U6 (C[15], S[14], X[14], Y[14], Z[14]);
  UBFA_15 U7 (C[16], S[15], X[15], Y[15], Z[15]);
  UBFA_16 U8 (C[17], S[16], X[16], Y[16], Z[16]);
  UBFA_17 U9 (C[18], S[17], X[17], Y[17], Z[17]);
  UBFA_18 U10 (C[19], S[18], X[18], Y[18], Z[18]);
  UBFA_19 U11 (C[20], S[19], X[19], Y[19], Z[19]);
  UBFA_20 U12 (C[21], S[20], X[20], Y[20], Z[20]);
  UBFA_21 U13 (C[22], S[21], X[21], Y[21], Z[21]);
  UBFA_22 U14 (C[23], S[22], X[22], Y[22], Z[22]);
  UBFA_23 U15 (C[24], S[23], X[23], Y[23], Z[23]);
  UBFA_24 U16 (C[25], S[24], X[24], Y[24], Z[24]);
  UBFA_25 U17 (C[26], S[25], X[25], Y[25], Z[25]);
  UBFA_26 U18 (C[27], S[26], X[26], Y[26], Z[26]);
  UBFA_27 U19 (C[28], S[27], X[27], Y[27], Z[27]);
  UBFA_28 U20 (C[29], S[28], X[28], Y[28], Z[28]);
  UBFA_29 U21 (C[30], S[29], X[29], Y[29], Z[29]);
  UBFA_30 U22 (C[31], S[30], X[30], Y[30], Z[30]);
  UBFA_31 U23 (C[32], S[31], X[31], Y[31], Z[31]);
  UBFA_32 U24 (C[33], S[32], X[32], Y[32], Z[32]);
  UBFA_33 U25 (C[34], S[33], X[33], Y[33], Z[33]);
  UBFA_34 U26 (C[35], S[34], X[34], Y[34], Z[34]);
  UBFA_35 U27 (C[36], S[35], X[35], Y[35], Z[35]);
  UBFA_36 U28 (C[37], S[36], X[36], Y[36], Z[36]);
  UBFA_37 U29 (C[38], S[37], X[37], Y[37], Z[37]);
  UBFA_38 U30 (C[39], S[38], X[38], Y[38], Z[38]);
  UBFA_39 U31 (C[40], S[39], X[39], Y[39], Z[39]);
  UBFA_40 U32 (C[41], S[40], X[40], Y[40], Z[40]);
endmodule

module PureCSA_43_13 (C, S, X, Y, Z);
  output [44:14] C;
  output [43:13] S;
  input [43:13] X;
  input [43:13] Y;
  input [43:13] Z;
  UBFA_13 U0 (C[14], S[13], X[13], Y[13], Z[13]);
  UBFA_14 U1 (C[15], S[14], X[14], Y[14], Z[14]);
  UBFA_15 U2 (C[16], S[15], X[15], Y[15], Z[15]);
  UBFA_16 U3 (C[17], S[16], X[16], Y[16], Z[16]);
  UBFA_17 U4 (C[18], S[17], X[17], Y[17], Z[17]);
  UBFA_18 U5 (C[19], S[18], X[18], Y[18], Z[18]);
  UBFA_19 U6 (C[20], S[19], X[19], Y[19], Z[19]);
  UBFA_20 U7 (C[21], S[20], X[20], Y[20], Z[20]);
  UBFA_21 U8 (C[22], S[21], X[21], Y[21], Z[21]);
  UBFA_22 U9 (C[23], S[22], X[22], Y[22], Z[22]);
  UBFA_23 U10 (C[24], S[23], X[23], Y[23], Z[23]);
  UBFA_24 U11 (C[25], S[24], X[24], Y[24], Z[24]);
  UBFA_25 U12 (C[26], S[25], X[25], Y[25], Z[25]);
  UBFA_26 U13 (C[27], S[26], X[26], Y[26], Z[26]);
  UBFA_27 U14 (C[28], S[27], X[27], Y[27], Z[27]);
  UBFA_28 U15 (C[29], S[28], X[28], Y[28], Z[28]);
  UBFA_29 U16 (C[30], S[29], X[29], Y[29], Z[29]);
  UBFA_30 U17 (C[31], S[30], X[30], Y[30], Z[30]);
  UBFA_31 U18 (C[32], S[31], X[31], Y[31], Z[31]);
  UBFA_32 U19 (C[33], S[32], X[32], Y[32], Z[32]);
  UBFA_33 U20 (C[34], S[33], X[33], Y[33], Z[33]);
  UBFA_34 U21 (C[35], S[34], X[34], Y[34], Z[34]);
  UBFA_35 U22 (C[36], S[35], X[35], Y[35], Z[35]);
  UBFA_36 U23 (C[37], S[36], X[36], Y[36], Z[36]);
  UBFA_37 U24 (C[38], S[37], X[37], Y[37], Z[37]);
  UBFA_38 U25 (C[39], S[38], X[38], Y[38], Z[38]);
  UBFA_39 U26 (C[40], S[39], X[39], Y[39], Z[39]);
  UBFA_40 U27 (C[41], S[40], X[40], Y[40], Z[40]);
  UBFA_41 U28 (C[42], S[41], X[41], Y[41], Z[41]);
  UBFA_42 U29 (C[43], S[42], X[42], Y[42], Z[42]);
  UBFA_43 U30 (C[44], S[43], X[43], Y[43], Z[43]);
endmodule

module PureCSA_45_11 (C, S, X, Y, Z);
  output [46:12] C;
  output [45:11] S;
  input [45:11] X;
  input [45:11] Y;
  input [45:11] Z;
  UBFA_11 U0 (C[12], S[11], X[11], Y[11], Z[11]);
  UBFA_12 U1 (C[13], S[12], X[12], Y[12], Z[12]);
  UBFA_13 U2 (C[14], S[13], X[13], Y[13], Z[13]);
  UBFA_14 U3 (C[15], S[14], X[14], Y[14], Z[14]);
  UBFA_15 U4 (C[16], S[15], X[15], Y[15], Z[15]);
  UBFA_16 U5 (C[17], S[16], X[16], Y[16], Z[16]);
  UBFA_17 U6 (C[18], S[17], X[17], Y[17], Z[17]);
  UBFA_18 U7 (C[19], S[18], X[18], Y[18], Z[18]);
  UBFA_19 U8 (C[20], S[19], X[19], Y[19], Z[19]);
  UBFA_20 U9 (C[21], S[20], X[20], Y[20], Z[20]);
  UBFA_21 U10 (C[22], S[21], X[21], Y[21], Z[21]);
  UBFA_22 U11 (C[23], S[22], X[22], Y[22], Z[22]);
  UBFA_23 U12 (C[24], S[23], X[23], Y[23], Z[23]);
  UBFA_24 U13 (C[25], S[24], X[24], Y[24], Z[24]);
  UBFA_25 U14 (C[26], S[25], X[25], Y[25], Z[25]);
  UBFA_26 U15 (C[27], S[26], X[26], Y[26], Z[26]);
  UBFA_27 U16 (C[28], S[27], X[27], Y[27], Z[27]);
  UBFA_28 U17 (C[29], S[28], X[28], Y[28], Z[28]);
  UBFA_29 U18 (C[30], S[29], X[29], Y[29], Z[29]);
  UBFA_30 U19 (C[31], S[30], X[30], Y[30], Z[30]);
  UBFA_31 U20 (C[32], S[31], X[31], Y[31], Z[31]);
  UBFA_32 U21 (C[33], S[32], X[32], Y[32], Z[32]);
  UBFA_33 U22 (C[34], S[33], X[33], Y[33], Z[33]);
  UBFA_34 U23 (C[35], S[34], X[34], Y[34], Z[34]);
  UBFA_35 U24 (C[36], S[35], X[35], Y[35], Z[35]);
  UBFA_36 U25 (C[37], S[36], X[36], Y[36], Z[36]);
  UBFA_37 U26 (C[38], S[37], X[37], Y[37], Z[37]);
  UBFA_38 U27 (C[39], S[38], X[38], Y[38], Z[38]);
  UBFA_39 U28 (C[40], S[39], X[39], Y[39], Z[39]);
  UBFA_40 U29 (C[41], S[40], X[40], Y[40], Z[40]);
  UBFA_41 U30 (C[42], S[41], X[41], Y[41], Z[41]);
  UBFA_42 U31 (C[43], S[42], X[42], Y[42], Z[42]);
  UBFA_43 U32 (C[44], S[43], X[43], Y[43], Z[43]);
  UBFA_44 U33 (C[45], S[44], X[44], Y[44], Z[44]);
  UBFA_45 U34 (C[46], S[45], X[45], Y[45], Z[45]);
endmodule

module PureCSA_46_14 (C, S, X, Y, Z);
  output [47:15] C;
  output [46:14] S;
  input [46:14] X;
  input [46:14] Y;
  input [46:14] Z;
  UBFA_14 U0 (C[15], S[14], X[14], Y[14], Z[14]);
  UBFA_15 U1 (C[16], S[15], X[15], Y[15], Z[15]);
  UBFA_16 U2 (C[17], S[16], X[16], Y[16], Z[16]);
  UBFA_17 U3 (C[18], S[17], X[17], Y[17], Z[17]);
  UBFA_18 U4 (C[19], S[18], X[18], Y[18], Z[18]);
  UBFA_19 U5 (C[20], S[19], X[19], Y[19], Z[19]);
  UBFA_20 U6 (C[21], S[20], X[20], Y[20], Z[20]);
  UBFA_21 U7 (C[22], S[21], X[21], Y[21], Z[21]);
  UBFA_22 U8 (C[23], S[22], X[22], Y[22], Z[22]);
  UBFA_23 U9 (C[24], S[23], X[23], Y[23], Z[23]);
  UBFA_24 U10 (C[25], S[24], X[24], Y[24], Z[24]);
  UBFA_25 U11 (C[26], S[25], X[25], Y[25], Z[25]);
  UBFA_26 U12 (C[27], S[26], X[26], Y[26], Z[26]);
  UBFA_27 U13 (C[28], S[27], X[27], Y[27], Z[27]);
  UBFA_28 U14 (C[29], S[28], X[28], Y[28], Z[28]);
  UBFA_29 U15 (C[30], S[29], X[29], Y[29], Z[29]);
  UBFA_30 U16 (C[31], S[30], X[30], Y[30], Z[30]);
  UBFA_31 U17 (C[32], S[31], X[31], Y[31], Z[31]);
  UBFA_32 U18 (C[33], S[32], X[32], Y[32], Z[32]);
  UBFA_33 U19 (C[34], S[33], X[33], Y[33], Z[33]);
  UBFA_34 U20 (C[35], S[34], X[34], Y[34], Z[34]);
  UBFA_35 U21 (C[36], S[35], X[35], Y[35], Z[35]);
  UBFA_36 U22 (C[37], S[36], X[36], Y[36], Z[36]);
  UBFA_37 U23 (C[38], S[37], X[37], Y[37], Z[37]);
  UBFA_38 U24 (C[39], S[38], X[38], Y[38], Z[38]);
  UBFA_39 U25 (C[40], S[39], X[39], Y[39], Z[39]);
  UBFA_40 U26 (C[41], S[40], X[40], Y[40], Z[40]);
  UBFA_41 U27 (C[42], S[41], X[41], Y[41], Z[41]);
  UBFA_42 U28 (C[43], S[42], X[42], Y[42], Z[42]);
  UBFA_43 U29 (C[44], S[43], X[43], Y[43], Z[43]);
  UBFA_44 U30 (C[45], S[44], X[44], Y[44], Z[44]);
  UBFA_45 U31 (C[46], S[45], X[45], Y[45], Z[45]);
  UBFA_46 U32 (C[47], S[46], X[46], Y[46], Z[46]);
endmodule

module PureCSA_50_20 (C, S, X, Y, Z);
  output [51:21] C;
  output [50:20] S;
  input [50:20] X;
  input [50:20] Y;
  input [50:20] Z;
  UBFA_20 U0 (C[21], S[20], X[20], Y[20], Z[20]);
  UBFA_21 U1 (C[22], S[21], X[21], Y[21], Z[21]);
  UBFA_22 U2 (C[23], S[22], X[22], Y[22], Z[22]);
  UBFA_23 U3 (C[24], S[23], X[23], Y[23], Z[23]);
  UBFA_24 U4 (C[25], S[24], X[24], Y[24], Z[24]);
  UBFA_25 U5 (C[26], S[25], X[25], Y[25], Z[25]);
  UBFA_26 U6 (C[27], S[26], X[26], Y[26], Z[26]);
  UBFA_27 U7 (C[28], S[27], X[27], Y[27], Z[27]);
  UBFA_28 U8 (C[29], S[28], X[28], Y[28], Z[28]);
  UBFA_29 U9 (C[30], S[29], X[29], Y[29], Z[29]);
  UBFA_30 U10 (C[31], S[30], X[30], Y[30], Z[30]);
  UBFA_31 U11 (C[32], S[31], X[31], Y[31], Z[31]);
  UBFA_32 U12 (C[33], S[32], X[32], Y[32], Z[32]);
  UBFA_33 U13 (C[34], S[33], X[33], Y[33], Z[33]);
  UBFA_34 U14 (C[35], S[34], X[34], Y[34], Z[34]);
  UBFA_35 U15 (C[36], S[35], X[35], Y[35], Z[35]);
  UBFA_36 U16 (C[37], S[36], X[36], Y[36], Z[36]);
  UBFA_37 U17 (C[38], S[37], X[37], Y[37], Z[37]);
  UBFA_38 U18 (C[39], S[38], X[38], Y[38], Z[38]);
  UBFA_39 U19 (C[40], S[39], X[39], Y[39], Z[39]);
  UBFA_40 U20 (C[41], S[40], X[40], Y[40], Z[40]);
  UBFA_41 U21 (C[42], S[41], X[41], Y[41], Z[41]);
  UBFA_42 U22 (C[43], S[42], X[42], Y[42], Z[42]);
  UBFA_43 U23 (C[44], S[43], X[43], Y[43], Z[43]);
  UBFA_44 U24 (C[45], S[44], X[44], Y[44], Z[44]);
  UBFA_45 U25 (C[46], S[45], X[45], Y[45], Z[45]);
  UBFA_46 U26 (C[47], S[46], X[46], Y[46], Z[46]);
  UBFA_47 U27 (C[48], S[47], X[47], Y[47], Z[47]);
  UBFA_48 U28 (C[49], S[48], X[48], Y[48], Z[48]);
  UBFA_49 U29 (C[50], S[49], X[49], Y[49], Z[49]);
  UBFA_50 U30 (C[51], S[50], X[50], Y[50], Z[50]);
endmodule

module PureCSA_51_17 (C, S, X, Y, Z);
  output [52:18] C;
  output [51:17] S;
  input [51:17] X;
  input [51:17] Y;
  input [51:17] Z;
  UBFA_17 U0 (C[18], S[17], X[17], Y[17], Z[17]);
  UBFA_18 U1 (C[19], S[18], X[18], Y[18], Z[18]);
  UBFA_19 U2 (C[20], S[19], X[19], Y[19], Z[19]);
  UBFA_20 U3 (C[21], S[20], X[20], Y[20], Z[20]);
  UBFA_21 U4 (C[22], S[21], X[21], Y[21], Z[21]);
  UBFA_22 U5 (C[23], S[22], X[22], Y[22], Z[22]);
  UBFA_23 U6 (C[24], S[23], X[23], Y[23], Z[23]);
  UBFA_24 U7 (C[25], S[24], X[24], Y[24], Z[24]);
  UBFA_25 U8 (C[26], S[25], X[25], Y[25], Z[25]);
  UBFA_26 U9 (C[27], S[26], X[26], Y[26], Z[26]);
  UBFA_27 U10 (C[28], S[27], X[27], Y[27], Z[27]);
  UBFA_28 U11 (C[29], S[28], X[28], Y[28], Z[28]);
  UBFA_29 U12 (C[30], S[29], X[29], Y[29], Z[29]);
  UBFA_30 U13 (C[31], S[30], X[30], Y[30], Z[30]);
  UBFA_31 U14 (C[32], S[31], X[31], Y[31], Z[31]);
  UBFA_32 U15 (C[33], S[32], X[32], Y[32], Z[32]);
  UBFA_33 U16 (C[34], S[33], X[33], Y[33], Z[33]);
  UBFA_34 U17 (C[35], S[34], X[34], Y[34], Z[34]);
  UBFA_35 U18 (C[36], S[35], X[35], Y[35], Z[35]);
  UBFA_36 U19 (C[37], S[36], X[36], Y[36], Z[36]);
  UBFA_37 U20 (C[38], S[37], X[37], Y[37], Z[37]);
  UBFA_38 U21 (C[39], S[38], X[38], Y[38], Z[38]);
  UBFA_39 U22 (C[40], S[39], X[39], Y[39], Z[39]);
  UBFA_40 U23 (C[41], S[40], X[40], Y[40], Z[40]);
  UBFA_41 U24 (C[42], S[41], X[41], Y[41], Z[41]);
  UBFA_42 U25 (C[43], S[42], X[42], Y[42], Z[42]);
  UBFA_43 U26 (C[44], S[43], X[43], Y[43], Z[43]);
  UBFA_44 U27 (C[45], S[44], X[44], Y[44], Z[44]);
  UBFA_45 U28 (C[46], S[45], X[45], Y[45], Z[45]);
  UBFA_46 U29 (C[47], S[46], X[46], Y[46], Z[46]);
  UBFA_47 U30 (C[48], S[47], X[47], Y[47], Z[47]);
  UBFA_48 U31 (C[49], S[48], X[48], Y[48], Z[48]);
  UBFA_49 U32 (C[50], S[49], X[49], Y[49], Z[49]);
  UBFA_50 U33 (C[51], S[50], X[50], Y[50], Z[50]);
  UBFA_51 U34 (C[52], S[51], X[51], Y[51], Z[51]);
endmodule

module PureCSA_52_20 (C, S, X, Y, Z);
  output [53:21] C;
  output [52:20] S;
  input [52:20] X;
  input [52:20] Y;
  input [52:20] Z;
  UBFA_20 U0 (C[21], S[20], X[20], Y[20], Z[20]);
  UBFA_21 U1 (C[22], S[21], X[21], Y[21], Z[21]);
  UBFA_22 U2 (C[23], S[22], X[22], Y[22], Z[22]);
  UBFA_23 U3 (C[24], S[23], X[23], Y[23], Z[23]);
  UBFA_24 U4 (C[25], S[24], X[24], Y[24], Z[24]);
  UBFA_25 U5 (C[26], S[25], X[25], Y[25], Z[25]);
  UBFA_26 U6 (C[27], S[26], X[26], Y[26], Z[26]);
  UBFA_27 U7 (C[28], S[27], X[27], Y[27], Z[27]);
  UBFA_28 U8 (C[29], S[28], X[28], Y[28], Z[28]);
  UBFA_29 U9 (C[30], S[29], X[29], Y[29], Z[29]);
  UBFA_30 U10 (C[31], S[30], X[30], Y[30], Z[30]);
  UBFA_31 U11 (C[32], S[31], X[31], Y[31], Z[31]);
  UBFA_32 U12 (C[33], S[32], X[32], Y[32], Z[32]);
  UBFA_33 U13 (C[34], S[33], X[33], Y[33], Z[33]);
  UBFA_34 U14 (C[35], S[34], X[34], Y[34], Z[34]);
  UBFA_35 U15 (C[36], S[35], X[35], Y[35], Z[35]);
  UBFA_36 U16 (C[37], S[36], X[36], Y[36], Z[36]);
  UBFA_37 U17 (C[38], S[37], X[37], Y[37], Z[37]);
  UBFA_38 U18 (C[39], S[38], X[38], Y[38], Z[38]);
  UBFA_39 U19 (C[40], S[39], X[39], Y[39], Z[39]);
  UBFA_40 U20 (C[41], S[40], X[40], Y[40], Z[40]);
  UBFA_41 U21 (C[42], S[41], X[41], Y[41], Z[41]);
  UBFA_42 U22 (C[43], S[42], X[42], Y[42], Z[42]);
  UBFA_43 U23 (C[44], S[43], X[43], Y[43], Z[43]);
  UBFA_44 U24 (C[45], S[44], X[44], Y[44], Z[44]);
  UBFA_45 U25 (C[46], S[45], X[45], Y[45], Z[45]);
  UBFA_46 U26 (C[47], S[46], X[46], Y[46], Z[46]);
  UBFA_47 U27 (C[48], S[47], X[47], Y[47], Z[47]);
  UBFA_48 U28 (C[49], S[48], X[48], Y[48], Z[48]);
  UBFA_49 U29 (C[50], S[49], X[49], Y[49], Z[49]);
  UBFA_50 U30 (C[51], S[50], X[50], Y[50], Z[50]);
  UBFA_51 U31 (C[52], S[51], X[51], Y[51], Z[51]);
  UBFA_52 U32 (C[53], S[52], X[52], Y[52], Z[52]);
endmodule

module PureCSA_55_22 (C, S, X, Y, Z);
  output [56:23] C;
  output [55:22] S;
  input [55:22] X;
  input [55:22] Y;
  input [55:22] Z;
  UBFA_22 U0 (C[23], S[22], X[22], Y[22], Z[22]);
  UBFA_23 U1 (C[24], S[23], X[23], Y[23], Z[23]);
  UBFA_24 U2 (C[25], S[24], X[24], Y[24], Z[24]);
  UBFA_25 U3 (C[26], S[25], X[25], Y[25], Z[25]);
  UBFA_26 U4 (C[27], S[26], X[26], Y[26], Z[26]);
  UBFA_27 U5 (C[28], S[27], X[27], Y[27], Z[27]);
  UBFA_28 U6 (C[29], S[28], X[28], Y[28], Z[28]);
  UBFA_29 U7 (C[30], S[29], X[29], Y[29], Z[29]);
  UBFA_30 U8 (C[31], S[30], X[30], Y[30], Z[30]);
  UBFA_31 U9 (C[32], S[31], X[31], Y[31], Z[31]);
  UBFA_32 U10 (C[33], S[32], X[32], Y[32], Z[32]);
  UBFA_33 U11 (C[34], S[33], X[33], Y[33], Z[33]);
  UBFA_34 U12 (C[35], S[34], X[34], Y[34], Z[34]);
  UBFA_35 U13 (C[36], S[35], X[35], Y[35], Z[35]);
  UBFA_36 U14 (C[37], S[36], X[36], Y[36], Z[36]);
  UBFA_37 U15 (C[38], S[37], X[37], Y[37], Z[37]);
  UBFA_38 U16 (C[39], S[38], X[38], Y[38], Z[38]);
  UBFA_39 U17 (C[40], S[39], X[39], Y[39], Z[39]);
  UBFA_40 U18 (C[41], S[40], X[40], Y[40], Z[40]);
  UBFA_41 U19 (C[42], S[41], X[41], Y[41], Z[41]);
  UBFA_42 U20 (C[43], S[42], X[42], Y[42], Z[42]);
  UBFA_43 U21 (C[44], S[43], X[43], Y[43], Z[43]);
  UBFA_44 U22 (C[45], S[44], X[44], Y[44], Z[44]);
  UBFA_45 U23 (C[46], S[45], X[45], Y[45], Z[45]);
  UBFA_46 U24 (C[47], S[46], X[46], Y[46], Z[46]);
  UBFA_47 U25 (C[48], S[47], X[47], Y[47], Z[47]);
  UBFA_48 U26 (C[49], S[48], X[48], Y[48], Z[48]);
  UBFA_49 U27 (C[50], S[49], X[49], Y[49], Z[49]);
  UBFA_50 U28 (C[51], S[50], X[50], Y[50], Z[50]);
  UBFA_51 U29 (C[52], S[51], X[51], Y[51], Z[51]);
  UBFA_52 U30 (C[53], S[52], X[52], Y[52], Z[52]);
  UBFA_53 U31 (C[54], S[53], X[53], Y[53], Z[53]);
  UBFA_54 U32 (C[55], S[54], X[54], Y[54], Z[54]);
  UBFA_55 U33 (C[56], S[55], X[55], Y[55], Z[55]);
endmodule

module PureCSA_58_26 (C, S, X, Y, Z);
  output [59:27] C;
  output [58:26] S;
  input [58:26] X;
  input [58:26] Y;
  input [58:26] Z;
  UBFA_26 U0 (C[27], S[26], X[26], Y[26], Z[26]);
  UBFA_27 U1 (C[28], S[27], X[27], Y[27], Z[27]);
  UBFA_28 U2 (C[29], S[28], X[28], Y[28], Z[28]);
  UBFA_29 U3 (C[30], S[29], X[29], Y[29], Z[29]);
  UBFA_30 U4 (C[31], S[30], X[30], Y[30], Z[30]);
  UBFA_31 U5 (C[32], S[31], X[31], Y[31], Z[31]);
  UBFA_32 U6 (C[33], S[32], X[32], Y[32], Z[32]);
  UBFA_33 U7 (C[34], S[33], X[33], Y[33], Z[33]);
  UBFA_34 U8 (C[35], S[34], X[34], Y[34], Z[34]);
  UBFA_35 U9 (C[36], S[35], X[35], Y[35], Z[35]);
  UBFA_36 U10 (C[37], S[36], X[36], Y[36], Z[36]);
  UBFA_37 U11 (C[38], S[37], X[37], Y[37], Z[37]);
  UBFA_38 U12 (C[39], S[38], X[38], Y[38], Z[38]);
  UBFA_39 U13 (C[40], S[39], X[39], Y[39], Z[39]);
  UBFA_40 U14 (C[41], S[40], X[40], Y[40], Z[40]);
  UBFA_41 U15 (C[42], S[41], X[41], Y[41], Z[41]);
  UBFA_42 U16 (C[43], S[42], X[42], Y[42], Z[42]);
  UBFA_43 U17 (C[44], S[43], X[43], Y[43], Z[43]);
  UBFA_44 U18 (C[45], S[44], X[44], Y[44], Z[44]);
  UBFA_45 U19 (C[46], S[45], X[45], Y[45], Z[45]);
  UBFA_46 U20 (C[47], S[46], X[46], Y[46], Z[46]);
  UBFA_47 U21 (C[48], S[47], X[47], Y[47], Z[47]);
  UBFA_48 U22 (C[49], S[48], X[48], Y[48], Z[48]);
  UBFA_49 U23 (C[50], S[49], X[49], Y[49], Z[49]);
  UBFA_50 U24 (C[51], S[50], X[50], Y[50], Z[50]);
  UBFA_51 U25 (C[52], S[51], X[51], Y[51], Z[51]);
  UBFA_52 U26 (C[53], S[52], X[52], Y[52], Z[52]);
  UBFA_53 U27 (C[54], S[53], X[53], Y[53], Z[53]);
  UBFA_54 U28 (C[55], S[54], X[54], Y[54], Z[54]);
  UBFA_55 U29 (C[56], S[55], X[55], Y[55], Z[55]);
  UBFA_56 U30 (C[57], S[56], X[56], Y[56], Z[56]);
  UBFA_57 U31 (C[58], S[57], X[57], Y[57], Z[57]);
  UBFA_58 U32 (C[59], S[58], X[58], Y[58], Z[58]);
endmodule

module PureCSA_58_30 (C, S, X, Y, Z);
  output [59:31] C;
  output [58:30] S;
  input [58:30] X;
  input [58:30] Y;
  input [58:30] Z;
  UBFA_30 U0 (C[31], S[30], X[30], Y[30], Z[30]);
  UBFA_31 U1 (C[32], S[31], X[31], Y[31], Z[31]);
  UBFA_32 U2 (C[33], S[32], X[32], Y[32], Z[32]);
  UBFA_33 U3 (C[34], S[33], X[33], Y[33], Z[33]);
  UBFA_34 U4 (C[35], S[34], X[34], Y[34], Z[34]);
  UBFA_35 U5 (C[36], S[35], X[35], Y[35], Z[35]);
  UBFA_36 U6 (C[37], S[36], X[36], Y[36], Z[36]);
  UBFA_37 U7 (C[38], S[37], X[37], Y[37], Z[37]);
  UBFA_38 U8 (C[39], S[38], X[38], Y[38], Z[38]);
  UBFA_39 U9 (C[40], S[39], X[39], Y[39], Z[39]);
  UBFA_40 U10 (C[41], S[40], X[40], Y[40], Z[40]);
  UBFA_41 U11 (C[42], S[41], X[41], Y[41], Z[41]);
  UBFA_42 U12 (C[43], S[42], X[42], Y[42], Z[42]);
  UBFA_43 U13 (C[44], S[43], X[43], Y[43], Z[43]);
  UBFA_44 U14 (C[45], S[44], X[44], Y[44], Z[44]);
  UBFA_45 U15 (C[46], S[45], X[45], Y[45], Z[45]);
  UBFA_46 U16 (C[47], S[46], X[46], Y[46], Z[46]);
  UBFA_47 U17 (C[48], S[47], X[47], Y[47], Z[47]);
  UBFA_48 U18 (C[49], S[48], X[48], Y[48], Z[48]);
  UBFA_49 U19 (C[50], S[49], X[49], Y[49], Z[49]);
  UBFA_50 U20 (C[51], S[50], X[50], Y[50], Z[50]);
  UBFA_51 U21 (C[52], S[51], X[51], Y[51], Z[51]);
  UBFA_52 U22 (C[53], S[52], X[52], Y[52], Z[52]);
  UBFA_53 U23 (C[54], S[53], X[53], Y[53], Z[53]);
  UBFA_54 U24 (C[55], S[54], X[54], Y[54], Z[54]);
  UBFA_55 U25 (C[56], S[55], X[55], Y[55], Z[55]);
  UBFA_56 U26 (C[57], S[56], X[56], Y[56], Z[56]);
  UBFA_57 U27 (C[58], S[57], X[57], Y[57], Z[57]);
  UBFA_58 U28 (C[59], S[58], X[58], Y[58], Z[58]);
endmodule

module PureCSA_61_30 (C, S, X, Y, Z);
  output [62:31] C;
  output [61:30] S;
  input [61:30] X;
  input [61:30] Y;
  input [61:30] Z;
  UBFA_30 U0 (C[31], S[30], X[30], Y[30], Z[30]);
  UBFA_31 U1 (C[32], S[31], X[31], Y[31], Z[31]);
  UBFA_32 U2 (C[33], S[32], X[32], Y[32], Z[32]);
  UBFA_33 U3 (C[34], S[33], X[33], Y[33], Z[33]);
  UBFA_34 U4 (C[35], S[34], X[34], Y[34], Z[34]);
  UBFA_35 U5 (C[36], S[35], X[35], Y[35], Z[35]);
  UBFA_36 U6 (C[37], S[36], X[36], Y[36], Z[36]);
  UBFA_37 U7 (C[38], S[37], X[37], Y[37], Z[37]);
  UBFA_38 U8 (C[39], S[38], X[38], Y[38], Z[38]);
  UBFA_39 U9 (C[40], S[39], X[39], Y[39], Z[39]);
  UBFA_40 U10 (C[41], S[40], X[40], Y[40], Z[40]);
  UBFA_41 U11 (C[42], S[41], X[41], Y[41], Z[41]);
  UBFA_42 U12 (C[43], S[42], X[42], Y[42], Z[42]);
  UBFA_43 U13 (C[44], S[43], X[43], Y[43], Z[43]);
  UBFA_44 U14 (C[45], S[44], X[44], Y[44], Z[44]);
  UBFA_45 U15 (C[46], S[45], X[45], Y[45], Z[45]);
  UBFA_46 U16 (C[47], S[46], X[46], Y[46], Z[46]);
  UBFA_47 U17 (C[48], S[47], X[47], Y[47], Z[47]);
  UBFA_48 U18 (C[49], S[48], X[48], Y[48], Z[48]);
  UBFA_49 U19 (C[50], S[49], X[49], Y[49], Z[49]);
  UBFA_50 U20 (C[51], S[50], X[50], Y[50], Z[50]);
  UBFA_51 U21 (C[52], S[51], X[51], Y[51], Z[51]);
  UBFA_52 U22 (C[53], S[52], X[52], Y[52], Z[52]);
  UBFA_53 U23 (C[54], S[53], X[53], Y[53], Z[53]);
  UBFA_54 U24 (C[55], S[54], X[54], Y[54], Z[54]);
  UBFA_55 U25 (C[56], S[55], X[55], Y[55], Z[55]);
  UBFA_56 U26 (C[57], S[56], X[56], Y[56], Z[56]);
  UBFA_57 U27 (C[58], S[57], X[57], Y[57], Z[57]);
  UBFA_58 U28 (C[59], S[58], X[58], Y[58], Z[58]);
  UBFA_59 U29 (C[60], S[59], X[59], Y[59], Z[59]);
  UBFA_60 U30 (C[61], S[60], X[60], Y[60], Z[60]);
  UBFA_61 U31 (C[62], S[61], X[61], Y[61], Z[61]);
endmodule

module PureCSA_63_26 (C, S, X, Y, Z);
  output [64:27] C;
  output [63:26] S;
  input [63:26] X;
  input [63:26] Y;
  input [63:26] Z;
  UBFA_26 U0 (C[27], S[26], X[26], Y[26], Z[26]);
  UBFA_27 U1 (C[28], S[27], X[27], Y[27], Z[27]);
  UBFA_28 U2 (C[29], S[28], X[28], Y[28], Z[28]);
  UBFA_29 U3 (C[30], S[29], X[29], Y[29], Z[29]);
  UBFA_30 U4 (C[31], S[30], X[30], Y[30], Z[30]);
  UBFA_31 U5 (C[32], S[31], X[31], Y[31], Z[31]);
  UBFA_32 U6 (C[33], S[32], X[32], Y[32], Z[32]);
  UBFA_33 U7 (C[34], S[33], X[33], Y[33], Z[33]);
  UBFA_34 U8 (C[35], S[34], X[34], Y[34], Z[34]);
  UBFA_35 U9 (C[36], S[35], X[35], Y[35], Z[35]);
  UBFA_36 U10 (C[37], S[36], X[36], Y[36], Z[36]);
  UBFA_37 U11 (C[38], S[37], X[37], Y[37], Z[37]);
  UBFA_38 U12 (C[39], S[38], X[38], Y[38], Z[38]);
  UBFA_39 U13 (C[40], S[39], X[39], Y[39], Z[39]);
  UBFA_40 U14 (C[41], S[40], X[40], Y[40], Z[40]);
  UBFA_41 U15 (C[42], S[41], X[41], Y[41], Z[41]);
  UBFA_42 U16 (C[43], S[42], X[42], Y[42], Z[42]);
  UBFA_43 U17 (C[44], S[43], X[43], Y[43], Z[43]);
  UBFA_44 U18 (C[45], S[44], X[44], Y[44], Z[44]);
  UBFA_45 U19 (C[46], S[45], X[45], Y[45], Z[45]);
  UBFA_46 U20 (C[47], S[46], X[46], Y[46], Z[46]);
  UBFA_47 U21 (C[48], S[47], X[47], Y[47], Z[47]);
  UBFA_48 U22 (C[49], S[48], X[48], Y[48], Z[48]);
  UBFA_49 U23 (C[50], S[49], X[49], Y[49], Z[49]);
  UBFA_50 U24 (C[51], S[50], X[50], Y[50], Z[50]);
  UBFA_51 U25 (C[52], S[51], X[51], Y[51], Z[51]);
  UBFA_52 U26 (C[53], S[52], X[52], Y[52], Z[52]);
  UBFA_53 U27 (C[54], S[53], X[53], Y[53], Z[53]);
  UBFA_54 U28 (C[55], S[54], X[54], Y[54], Z[54]);
  UBFA_55 U29 (C[56], S[55], X[55], Y[55], Z[55]);
  UBFA_56 U30 (C[57], S[56], X[56], Y[56], Z[56]);
  UBFA_57 U31 (C[58], S[57], X[57], Y[57], Z[57]);
  UBFA_58 U32 (C[59], S[58], X[58], Y[58], Z[58]);
  UBFA_59 U33 (C[60], S[59], X[59], Y[59], Z[59]);
  UBFA_60 U34 (C[61], S[60], X[60], Y[60], Z[60]);
  UBFA_61 U35 (C[62], S[61], X[61], Y[61], Z[61]);
  UBFA_62 U36 (C[63], S[62], X[62], Y[62], Z[62]);
  UBFA_63 U37 (C[64], S[63], X[63], Y[63], Z[63]);
endmodule

module PureCSHA_10_3 (C, S, X, Y);
  output [11:4] C;
  output [10:3] S;
  input [10:3] X;
  input [10:3] Y;
  UBHA_3 U0 (C[4], S[3], X[3], Y[3]);
  UBHA_4 U1 (C[5], S[4], X[4], Y[4]);
  UBHA_5 U2 (C[6], S[5], X[5], Y[5]);
  UBHA_6 U3 (C[7], S[6], X[6], Y[6]);
  UBHA_7 U4 (C[8], S[7], X[7], Y[7]);
  UBHA_8 U5 (C[9], S[8], X[8], Y[8]);
  UBHA_9 U6 (C[10], S[9], X[9], Y[9]);
  UBHA_10 U7 (C[11], S[10], X[10], Y[10]);
endmodule

module PureCSHA_12_10 (C, S, X, Y);
  output [13:11] C;
  output [12:10] S;
  input [12:10] X;
  input [12:10] Y;
  UBHA_10 U0 (C[11], S[10], X[10], Y[10]);
  UBHA_11 U1 (C[12], S[11], X[11], Y[11]);
  UBHA_12 U2 (C[13], S[12], X[12], Y[12]);
endmodule

module PureCSHA_13_12 (C, S, X, Y);
  output [14:13] C;
  output [13:12] S;
  input [13:12] X;
  input [13:12] Y;
  UBHA_12 U0 (C[13], S[12], X[12], Y[12]);
  UBHA_13 U1 (C[14], S[13], X[13], Y[13]);
endmodule

module PureCSHA_16_4 (C, S, X, Y);
  output [17:5] C;
  output [16:4] S;
  input [16:4] X;
  input [16:4] Y;
  UBHA_4 U0 (C[5], S[4], X[4], Y[4]);
  UBHA_5 U1 (C[6], S[5], X[5], Y[5]);
  UBHA_6 U2 (C[7], S[6], X[6], Y[6]);
  UBHA_7 U3 (C[8], S[7], X[7], Y[7]);
  UBHA_8 U4 (C[9], S[8], X[8], Y[8]);
  UBHA_9 U5 (C[10], S[9], X[9], Y[9]);
  UBHA_10 U6 (C[11], S[10], X[10], Y[10]);
  UBHA_11 U7 (C[12], S[11], X[11], Y[11]);
  UBHA_12 U8 (C[13], S[12], X[12], Y[12]);
  UBHA_13 U9 (C[14], S[13], X[13], Y[13]);
  UBHA_14 U10 (C[15], S[14], X[14], Y[14]);
  UBHA_15 U11 (C[16], S[15], X[15], Y[15]);
  UBHA_16 U12 (C[17], S[16], X[16], Y[16]);
endmodule

module PureCSHA_19_16 (C, S, X, Y);
  output [20:17] C;
  output [19:16] S;
  input [19:16] X;
  input [19:16] Y;
  UBHA_16 U0 (C[17], S[16], X[16], Y[16]);
  UBHA_17 U1 (C[18], S[17], X[17], Y[17]);
  UBHA_18 U2 (C[19], S[18], X[18], Y[18]);
  UBHA_19 U3 (C[20], S[19], X[19], Y[19]);
endmodule

module PureCSHA_19_18 (C, S, X, Y);
  output [20:19] C;
  output [19:18] S;
  input [19:18] X;
  input [19:18] Y;
  UBHA_18 U0 (C[19], S[18], X[18], Y[18]);
  UBHA_19 U1 (C[20], S[19], X[19], Y[19]);
endmodule

module PureCSHA_1_0 (C, S, X, Y);
  output [2:1] C;
  output [1:0] S;
  input [1:0] X;
  input [1:0] Y;
  UBHA_0 U0 (C[1], S[0], X[0], Y[0]);
  UBHA_1 U1 (C[2], S[1], X[1], Y[1]);
endmodule

module PureCSHA_21_19 (C, S, X, Y);
  output [22:20] C;
  output [21:19] S;
  input [21:19] X;
  input [21:19] Y;
  UBHA_19 U0 (C[20], S[19], X[19], Y[19]);
  UBHA_20 U1 (C[21], S[20], X[20], Y[20]);
  UBHA_21 U2 (C[22], S[21], X[21], Y[21]);
endmodule

module PureCSHA_25_24 (C, S, X, Y);
  output [26:25] C;
  output [25:24] S;
  input [25:24] X;
  input [25:24] Y;
  UBHA_24 U0 (C[25], S[24], X[24], Y[24]);
  UBHA_25 U1 (C[26], S[25], X[25], Y[25]);
endmodule

module PureCSHA_25_5 (C, S, X, Y);
  output [26:6] C;
  output [25:5] S;
  input [25:5] X;
  input [25:5] Y;
  UBHA_5 U0 (C[6], S[5], X[5], Y[5]);
  UBHA_6 U1 (C[7], S[6], X[6], Y[6]);
  UBHA_7 U2 (C[8], S[7], X[7], Y[7]);
  UBHA_8 U3 (C[9], S[8], X[8], Y[8]);
  UBHA_9 U4 (C[10], S[9], X[9], Y[9]);
  UBHA_10 U5 (C[11], S[10], X[10], Y[10]);
  UBHA_11 U6 (C[12], S[11], X[11], Y[11]);
  UBHA_12 U7 (C[13], S[12], X[12], Y[12]);
  UBHA_13 U8 (C[14], S[13], X[13], Y[13]);
  UBHA_14 U9 (C[15], S[14], X[14], Y[14]);
  UBHA_15 U10 (C[16], S[15], X[15], Y[15]);
  UBHA_16 U11 (C[17], S[16], X[16], Y[16]);
  UBHA_17 U12 (C[18], S[17], X[17], Y[17]);
  UBHA_18 U13 (C[19], S[18], X[18], Y[18]);
  UBHA_19 U14 (C[20], S[19], X[19], Y[19]);
  UBHA_20 U15 (C[21], S[20], X[20], Y[20]);
  UBHA_21 U16 (C[22], S[21], X[21], Y[21]);
  UBHA_22 U17 (C[23], S[22], X[22], Y[22]);
  UBHA_23 U18 (C[24], S[23], X[23], Y[23]);
  UBHA_24 U19 (C[25], S[24], X[24], Y[24]);
  UBHA_25 U20 (C[26], S[25], X[25], Y[25]);
endmodule

module PureCSHA_29_25 (C, S, X, Y);
  output [30:26] C;
  output [29:25] S;
  input [29:25] X;
  input [29:25] Y;
  UBHA_25 U0 (C[26], S[25], X[25], Y[25]);
  UBHA_26 U1 (C[27], S[26], X[26], Y[26]);
  UBHA_27 U2 (C[28], S[27], X[27], Y[27]);
  UBHA_28 U3 (C[29], S[28], X[28], Y[28]);
  UBHA_29 U4 (C[30], S[29], X[29], Y[29]);
endmodule

module PureCSHA_29_28 (C, S, X, Y);
  output [30:29] C;
  output [29:28] S;
  input [29:28] X;
  input [29:28] Y;
  UBHA_28 U0 (C[29], S[28], X[28], Y[28]);
  UBHA_29 U1 (C[30], S[29], X[29], Y[29]);
endmodule

module PureCSHA_31_29 (C, S, X, Y);
  output [32:30] C;
  output [31:29] S;
  input [31:29] X;
  input [31:29] Y;
  UBHA_29 U0 (C[30], S[29], X[29], Y[29]);
  UBHA_30 U1 (C[31], S[30], X[30], Y[30]);
  UBHA_31 U2 (C[32], S[31], X[31], Y[31]);
endmodule

module PureCSHA_36_35 (C, S, X, Y);
  output [37:36] C;
  output [36:35] S;
  input [36:35] X;
  input [36:35] Y;
  UBHA_35 U0 (C[36], S[35], X[35], Y[35]);
  UBHA_36 U1 (C[37], S[36], X[36], Y[36]);
endmodule

module PureCSHA_3_1 (C, S, X, Y);
  output [4:2] C;
  output [3:1] S;
  input [3:1] X;
  input [3:1] Y;
  UBHA_1 U0 (C[2], S[1], X[1], Y[1]);
  UBHA_2 U1 (C[3], S[2], X[2], Y[2]);
  UBHA_3 U2 (C[4], S[3], X[3], Y[3]);
endmodule

module PureCSHA_42_41 (C, S, X, Y);
  output [43:42] C;
  output [42:41] S;
  input [42:41] X;
  input [42:41] Y;
  UBHA_41 U0 (C[42], S[41], X[41], Y[41]);
  UBHA_42 U1 (C[43], S[42], X[42], Y[42]);
endmodule

module PureCSHA_44_40 (C, S, X, Y);
  output [45:41] C;
  output [44:40] S;
  input [44:40] X;
  input [44:40] Y;
  UBHA_40 U0 (C[41], S[40], X[40], Y[40]);
  UBHA_41 U1 (C[42], S[41], X[41], Y[41]);
  UBHA_42 U2 (C[43], S[42], X[42], Y[42]);
  UBHA_43 U3 (C[44], S[43], X[43], Y[43]);
  UBHA_44 U4 (C[45], S[44], X[44], Y[44]);
endmodule

module PureCSHA_48_47 (C, S, X, Y);
  output [49:48] C;
  output [48:47] S;
  input [48:47] X;
  input [48:47] Y;
  UBHA_47 U0 (C[48], S[47], X[47], Y[47]);
  UBHA_48 U1 (C[49], S[48], X[48], Y[48]);
endmodule

module PureCSHA_49_44 (C, S, X, Y);
  output [50:45] C;
  output [49:44] S;
  input [49:44] X;
  input [49:44] Y;
  UBHA_44 U0 (C[45], S[44], X[44], Y[44]);
  UBHA_45 U1 (C[46], S[45], X[45], Y[45]);
  UBHA_46 U2 (C[47], S[46], X[46], Y[46]);
  UBHA_47 U3 (C[48], S[47], X[47], Y[47]);
  UBHA_48 U4 (C[49], S[48], X[48], Y[48]);
  UBHA_49 U5 (C[50], S[49], X[49], Y[49]);
endmodule

module PureCSHA_50_46 (C, S, X, Y);
  output [51:47] C;
  output [50:46] S;
  input [50:46] X;
  input [50:46] Y;
  UBHA_46 U0 (C[47], S[46], X[46], Y[46]);
  UBHA_47 U1 (C[48], S[47], X[47], Y[47]);
  UBHA_48 U2 (C[49], S[48], X[48], Y[48]);
  UBHA_49 U3 (C[50], S[49], X[49], Y[49]);
  UBHA_50 U4 (C[51], S[50], X[50], Y[50]);
endmodule

module PureCSHA_54_53 (C, S, X, Y);
  output [55:54] C;
  output [54:53] S;
  input [54:53] X;
  input [54:53] Y;
  UBHA_53 U0 (C[54], S[53], X[53], Y[53]);
  UBHA_54 U1 (C[55], S[54], X[54], Y[54]);
endmodule

module PureCSHA_57_51 (C, S, X, Y);
  output [58:52] C;
  output [57:51] S;
  input [57:51] X;
  input [57:51] Y;
  UBHA_51 U0 (C[52], S[51], X[51], Y[51]);
  UBHA_52 U1 (C[53], S[52], X[52], Y[52]);
  UBHA_53 U2 (C[54], S[53], X[53], Y[53]);
  UBHA_54 U3 (C[55], S[54], X[54], Y[54]);
  UBHA_55 U4 (C[56], S[55], X[55], Y[55]);
  UBHA_56 U5 (C[57], S[56], X[56], Y[56]);
  UBHA_57 U6 (C[58], S[57], X[57], Y[57]);
endmodule

module PureCSHA_60_59 (C, S, X, Y);
  output [61:60] C;
  output [60:59] S;
  input [60:59] X;
  input [60:59] Y;
  UBHA_59 U0 (C[60], S[59], X[59], Y[59]);
  UBHA_60 U1 (C[61], S[60], X[60], Y[60]);
endmodule

module PureCSHA_62_52 (C, S, X, Y);
  output [63:53] C;
  output [62:52] S;
  input [62:52] X;
  input [62:52] Y;
  UBHA_52 U0 (C[53], S[52], X[52], Y[52]);
  UBHA_53 U1 (C[54], S[53], X[53], Y[53]);
  UBHA_54 U2 (C[55], S[54], X[54], Y[54]);
  UBHA_55 U3 (C[56], S[55], X[55], Y[55]);
  UBHA_56 U4 (C[57], S[56], X[56], Y[56]);
  UBHA_57 U5 (C[58], S[57], X[57], Y[57]);
  UBHA_58 U6 (C[59], S[58], X[58], Y[58]);
  UBHA_59 U7 (C[60], S[59], X[59], Y[59]);
  UBHA_60 U8 (C[61], S[60], X[60], Y[60]);
  UBHA_61 U9 (C[62], S[61], X[61], Y[61]);
  UBHA_62 U10 (C[63], S[62], X[62], Y[62]);
endmodule

module PureCSHA_64_62 (C, S, X, Y);
  output [65:63] C;
  output [64:62] S;
  input [64:62] X;
  input [64:62] Y;
  UBHA_62 U0 (C[63], S[62], X[62], Y[62]);
  UBHA_63 U1 (C[64], S[63], X[63], Y[63]);
  UBHA_64 U2 (C[65], S[64], X[64], Y[64]);
endmodule

module PureCSHA_65_34 (C, S, X, Y);
  output [66:35] C;
  output [65:34] S;
  input [65:34] X;
  input [65:34] Y;
  UBHA_34 U0 (C[35], S[34], X[34], Y[34]);
  UBHA_35 U1 (C[36], S[35], X[35], Y[35]);
  UBHA_36 U2 (C[37], S[36], X[36], Y[36]);
  UBHA_37 U3 (C[38], S[37], X[37], Y[37]);
  UBHA_38 U4 (C[39], S[38], X[38], Y[38]);
  UBHA_39 U5 (C[40], S[39], X[39], Y[39]);
  UBHA_40 U6 (C[41], S[40], X[40], Y[40]);
  UBHA_41 U7 (C[42], S[41], X[41], Y[41]);
  UBHA_42 U8 (C[43], S[42], X[42], Y[42]);
  UBHA_43 U9 (C[44], S[43], X[43], Y[43]);
  UBHA_44 U10 (C[45], S[44], X[44], Y[44]);
  UBHA_45 U11 (C[46], S[45], X[45], Y[45]);
  UBHA_46 U12 (C[47], S[46], X[46], Y[46]);
  UBHA_47 U13 (C[48], S[47], X[47], Y[47]);
  UBHA_48 U14 (C[49], S[48], X[48], Y[48]);
  UBHA_49 U15 (C[50], S[49], X[49], Y[49]);
  UBHA_50 U16 (C[51], S[50], X[50], Y[50]);
  UBHA_51 U17 (C[52], S[51], X[51], Y[51]);
  UBHA_52 U18 (C[53], S[52], X[52], Y[52]);
  UBHA_53 U19 (C[54], S[53], X[53], Y[53]);
  UBHA_54 U20 (C[55], S[54], X[54], Y[54]);
  UBHA_55 U21 (C[56], S[55], X[55], Y[55]);
  UBHA_56 U22 (C[57], S[56], X[56], Y[56]);
  UBHA_57 U23 (C[58], S[57], X[57], Y[57]);
  UBHA_58 U24 (C[59], S[58], X[58], Y[58]);
  UBHA_59 U25 (C[60], S[59], X[59], Y[59]);
  UBHA_60 U26 (C[61], S[60], X[60], Y[60]);
  UBHA_61 U27 (C[62], S[61], X[61], Y[61]);
  UBHA_62 U28 (C[63], S[62], X[62], Y[62]);
  UBHA_63 U29 (C[64], S[63], X[63], Y[63]);
  UBHA_64 U30 (C[65], S[64], X[64], Y[64]);
  UBHA_65 U31 (C[66], S[65], X[65], Y[65]);
endmodule

module PureCSHA_65_59 (C, S, X, Y);
  output [66:60] C;
  output [65:59] S;
  input [65:59] X;
  input [65:59] Y;
  UBHA_59 U0 (C[60], S[59], X[59], Y[59]);
  UBHA_60 U1 (C[61], S[60], X[60], Y[60]);
  UBHA_61 U2 (C[62], S[61], X[61], Y[61]);
  UBHA_62 U3 (C[63], S[62], X[62], Y[62]);
  UBHA_63 U4 (C[64], S[63], X[63], Y[63]);
  UBHA_64 U5 (C[65], S[64], X[64], Y[64]);
  UBHA_65 U6 (C[66], S[65], X[65], Y[65]);
endmodule

module PureCSHA_66_64 (C, S, X, Y);
  output [67:65] C;
  output [66:64] S;
  input [66:64] X;
  input [66:64] Y;
  UBHA_64 U0 (C[65], S[64], X[64], Y[64]);
  UBHA_65 U1 (C[66], S[65], X[65], Y[65]);
  UBHA_66 U2 (C[67], S[66], X[66], Y[66]);
endmodule

module PureCSHA_6_2 (C, S, X, Y);
  output [7:3] C;
  output [6:2] S;
  input [6:2] X;
  input [6:2] Y;
  UBHA_2 U0 (C[3], S[2], X[2], Y[2]);
  UBHA_3 U1 (C[4], S[3], X[3], Y[3]);
  UBHA_4 U2 (C[5], S[4], X[4], Y[4]);
  UBHA_5 U3 (C[6], S[5], X[5], Y[5]);
  UBHA_6 U4 (C[7], S[6], X[6], Y[6]);
endmodule

module PureCSHA_7_6 (C, S, X, Y);
  output [8:7] C;
  output [7:6] S;
  input [7:6] X;
  input [7:6] Y;
  UBHA_6 U0 (C[7], S[6], X[6], Y[6]);
  UBHA_7 U1 (C[8], S[7], X[7], Y[7]);
endmodule

module TCU4VPPG_32_0_0 (O_T, O_R, IN1_T, IN1_R, U__d1, U__d0);
  output [32:0] O_R;
  output O_T;
  input [31:0] IN1_R;
  input IN1_T;
  input U__d1, U__d0;
  wire [32:1] P;
  U4DPPGL_0_0 U0 (P[1], O_R[0], IN1_R[0], U__d1, U__d0);
  U4DPPG_1_0 U1 (P[2], O_R[1], IN1_R[1], U__d1, U__d0, P[1]);
  U4DPPG_2_0 U2 (P[3], O_R[2], IN1_R[2], U__d1, U__d0, P[2]);
  U4DPPG_3_0 U3 (P[4], O_R[3], IN1_R[3], U__d1, U__d0, P[3]);
  U4DPPG_4_0 U4 (P[5], O_R[4], IN1_R[4], U__d1, U__d0, P[4]);
  U4DPPG_5_0 U5 (P[6], O_R[5], IN1_R[5], U__d1, U__d0, P[5]);
  U4DPPG_6_0 U6 (P[7], O_R[6], IN1_R[6], U__d1, U__d0, P[6]);
  U4DPPG_7_0 U7 (P[8], O_R[7], IN1_R[7], U__d1, U__d0, P[7]);
  U4DPPG_8_0 U8 (P[9], O_R[8], IN1_R[8], U__d1, U__d0, P[8]);
  U4DPPG_9_0 U9 (P[10], O_R[9], IN1_R[9], U__d1, U__d0, P[9]);
  U4DPPG_10_0 U10 (P[11], O_R[10], IN1_R[10], U__d1, U__d0, P[10]);
  U4DPPG_11_0 U11 (P[12], O_R[11], IN1_R[11], U__d1, U__d0, P[11]);
  U4DPPG_12_0 U12 (P[13], O_R[12], IN1_R[12], U__d1, U__d0, P[12]);
  U4DPPG_13_0 U13 (P[14], O_R[13], IN1_R[13], U__d1, U__d0, P[13]);
  U4DPPG_14_0 U14 (P[15], O_R[14], IN1_R[14], U__d1, U__d0, P[14]);
  U4DPPG_15_0 U15 (P[16], O_R[15], IN1_R[15], U__d1, U__d0, P[15]);
  U4DPPG_16_0 U16 (P[17], O_R[16], IN1_R[16], U__d1, U__d0, P[16]);
  U4DPPG_17_0 U17 (P[18], O_R[17], IN1_R[17], U__d1, U__d0, P[17]);
  U4DPPG_18_0 U18 (P[19], O_R[18], IN1_R[18], U__d1, U__d0, P[18]);
  U4DPPG_19_0 U19 (P[20], O_R[19], IN1_R[19], U__d1, U__d0, P[19]);
  U4DPPG_20_0 U20 (P[21], O_R[20], IN1_R[20], U__d1, U__d0, P[20]);
  U4DPPG_21_0 U21 (P[22], O_R[21], IN1_R[21], U__d1, U__d0, P[21]);
  U4DPPG_22_0 U22 (P[23], O_R[22], IN1_R[22], U__d1, U__d0, P[22]);
  U4DPPG_23_0 U23 (P[24], O_R[23], IN1_R[23], U__d1, U__d0, P[23]);
  U4DPPG_24_0 U24 (P[25], O_R[24], IN1_R[24], U__d1, U__d0, P[24]);
  U4DPPG_25_0 U25 (P[26], O_R[25], IN1_R[25], U__d1, U__d0, P[25]);
  U4DPPG_26_0 U26 (P[27], O_R[26], IN1_R[26], U__d1, U__d0, P[26]);
  U4DPPG_27_0 U27 (P[28], O_R[27], IN1_R[27], U__d1, U__d0, P[27]);
  U4DPPG_28_0 U28 (P[29], O_R[28], IN1_R[28], U__d1, U__d0, P[28]);
  U4DPPG_29_0 U29 (P[30], O_R[29], IN1_R[29], U__d1, U__d0, P[29]);
  U4DPPG_30_0 U30 (P[31], O_R[30], IN1_R[30], U__d1, U__d0, P[30]);
  U4DPPG_31_0 U31 (P[32], O_R[31], IN1_R[31], U__d1, U__d0, P[31]);
  U4DPPGH_32_0 U32 (O_T, O_R[32], IN1_T, U__d1, U__d0, P[32]);
endmodule

module TCU4VPPG_32_0_1 (O_T, O_R, IN1_T, IN1_R, U__d1, U__d0);
  output [34:2] O_R;
  output O_T;
  input [31:0] IN1_R;
  input IN1_T;
  input U__d1, U__d0;
  wire [34:3] P;
  U4DPPGL_0_1 U0 (P[3], O_R[2], IN1_R[0], U__d1, U__d0);
  U4DPPG_1_1 U1 (P[4], O_R[3], IN1_R[1], U__d1, U__d0, P[3]);
  U4DPPG_2_1 U2 (P[5], O_R[4], IN1_R[2], U__d1, U__d0, P[4]);
  U4DPPG_3_1 U3 (P[6], O_R[5], IN1_R[3], U__d1, U__d0, P[5]);
  U4DPPG_4_1 U4 (P[7], O_R[6], IN1_R[4], U__d1, U__d0, P[6]);
  U4DPPG_5_1 U5 (P[8], O_R[7], IN1_R[5], U__d1, U__d0, P[7]);
  U4DPPG_6_1 U6 (P[9], O_R[8], IN1_R[6], U__d1, U__d0, P[8]);
  U4DPPG_7_1 U7 (P[10], O_R[9], IN1_R[7], U__d1, U__d0, P[9]);
  U4DPPG_8_1 U8 (P[11], O_R[10], IN1_R[8], U__d1, U__d0, P[10]);
  U4DPPG_9_1 U9 (P[12], O_R[11], IN1_R[9], U__d1, U__d0, P[11]);
  U4DPPG_10_1 U10 (P[13], O_R[12], IN1_R[10], U__d1, U__d0, P[12]);
  U4DPPG_11_1 U11 (P[14], O_R[13], IN1_R[11], U__d1, U__d0, P[13]);
  U4DPPG_12_1 U12 (P[15], O_R[14], IN1_R[12], U__d1, U__d0, P[14]);
  U4DPPG_13_1 U13 (P[16], O_R[15], IN1_R[13], U__d1, U__d0, P[15]);
  U4DPPG_14_1 U14 (P[17], O_R[16], IN1_R[14], U__d1, U__d0, P[16]);
  U4DPPG_15_1 U15 (P[18], O_R[17], IN1_R[15], U__d1, U__d0, P[17]);
  U4DPPG_16_1 U16 (P[19], O_R[18], IN1_R[16], U__d1, U__d0, P[18]);
  U4DPPG_17_1 U17 (P[20], O_R[19], IN1_R[17], U__d1, U__d0, P[19]);
  U4DPPG_18_1 U18 (P[21], O_R[20], IN1_R[18], U__d1, U__d0, P[20]);
  U4DPPG_19_1 U19 (P[22], O_R[21], IN1_R[19], U__d1, U__d0, P[21]);
  U4DPPG_20_1 U20 (P[23], O_R[22], IN1_R[20], U__d1, U__d0, P[22]);
  U4DPPG_21_1 U21 (P[24], O_R[23], IN1_R[21], U__d1, U__d0, P[23]);
  U4DPPG_22_1 U22 (P[25], O_R[24], IN1_R[22], U__d1, U__d0, P[24]);
  U4DPPG_23_1 U23 (P[26], O_R[25], IN1_R[23], U__d1, U__d0, P[25]);
  U4DPPG_24_1 U24 (P[27], O_R[26], IN1_R[24], U__d1, U__d0, P[26]);
  U4DPPG_25_1 U25 (P[28], O_R[27], IN1_R[25], U__d1, U__d0, P[27]);
  U4DPPG_26_1 U26 (P[29], O_R[28], IN1_R[26], U__d1, U__d0, P[28]);
  U4DPPG_27_1 U27 (P[30], O_R[29], IN1_R[27], U__d1, U__d0, P[29]);
  U4DPPG_28_1 U28 (P[31], O_R[30], IN1_R[28], U__d1, U__d0, P[30]);
  U4DPPG_29_1 U29 (P[32], O_R[31], IN1_R[29], U__d1, U__d0, P[31]);
  U4DPPG_30_1 U30 (P[33], O_R[32], IN1_R[30], U__d1, U__d0, P[32]);
  U4DPPG_31_1 U31 (P[34], O_R[33], IN1_R[31], U__d1, U__d0, P[33]);
  U4DPPGH_32_1 U32 (O_T, O_R[34], IN1_T, U__d1, U__d0, P[34]);
endmodule

module TCU4VPPG_32_0_10 (O_T, O_R, IN1_T, IN1_R, U__d1, U__d0);
  output [52:20] O_R;
  output O_T;
  input [31:0] IN1_R;
  input IN1_T;
  input U__d1, U__d0;
  wire [52:21] P;
  U4DPPGL_0_10 U0 (P[21], O_R[20], IN1_R[0], U__d1, U__d0);
  U4DPPG_1_10 U1 (P[22], O_R[21], IN1_R[1], U__d1, U__d0, P[21]);
  U4DPPG_2_10 U2 (P[23], O_R[22], IN1_R[2], U__d1, U__d0, P[22]);
  U4DPPG_3_10 U3 (P[24], O_R[23], IN1_R[3], U__d1, U__d0, P[23]);
  U4DPPG_4_10 U4 (P[25], O_R[24], IN1_R[4], U__d1, U__d0, P[24]);
  U4DPPG_5_10 U5 (P[26], O_R[25], IN1_R[5], U__d1, U__d0, P[25]);
  U4DPPG_6_10 U6 (P[27], O_R[26], IN1_R[6], U__d1, U__d0, P[26]);
  U4DPPG_7_10 U7 (P[28], O_R[27], IN1_R[7], U__d1, U__d0, P[27]);
  U4DPPG_8_10 U8 (P[29], O_R[28], IN1_R[8], U__d1, U__d0, P[28]);
  U4DPPG_9_10 U9 (P[30], O_R[29], IN1_R[9], U__d1, U__d0, P[29]);
  U4DPPG_10_10 U10 (P[31], O_R[30], IN1_R[10], U__d1, U__d0, P[30]);
  U4DPPG_11_10 U11 (P[32], O_R[31], IN1_R[11], U__d1, U__d0, P[31]);
  U4DPPG_12_10 U12 (P[33], O_R[32], IN1_R[12], U__d1, U__d0, P[32]);
  U4DPPG_13_10 U13 (P[34], O_R[33], IN1_R[13], U__d1, U__d0, P[33]);
  U4DPPG_14_10 U14 (P[35], O_R[34], IN1_R[14], U__d1, U__d0, P[34]);
  U4DPPG_15_10 U15 (P[36], O_R[35], IN1_R[15], U__d1, U__d0, P[35]);
  U4DPPG_16_10 U16 (P[37], O_R[36], IN1_R[16], U__d1, U__d0, P[36]);
  U4DPPG_17_10 U17 (P[38], O_R[37], IN1_R[17], U__d1, U__d0, P[37]);
  U4DPPG_18_10 U18 (P[39], O_R[38], IN1_R[18], U__d1, U__d0, P[38]);
  U4DPPG_19_10 U19 (P[40], O_R[39], IN1_R[19], U__d1, U__d0, P[39]);
  U4DPPG_20_10 U20 (P[41], O_R[40], IN1_R[20], U__d1, U__d0, P[40]);
  U4DPPG_21_10 U21 (P[42], O_R[41], IN1_R[21], U__d1, U__d0, P[41]);
  U4DPPG_22_10 U22 (P[43], O_R[42], IN1_R[22], U__d1, U__d0, P[42]);
  U4DPPG_23_10 U23 (P[44], O_R[43], IN1_R[23], U__d1, U__d0, P[43]);
  U4DPPG_24_10 U24 (P[45], O_R[44], IN1_R[24], U__d1, U__d0, P[44]);
  U4DPPG_25_10 U25 (P[46], O_R[45], IN1_R[25], U__d1, U__d0, P[45]);
  U4DPPG_26_10 U26 (P[47], O_R[46], IN1_R[26], U__d1, U__d0, P[46]);
  U4DPPG_27_10 U27 (P[48], O_R[47], IN1_R[27], U__d1, U__d0, P[47]);
  U4DPPG_28_10 U28 (P[49], O_R[48], IN1_R[28], U__d1, U__d0, P[48]);
  U4DPPG_29_10 U29 (P[50], O_R[49], IN1_R[29], U__d1, U__d0, P[49]);
  U4DPPG_30_10 U30 (P[51], O_R[50], IN1_R[30], U__d1, U__d0, P[50]);
  U4DPPG_31_10 U31 (P[52], O_R[51], IN1_R[31], U__d1, U__d0, P[51]);
  U4DPPGH_32_10 U32 (O_T, O_R[52], IN1_T, U__d1, U__d0, P[52]);
endmodule

module TCU4VPPG_32_0_11 (O_T, O_R, IN1_T, IN1_R, U__d1, U__d0);
  output [54:22] O_R;
  output O_T;
  input [31:0] IN1_R;
  input IN1_T;
  input U__d1, U__d0;
  wire [54:23] P;
  U4DPPGL_0_11 U0 (P[23], O_R[22], IN1_R[0], U__d1, U__d0);
  U4DPPG_1_11 U1 (P[24], O_R[23], IN1_R[1], U__d1, U__d0, P[23]);
  U4DPPG_2_11 U2 (P[25], O_R[24], IN1_R[2], U__d1, U__d0, P[24]);
  U4DPPG_3_11 U3 (P[26], O_R[25], IN1_R[3], U__d1, U__d0, P[25]);
  U4DPPG_4_11 U4 (P[27], O_R[26], IN1_R[4], U__d1, U__d0, P[26]);
  U4DPPG_5_11 U5 (P[28], O_R[27], IN1_R[5], U__d1, U__d0, P[27]);
  U4DPPG_6_11 U6 (P[29], O_R[28], IN1_R[6], U__d1, U__d0, P[28]);
  U4DPPG_7_11 U7 (P[30], O_R[29], IN1_R[7], U__d1, U__d0, P[29]);
  U4DPPG_8_11 U8 (P[31], O_R[30], IN1_R[8], U__d1, U__d0, P[30]);
  U4DPPG_9_11 U9 (P[32], O_R[31], IN1_R[9], U__d1, U__d0, P[31]);
  U4DPPG_10_11 U10 (P[33], O_R[32], IN1_R[10], U__d1, U__d0, P[32]);
  U4DPPG_11_11 U11 (P[34], O_R[33], IN1_R[11], U__d1, U__d0, P[33]);
  U4DPPG_12_11 U12 (P[35], O_R[34], IN1_R[12], U__d1, U__d0, P[34]);
  U4DPPG_13_11 U13 (P[36], O_R[35], IN1_R[13], U__d1, U__d0, P[35]);
  U4DPPG_14_11 U14 (P[37], O_R[36], IN1_R[14], U__d1, U__d0, P[36]);
  U4DPPG_15_11 U15 (P[38], O_R[37], IN1_R[15], U__d1, U__d0, P[37]);
  U4DPPG_16_11 U16 (P[39], O_R[38], IN1_R[16], U__d1, U__d0, P[38]);
  U4DPPG_17_11 U17 (P[40], O_R[39], IN1_R[17], U__d1, U__d0, P[39]);
  U4DPPG_18_11 U18 (P[41], O_R[40], IN1_R[18], U__d1, U__d0, P[40]);
  U4DPPG_19_11 U19 (P[42], O_R[41], IN1_R[19], U__d1, U__d0, P[41]);
  U4DPPG_20_11 U20 (P[43], O_R[42], IN1_R[20], U__d1, U__d0, P[42]);
  U4DPPG_21_11 U21 (P[44], O_R[43], IN1_R[21], U__d1, U__d0, P[43]);
  U4DPPG_22_11 U22 (P[45], O_R[44], IN1_R[22], U__d1, U__d0, P[44]);
  U4DPPG_23_11 U23 (P[46], O_R[45], IN1_R[23], U__d1, U__d0, P[45]);
  U4DPPG_24_11 U24 (P[47], O_R[46], IN1_R[24], U__d1, U__d0, P[46]);
  U4DPPG_25_11 U25 (P[48], O_R[47], IN1_R[25], U__d1, U__d0, P[47]);
  U4DPPG_26_11 U26 (P[49], O_R[48], IN1_R[26], U__d1, U__d0, P[48]);
  U4DPPG_27_11 U27 (P[50], O_R[49], IN1_R[27], U__d1, U__d0, P[49]);
  U4DPPG_28_11 U28 (P[51], O_R[50], IN1_R[28], U__d1, U__d0, P[50]);
  U4DPPG_29_11 U29 (P[52], O_R[51], IN1_R[29], U__d1, U__d0, P[51]);
  U4DPPG_30_11 U30 (P[53], O_R[52], IN1_R[30], U__d1, U__d0, P[52]);
  U4DPPG_31_11 U31 (P[54], O_R[53], IN1_R[31], U__d1, U__d0, P[53]);
  U4DPPGH_32_11 U32 (O_T, O_R[54], IN1_T, U__d1, U__d0, P[54]);
endmodule

module TCU4VPPG_32_0_12 (O_T, O_R, IN1_T, IN1_R, U__d1, U__d0);
  output [56:24] O_R;
  output O_T;
  input [31:0] IN1_R;
  input IN1_T;
  input U__d1, U__d0;
  wire [56:25] P;
  U4DPPGL_0_12 U0 (P[25], O_R[24], IN1_R[0], U__d1, U__d0);
  U4DPPG_1_12 U1 (P[26], O_R[25], IN1_R[1], U__d1, U__d0, P[25]);
  U4DPPG_2_12 U2 (P[27], O_R[26], IN1_R[2], U__d1, U__d0, P[26]);
  U4DPPG_3_12 U3 (P[28], O_R[27], IN1_R[3], U__d1, U__d0, P[27]);
  U4DPPG_4_12 U4 (P[29], O_R[28], IN1_R[4], U__d1, U__d0, P[28]);
  U4DPPG_5_12 U5 (P[30], O_R[29], IN1_R[5], U__d1, U__d0, P[29]);
  U4DPPG_6_12 U6 (P[31], O_R[30], IN1_R[6], U__d1, U__d0, P[30]);
  U4DPPG_7_12 U7 (P[32], O_R[31], IN1_R[7], U__d1, U__d0, P[31]);
  U4DPPG_8_12 U8 (P[33], O_R[32], IN1_R[8], U__d1, U__d0, P[32]);
  U4DPPG_9_12 U9 (P[34], O_R[33], IN1_R[9], U__d1, U__d0, P[33]);
  U4DPPG_10_12 U10 (P[35], O_R[34], IN1_R[10], U__d1, U__d0, P[34]);
  U4DPPG_11_12 U11 (P[36], O_R[35], IN1_R[11], U__d1, U__d0, P[35]);
  U4DPPG_12_12 U12 (P[37], O_R[36], IN1_R[12], U__d1, U__d0, P[36]);
  U4DPPG_13_12 U13 (P[38], O_R[37], IN1_R[13], U__d1, U__d0, P[37]);
  U4DPPG_14_12 U14 (P[39], O_R[38], IN1_R[14], U__d1, U__d0, P[38]);
  U4DPPG_15_12 U15 (P[40], O_R[39], IN1_R[15], U__d1, U__d0, P[39]);
  U4DPPG_16_12 U16 (P[41], O_R[40], IN1_R[16], U__d1, U__d0, P[40]);
  U4DPPG_17_12 U17 (P[42], O_R[41], IN1_R[17], U__d1, U__d0, P[41]);
  U4DPPG_18_12 U18 (P[43], O_R[42], IN1_R[18], U__d1, U__d0, P[42]);
  U4DPPG_19_12 U19 (P[44], O_R[43], IN1_R[19], U__d1, U__d0, P[43]);
  U4DPPG_20_12 U20 (P[45], O_R[44], IN1_R[20], U__d1, U__d0, P[44]);
  U4DPPG_21_12 U21 (P[46], O_R[45], IN1_R[21], U__d1, U__d0, P[45]);
  U4DPPG_22_12 U22 (P[47], O_R[46], IN1_R[22], U__d1, U__d0, P[46]);
  U4DPPG_23_12 U23 (P[48], O_R[47], IN1_R[23], U__d1, U__d0, P[47]);
  U4DPPG_24_12 U24 (P[49], O_R[48], IN1_R[24], U__d1, U__d0, P[48]);
  U4DPPG_25_12 U25 (P[50], O_R[49], IN1_R[25], U__d1, U__d0, P[49]);
  U4DPPG_26_12 U26 (P[51], O_R[50], IN1_R[26], U__d1, U__d0, P[50]);
  U4DPPG_27_12 U27 (P[52], O_R[51], IN1_R[27], U__d1, U__d0, P[51]);
  U4DPPG_28_12 U28 (P[53], O_R[52], IN1_R[28], U__d1, U__d0, P[52]);
  U4DPPG_29_12 U29 (P[54], O_R[53], IN1_R[29], U__d1, U__d0, P[53]);
  U4DPPG_30_12 U30 (P[55], O_R[54], IN1_R[30], U__d1, U__d0, P[54]);
  U4DPPG_31_12 U31 (P[56], O_R[55], IN1_R[31], U__d1, U__d0, P[55]);
  U4DPPGH_32_12 U32 (O_T, O_R[56], IN1_T, U__d1, U__d0, P[56]);
endmodule

module TCU4VPPG_32_0_13 (O_T, O_R, IN1_T, IN1_R, U__d1, U__d0);
  output [58:26] O_R;
  output O_T;
  input [31:0] IN1_R;
  input IN1_T;
  input U__d1, U__d0;
  wire [58:27] P;
  U4DPPGL_0_13 U0 (P[27], O_R[26], IN1_R[0], U__d1, U__d0);
  U4DPPG_1_13 U1 (P[28], O_R[27], IN1_R[1], U__d1, U__d0, P[27]);
  U4DPPG_2_13 U2 (P[29], O_R[28], IN1_R[2], U__d1, U__d0, P[28]);
  U4DPPG_3_13 U3 (P[30], O_R[29], IN1_R[3], U__d1, U__d0, P[29]);
  U4DPPG_4_13 U4 (P[31], O_R[30], IN1_R[4], U__d1, U__d0, P[30]);
  U4DPPG_5_13 U5 (P[32], O_R[31], IN1_R[5], U__d1, U__d0, P[31]);
  U4DPPG_6_13 U6 (P[33], O_R[32], IN1_R[6], U__d1, U__d0, P[32]);
  U4DPPG_7_13 U7 (P[34], O_R[33], IN1_R[7], U__d1, U__d0, P[33]);
  U4DPPG_8_13 U8 (P[35], O_R[34], IN1_R[8], U__d1, U__d0, P[34]);
  U4DPPG_9_13 U9 (P[36], O_R[35], IN1_R[9], U__d1, U__d0, P[35]);
  U4DPPG_10_13 U10 (P[37], O_R[36], IN1_R[10], U__d1, U__d0, P[36]);
  U4DPPG_11_13 U11 (P[38], O_R[37], IN1_R[11], U__d1, U__d0, P[37]);
  U4DPPG_12_13 U12 (P[39], O_R[38], IN1_R[12], U__d1, U__d0, P[38]);
  U4DPPG_13_13 U13 (P[40], O_R[39], IN1_R[13], U__d1, U__d0, P[39]);
  U4DPPG_14_13 U14 (P[41], O_R[40], IN1_R[14], U__d1, U__d0, P[40]);
  U4DPPG_15_13 U15 (P[42], O_R[41], IN1_R[15], U__d1, U__d0, P[41]);
  U4DPPG_16_13 U16 (P[43], O_R[42], IN1_R[16], U__d1, U__d0, P[42]);
  U4DPPG_17_13 U17 (P[44], O_R[43], IN1_R[17], U__d1, U__d0, P[43]);
  U4DPPG_18_13 U18 (P[45], O_R[44], IN1_R[18], U__d1, U__d0, P[44]);
  U4DPPG_19_13 U19 (P[46], O_R[45], IN1_R[19], U__d1, U__d0, P[45]);
  U4DPPG_20_13 U20 (P[47], O_R[46], IN1_R[20], U__d1, U__d0, P[46]);
  U4DPPG_21_13 U21 (P[48], O_R[47], IN1_R[21], U__d1, U__d0, P[47]);
  U4DPPG_22_13 U22 (P[49], O_R[48], IN1_R[22], U__d1, U__d0, P[48]);
  U4DPPG_23_13 U23 (P[50], O_R[49], IN1_R[23], U__d1, U__d0, P[49]);
  U4DPPG_24_13 U24 (P[51], O_R[50], IN1_R[24], U__d1, U__d0, P[50]);
  U4DPPG_25_13 U25 (P[52], O_R[51], IN1_R[25], U__d1, U__d0, P[51]);
  U4DPPG_26_13 U26 (P[53], O_R[52], IN1_R[26], U__d1, U__d0, P[52]);
  U4DPPG_27_13 U27 (P[54], O_R[53], IN1_R[27], U__d1, U__d0, P[53]);
  U4DPPG_28_13 U28 (P[55], O_R[54], IN1_R[28], U__d1, U__d0, P[54]);
  U4DPPG_29_13 U29 (P[56], O_R[55], IN1_R[29], U__d1, U__d0, P[55]);
  U4DPPG_30_13 U30 (P[57], O_R[56], IN1_R[30], U__d1, U__d0, P[56]);
  U4DPPG_31_13 U31 (P[58], O_R[57], IN1_R[31], U__d1, U__d0, P[57]);
  U4DPPGH_32_13 U32 (O_T, O_R[58], IN1_T, U__d1, U__d0, P[58]);
endmodule

module TCU4VPPG_32_0_14 (O_T, O_R, IN1_T, IN1_R, U__d1, U__d0);
  output [60:28] O_R;
  output O_T;
  input [31:0] IN1_R;
  input IN1_T;
  input U__d1, U__d0;
  wire [60:29] P;
  U4DPPGL_0_14 U0 (P[29], O_R[28], IN1_R[0], U__d1, U__d0);
  U4DPPG_1_14 U1 (P[30], O_R[29], IN1_R[1], U__d1, U__d0, P[29]);
  U4DPPG_2_14 U2 (P[31], O_R[30], IN1_R[2], U__d1, U__d0, P[30]);
  U4DPPG_3_14 U3 (P[32], O_R[31], IN1_R[3], U__d1, U__d0, P[31]);
  U4DPPG_4_14 U4 (P[33], O_R[32], IN1_R[4], U__d1, U__d0, P[32]);
  U4DPPG_5_14 U5 (P[34], O_R[33], IN1_R[5], U__d1, U__d0, P[33]);
  U4DPPG_6_14 U6 (P[35], O_R[34], IN1_R[6], U__d1, U__d0, P[34]);
  U4DPPG_7_14 U7 (P[36], O_R[35], IN1_R[7], U__d1, U__d0, P[35]);
  U4DPPG_8_14 U8 (P[37], O_R[36], IN1_R[8], U__d1, U__d0, P[36]);
  U4DPPG_9_14 U9 (P[38], O_R[37], IN1_R[9], U__d1, U__d0, P[37]);
  U4DPPG_10_14 U10 (P[39], O_R[38], IN1_R[10], U__d1, U__d0, P[38]);
  U4DPPG_11_14 U11 (P[40], O_R[39], IN1_R[11], U__d1, U__d0, P[39]);
  U4DPPG_12_14 U12 (P[41], O_R[40], IN1_R[12], U__d1, U__d0, P[40]);
  U4DPPG_13_14 U13 (P[42], O_R[41], IN1_R[13], U__d1, U__d0, P[41]);
  U4DPPG_14_14 U14 (P[43], O_R[42], IN1_R[14], U__d1, U__d0, P[42]);
  U4DPPG_15_14 U15 (P[44], O_R[43], IN1_R[15], U__d1, U__d0, P[43]);
  U4DPPG_16_14 U16 (P[45], O_R[44], IN1_R[16], U__d1, U__d0, P[44]);
  U4DPPG_17_14 U17 (P[46], O_R[45], IN1_R[17], U__d1, U__d0, P[45]);
  U4DPPG_18_14 U18 (P[47], O_R[46], IN1_R[18], U__d1, U__d0, P[46]);
  U4DPPG_19_14 U19 (P[48], O_R[47], IN1_R[19], U__d1, U__d0, P[47]);
  U4DPPG_20_14 U20 (P[49], O_R[48], IN1_R[20], U__d1, U__d0, P[48]);
  U4DPPG_21_14 U21 (P[50], O_R[49], IN1_R[21], U__d1, U__d0, P[49]);
  U4DPPG_22_14 U22 (P[51], O_R[50], IN1_R[22], U__d1, U__d0, P[50]);
  U4DPPG_23_14 U23 (P[52], O_R[51], IN1_R[23], U__d1, U__d0, P[51]);
  U4DPPG_24_14 U24 (P[53], O_R[52], IN1_R[24], U__d1, U__d0, P[52]);
  U4DPPG_25_14 U25 (P[54], O_R[53], IN1_R[25], U__d1, U__d0, P[53]);
  U4DPPG_26_14 U26 (P[55], O_R[54], IN1_R[26], U__d1, U__d0, P[54]);
  U4DPPG_27_14 U27 (P[56], O_R[55], IN1_R[27], U__d1, U__d0, P[55]);
  U4DPPG_28_14 U28 (P[57], O_R[56], IN1_R[28], U__d1, U__d0, P[56]);
  U4DPPG_29_14 U29 (P[58], O_R[57], IN1_R[29], U__d1, U__d0, P[57]);
  U4DPPG_30_14 U30 (P[59], O_R[58], IN1_R[30], U__d1, U__d0, P[58]);
  U4DPPG_31_14 U31 (P[60], O_R[59], IN1_R[31], U__d1, U__d0, P[59]);
  U4DPPGH_32_14 U32 (O_T, O_R[60], IN1_T, U__d1, U__d0, P[60]);
endmodule

module TCU4VPPG_32_0_15 (O_T, O_R, IN1_T, IN1_R, U__d1, U__d0);
  output [62:30] O_R;
  output O_T;
  input [31:0] IN1_R;
  input IN1_T;
  input U__d1, U__d0;
  wire [62:31] P;
  U4DPPGL_0_15 U0 (P[31], O_R[30], IN1_R[0], U__d1, U__d0);
  U4DPPG_1_15 U1 (P[32], O_R[31], IN1_R[1], U__d1, U__d0, P[31]);
  U4DPPG_2_15 U2 (P[33], O_R[32], IN1_R[2], U__d1, U__d0, P[32]);
  U4DPPG_3_15 U3 (P[34], O_R[33], IN1_R[3], U__d1, U__d0, P[33]);
  U4DPPG_4_15 U4 (P[35], O_R[34], IN1_R[4], U__d1, U__d0, P[34]);
  U4DPPG_5_15 U5 (P[36], O_R[35], IN1_R[5], U__d1, U__d0, P[35]);
  U4DPPG_6_15 U6 (P[37], O_R[36], IN1_R[6], U__d1, U__d0, P[36]);
  U4DPPG_7_15 U7 (P[38], O_R[37], IN1_R[7], U__d1, U__d0, P[37]);
  U4DPPG_8_15 U8 (P[39], O_R[38], IN1_R[8], U__d1, U__d0, P[38]);
  U4DPPG_9_15 U9 (P[40], O_R[39], IN1_R[9], U__d1, U__d0, P[39]);
  U4DPPG_10_15 U10 (P[41], O_R[40], IN1_R[10], U__d1, U__d0, P[40]);
  U4DPPG_11_15 U11 (P[42], O_R[41], IN1_R[11], U__d1, U__d0, P[41]);
  U4DPPG_12_15 U12 (P[43], O_R[42], IN1_R[12], U__d1, U__d0, P[42]);
  U4DPPG_13_15 U13 (P[44], O_R[43], IN1_R[13], U__d1, U__d0, P[43]);
  U4DPPG_14_15 U14 (P[45], O_R[44], IN1_R[14], U__d1, U__d0, P[44]);
  U4DPPG_15_15 U15 (P[46], O_R[45], IN1_R[15], U__d1, U__d0, P[45]);
  U4DPPG_16_15 U16 (P[47], O_R[46], IN1_R[16], U__d1, U__d0, P[46]);
  U4DPPG_17_15 U17 (P[48], O_R[47], IN1_R[17], U__d1, U__d0, P[47]);
  U4DPPG_18_15 U18 (P[49], O_R[48], IN1_R[18], U__d1, U__d0, P[48]);
  U4DPPG_19_15 U19 (P[50], O_R[49], IN1_R[19], U__d1, U__d0, P[49]);
  U4DPPG_20_15 U20 (P[51], O_R[50], IN1_R[20], U__d1, U__d0, P[50]);
  U4DPPG_21_15 U21 (P[52], O_R[51], IN1_R[21], U__d1, U__d0, P[51]);
  U4DPPG_22_15 U22 (P[53], O_R[52], IN1_R[22], U__d1, U__d0, P[52]);
  U4DPPG_23_15 U23 (P[54], O_R[53], IN1_R[23], U__d1, U__d0, P[53]);
  U4DPPG_24_15 U24 (P[55], O_R[54], IN1_R[24], U__d1, U__d0, P[54]);
  U4DPPG_25_15 U25 (P[56], O_R[55], IN1_R[25], U__d1, U__d0, P[55]);
  U4DPPG_26_15 U26 (P[57], O_R[56], IN1_R[26], U__d1, U__d0, P[56]);
  U4DPPG_27_15 U27 (P[58], O_R[57], IN1_R[27], U__d1, U__d0, P[57]);
  U4DPPG_28_15 U28 (P[59], O_R[58], IN1_R[28], U__d1, U__d0, P[58]);
  U4DPPG_29_15 U29 (P[60], O_R[59], IN1_R[29], U__d1, U__d0, P[59]);
  U4DPPG_30_15 U30 (P[61], O_R[60], IN1_R[30], U__d1, U__d0, P[60]);
  U4DPPG_31_15 U31 (P[62], O_R[61], IN1_R[31], U__d1, U__d0, P[61]);
  U4DPPGH_32_15 U32 (O_T, O_R[62], IN1_T, U__d1, U__d0, P[62]);
endmodule

module TCU4VPPG_32_0_16 (O_T, O_R, IN1_T, IN1_R, U__d1, U__d0);
  output [64:32] O_R;
  output O_T;
  input [31:0] IN1_R;
  input IN1_T;
  input U__d1, U__d0;
  wire [64:33] P;
  U4DPPGL_0_16 U0 (P[33], O_R[32], IN1_R[0], U__d1, U__d0);
  U4DPPG_1_16 U1 (P[34], O_R[33], IN1_R[1], U__d1, U__d0, P[33]);
  U4DPPG_2_16 U2 (P[35], O_R[34], IN1_R[2], U__d1, U__d0, P[34]);
  U4DPPG_3_16 U3 (P[36], O_R[35], IN1_R[3], U__d1, U__d0, P[35]);
  U4DPPG_4_16 U4 (P[37], O_R[36], IN1_R[4], U__d1, U__d0, P[36]);
  U4DPPG_5_16 U5 (P[38], O_R[37], IN1_R[5], U__d1, U__d0, P[37]);
  U4DPPG_6_16 U6 (P[39], O_R[38], IN1_R[6], U__d1, U__d0, P[38]);
  U4DPPG_7_16 U7 (P[40], O_R[39], IN1_R[7], U__d1, U__d0, P[39]);
  U4DPPG_8_16 U8 (P[41], O_R[40], IN1_R[8], U__d1, U__d0, P[40]);
  U4DPPG_9_16 U9 (P[42], O_R[41], IN1_R[9], U__d1, U__d0, P[41]);
  U4DPPG_10_16 U10 (P[43], O_R[42], IN1_R[10], U__d1, U__d0, P[42]);
  U4DPPG_11_16 U11 (P[44], O_R[43], IN1_R[11], U__d1, U__d0, P[43]);
  U4DPPG_12_16 U12 (P[45], O_R[44], IN1_R[12], U__d1, U__d0, P[44]);
  U4DPPG_13_16 U13 (P[46], O_R[45], IN1_R[13], U__d1, U__d0, P[45]);
  U4DPPG_14_16 U14 (P[47], O_R[46], IN1_R[14], U__d1, U__d0, P[46]);
  U4DPPG_15_16 U15 (P[48], O_R[47], IN1_R[15], U__d1, U__d0, P[47]);
  U4DPPG_16_16 U16 (P[49], O_R[48], IN1_R[16], U__d1, U__d0, P[48]);
  U4DPPG_17_16 U17 (P[50], O_R[49], IN1_R[17], U__d1, U__d0, P[49]);
  U4DPPG_18_16 U18 (P[51], O_R[50], IN1_R[18], U__d1, U__d0, P[50]);
  U4DPPG_19_16 U19 (P[52], O_R[51], IN1_R[19], U__d1, U__d0, P[51]);
  U4DPPG_20_16 U20 (P[53], O_R[52], IN1_R[20], U__d1, U__d0, P[52]);
  U4DPPG_21_16 U21 (P[54], O_R[53], IN1_R[21], U__d1, U__d0, P[53]);
  U4DPPG_22_16 U22 (P[55], O_R[54], IN1_R[22], U__d1, U__d0, P[54]);
  U4DPPG_23_16 U23 (P[56], O_R[55], IN1_R[23], U__d1, U__d0, P[55]);
  U4DPPG_24_16 U24 (P[57], O_R[56], IN1_R[24], U__d1, U__d0, P[56]);
  U4DPPG_25_16 U25 (P[58], O_R[57], IN1_R[25], U__d1, U__d0, P[57]);
  U4DPPG_26_16 U26 (P[59], O_R[58], IN1_R[26], U__d1, U__d0, P[58]);
  U4DPPG_27_16 U27 (P[60], O_R[59], IN1_R[27], U__d1, U__d0, P[59]);
  U4DPPG_28_16 U28 (P[61], O_R[60], IN1_R[28], U__d1, U__d0, P[60]);
  U4DPPG_29_16 U29 (P[62], O_R[61], IN1_R[29], U__d1, U__d0, P[61]);
  U4DPPG_30_16 U30 (P[63], O_R[62], IN1_R[30], U__d1, U__d0, P[62]);
  U4DPPG_31_16 U31 (P[64], O_R[63], IN1_R[31], U__d1, U__d0, P[63]);
  U4DPPGH_32_16 U32 (O_T, O_R[64], IN1_T, U__d1, U__d0, P[64]);
endmodule

module TCU4VPPG_32_0_2 (O_T, O_R, IN1_T, IN1_R, U__d1, U__d0);
  output [36:4] O_R;
  output O_T;
  input [31:0] IN1_R;
  input IN1_T;
  input U__d1, U__d0;
  wire [36:5] P;
  U4DPPGL_0_2 U0 (P[5], O_R[4], IN1_R[0], U__d1, U__d0);
  U4DPPG_1_2 U1 (P[6], O_R[5], IN1_R[1], U__d1, U__d0, P[5]);
  U4DPPG_2_2 U2 (P[7], O_R[6], IN1_R[2], U__d1, U__d0, P[6]);
  U4DPPG_3_2 U3 (P[8], O_R[7], IN1_R[3], U__d1, U__d0, P[7]);
  U4DPPG_4_2 U4 (P[9], O_R[8], IN1_R[4], U__d1, U__d0, P[8]);
  U4DPPG_5_2 U5 (P[10], O_R[9], IN1_R[5], U__d1, U__d0, P[9]);
  U4DPPG_6_2 U6 (P[11], O_R[10], IN1_R[6], U__d1, U__d0, P[10]);
  U4DPPG_7_2 U7 (P[12], O_R[11], IN1_R[7], U__d1, U__d0, P[11]);
  U4DPPG_8_2 U8 (P[13], O_R[12], IN1_R[8], U__d1, U__d0, P[12]);
  U4DPPG_9_2 U9 (P[14], O_R[13], IN1_R[9], U__d1, U__d0, P[13]);
  U4DPPG_10_2 U10 (P[15], O_R[14], IN1_R[10], U__d1, U__d0, P[14]);
  U4DPPG_11_2 U11 (P[16], O_R[15], IN1_R[11], U__d1, U__d0, P[15]);
  U4DPPG_12_2 U12 (P[17], O_R[16], IN1_R[12], U__d1, U__d0, P[16]);
  U4DPPG_13_2 U13 (P[18], O_R[17], IN1_R[13], U__d1, U__d0, P[17]);
  U4DPPG_14_2 U14 (P[19], O_R[18], IN1_R[14], U__d1, U__d0, P[18]);
  U4DPPG_15_2 U15 (P[20], O_R[19], IN1_R[15], U__d1, U__d0, P[19]);
  U4DPPG_16_2 U16 (P[21], O_R[20], IN1_R[16], U__d1, U__d0, P[20]);
  U4DPPG_17_2 U17 (P[22], O_R[21], IN1_R[17], U__d1, U__d0, P[21]);
  U4DPPG_18_2 U18 (P[23], O_R[22], IN1_R[18], U__d1, U__d0, P[22]);
  U4DPPG_19_2 U19 (P[24], O_R[23], IN1_R[19], U__d1, U__d0, P[23]);
  U4DPPG_20_2 U20 (P[25], O_R[24], IN1_R[20], U__d1, U__d0, P[24]);
  U4DPPG_21_2 U21 (P[26], O_R[25], IN1_R[21], U__d1, U__d0, P[25]);
  U4DPPG_22_2 U22 (P[27], O_R[26], IN1_R[22], U__d1, U__d0, P[26]);
  U4DPPG_23_2 U23 (P[28], O_R[27], IN1_R[23], U__d1, U__d0, P[27]);
  U4DPPG_24_2 U24 (P[29], O_R[28], IN1_R[24], U__d1, U__d0, P[28]);
  U4DPPG_25_2 U25 (P[30], O_R[29], IN1_R[25], U__d1, U__d0, P[29]);
  U4DPPG_26_2 U26 (P[31], O_R[30], IN1_R[26], U__d1, U__d0, P[30]);
  U4DPPG_27_2 U27 (P[32], O_R[31], IN1_R[27], U__d1, U__d0, P[31]);
  U4DPPG_28_2 U28 (P[33], O_R[32], IN1_R[28], U__d1, U__d0, P[32]);
  U4DPPG_29_2 U29 (P[34], O_R[33], IN1_R[29], U__d1, U__d0, P[33]);
  U4DPPG_30_2 U30 (P[35], O_R[34], IN1_R[30], U__d1, U__d0, P[34]);
  U4DPPG_31_2 U31 (P[36], O_R[35], IN1_R[31], U__d1, U__d0, P[35]);
  U4DPPGH_32_2 U32 (O_T, O_R[36], IN1_T, U__d1, U__d0, P[36]);
endmodule

module TCU4VPPG_32_0_3 (O_T, O_R, IN1_T, IN1_R, U__d1, U__d0);
  output [38:6] O_R;
  output O_T;
  input [31:0] IN1_R;
  input IN1_T;
  input U__d1, U__d0;
  wire [38:7] P;
  U4DPPGL_0_3 U0 (P[7], O_R[6], IN1_R[0], U__d1, U__d0);
  U4DPPG_1_3 U1 (P[8], O_R[7], IN1_R[1], U__d1, U__d0, P[7]);
  U4DPPG_2_3 U2 (P[9], O_R[8], IN1_R[2], U__d1, U__d0, P[8]);
  U4DPPG_3_3 U3 (P[10], O_R[9], IN1_R[3], U__d1, U__d0, P[9]);
  U4DPPG_4_3 U4 (P[11], O_R[10], IN1_R[4], U__d1, U__d0, P[10]);
  U4DPPG_5_3 U5 (P[12], O_R[11], IN1_R[5], U__d1, U__d0, P[11]);
  U4DPPG_6_3 U6 (P[13], O_R[12], IN1_R[6], U__d1, U__d0, P[12]);
  U4DPPG_7_3 U7 (P[14], O_R[13], IN1_R[7], U__d1, U__d0, P[13]);
  U4DPPG_8_3 U8 (P[15], O_R[14], IN1_R[8], U__d1, U__d0, P[14]);
  U4DPPG_9_3 U9 (P[16], O_R[15], IN1_R[9], U__d1, U__d0, P[15]);
  U4DPPG_10_3 U10 (P[17], O_R[16], IN1_R[10], U__d1, U__d0, P[16]);
  U4DPPG_11_3 U11 (P[18], O_R[17], IN1_R[11], U__d1, U__d0, P[17]);
  U4DPPG_12_3 U12 (P[19], O_R[18], IN1_R[12], U__d1, U__d0, P[18]);
  U4DPPG_13_3 U13 (P[20], O_R[19], IN1_R[13], U__d1, U__d0, P[19]);
  U4DPPG_14_3 U14 (P[21], O_R[20], IN1_R[14], U__d1, U__d0, P[20]);
  U4DPPG_15_3 U15 (P[22], O_R[21], IN1_R[15], U__d1, U__d0, P[21]);
  U4DPPG_16_3 U16 (P[23], O_R[22], IN1_R[16], U__d1, U__d0, P[22]);
  U4DPPG_17_3 U17 (P[24], O_R[23], IN1_R[17], U__d1, U__d0, P[23]);
  U4DPPG_18_3 U18 (P[25], O_R[24], IN1_R[18], U__d1, U__d0, P[24]);
  U4DPPG_19_3 U19 (P[26], O_R[25], IN1_R[19], U__d1, U__d0, P[25]);
  U4DPPG_20_3 U20 (P[27], O_R[26], IN1_R[20], U__d1, U__d0, P[26]);
  U4DPPG_21_3 U21 (P[28], O_R[27], IN1_R[21], U__d1, U__d0, P[27]);
  U4DPPG_22_3 U22 (P[29], O_R[28], IN1_R[22], U__d1, U__d0, P[28]);
  U4DPPG_23_3 U23 (P[30], O_R[29], IN1_R[23], U__d1, U__d0, P[29]);
  U4DPPG_24_3 U24 (P[31], O_R[30], IN1_R[24], U__d1, U__d0, P[30]);
  U4DPPG_25_3 U25 (P[32], O_R[31], IN1_R[25], U__d1, U__d0, P[31]);
  U4DPPG_26_3 U26 (P[33], O_R[32], IN1_R[26], U__d1, U__d0, P[32]);
  U4DPPG_27_3 U27 (P[34], O_R[33], IN1_R[27], U__d1, U__d0, P[33]);
  U4DPPG_28_3 U28 (P[35], O_R[34], IN1_R[28], U__d1, U__d0, P[34]);
  U4DPPG_29_3 U29 (P[36], O_R[35], IN1_R[29], U__d1, U__d0, P[35]);
  U4DPPG_30_3 U30 (P[37], O_R[36], IN1_R[30], U__d1, U__d0, P[36]);
  U4DPPG_31_3 U31 (P[38], O_R[37], IN1_R[31], U__d1, U__d0, P[37]);
  U4DPPGH_32_3 U32 (O_T, O_R[38], IN1_T, U__d1, U__d0, P[38]);
endmodule

module TCU4VPPG_32_0_4 (O_T, O_R, IN1_T, IN1_R, U__d1, U__d0);
  output [40:8] O_R;
  output O_T;
  input [31:0] IN1_R;
  input IN1_T;
  input U__d1, U__d0;
  wire [40:9] P;
  U4DPPGL_0_4 U0 (P[9], O_R[8], IN1_R[0], U__d1, U__d0);
  U4DPPG_1_4 U1 (P[10], O_R[9], IN1_R[1], U__d1, U__d0, P[9]);
  U4DPPG_2_4 U2 (P[11], O_R[10], IN1_R[2], U__d1, U__d0, P[10]);
  U4DPPG_3_4 U3 (P[12], O_R[11], IN1_R[3], U__d1, U__d0, P[11]);
  U4DPPG_4_4 U4 (P[13], O_R[12], IN1_R[4], U__d1, U__d0, P[12]);
  U4DPPG_5_4 U5 (P[14], O_R[13], IN1_R[5], U__d1, U__d0, P[13]);
  U4DPPG_6_4 U6 (P[15], O_R[14], IN1_R[6], U__d1, U__d0, P[14]);
  U4DPPG_7_4 U7 (P[16], O_R[15], IN1_R[7], U__d1, U__d0, P[15]);
  U4DPPG_8_4 U8 (P[17], O_R[16], IN1_R[8], U__d1, U__d0, P[16]);
  U4DPPG_9_4 U9 (P[18], O_R[17], IN1_R[9], U__d1, U__d0, P[17]);
  U4DPPG_10_4 U10 (P[19], O_R[18], IN1_R[10], U__d1, U__d0, P[18]);
  U4DPPG_11_4 U11 (P[20], O_R[19], IN1_R[11], U__d1, U__d0, P[19]);
  U4DPPG_12_4 U12 (P[21], O_R[20], IN1_R[12], U__d1, U__d0, P[20]);
  U4DPPG_13_4 U13 (P[22], O_R[21], IN1_R[13], U__d1, U__d0, P[21]);
  U4DPPG_14_4 U14 (P[23], O_R[22], IN1_R[14], U__d1, U__d0, P[22]);
  U4DPPG_15_4 U15 (P[24], O_R[23], IN1_R[15], U__d1, U__d0, P[23]);
  U4DPPG_16_4 U16 (P[25], O_R[24], IN1_R[16], U__d1, U__d0, P[24]);
  U4DPPG_17_4 U17 (P[26], O_R[25], IN1_R[17], U__d1, U__d0, P[25]);
  U4DPPG_18_4 U18 (P[27], O_R[26], IN1_R[18], U__d1, U__d0, P[26]);
  U4DPPG_19_4 U19 (P[28], O_R[27], IN1_R[19], U__d1, U__d0, P[27]);
  U4DPPG_20_4 U20 (P[29], O_R[28], IN1_R[20], U__d1, U__d0, P[28]);
  U4DPPG_21_4 U21 (P[30], O_R[29], IN1_R[21], U__d1, U__d0, P[29]);
  U4DPPG_22_4 U22 (P[31], O_R[30], IN1_R[22], U__d1, U__d0, P[30]);
  U4DPPG_23_4 U23 (P[32], O_R[31], IN1_R[23], U__d1, U__d0, P[31]);
  U4DPPG_24_4 U24 (P[33], O_R[32], IN1_R[24], U__d1, U__d0, P[32]);
  U4DPPG_25_4 U25 (P[34], O_R[33], IN1_R[25], U__d1, U__d0, P[33]);
  U4DPPG_26_4 U26 (P[35], O_R[34], IN1_R[26], U__d1, U__d0, P[34]);
  U4DPPG_27_4 U27 (P[36], O_R[35], IN1_R[27], U__d1, U__d0, P[35]);
  U4DPPG_28_4 U28 (P[37], O_R[36], IN1_R[28], U__d1, U__d0, P[36]);
  U4DPPG_29_4 U29 (P[38], O_R[37], IN1_R[29], U__d1, U__d0, P[37]);
  U4DPPG_30_4 U30 (P[39], O_R[38], IN1_R[30], U__d1, U__d0, P[38]);
  U4DPPG_31_4 U31 (P[40], O_R[39], IN1_R[31], U__d1, U__d0, P[39]);
  U4DPPGH_32_4 U32 (O_T, O_R[40], IN1_T, U__d1, U__d0, P[40]);
endmodule

module TCU4VPPG_32_0_5 (O_T, O_R, IN1_T, IN1_R, U__d1, U__d0);
  output [42:10] O_R;
  output O_T;
  input [31:0] IN1_R;
  input IN1_T;
  input U__d1, U__d0;
  wire [42:11] P;
  U4DPPGL_0_5 U0 (P[11], O_R[10], IN1_R[0], U__d1, U__d0);
  U4DPPG_1_5 U1 (P[12], O_R[11], IN1_R[1], U__d1, U__d0, P[11]);
  U4DPPG_2_5 U2 (P[13], O_R[12], IN1_R[2], U__d1, U__d0, P[12]);
  U4DPPG_3_5 U3 (P[14], O_R[13], IN1_R[3], U__d1, U__d0, P[13]);
  U4DPPG_4_5 U4 (P[15], O_R[14], IN1_R[4], U__d1, U__d0, P[14]);
  U4DPPG_5_5 U5 (P[16], O_R[15], IN1_R[5], U__d1, U__d0, P[15]);
  U4DPPG_6_5 U6 (P[17], O_R[16], IN1_R[6], U__d1, U__d0, P[16]);
  U4DPPG_7_5 U7 (P[18], O_R[17], IN1_R[7], U__d1, U__d0, P[17]);
  U4DPPG_8_5 U8 (P[19], O_R[18], IN1_R[8], U__d1, U__d0, P[18]);
  U4DPPG_9_5 U9 (P[20], O_R[19], IN1_R[9], U__d1, U__d0, P[19]);
  U4DPPG_10_5 U10 (P[21], O_R[20], IN1_R[10], U__d1, U__d0, P[20]);
  U4DPPG_11_5 U11 (P[22], O_R[21], IN1_R[11], U__d1, U__d0, P[21]);
  U4DPPG_12_5 U12 (P[23], O_R[22], IN1_R[12], U__d1, U__d0, P[22]);
  U4DPPG_13_5 U13 (P[24], O_R[23], IN1_R[13], U__d1, U__d0, P[23]);
  U4DPPG_14_5 U14 (P[25], O_R[24], IN1_R[14], U__d1, U__d0, P[24]);
  U4DPPG_15_5 U15 (P[26], O_R[25], IN1_R[15], U__d1, U__d0, P[25]);
  U4DPPG_16_5 U16 (P[27], O_R[26], IN1_R[16], U__d1, U__d0, P[26]);
  U4DPPG_17_5 U17 (P[28], O_R[27], IN1_R[17], U__d1, U__d0, P[27]);
  U4DPPG_18_5 U18 (P[29], O_R[28], IN1_R[18], U__d1, U__d0, P[28]);
  U4DPPG_19_5 U19 (P[30], O_R[29], IN1_R[19], U__d1, U__d0, P[29]);
  U4DPPG_20_5 U20 (P[31], O_R[30], IN1_R[20], U__d1, U__d0, P[30]);
  U4DPPG_21_5 U21 (P[32], O_R[31], IN1_R[21], U__d1, U__d0, P[31]);
  U4DPPG_22_5 U22 (P[33], O_R[32], IN1_R[22], U__d1, U__d0, P[32]);
  U4DPPG_23_5 U23 (P[34], O_R[33], IN1_R[23], U__d1, U__d0, P[33]);
  U4DPPG_24_5 U24 (P[35], O_R[34], IN1_R[24], U__d1, U__d0, P[34]);
  U4DPPG_25_5 U25 (P[36], O_R[35], IN1_R[25], U__d1, U__d0, P[35]);
  U4DPPG_26_5 U26 (P[37], O_R[36], IN1_R[26], U__d1, U__d0, P[36]);
  U4DPPG_27_5 U27 (P[38], O_R[37], IN1_R[27], U__d1, U__d0, P[37]);
  U4DPPG_28_5 U28 (P[39], O_R[38], IN1_R[28], U__d1, U__d0, P[38]);
  U4DPPG_29_5 U29 (P[40], O_R[39], IN1_R[29], U__d1, U__d0, P[39]);
  U4DPPG_30_5 U30 (P[41], O_R[40], IN1_R[30], U__d1, U__d0, P[40]);
  U4DPPG_31_5 U31 (P[42], O_R[41], IN1_R[31], U__d1, U__d0, P[41]);
  U4DPPGH_32_5 U32 (O_T, O_R[42], IN1_T, U__d1, U__d0, P[42]);
endmodule

module TCU4VPPG_32_0_6 (O_T, O_R, IN1_T, IN1_R, U__d1, U__d0);
  output [44:12] O_R;
  output O_T;
  input [31:0] IN1_R;
  input IN1_T;
  input U__d1, U__d0;
  wire [44:13] P;
  U4DPPGL_0_6 U0 (P[13], O_R[12], IN1_R[0], U__d1, U__d0);
  U4DPPG_1_6 U1 (P[14], O_R[13], IN1_R[1], U__d1, U__d0, P[13]);
  U4DPPG_2_6 U2 (P[15], O_R[14], IN1_R[2], U__d1, U__d0, P[14]);
  U4DPPG_3_6 U3 (P[16], O_R[15], IN1_R[3], U__d1, U__d0, P[15]);
  U4DPPG_4_6 U4 (P[17], O_R[16], IN1_R[4], U__d1, U__d0, P[16]);
  U4DPPG_5_6 U5 (P[18], O_R[17], IN1_R[5], U__d1, U__d0, P[17]);
  U4DPPG_6_6 U6 (P[19], O_R[18], IN1_R[6], U__d1, U__d0, P[18]);
  U4DPPG_7_6 U7 (P[20], O_R[19], IN1_R[7], U__d1, U__d0, P[19]);
  U4DPPG_8_6 U8 (P[21], O_R[20], IN1_R[8], U__d1, U__d0, P[20]);
  U4DPPG_9_6 U9 (P[22], O_R[21], IN1_R[9], U__d1, U__d0, P[21]);
  U4DPPG_10_6 U10 (P[23], O_R[22], IN1_R[10], U__d1, U__d0, P[22]);
  U4DPPG_11_6 U11 (P[24], O_R[23], IN1_R[11], U__d1, U__d0, P[23]);
  U4DPPG_12_6 U12 (P[25], O_R[24], IN1_R[12], U__d1, U__d0, P[24]);
  U4DPPG_13_6 U13 (P[26], O_R[25], IN1_R[13], U__d1, U__d0, P[25]);
  U4DPPG_14_6 U14 (P[27], O_R[26], IN1_R[14], U__d1, U__d0, P[26]);
  U4DPPG_15_6 U15 (P[28], O_R[27], IN1_R[15], U__d1, U__d0, P[27]);
  U4DPPG_16_6 U16 (P[29], O_R[28], IN1_R[16], U__d1, U__d0, P[28]);
  U4DPPG_17_6 U17 (P[30], O_R[29], IN1_R[17], U__d1, U__d0, P[29]);
  U4DPPG_18_6 U18 (P[31], O_R[30], IN1_R[18], U__d1, U__d0, P[30]);
  U4DPPG_19_6 U19 (P[32], O_R[31], IN1_R[19], U__d1, U__d0, P[31]);
  U4DPPG_20_6 U20 (P[33], O_R[32], IN1_R[20], U__d1, U__d0, P[32]);
  U4DPPG_21_6 U21 (P[34], O_R[33], IN1_R[21], U__d1, U__d0, P[33]);
  U4DPPG_22_6 U22 (P[35], O_R[34], IN1_R[22], U__d1, U__d0, P[34]);
  U4DPPG_23_6 U23 (P[36], O_R[35], IN1_R[23], U__d1, U__d0, P[35]);
  U4DPPG_24_6 U24 (P[37], O_R[36], IN1_R[24], U__d1, U__d0, P[36]);
  U4DPPG_25_6 U25 (P[38], O_R[37], IN1_R[25], U__d1, U__d0, P[37]);
  U4DPPG_26_6 U26 (P[39], O_R[38], IN1_R[26], U__d1, U__d0, P[38]);
  U4DPPG_27_6 U27 (P[40], O_R[39], IN1_R[27], U__d1, U__d0, P[39]);
  U4DPPG_28_6 U28 (P[41], O_R[40], IN1_R[28], U__d1, U__d0, P[40]);
  U4DPPG_29_6 U29 (P[42], O_R[41], IN1_R[29], U__d1, U__d0, P[41]);
  U4DPPG_30_6 U30 (P[43], O_R[42], IN1_R[30], U__d1, U__d0, P[42]);
  U4DPPG_31_6 U31 (P[44], O_R[43], IN1_R[31], U__d1, U__d0, P[43]);
  U4DPPGH_32_6 U32 (O_T, O_R[44], IN1_T, U__d1, U__d0, P[44]);
endmodule

module TCU4VPPG_32_0_7 (O_T, O_R, IN1_T, IN1_R, U__d1, U__d0);
  output [46:14] O_R;
  output O_T;
  input [31:0] IN1_R;
  input IN1_T;
  input U__d1, U__d0;
  wire [46:15] P;
  U4DPPGL_0_7 U0 (P[15], O_R[14], IN1_R[0], U__d1, U__d0);
  U4DPPG_1_7 U1 (P[16], O_R[15], IN1_R[1], U__d1, U__d0, P[15]);
  U4DPPG_2_7 U2 (P[17], O_R[16], IN1_R[2], U__d1, U__d0, P[16]);
  U4DPPG_3_7 U3 (P[18], O_R[17], IN1_R[3], U__d1, U__d0, P[17]);
  U4DPPG_4_7 U4 (P[19], O_R[18], IN1_R[4], U__d1, U__d0, P[18]);
  U4DPPG_5_7 U5 (P[20], O_R[19], IN1_R[5], U__d1, U__d0, P[19]);
  U4DPPG_6_7 U6 (P[21], O_R[20], IN1_R[6], U__d1, U__d0, P[20]);
  U4DPPG_7_7 U7 (P[22], O_R[21], IN1_R[7], U__d1, U__d0, P[21]);
  U4DPPG_8_7 U8 (P[23], O_R[22], IN1_R[8], U__d1, U__d0, P[22]);
  U4DPPG_9_7 U9 (P[24], O_R[23], IN1_R[9], U__d1, U__d0, P[23]);
  U4DPPG_10_7 U10 (P[25], O_R[24], IN1_R[10], U__d1, U__d0, P[24]);
  U4DPPG_11_7 U11 (P[26], O_R[25], IN1_R[11], U__d1, U__d0, P[25]);
  U4DPPG_12_7 U12 (P[27], O_R[26], IN1_R[12], U__d1, U__d0, P[26]);
  U4DPPG_13_7 U13 (P[28], O_R[27], IN1_R[13], U__d1, U__d0, P[27]);
  U4DPPG_14_7 U14 (P[29], O_R[28], IN1_R[14], U__d1, U__d0, P[28]);
  U4DPPG_15_7 U15 (P[30], O_R[29], IN1_R[15], U__d1, U__d0, P[29]);
  U4DPPG_16_7 U16 (P[31], O_R[30], IN1_R[16], U__d1, U__d0, P[30]);
  U4DPPG_17_7 U17 (P[32], O_R[31], IN1_R[17], U__d1, U__d0, P[31]);
  U4DPPG_18_7 U18 (P[33], O_R[32], IN1_R[18], U__d1, U__d0, P[32]);
  U4DPPG_19_7 U19 (P[34], O_R[33], IN1_R[19], U__d1, U__d0, P[33]);
  U4DPPG_20_7 U20 (P[35], O_R[34], IN1_R[20], U__d1, U__d0, P[34]);
  U4DPPG_21_7 U21 (P[36], O_R[35], IN1_R[21], U__d1, U__d0, P[35]);
  U4DPPG_22_7 U22 (P[37], O_R[36], IN1_R[22], U__d1, U__d0, P[36]);
  U4DPPG_23_7 U23 (P[38], O_R[37], IN1_R[23], U__d1, U__d0, P[37]);
  U4DPPG_24_7 U24 (P[39], O_R[38], IN1_R[24], U__d1, U__d0, P[38]);
  U4DPPG_25_7 U25 (P[40], O_R[39], IN1_R[25], U__d1, U__d0, P[39]);
  U4DPPG_26_7 U26 (P[41], O_R[40], IN1_R[26], U__d1, U__d0, P[40]);
  U4DPPG_27_7 U27 (P[42], O_R[41], IN1_R[27], U__d1, U__d0, P[41]);
  U4DPPG_28_7 U28 (P[43], O_R[42], IN1_R[28], U__d1, U__d0, P[42]);
  U4DPPG_29_7 U29 (P[44], O_R[43], IN1_R[29], U__d1, U__d0, P[43]);
  U4DPPG_30_7 U30 (P[45], O_R[44], IN1_R[30], U__d1, U__d0, P[44]);
  U4DPPG_31_7 U31 (P[46], O_R[45], IN1_R[31], U__d1, U__d0, P[45]);
  U4DPPGH_32_7 U32 (O_T, O_R[46], IN1_T, U__d1, U__d0, P[46]);
endmodule

module TCU4VPPG_32_0_8 (O_T, O_R, IN1_T, IN1_R, U__d1, U__d0);
  output [48:16] O_R;
  output O_T;
  input [31:0] IN1_R;
  input IN1_T;
  input U__d1, U__d0;
  wire [48:17] P;
  U4DPPGL_0_8 U0 (P[17], O_R[16], IN1_R[0], U__d1, U__d0);
  U4DPPG_1_8 U1 (P[18], O_R[17], IN1_R[1], U__d1, U__d0, P[17]);
  U4DPPG_2_8 U2 (P[19], O_R[18], IN1_R[2], U__d1, U__d0, P[18]);
  U4DPPG_3_8 U3 (P[20], O_R[19], IN1_R[3], U__d1, U__d0, P[19]);
  U4DPPG_4_8 U4 (P[21], O_R[20], IN1_R[4], U__d1, U__d0, P[20]);
  U4DPPG_5_8 U5 (P[22], O_R[21], IN1_R[5], U__d1, U__d0, P[21]);
  U4DPPG_6_8 U6 (P[23], O_R[22], IN1_R[6], U__d1, U__d0, P[22]);
  U4DPPG_7_8 U7 (P[24], O_R[23], IN1_R[7], U__d1, U__d0, P[23]);
  U4DPPG_8_8 U8 (P[25], O_R[24], IN1_R[8], U__d1, U__d0, P[24]);
  U4DPPG_9_8 U9 (P[26], O_R[25], IN1_R[9], U__d1, U__d0, P[25]);
  U4DPPG_10_8 U10 (P[27], O_R[26], IN1_R[10], U__d1, U__d0, P[26]);
  U4DPPG_11_8 U11 (P[28], O_R[27], IN1_R[11], U__d1, U__d0, P[27]);
  U4DPPG_12_8 U12 (P[29], O_R[28], IN1_R[12], U__d1, U__d0, P[28]);
  U4DPPG_13_8 U13 (P[30], O_R[29], IN1_R[13], U__d1, U__d0, P[29]);
  U4DPPG_14_8 U14 (P[31], O_R[30], IN1_R[14], U__d1, U__d0, P[30]);
  U4DPPG_15_8 U15 (P[32], O_R[31], IN1_R[15], U__d1, U__d0, P[31]);
  U4DPPG_16_8 U16 (P[33], O_R[32], IN1_R[16], U__d1, U__d0, P[32]);
  U4DPPG_17_8 U17 (P[34], O_R[33], IN1_R[17], U__d1, U__d0, P[33]);
  U4DPPG_18_8 U18 (P[35], O_R[34], IN1_R[18], U__d1, U__d0, P[34]);
  U4DPPG_19_8 U19 (P[36], O_R[35], IN1_R[19], U__d1, U__d0, P[35]);
  U4DPPG_20_8 U20 (P[37], O_R[36], IN1_R[20], U__d1, U__d0, P[36]);
  U4DPPG_21_8 U21 (P[38], O_R[37], IN1_R[21], U__d1, U__d0, P[37]);
  U4DPPG_22_8 U22 (P[39], O_R[38], IN1_R[22], U__d1, U__d0, P[38]);
  U4DPPG_23_8 U23 (P[40], O_R[39], IN1_R[23], U__d1, U__d0, P[39]);
  U4DPPG_24_8 U24 (P[41], O_R[40], IN1_R[24], U__d1, U__d0, P[40]);
  U4DPPG_25_8 U25 (P[42], O_R[41], IN1_R[25], U__d1, U__d0, P[41]);
  U4DPPG_26_8 U26 (P[43], O_R[42], IN1_R[26], U__d1, U__d0, P[42]);
  U4DPPG_27_8 U27 (P[44], O_R[43], IN1_R[27], U__d1, U__d0, P[43]);
  U4DPPG_28_8 U28 (P[45], O_R[44], IN1_R[28], U__d1, U__d0, P[44]);
  U4DPPG_29_8 U29 (P[46], O_R[45], IN1_R[29], U__d1, U__d0, P[45]);
  U4DPPG_30_8 U30 (P[47], O_R[46], IN1_R[30], U__d1, U__d0, P[46]);
  U4DPPG_31_8 U31 (P[48], O_R[47], IN1_R[31], U__d1, U__d0, P[47]);
  U4DPPGH_32_8 U32 (O_T, O_R[48], IN1_T, U__d1, U__d0, P[48]);
endmodule

module TCU4VPPG_32_0_9 (O_T, O_R, IN1_T, IN1_R, U__d1, U__d0);
  output [50:18] O_R;
  output O_T;
  input [31:0] IN1_R;
  input IN1_T;
  input U__d1, U__d0;
  wire [50:19] P;
  U4DPPGL_0_9 U0 (P[19], O_R[18], IN1_R[0], U__d1, U__d0);
  U4DPPG_1_9 U1 (P[20], O_R[19], IN1_R[1], U__d1, U__d0, P[19]);
  U4DPPG_2_9 U2 (P[21], O_R[20], IN1_R[2], U__d1, U__d0, P[20]);
  U4DPPG_3_9 U3 (P[22], O_R[21], IN1_R[3], U__d1, U__d0, P[21]);
  U4DPPG_4_9 U4 (P[23], O_R[22], IN1_R[4], U__d1, U__d0, P[22]);
  U4DPPG_5_9 U5 (P[24], O_R[23], IN1_R[5], U__d1, U__d0, P[23]);
  U4DPPG_6_9 U6 (P[25], O_R[24], IN1_R[6], U__d1, U__d0, P[24]);
  U4DPPG_7_9 U7 (P[26], O_R[25], IN1_R[7], U__d1, U__d0, P[25]);
  U4DPPG_8_9 U8 (P[27], O_R[26], IN1_R[8], U__d1, U__d0, P[26]);
  U4DPPG_9_9 U9 (P[28], O_R[27], IN1_R[9], U__d1, U__d0, P[27]);
  U4DPPG_10_9 U10 (P[29], O_R[28], IN1_R[10], U__d1, U__d0, P[28]);
  U4DPPG_11_9 U11 (P[30], O_R[29], IN1_R[11], U__d1, U__d0, P[29]);
  U4DPPG_12_9 U12 (P[31], O_R[30], IN1_R[12], U__d1, U__d0, P[30]);
  U4DPPG_13_9 U13 (P[32], O_R[31], IN1_R[13], U__d1, U__d0, P[31]);
  U4DPPG_14_9 U14 (P[33], O_R[32], IN1_R[14], U__d1, U__d0, P[32]);
  U4DPPG_15_9 U15 (P[34], O_R[33], IN1_R[15], U__d1, U__d0, P[33]);
  U4DPPG_16_9 U16 (P[35], O_R[34], IN1_R[16], U__d1, U__d0, P[34]);
  U4DPPG_17_9 U17 (P[36], O_R[35], IN1_R[17], U__d1, U__d0, P[35]);
  U4DPPG_18_9 U18 (P[37], O_R[36], IN1_R[18], U__d1, U__d0, P[36]);
  U4DPPG_19_9 U19 (P[38], O_R[37], IN1_R[19], U__d1, U__d0, P[37]);
  U4DPPG_20_9 U20 (P[39], O_R[38], IN1_R[20], U__d1, U__d0, P[38]);
  U4DPPG_21_9 U21 (P[40], O_R[39], IN1_R[21], U__d1, U__d0, P[39]);
  U4DPPG_22_9 U22 (P[41], O_R[40], IN1_R[22], U__d1, U__d0, P[40]);
  U4DPPG_23_9 U23 (P[42], O_R[41], IN1_R[23], U__d1, U__d0, P[41]);
  U4DPPG_24_9 U24 (P[43], O_R[42], IN1_R[24], U__d1, U__d0, P[42]);
  U4DPPG_25_9 U25 (P[44], O_R[43], IN1_R[25], U__d1, U__d0, P[43]);
  U4DPPG_26_9 U26 (P[45], O_R[44], IN1_R[26], U__d1, U__d0, P[44]);
  U4DPPG_27_9 U27 (P[46], O_R[45], IN1_R[27], U__d1, U__d0, P[45]);
  U4DPPG_28_9 U28 (P[47], O_R[46], IN1_R[28], U__d1, U__d0, P[46]);
  U4DPPG_29_9 U29 (P[48], O_R[47], IN1_R[29], U__d1, U__d0, P[47]);
  U4DPPG_30_9 U30 (P[49], O_R[48], IN1_R[30], U__d1, U__d0, P[48]);
  U4DPPG_31_9 U31 (P[50], O_R[49], IN1_R[31], U__d1, U__d0, P[49]);
  U4DPPGH_32_9 U32 (O_T, O_R[50], IN1_T, U__d1, U__d0, P[50]);
endmodule

module TUBWCON_33_0 (O, I_T, I_R, S);
  output [33:0] O;
  input [32:0] I_R;
  input I_T;
  input S;
  BWCPP_0 U0 (O[0], I_R[0], S);
  BWCPP_1 U1 (O[1], I_R[1], S);
  BWCPP_2 U2 (O[2], I_R[2], S);
  BWCPP_3 U3 (O[3], I_R[3], S);
  BWCPP_4 U4 (O[4], I_R[4], S);
  BWCPP_5 U5 (O[5], I_R[5], S);
  BWCPP_6 U6 (O[6], I_R[6], S);
  BWCPP_7 U7 (O[7], I_R[7], S);
  BWCPP_8 U8 (O[8], I_R[8], S);
  BWCPP_9 U9 (O[9], I_R[9], S);
  BWCPP_10 U10 (O[10], I_R[10], S);
  BWCPP_11 U11 (O[11], I_R[11], S);
  BWCPP_12 U12 (O[12], I_R[12], S);
  BWCPP_13 U13 (O[13], I_R[13], S);
  BWCPP_14 U14 (O[14], I_R[14], S);
  BWCPP_15 U15 (O[15], I_R[15], S);
  BWCPP_16 U16 (O[16], I_R[16], S);
  BWCPP_17 U17 (O[17], I_R[17], S);
  BWCPP_18 U18 (O[18], I_R[18], S);
  BWCPP_19 U19 (O[19], I_R[19], S);
  BWCPP_20 U20 (O[20], I_R[20], S);
  BWCPP_21 U21 (O[21], I_R[21], S);
  BWCPP_22 U22 (O[22], I_R[22], S);
  BWCPP_23 U23 (O[23], I_R[23], S);
  BWCPP_24 U24 (O[24], I_R[24], S);
  BWCPP_25 U25 (O[25], I_R[25], S);
  BWCPP_26 U26 (O[26], I_R[26], S);
  BWCPP_27 U27 (O[27], I_R[27], S);
  BWCPP_28 U28 (O[28], I_R[28], S);
  BWCPP_29 U29 (O[29], I_R[29], S);
  BWCPP_30 U30 (O[30], I_R[30], S);
  BWCPP_31 U31 (O[31], I_R[31], S);
  BWCPP_32 U32 (O[32], I_R[32], S);
  BWCNP_33 U33 (O[33], I_T, S);
endmodule

module TUBWCON_35_2 (O, I_T, I_R, S);
  output [35:2] O;
  input [34:2] I_R;
  input I_T;
  input S;
  BWCPP_2 U0 (O[2], I_R[2], S);
  BWCPP_3 U1 (O[3], I_R[3], S);
  BWCPP_4 U2 (O[4], I_R[4], S);
  BWCPP_5 U3 (O[5], I_R[5], S);
  BWCPP_6 U4 (O[6], I_R[6], S);
  BWCPP_7 U5 (O[7], I_R[7], S);
  BWCPP_8 U6 (O[8], I_R[8], S);
  BWCPP_9 U7 (O[9], I_R[9], S);
  BWCPP_10 U8 (O[10], I_R[10], S);
  BWCPP_11 U9 (O[11], I_R[11], S);
  BWCPP_12 U10 (O[12], I_R[12], S);
  BWCPP_13 U11 (O[13], I_R[13], S);
  BWCPP_14 U12 (O[14], I_R[14], S);
  BWCPP_15 U13 (O[15], I_R[15], S);
  BWCPP_16 U14 (O[16], I_R[16], S);
  BWCPP_17 U15 (O[17], I_R[17], S);
  BWCPP_18 U16 (O[18], I_R[18], S);
  BWCPP_19 U17 (O[19], I_R[19], S);
  BWCPP_20 U18 (O[20], I_R[20], S);
  BWCPP_21 U19 (O[21], I_R[21], S);
  BWCPP_22 U20 (O[22], I_R[22], S);
  BWCPP_23 U21 (O[23], I_R[23], S);
  BWCPP_24 U22 (O[24], I_R[24], S);
  BWCPP_25 U23 (O[25], I_R[25], S);
  BWCPP_26 U24 (O[26], I_R[26], S);
  BWCPP_27 U25 (O[27], I_R[27], S);
  BWCPP_28 U26 (O[28], I_R[28], S);
  BWCPP_29 U27 (O[29], I_R[29], S);
  BWCPP_30 U28 (O[30], I_R[30], S);
  BWCPP_31 U29 (O[31], I_R[31], S);
  BWCPP_32 U30 (O[32], I_R[32], S);
  BWCPP_33 U31 (O[33], I_R[33], S);
  BWCPP_34 U32 (O[34], I_R[34], S);
  BWCNP_35 U33 (O[35], I_T, S);
endmodule

module TUBWCON_37_4 (O, I_T, I_R, S);
  output [37:4] O;
  input [36:4] I_R;
  input I_T;
  input S;
  BWCPP_4 U0 (O[4], I_R[4], S);
  BWCPP_5 U1 (O[5], I_R[5], S);
  BWCPP_6 U2 (O[6], I_R[6], S);
  BWCPP_7 U3 (O[7], I_R[7], S);
  BWCPP_8 U4 (O[8], I_R[8], S);
  BWCPP_9 U5 (O[9], I_R[9], S);
  BWCPP_10 U6 (O[10], I_R[10], S);
  BWCPP_11 U7 (O[11], I_R[11], S);
  BWCPP_12 U8 (O[12], I_R[12], S);
  BWCPP_13 U9 (O[13], I_R[13], S);
  BWCPP_14 U10 (O[14], I_R[14], S);
  BWCPP_15 U11 (O[15], I_R[15], S);
  BWCPP_16 U12 (O[16], I_R[16], S);
  BWCPP_17 U13 (O[17], I_R[17], S);
  BWCPP_18 U14 (O[18], I_R[18], S);
  BWCPP_19 U15 (O[19], I_R[19], S);
  BWCPP_20 U16 (O[20], I_R[20], S);
  BWCPP_21 U17 (O[21], I_R[21], S);
  BWCPP_22 U18 (O[22], I_R[22], S);
  BWCPP_23 U19 (O[23], I_R[23], S);
  BWCPP_24 U20 (O[24], I_R[24], S);
  BWCPP_25 U21 (O[25], I_R[25], S);
  BWCPP_26 U22 (O[26], I_R[26], S);
  BWCPP_27 U23 (O[27], I_R[27], S);
  BWCPP_28 U24 (O[28], I_R[28], S);
  BWCPP_29 U25 (O[29], I_R[29], S);
  BWCPP_30 U26 (O[30], I_R[30], S);
  BWCPP_31 U27 (O[31], I_R[31], S);
  BWCPP_32 U28 (O[32], I_R[32], S);
  BWCPP_33 U29 (O[33], I_R[33], S);
  BWCPP_34 U30 (O[34], I_R[34], S);
  BWCPP_35 U31 (O[35], I_R[35], S);
  BWCPP_36 U32 (O[36], I_R[36], S);
  BWCNP_37 U33 (O[37], I_T, S);
endmodule

module TUBWCON_39_6 (O, I_T, I_R, S);
  output [39:6] O;
  input [38:6] I_R;
  input I_T;
  input S;
  BWCPP_6 U0 (O[6], I_R[6], S);
  BWCPP_7 U1 (O[7], I_R[7], S);
  BWCPP_8 U2 (O[8], I_R[8], S);
  BWCPP_9 U3 (O[9], I_R[9], S);
  BWCPP_10 U4 (O[10], I_R[10], S);
  BWCPP_11 U5 (O[11], I_R[11], S);
  BWCPP_12 U6 (O[12], I_R[12], S);
  BWCPP_13 U7 (O[13], I_R[13], S);
  BWCPP_14 U8 (O[14], I_R[14], S);
  BWCPP_15 U9 (O[15], I_R[15], S);
  BWCPP_16 U10 (O[16], I_R[16], S);
  BWCPP_17 U11 (O[17], I_R[17], S);
  BWCPP_18 U12 (O[18], I_R[18], S);
  BWCPP_19 U13 (O[19], I_R[19], S);
  BWCPP_20 U14 (O[20], I_R[20], S);
  BWCPP_21 U15 (O[21], I_R[21], S);
  BWCPP_22 U16 (O[22], I_R[22], S);
  BWCPP_23 U17 (O[23], I_R[23], S);
  BWCPP_24 U18 (O[24], I_R[24], S);
  BWCPP_25 U19 (O[25], I_R[25], S);
  BWCPP_26 U20 (O[26], I_R[26], S);
  BWCPP_27 U21 (O[27], I_R[27], S);
  BWCPP_28 U22 (O[28], I_R[28], S);
  BWCPP_29 U23 (O[29], I_R[29], S);
  BWCPP_30 U24 (O[30], I_R[30], S);
  BWCPP_31 U25 (O[31], I_R[31], S);
  BWCPP_32 U26 (O[32], I_R[32], S);
  BWCPP_33 U27 (O[33], I_R[33], S);
  BWCPP_34 U28 (O[34], I_R[34], S);
  BWCPP_35 U29 (O[35], I_R[35], S);
  BWCPP_36 U30 (O[36], I_R[36], S);
  BWCPP_37 U31 (O[37], I_R[37], S);
  BWCPP_38 U32 (O[38], I_R[38], S);
  BWCNP_39 U33 (O[39], I_T, S);
endmodule

module TUBWCON_41_8 (O, I_T, I_R, S);
  output [41:8] O;
  input [40:8] I_R;
  input I_T;
  input S;
  BWCPP_8 U0 (O[8], I_R[8], S);
  BWCPP_9 U1 (O[9], I_R[9], S);
  BWCPP_10 U2 (O[10], I_R[10], S);
  BWCPP_11 U3 (O[11], I_R[11], S);
  BWCPP_12 U4 (O[12], I_R[12], S);
  BWCPP_13 U5 (O[13], I_R[13], S);
  BWCPP_14 U6 (O[14], I_R[14], S);
  BWCPP_15 U7 (O[15], I_R[15], S);
  BWCPP_16 U8 (O[16], I_R[16], S);
  BWCPP_17 U9 (O[17], I_R[17], S);
  BWCPP_18 U10 (O[18], I_R[18], S);
  BWCPP_19 U11 (O[19], I_R[19], S);
  BWCPP_20 U12 (O[20], I_R[20], S);
  BWCPP_21 U13 (O[21], I_R[21], S);
  BWCPP_22 U14 (O[22], I_R[22], S);
  BWCPP_23 U15 (O[23], I_R[23], S);
  BWCPP_24 U16 (O[24], I_R[24], S);
  BWCPP_25 U17 (O[25], I_R[25], S);
  BWCPP_26 U18 (O[26], I_R[26], S);
  BWCPP_27 U19 (O[27], I_R[27], S);
  BWCPP_28 U20 (O[28], I_R[28], S);
  BWCPP_29 U21 (O[29], I_R[29], S);
  BWCPP_30 U22 (O[30], I_R[30], S);
  BWCPP_31 U23 (O[31], I_R[31], S);
  BWCPP_32 U24 (O[32], I_R[32], S);
  BWCPP_33 U25 (O[33], I_R[33], S);
  BWCPP_34 U26 (O[34], I_R[34], S);
  BWCPP_35 U27 (O[35], I_R[35], S);
  BWCPP_36 U28 (O[36], I_R[36], S);
  BWCPP_37 U29 (O[37], I_R[37], S);
  BWCPP_38 U30 (O[38], I_R[38], S);
  BWCPP_39 U31 (O[39], I_R[39], S);
  BWCPP_40 U32 (O[40], I_R[40], S);
  BWCNP_41 U33 (O[41], I_T, S);
endmodule

module TUBWCON_43_10 (O, I_T, I_R, S);
  output [43:10] O;
  input [42:10] I_R;
  input I_T;
  input S;
  BWCPP_10 U0 (O[10], I_R[10], S);
  BWCPP_11 U1 (O[11], I_R[11], S);
  BWCPP_12 U2 (O[12], I_R[12], S);
  BWCPP_13 U3 (O[13], I_R[13], S);
  BWCPP_14 U4 (O[14], I_R[14], S);
  BWCPP_15 U5 (O[15], I_R[15], S);
  BWCPP_16 U6 (O[16], I_R[16], S);
  BWCPP_17 U7 (O[17], I_R[17], S);
  BWCPP_18 U8 (O[18], I_R[18], S);
  BWCPP_19 U9 (O[19], I_R[19], S);
  BWCPP_20 U10 (O[20], I_R[20], S);
  BWCPP_21 U11 (O[21], I_R[21], S);
  BWCPP_22 U12 (O[22], I_R[22], S);
  BWCPP_23 U13 (O[23], I_R[23], S);
  BWCPP_24 U14 (O[24], I_R[24], S);
  BWCPP_25 U15 (O[25], I_R[25], S);
  BWCPP_26 U16 (O[26], I_R[26], S);
  BWCPP_27 U17 (O[27], I_R[27], S);
  BWCPP_28 U18 (O[28], I_R[28], S);
  BWCPP_29 U19 (O[29], I_R[29], S);
  BWCPP_30 U20 (O[30], I_R[30], S);
  BWCPP_31 U21 (O[31], I_R[31], S);
  BWCPP_32 U22 (O[32], I_R[32], S);
  BWCPP_33 U23 (O[33], I_R[33], S);
  BWCPP_34 U24 (O[34], I_R[34], S);
  BWCPP_35 U25 (O[35], I_R[35], S);
  BWCPP_36 U26 (O[36], I_R[36], S);
  BWCPP_37 U27 (O[37], I_R[37], S);
  BWCPP_38 U28 (O[38], I_R[38], S);
  BWCPP_39 U29 (O[39], I_R[39], S);
  BWCPP_40 U30 (O[40], I_R[40], S);
  BWCPP_41 U31 (O[41], I_R[41], S);
  BWCPP_42 U32 (O[42], I_R[42], S);
  BWCNP_43 U33 (O[43], I_T, S);
endmodule

module TUBWCON_45_12 (O, I_T, I_R, S);
  output [45:12] O;
  input [44:12] I_R;
  input I_T;
  input S;
  BWCPP_12 U0 (O[12], I_R[12], S);
  BWCPP_13 U1 (O[13], I_R[13], S);
  BWCPP_14 U2 (O[14], I_R[14], S);
  BWCPP_15 U3 (O[15], I_R[15], S);
  BWCPP_16 U4 (O[16], I_R[16], S);
  BWCPP_17 U5 (O[17], I_R[17], S);
  BWCPP_18 U6 (O[18], I_R[18], S);
  BWCPP_19 U7 (O[19], I_R[19], S);
  BWCPP_20 U8 (O[20], I_R[20], S);
  BWCPP_21 U9 (O[21], I_R[21], S);
  BWCPP_22 U10 (O[22], I_R[22], S);
  BWCPP_23 U11 (O[23], I_R[23], S);
  BWCPP_24 U12 (O[24], I_R[24], S);
  BWCPP_25 U13 (O[25], I_R[25], S);
  BWCPP_26 U14 (O[26], I_R[26], S);
  BWCPP_27 U15 (O[27], I_R[27], S);
  BWCPP_28 U16 (O[28], I_R[28], S);
  BWCPP_29 U17 (O[29], I_R[29], S);
  BWCPP_30 U18 (O[30], I_R[30], S);
  BWCPP_31 U19 (O[31], I_R[31], S);
  BWCPP_32 U20 (O[32], I_R[32], S);
  BWCPP_33 U21 (O[33], I_R[33], S);
  BWCPP_34 U22 (O[34], I_R[34], S);
  BWCPP_35 U23 (O[35], I_R[35], S);
  BWCPP_36 U24 (O[36], I_R[36], S);
  BWCPP_37 U25 (O[37], I_R[37], S);
  BWCPP_38 U26 (O[38], I_R[38], S);
  BWCPP_39 U27 (O[39], I_R[39], S);
  BWCPP_40 U28 (O[40], I_R[40], S);
  BWCPP_41 U29 (O[41], I_R[41], S);
  BWCPP_42 U30 (O[42], I_R[42], S);
  BWCPP_43 U31 (O[43], I_R[43], S);
  BWCPP_44 U32 (O[44], I_R[44], S);
  BWCNP_45 U33 (O[45], I_T, S);
endmodule

module TUBWCON_47_14 (O, I_T, I_R, S);
  output [47:14] O;
  input [46:14] I_R;
  input I_T;
  input S;
  BWCPP_14 U0 (O[14], I_R[14], S);
  BWCPP_15 U1 (O[15], I_R[15], S);
  BWCPP_16 U2 (O[16], I_R[16], S);
  BWCPP_17 U3 (O[17], I_R[17], S);
  BWCPP_18 U4 (O[18], I_R[18], S);
  BWCPP_19 U5 (O[19], I_R[19], S);
  BWCPP_20 U6 (O[20], I_R[20], S);
  BWCPP_21 U7 (O[21], I_R[21], S);
  BWCPP_22 U8 (O[22], I_R[22], S);
  BWCPP_23 U9 (O[23], I_R[23], S);
  BWCPP_24 U10 (O[24], I_R[24], S);
  BWCPP_25 U11 (O[25], I_R[25], S);
  BWCPP_26 U12 (O[26], I_R[26], S);
  BWCPP_27 U13 (O[27], I_R[27], S);
  BWCPP_28 U14 (O[28], I_R[28], S);
  BWCPP_29 U15 (O[29], I_R[29], S);
  BWCPP_30 U16 (O[30], I_R[30], S);
  BWCPP_31 U17 (O[31], I_R[31], S);
  BWCPP_32 U18 (O[32], I_R[32], S);
  BWCPP_33 U19 (O[33], I_R[33], S);
  BWCPP_34 U20 (O[34], I_R[34], S);
  BWCPP_35 U21 (O[35], I_R[35], S);
  BWCPP_36 U22 (O[36], I_R[36], S);
  BWCPP_37 U23 (O[37], I_R[37], S);
  BWCPP_38 U24 (O[38], I_R[38], S);
  BWCPP_39 U25 (O[39], I_R[39], S);
  BWCPP_40 U26 (O[40], I_R[40], S);
  BWCPP_41 U27 (O[41], I_R[41], S);
  BWCPP_42 U28 (O[42], I_R[42], S);
  BWCPP_43 U29 (O[43], I_R[43], S);
  BWCPP_44 U30 (O[44], I_R[44], S);
  BWCPP_45 U31 (O[45], I_R[45], S);
  BWCPP_46 U32 (O[46], I_R[46], S);
  BWCNP_47 U33 (O[47], I_T, S);
endmodule

module TUBWCON_49_16 (O, I_T, I_R, S);
  output [49:16] O;
  input [48:16] I_R;
  input I_T;
  input S;
  BWCPP_16 U0 (O[16], I_R[16], S);
  BWCPP_17 U1 (O[17], I_R[17], S);
  BWCPP_18 U2 (O[18], I_R[18], S);
  BWCPP_19 U3 (O[19], I_R[19], S);
  BWCPP_20 U4 (O[20], I_R[20], S);
  BWCPP_21 U5 (O[21], I_R[21], S);
  BWCPP_22 U6 (O[22], I_R[22], S);
  BWCPP_23 U7 (O[23], I_R[23], S);
  BWCPP_24 U8 (O[24], I_R[24], S);
  BWCPP_25 U9 (O[25], I_R[25], S);
  BWCPP_26 U10 (O[26], I_R[26], S);
  BWCPP_27 U11 (O[27], I_R[27], S);
  BWCPP_28 U12 (O[28], I_R[28], S);
  BWCPP_29 U13 (O[29], I_R[29], S);
  BWCPP_30 U14 (O[30], I_R[30], S);
  BWCPP_31 U15 (O[31], I_R[31], S);
  BWCPP_32 U16 (O[32], I_R[32], S);
  BWCPP_33 U17 (O[33], I_R[33], S);
  BWCPP_34 U18 (O[34], I_R[34], S);
  BWCPP_35 U19 (O[35], I_R[35], S);
  BWCPP_36 U20 (O[36], I_R[36], S);
  BWCPP_37 U21 (O[37], I_R[37], S);
  BWCPP_38 U22 (O[38], I_R[38], S);
  BWCPP_39 U23 (O[39], I_R[39], S);
  BWCPP_40 U24 (O[40], I_R[40], S);
  BWCPP_41 U25 (O[41], I_R[41], S);
  BWCPP_42 U26 (O[42], I_R[42], S);
  BWCPP_43 U27 (O[43], I_R[43], S);
  BWCPP_44 U28 (O[44], I_R[44], S);
  BWCPP_45 U29 (O[45], I_R[45], S);
  BWCPP_46 U30 (O[46], I_R[46], S);
  BWCPP_47 U31 (O[47], I_R[47], S);
  BWCPP_48 U32 (O[48], I_R[48], S);
  BWCNP_49 U33 (O[49], I_T, S);
endmodule

module TUBWCON_51_18 (O, I_T, I_R, S);
  output [51:18] O;
  input [50:18] I_R;
  input I_T;
  input S;
  BWCPP_18 U0 (O[18], I_R[18], S);
  BWCPP_19 U1 (O[19], I_R[19], S);
  BWCPP_20 U2 (O[20], I_R[20], S);
  BWCPP_21 U3 (O[21], I_R[21], S);
  BWCPP_22 U4 (O[22], I_R[22], S);
  BWCPP_23 U5 (O[23], I_R[23], S);
  BWCPP_24 U6 (O[24], I_R[24], S);
  BWCPP_25 U7 (O[25], I_R[25], S);
  BWCPP_26 U8 (O[26], I_R[26], S);
  BWCPP_27 U9 (O[27], I_R[27], S);
  BWCPP_28 U10 (O[28], I_R[28], S);
  BWCPP_29 U11 (O[29], I_R[29], S);
  BWCPP_30 U12 (O[30], I_R[30], S);
  BWCPP_31 U13 (O[31], I_R[31], S);
  BWCPP_32 U14 (O[32], I_R[32], S);
  BWCPP_33 U15 (O[33], I_R[33], S);
  BWCPP_34 U16 (O[34], I_R[34], S);
  BWCPP_35 U17 (O[35], I_R[35], S);
  BWCPP_36 U18 (O[36], I_R[36], S);
  BWCPP_37 U19 (O[37], I_R[37], S);
  BWCPP_38 U20 (O[38], I_R[38], S);
  BWCPP_39 U21 (O[39], I_R[39], S);
  BWCPP_40 U22 (O[40], I_R[40], S);
  BWCPP_41 U23 (O[41], I_R[41], S);
  BWCPP_42 U24 (O[42], I_R[42], S);
  BWCPP_43 U25 (O[43], I_R[43], S);
  BWCPP_44 U26 (O[44], I_R[44], S);
  BWCPP_45 U27 (O[45], I_R[45], S);
  BWCPP_46 U28 (O[46], I_R[46], S);
  BWCPP_47 U29 (O[47], I_R[47], S);
  BWCPP_48 U30 (O[48], I_R[48], S);
  BWCPP_49 U31 (O[49], I_R[49], S);
  BWCPP_50 U32 (O[50], I_R[50], S);
  BWCNP_51 U33 (O[51], I_T, S);
endmodule

module TUBWCON_53_20 (O, I_T, I_R, S);
  output [53:20] O;
  input [52:20] I_R;
  input I_T;
  input S;
  BWCPP_20 U0 (O[20], I_R[20], S);
  BWCPP_21 U1 (O[21], I_R[21], S);
  BWCPP_22 U2 (O[22], I_R[22], S);
  BWCPP_23 U3 (O[23], I_R[23], S);
  BWCPP_24 U4 (O[24], I_R[24], S);
  BWCPP_25 U5 (O[25], I_R[25], S);
  BWCPP_26 U6 (O[26], I_R[26], S);
  BWCPP_27 U7 (O[27], I_R[27], S);
  BWCPP_28 U8 (O[28], I_R[28], S);
  BWCPP_29 U9 (O[29], I_R[29], S);
  BWCPP_30 U10 (O[30], I_R[30], S);
  BWCPP_31 U11 (O[31], I_R[31], S);
  BWCPP_32 U12 (O[32], I_R[32], S);
  BWCPP_33 U13 (O[33], I_R[33], S);
  BWCPP_34 U14 (O[34], I_R[34], S);
  BWCPP_35 U15 (O[35], I_R[35], S);
  BWCPP_36 U16 (O[36], I_R[36], S);
  BWCPP_37 U17 (O[37], I_R[37], S);
  BWCPP_38 U18 (O[38], I_R[38], S);
  BWCPP_39 U19 (O[39], I_R[39], S);
  BWCPP_40 U20 (O[40], I_R[40], S);
  BWCPP_41 U21 (O[41], I_R[41], S);
  BWCPP_42 U22 (O[42], I_R[42], S);
  BWCPP_43 U23 (O[43], I_R[43], S);
  BWCPP_44 U24 (O[44], I_R[44], S);
  BWCPP_45 U25 (O[45], I_R[45], S);
  BWCPP_46 U26 (O[46], I_R[46], S);
  BWCPP_47 U27 (O[47], I_R[47], S);
  BWCPP_48 U28 (O[48], I_R[48], S);
  BWCPP_49 U29 (O[49], I_R[49], S);
  BWCPP_50 U30 (O[50], I_R[50], S);
  BWCPP_51 U31 (O[51], I_R[51], S);
  BWCPP_52 U32 (O[52], I_R[52], S);
  BWCNP_53 U33 (O[53], I_T, S);
endmodule

module TUBWCON_55_22 (O, I_T, I_R, S);
  output [55:22] O;
  input [54:22] I_R;
  input I_T;
  input S;
  BWCPP_22 U0 (O[22], I_R[22], S);
  BWCPP_23 U1 (O[23], I_R[23], S);
  BWCPP_24 U2 (O[24], I_R[24], S);
  BWCPP_25 U3 (O[25], I_R[25], S);
  BWCPP_26 U4 (O[26], I_R[26], S);
  BWCPP_27 U5 (O[27], I_R[27], S);
  BWCPP_28 U6 (O[28], I_R[28], S);
  BWCPP_29 U7 (O[29], I_R[29], S);
  BWCPP_30 U8 (O[30], I_R[30], S);
  BWCPP_31 U9 (O[31], I_R[31], S);
  BWCPP_32 U10 (O[32], I_R[32], S);
  BWCPP_33 U11 (O[33], I_R[33], S);
  BWCPP_34 U12 (O[34], I_R[34], S);
  BWCPP_35 U13 (O[35], I_R[35], S);
  BWCPP_36 U14 (O[36], I_R[36], S);
  BWCPP_37 U15 (O[37], I_R[37], S);
  BWCPP_38 U16 (O[38], I_R[38], S);
  BWCPP_39 U17 (O[39], I_R[39], S);
  BWCPP_40 U18 (O[40], I_R[40], S);
  BWCPP_41 U19 (O[41], I_R[41], S);
  BWCPP_42 U20 (O[42], I_R[42], S);
  BWCPP_43 U21 (O[43], I_R[43], S);
  BWCPP_44 U22 (O[44], I_R[44], S);
  BWCPP_45 U23 (O[45], I_R[45], S);
  BWCPP_46 U24 (O[46], I_R[46], S);
  BWCPP_47 U25 (O[47], I_R[47], S);
  BWCPP_48 U26 (O[48], I_R[48], S);
  BWCPP_49 U27 (O[49], I_R[49], S);
  BWCPP_50 U28 (O[50], I_R[50], S);
  BWCPP_51 U29 (O[51], I_R[51], S);
  BWCPP_52 U30 (O[52], I_R[52], S);
  BWCPP_53 U31 (O[53], I_R[53], S);
  BWCPP_54 U32 (O[54], I_R[54], S);
  BWCNP_55 U33 (O[55], I_T, S);
endmodule

module TUBWCON_57_24 (O, I_T, I_R, S);
  output [57:24] O;
  input [56:24] I_R;
  input I_T;
  input S;
  BWCPP_24 U0 (O[24], I_R[24], S);
  BWCPP_25 U1 (O[25], I_R[25], S);
  BWCPP_26 U2 (O[26], I_R[26], S);
  BWCPP_27 U3 (O[27], I_R[27], S);
  BWCPP_28 U4 (O[28], I_R[28], S);
  BWCPP_29 U5 (O[29], I_R[29], S);
  BWCPP_30 U6 (O[30], I_R[30], S);
  BWCPP_31 U7 (O[31], I_R[31], S);
  BWCPP_32 U8 (O[32], I_R[32], S);
  BWCPP_33 U9 (O[33], I_R[33], S);
  BWCPP_34 U10 (O[34], I_R[34], S);
  BWCPP_35 U11 (O[35], I_R[35], S);
  BWCPP_36 U12 (O[36], I_R[36], S);
  BWCPP_37 U13 (O[37], I_R[37], S);
  BWCPP_38 U14 (O[38], I_R[38], S);
  BWCPP_39 U15 (O[39], I_R[39], S);
  BWCPP_40 U16 (O[40], I_R[40], S);
  BWCPP_41 U17 (O[41], I_R[41], S);
  BWCPP_42 U18 (O[42], I_R[42], S);
  BWCPP_43 U19 (O[43], I_R[43], S);
  BWCPP_44 U20 (O[44], I_R[44], S);
  BWCPP_45 U21 (O[45], I_R[45], S);
  BWCPP_46 U22 (O[46], I_R[46], S);
  BWCPP_47 U23 (O[47], I_R[47], S);
  BWCPP_48 U24 (O[48], I_R[48], S);
  BWCPP_49 U25 (O[49], I_R[49], S);
  BWCPP_50 U26 (O[50], I_R[50], S);
  BWCPP_51 U27 (O[51], I_R[51], S);
  BWCPP_52 U28 (O[52], I_R[52], S);
  BWCPP_53 U29 (O[53], I_R[53], S);
  BWCPP_54 U30 (O[54], I_R[54], S);
  BWCPP_55 U31 (O[55], I_R[55], S);
  BWCPP_56 U32 (O[56], I_R[56], S);
  BWCNP_57 U33 (O[57], I_T, S);
endmodule

module TUBWCON_59_26 (O, I_T, I_R, S);
  output [59:26] O;
  input [58:26] I_R;
  input I_T;
  input S;
  BWCPP_26 U0 (O[26], I_R[26], S);
  BWCPP_27 U1 (O[27], I_R[27], S);
  BWCPP_28 U2 (O[28], I_R[28], S);
  BWCPP_29 U3 (O[29], I_R[29], S);
  BWCPP_30 U4 (O[30], I_R[30], S);
  BWCPP_31 U5 (O[31], I_R[31], S);
  BWCPP_32 U6 (O[32], I_R[32], S);
  BWCPP_33 U7 (O[33], I_R[33], S);
  BWCPP_34 U8 (O[34], I_R[34], S);
  BWCPP_35 U9 (O[35], I_R[35], S);
  BWCPP_36 U10 (O[36], I_R[36], S);
  BWCPP_37 U11 (O[37], I_R[37], S);
  BWCPP_38 U12 (O[38], I_R[38], S);
  BWCPP_39 U13 (O[39], I_R[39], S);
  BWCPP_40 U14 (O[40], I_R[40], S);
  BWCPP_41 U15 (O[41], I_R[41], S);
  BWCPP_42 U16 (O[42], I_R[42], S);
  BWCPP_43 U17 (O[43], I_R[43], S);
  BWCPP_44 U18 (O[44], I_R[44], S);
  BWCPP_45 U19 (O[45], I_R[45], S);
  BWCPP_46 U20 (O[46], I_R[46], S);
  BWCPP_47 U21 (O[47], I_R[47], S);
  BWCPP_48 U22 (O[48], I_R[48], S);
  BWCPP_49 U23 (O[49], I_R[49], S);
  BWCPP_50 U24 (O[50], I_R[50], S);
  BWCPP_51 U25 (O[51], I_R[51], S);
  BWCPP_52 U26 (O[52], I_R[52], S);
  BWCPP_53 U27 (O[53], I_R[53], S);
  BWCPP_54 U28 (O[54], I_R[54], S);
  BWCPP_55 U29 (O[55], I_R[55], S);
  BWCPP_56 U30 (O[56], I_R[56], S);
  BWCPP_57 U31 (O[57], I_R[57], S);
  BWCPP_58 U32 (O[58], I_R[58], S);
  BWCNP_59 U33 (O[59], I_T, S);
endmodule

module TUBWCON_61_28 (O, I_T, I_R, S);
  output [61:28] O;
  input [60:28] I_R;
  input I_T;
  input S;
  BWCPP_28 U0 (O[28], I_R[28], S);
  BWCPP_29 U1 (O[29], I_R[29], S);
  BWCPP_30 U2 (O[30], I_R[30], S);
  BWCPP_31 U3 (O[31], I_R[31], S);
  BWCPP_32 U4 (O[32], I_R[32], S);
  BWCPP_33 U5 (O[33], I_R[33], S);
  BWCPP_34 U6 (O[34], I_R[34], S);
  BWCPP_35 U7 (O[35], I_R[35], S);
  BWCPP_36 U8 (O[36], I_R[36], S);
  BWCPP_37 U9 (O[37], I_R[37], S);
  BWCPP_38 U10 (O[38], I_R[38], S);
  BWCPP_39 U11 (O[39], I_R[39], S);
  BWCPP_40 U12 (O[40], I_R[40], S);
  BWCPP_41 U13 (O[41], I_R[41], S);
  BWCPP_42 U14 (O[42], I_R[42], S);
  BWCPP_43 U15 (O[43], I_R[43], S);
  BWCPP_44 U16 (O[44], I_R[44], S);
  BWCPP_45 U17 (O[45], I_R[45], S);
  BWCPP_46 U18 (O[46], I_R[46], S);
  BWCPP_47 U19 (O[47], I_R[47], S);
  BWCPP_48 U20 (O[48], I_R[48], S);
  BWCPP_49 U21 (O[49], I_R[49], S);
  BWCPP_50 U22 (O[50], I_R[50], S);
  BWCPP_51 U23 (O[51], I_R[51], S);
  BWCPP_52 U24 (O[52], I_R[52], S);
  BWCPP_53 U25 (O[53], I_R[53], S);
  BWCPP_54 U26 (O[54], I_R[54], S);
  BWCPP_55 U27 (O[55], I_R[55], S);
  BWCPP_56 U28 (O[56], I_R[56], S);
  BWCPP_57 U29 (O[57], I_R[57], S);
  BWCPP_58 U30 (O[58], I_R[58], S);
  BWCPP_59 U31 (O[59], I_R[59], S);
  BWCPP_60 U32 (O[60], I_R[60], S);
  BWCNP_61 U33 (O[61], I_T, S);
endmodule

module TUBWCON_63_30 (O, I_T, I_R, S);
  output [63:30] O;
  input [62:30] I_R;
  input I_T;
  input S;
  BWCPP_30 U0 (O[30], I_R[30], S);
  BWCPP_31 U1 (O[31], I_R[31], S);
  BWCPP_32 U2 (O[32], I_R[32], S);
  BWCPP_33 U3 (O[33], I_R[33], S);
  BWCPP_34 U4 (O[34], I_R[34], S);
  BWCPP_35 U5 (O[35], I_R[35], S);
  BWCPP_36 U6 (O[36], I_R[36], S);
  BWCPP_37 U7 (O[37], I_R[37], S);
  BWCPP_38 U8 (O[38], I_R[38], S);
  BWCPP_39 U9 (O[39], I_R[39], S);
  BWCPP_40 U10 (O[40], I_R[40], S);
  BWCPP_41 U11 (O[41], I_R[41], S);
  BWCPP_42 U12 (O[42], I_R[42], S);
  BWCPP_43 U13 (O[43], I_R[43], S);
  BWCPP_44 U14 (O[44], I_R[44], S);
  BWCPP_45 U15 (O[45], I_R[45], S);
  BWCPP_46 U16 (O[46], I_R[46], S);
  BWCPP_47 U17 (O[47], I_R[47], S);
  BWCPP_48 U18 (O[48], I_R[48], S);
  BWCPP_49 U19 (O[49], I_R[49], S);
  BWCPP_50 U20 (O[50], I_R[50], S);
  BWCPP_51 U21 (O[51], I_R[51], S);
  BWCPP_52 U22 (O[52], I_R[52], S);
  BWCPP_53 U23 (O[53], I_R[53], S);
  BWCPP_54 U24 (O[54], I_R[54], S);
  BWCPP_55 U25 (O[55], I_R[55], S);
  BWCPP_56 U26 (O[56], I_R[56], S);
  BWCPP_57 U27 (O[57], I_R[57], S);
  BWCPP_58 U28 (O[58], I_R[58], S);
  BWCPP_59 U29 (O[59], I_R[59], S);
  BWCPP_60 U30 (O[60], I_R[60], S);
  BWCPP_61 U31 (O[61], I_R[61], S);
  BWCPP_62 U32 (O[62], I_R[62], S);
  BWCNP_63 U33 (O[63], I_T, S);
endmodule

module TUBWCON_65_32 (O, I_T, I_R, S);
  output [65:32] O;
  input [64:32] I_R;
  input I_T;
  input S;
  BWCPP_32 U0 (O[32], I_R[32], S);
  BWCPP_33 U1 (O[33], I_R[33], S);
  BWCPP_34 U2 (O[34], I_R[34], S);
  BWCPP_35 U3 (O[35], I_R[35], S);
  BWCPP_36 U4 (O[36], I_R[36], S);
  BWCPP_37 U5 (O[37], I_R[37], S);
  BWCPP_38 U6 (O[38], I_R[38], S);
  BWCPP_39 U7 (O[39], I_R[39], S);
  BWCPP_40 U8 (O[40], I_R[40], S);
  BWCPP_41 U9 (O[41], I_R[41], S);
  BWCPP_42 U10 (O[42], I_R[42], S);
  BWCPP_43 U11 (O[43], I_R[43], S);
  BWCPP_44 U12 (O[44], I_R[44], S);
  BWCPP_45 U13 (O[45], I_R[45], S);
  BWCPP_46 U14 (O[46], I_R[46], S);
  BWCPP_47 U15 (O[47], I_R[47], S);
  BWCPP_48 U16 (O[48], I_R[48], S);
  BWCPP_49 U17 (O[49], I_R[49], S);
  BWCPP_50 U18 (O[50], I_R[50], S);
  BWCPP_51 U19 (O[51], I_R[51], S);
  BWCPP_52 U20 (O[52], I_R[52], S);
  BWCPP_53 U21 (O[53], I_R[53], S);
  BWCPP_54 U22 (O[54], I_R[54], S);
  BWCPP_55 U23 (O[55], I_R[55], S);
  BWCPP_56 U24 (O[56], I_R[56], S);
  BWCPP_57 U25 (O[57], I_R[57], S);
  BWCPP_58 U26 (O[58], I_R[58], S);
  BWCPP_59 U27 (O[59], I_R[59], S);
  BWCPP_60 U28 (O[60], I_R[60], S);
  BWCPP_61 U29 (O[61], I_R[61], S);
  BWCPP_62 U30 (O[62], I_R[62], S);
  BWCPP_63 U31 (O[63], I_R[63], S);
  BWCPP_64 U32 (O[64], I_R[64], S);
  BWCNP_65 U33 (O[65], I_T, S);
endmodule

module UBCLA_67_6_66_0 (S, X, Y);
  output [68:0] S;
  input [67:6] X;
  input [66:0] Y;
  wire [67:6] Z;
  UBExtender_66_6_6000 U0 (Z[67:6], Y[66:6]);
  UBPureCLA_67_6 U1 (S[68:6], X[67:6], Z[67:6]);
  UBCON_5_0 U2 (S[5:0], Y[5:0]);
endmodule

module UBCMBIN_33_33_32_000 (O, IN0, IN1);
  output [33:32] O;
  input IN0;
  input IN1;
  UB1DCON_33 U0 (O[33], IN0);
  UB1DCON_32 U1 (O[32], IN1);
endmodule

module UBCMBIN_34_34_33_000 (O, IN0, IN1);
  output [34:0] O;
  input IN0;
  input [33:0] IN1;
  UB1DCON_34 U0 (O[34], IN0);
  UBCON_33_0 U1 (O[33:0], IN1);
endmodule

module UBCMBIN_36_36_35_000 (O, IN0, IN1, IN2);
  output [36:0] O;
  input IN0;
  input [35:2] IN1;
  input IN2;
  UB1DCON_36 U0 (O[36], IN0);
  UBCON_35_2 U1 (O[35:2], IN1);
  UBZero_1_1 U2 (O[1]);
  UB1DCON_0 U3 (O[0], IN2);
endmodule

module UBCMBIN_38_38_37_000 (O, IN0, IN1, IN2);
  output [38:2] O;
  input IN0;
  input [37:4] IN1;
  input IN2;
  UB1DCON_38 U0 (O[38], IN0);
  UBCON_37_4 U1 (O[37:4], IN1);
  UBZero_3_3 U2 (O[3]);
  UB1DCON_2 U3 (O[2], IN2);
endmodule

module UBCMBIN_40_40_39_000 (O, IN0, IN1, IN2);
  output [40:4] O;
  input IN0;
  input [39:6] IN1;
  input IN2;
  UB1DCON_40 U0 (O[40], IN0);
  UBCON_39_6 U1 (O[39:6], IN1);
  UBZero_5_5 U2 (O[5]);
  UB1DCON_4 U3 (O[4], IN2);
endmodule

module UBCMBIN_42_42_41_000 (O, IN0, IN1, IN2);
  output [42:6] O;
  input IN0;
  input [41:8] IN1;
  input IN2;
  UB1DCON_42 U0 (O[42], IN0);
  UBCON_41_8 U1 (O[41:8], IN1);
  UBZero_7_7 U2 (O[7]);
  UB1DCON_6 U3 (O[6], IN2);
endmodule

module UBCMBIN_44_44_43_000 (O, IN0, IN1, IN2);
  output [44:8] O;
  input IN0;
  input [43:10] IN1;
  input IN2;
  UB1DCON_44 U0 (O[44], IN0);
  UBCON_43_10 U1 (O[43:10], IN1);
  UBZero_9_9 U2 (O[9]);
  UB1DCON_8 U3 (O[8], IN2);
endmodule

module UBCMBIN_46_46_45_000 (O, IN0, IN1, IN2);
  output [46:10] O;
  input IN0;
  input [45:12] IN1;
  input IN2;
  UB1DCON_46 U0 (O[46], IN0);
  UBCON_45_12 U1 (O[45:12], IN1);
  UBZero_11_11 U2 (O[11]);
  UB1DCON_10 U3 (O[10], IN2);
endmodule

module UBCMBIN_48_48_47_000 (O, IN0, IN1, IN2);
  output [48:12] O;
  input IN0;
  input [47:14] IN1;
  input IN2;
  UB1DCON_48 U0 (O[48], IN0);
  UBCON_47_14 U1 (O[47:14], IN1);
  UBZero_13_13 U2 (O[13]);
  UB1DCON_12 U3 (O[12], IN2);
endmodule

module UBCMBIN_50_50_49_000 (O, IN0, IN1, IN2);
  output [50:14] O;
  input IN0;
  input [49:16] IN1;
  input IN2;
  UB1DCON_50 U0 (O[50], IN0);
  UBCON_49_16 U1 (O[49:16], IN1);
  UBZero_15_15 U2 (O[15]);
  UB1DCON_14 U3 (O[14], IN2);
endmodule

module UBCMBIN_52_52_51_000 (O, IN0, IN1, IN2);
  output [52:16] O;
  input IN0;
  input [51:18] IN1;
  input IN2;
  UB1DCON_52 U0 (O[52], IN0);
  UBCON_51_18 U1 (O[51:18], IN1);
  UBZero_17_17 U2 (O[17]);
  UB1DCON_16 U3 (O[16], IN2);
endmodule

module UBCMBIN_54_54_53_000 (O, IN0, IN1, IN2);
  output [54:18] O;
  input IN0;
  input [53:20] IN1;
  input IN2;
  UB1DCON_54 U0 (O[54], IN0);
  UBCON_53_20 U1 (O[53:20], IN1);
  UBZero_19_19 U2 (O[19]);
  UB1DCON_18 U3 (O[18], IN2);
endmodule

module UBCMBIN_56_56_55_000 (O, IN0, IN1, IN2);
  output [56:20] O;
  input IN0;
  input [55:22] IN1;
  input IN2;
  UB1DCON_56 U0 (O[56], IN0);
  UBCON_55_22 U1 (O[55:22], IN1);
  UBZero_21_21 U2 (O[21]);
  UB1DCON_20 U3 (O[20], IN2);
endmodule

module UBCMBIN_58_58_57_000 (O, IN0, IN1, IN2);
  output [58:22] O;
  input IN0;
  input [57:24] IN1;
  input IN2;
  UB1DCON_58 U0 (O[58], IN0);
  UBCON_57_24 U1 (O[57:24], IN1);
  UBZero_23_23 U2 (O[23]);
  UB1DCON_22 U3 (O[22], IN2);
endmodule

module UBCMBIN_60_60_59_000 (O, IN0, IN1, IN2);
  output [60:24] O;
  input IN0;
  input [59:26] IN1;
  input IN2;
  UB1DCON_60 U0 (O[60], IN0);
  UBCON_59_26 U1 (O[59:26], IN1);
  UBZero_25_25 U2 (O[25]);
  UB1DCON_24 U3 (O[24], IN2);
endmodule

module UBCMBIN_62_62_61_000 (O, IN0, IN1, IN2);
  output [62:26] O;
  input IN0;
  input [61:28] IN1;
  input IN2;
  UB1DCON_62 U0 (O[62], IN0);
  UBCON_61_28 U1 (O[61:28], IN1);
  UBZero_27_27 U2 (O[27]);
  UB1DCON_26 U3 (O[26], IN2);
endmodule

module UBCMBIN_64_64_63_000 (O, IN0, IN1, IN2);
  output [64:28] O;
  input IN0;
  input [63:30] IN1;
  input IN2;
  UB1DCON_64 U0 (O[64], IN0);
  UBCON_63_30 U1 (O[63:30], IN1);
  UBZero_29_29 U2 (O[29]);
  UB1DCON_28 U3 (O[28], IN2);
endmodule

module UBCMBIN_65_32_30_000 (O, IN0, IN1);
  output [65:30] O;
  input [65:32] IN0;
  input IN1;
  UBCON_65_32 U0 (O[65:32], IN0);
  UBZero_31_31 U1 (O[31]);
  UB1DCON_30 U2 (O[30], IN1);
endmodule

module UBCON_11_10 (O, I);
  output [11:10] O;
  input [11:10] I;
  UB1DCON_10 U0 (O[10], I[10]);
  UB1DCON_11 U1 (O[11], I[11]);
endmodule

module UBCON_15_11 (O, I);
  output [15:11] O;
  input [15:11] I;
  UB1DCON_11 U0 (O[11], I[11]);
  UB1DCON_12 U1 (O[12], I[12]);
  UB1DCON_13 U2 (O[13], I[13]);
  UB1DCON_14 U3 (O[14], I[14]);
  UB1DCON_15 U4 (O[15], I[15]);
endmodule

module UBCON_17_16 (O, I);
  output [17:16] O;
  input [17:16] I;
  UB1DCON_16 U0 (O[16], I[16]);
  UB1DCON_17 U1 (O[17], I[17]);
endmodule

module UBCON_18_16 (O, I);
  output [18:16] O;
  input [18:16] I;
  UB1DCON_16 U0 (O[16], I[16]);
  UB1DCON_17 U1 (O[17], I[17]);
  UB1DCON_18 U2 (O[18], I[18]);
endmodule

module UBCON_1_0 (O, I);
  output [1:0] O;
  input [1:0] I;
  UB1DCON_0 U0 (O[0], I[0]);
  UB1DCON_1 U1 (O[1], I[1]);
endmodule

module UBCON_23_22 (O, I);
  output [23:22] O;
  input [23:22] I;
  UB1DCON_22 U0 (O[22], I[22]);
  UB1DCON_23 U1 (O[23], I[23]);
endmodule

module UBCON_24_17 (O, I);
  output [24:17] O;
  input [24:17] I;
  UB1DCON_17 U0 (O[17], I[17]);
  UB1DCON_18 U1 (O[18], I[18]);
  UB1DCON_19 U2 (O[19], I[19]);
  UB1DCON_20 U3 (O[20], I[20]);
  UB1DCON_21 U4 (O[21], I[21]);
  UB1DCON_22 U5 (O[22], I[22]);
  UB1DCON_23 U6 (O[23], I[23]);
  UB1DCON_24 U7 (O[24], I[24]);
endmodule

module UBCON_27_25 (O, I);
  output [27:25] O;
  input [27:25] I;
  UB1DCON_25 U0 (O[25], I[25]);
  UB1DCON_26 U1 (O[26], I[26]);
  UB1DCON_27 U2 (O[27], I[27]);
endmodule

module UBCON_28_25 (O, I);
  output [28:25] O;
  input [28:25] I;
  UB1DCON_25 U0 (O[25], I[25]);
  UB1DCON_26 U1 (O[26], I[26]);
  UB1DCON_27 U2 (O[27], I[27]);
  UB1DCON_28 U3 (O[28], I[28]);
endmodule

module UBCON_2_0 (O, I);
  output [2:0] O;
  input [2:0] I;
  UB1DCON_0 U0 (O[0], I[0]);
  UB1DCON_1 U1 (O[1], I[1]);
  UB1DCON_2 U2 (O[2], I[2]);
endmodule

module UBCON_33_0 (O, I);
  output [33:0] O;
  input [33:0] I;
  UB1DCON_0 U0 (O[0], I[0]);
  UB1DCON_1 U1 (O[1], I[1]);
  UB1DCON_2 U2 (O[2], I[2]);
  UB1DCON_3 U3 (O[3], I[3]);
  UB1DCON_4 U4 (O[4], I[4]);
  UB1DCON_5 U5 (O[5], I[5]);
  UB1DCON_6 U6 (O[6], I[6]);
  UB1DCON_7 U7 (O[7], I[7]);
  UB1DCON_8 U8 (O[8], I[8]);
  UB1DCON_9 U9 (O[9], I[9]);
  UB1DCON_10 U10 (O[10], I[10]);
  UB1DCON_11 U11 (O[11], I[11]);
  UB1DCON_12 U12 (O[12], I[12]);
  UB1DCON_13 U13 (O[13], I[13]);
  UB1DCON_14 U14 (O[14], I[14]);
  UB1DCON_15 U15 (O[15], I[15]);
  UB1DCON_16 U16 (O[16], I[16]);
  UB1DCON_17 U17 (O[17], I[17]);
  UB1DCON_18 U18 (O[18], I[18]);
  UB1DCON_19 U19 (O[19], I[19]);
  UB1DCON_20 U20 (O[20], I[20]);
  UB1DCON_21 U21 (O[21], I[21]);
  UB1DCON_22 U22 (O[22], I[22]);
  UB1DCON_23 U23 (O[23], I[23]);
  UB1DCON_24 U24 (O[24], I[24]);
  UB1DCON_25 U25 (O[25], I[25]);
  UB1DCON_26 U26 (O[26], I[26]);
  UB1DCON_27 U27 (O[27], I[27]);
  UB1DCON_28 U28 (O[28], I[28]);
  UB1DCON_29 U29 (O[29], I[29]);
  UB1DCON_30 U30 (O[30], I[30]);
  UB1DCON_31 U31 (O[31], I[31]);
  UB1DCON_32 U32 (O[32], I[32]);
  UB1DCON_33 U33 (O[33], I[33]);
endmodule

module UBCON_35_2 (O, I);
  output [35:2] O;
  input [35:2] I;
  UB1DCON_2 U0 (O[2], I[2]);
  UB1DCON_3 U1 (O[3], I[3]);
  UB1DCON_4 U2 (O[4], I[4]);
  UB1DCON_5 U3 (O[5], I[5]);
  UB1DCON_6 U4 (O[6], I[6]);
  UB1DCON_7 U5 (O[7], I[7]);
  UB1DCON_8 U6 (O[8], I[8]);
  UB1DCON_9 U7 (O[9], I[9]);
  UB1DCON_10 U8 (O[10], I[10]);
  UB1DCON_11 U9 (O[11], I[11]);
  UB1DCON_12 U10 (O[12], I[12]);
  UB1DCON_13 U11 (O[13], I[13]);
  UB1DCON_14 U12 (O[14], I[14]);
  UB1DCON_15 U13 (O[15], I[15]);
  UB1DCON_16 U14 (O[16], I[16]);
  UB1DCON_17 U15 (O[17], I[17]);
  UB1DCON_18 U16 (O[18], I[18]);
  UB1DCON_19 U17 (O[19], I[19]);
  UB1DCON_20 U18 (O[20], I[20]);
  UB1DCON_21 U19 (O[21], I[21]);
  UB1DCON_22 U20 (O[22], I[22]);
  UB1DCON_23 U21 (O[23], I[23]);
  UB1DCON_24 U22 (O[24], I[24]);
  UB1DCON_25 U23 (O[25], I[25]);
  UB1DCON_26 U24 (O[26], I[26]);
  UB1DCON_27 U25 (O[27], I[27]);
  UB1DCON_28 U26 (O[28], I[28]);
  UB1DCON_29 U27 (O[29], I[29]);
  UB1DCON_30 U28 (O[30], I[30]);
  UB1DCON_31 U29 (O[31], I[31]);
  UB1DCON_32 U30 (O[32], I[32]);
  UB1DCON_33 U31 (O[33], I[33]);
  UB1DCON_34 U32 (O[34], I[34]);
  UB1DCON_35 U33 (O[35], I[35]);
endmodule

module UBCON_37_4 (O, I);
  output [37:4] O;
  input [37:4] I;
  UB1DCON_4 U0 (O[4], I[4]);
  UB1DCON_5 U1 (O[5], I[5]);
  UB1DCON_6 U2 (O[6], I[6]);
  UB1DCON_7 U3 (O[7], I[7]);
  UB1DCON_8 U4 (O[8], I[8]);
  UB1DCON_9 U5 (O[9], I[9]);
  UB1DCON_10 U6 (O[10], I[10]);
  UB1DCON_11 U7 (O[11], I[11]);
  UB1DCON_12 U8 (O[12], I[12]);
  UB1DCON_13 U9 (O[13], I[13]);
  UB1DCON_14 U10 (O[14], I[14]);
  UB1DCON_15 U11 (O[15], I[15]);
  UB1DCON_16 U12 (O[16], I[16]);
  UB1DCON_17 U13 (O[17], I[17]);
  UB1DCON_18 U14 (O[18], I[18]);
  UB1DCON_19 U15 (O[19], I[19]);
  UB1DCON_20 U16 (O[20], I[20]);
  UB1DCON_21 U17 (O[21], I[21]);
  UB1DCON_22 U18 (O[22], I[22]);
  UB1DCON_23 U19 (O[23], I[23]);
  UB1DCON_24 U20 (O[24], I[24]);
  UB1DCON_25 U21 (O[25], I[25]);
  UB1DCON_26 U22 (O[26], I[26]);
  UB1DCON_27 U23 (O[27], I[27]);
  UB1DCON_28 U24 (O[28], I[28]);
  UB1DCON_29 U25 (O[29], I[29]);
  UB1DCON_30 U26 (O[30], I[30]);
  UB1DCON_31 U27 (O[31], I[31]);
  UB1DCON_32 U28 (O[32], I[32]);
  UB1DCON_33 U29 (O[33], I[33]);
  UB1DCON_34 U30 (O[34], I[34]);
  UB1DCON_35 U31 (O[35], I[35]);
  UB1DCON_36 U32 (O[36], I[36]);
  UB1DCON_37 U33 (O[37], I[37]);
endmodule

module UBCON_38_37 (O, I);
  output [38:37] O;
  input [38:37] I;
  UB1DCON_37 U0 (O[37], I[37]);
  UB1DCON_38 U1 (O[38], I[38]);
endmodule

module UBCON_39_6 (O, I);
  output [39:6] O;
  input [39:6] I;
  UB1DCON_6 U0 (O[6], I[6]);
  UB1DCON_7 U1 (O[7], I[7]);
  UB1DCON_8 U2 (O[8], I[8]);
  UB1DCON_9 U3 (O[9], I[9]);
  UB1DCON_10 U4 (O[10], I[10]);
  UB1DCON_11 U5 (O[11], I[11]);
  UB1DCON_12 U6 (O[12], I[12]);
  UB1DCON_13 U7 (O[13], I[13]);
  UB1DCON_14 U8 (O[14], I[14]);
  UB1DCON_15 U9 (O[15], I[15]);
  UB1DCON_16 U10 (O[16], I[16]);
  UB1DCON_17 U11 (O[17], I[17]);
  UB1DCON_18 U12 (O[18], I[18]);
  UB1DCON_19 U13 (O[19], I[19]);
  UB1DCON_20 U14 (O[20], I[20]);
  UB1DCON_21 U15 (O[21], I[21]);
  UB1DCON_22 U16 (O[22], I[22]);
  UB1DCON_23 U17 (O[23], I[23]);
  UB1DCON_24 U18 (O[24], I[24]);
  UB1DCON_25 U19 (O[25], I[25]);
  UB1DCON_26 U20 (O[26], I[26]);
  UB1DCON_27 U21 (O[27], I[27]);
  UB1DCON_28 U22 (O[28], I[28]);
  UB1DCON_29 U23 (O[29], I[29]);
  UB1DCON_30 U24 (O[30], I[30]);
  UB1DCON_31 U25 (O[31], I[31]);
  UB1DCON_32 U26 (O[32], I[32]);
  UB1DCON_33 U27 (O[33], I[33]);
  UB1DCON_34 U28 (O[34], I[34]);
  UB1DCON_35 U29 (O[35], I[35]);
  UB1DCON_36 U30 (O[36], I[36]);
  UB1DCON_37 U31 (O[37], I[37]);
  UB1DCON_38 U32 (O[38], I[38]);
  UB1DCON_39 U33 (O[39], I[39]);
endmodule

module UBCON_3_0 (O, I);
  output [3:0] O;
  input [3:0] I;
  UB1DCON_0 U0 (O[0], I[0]);
  UB1DCON_1 U1 (O[1], I[1]);
  UB1DCON_2 U2 (O[2], I[2]);
  UB1DCON_3 U3 (O[3], I[3]);
endmodule

module UBCON_41_8 (O, I);
  output [41:8] O;
  input [41:8] I;
  UB1DCON_8 U0 (O[8], I[8]);
  UB1DCON_9 U1 (O[9], I[9]);
  UB1DCON_10 U2 (O[10], I[10]);
  UB1DCON_11 U3 (O[11], I[11]);
  UB1DCON_12 U4 (O[12], I[12]);
  UB1DCON_13 U5 (O[13], I[13]);
  UB1DCON_14 U6 (O[14], I[14]);
  UB1DCON_15 U7 (O[15], I[15]);
  UB1DCON_16 U8 (O[16], I[16]);
  UB1DCON_17 U9 (O[17], I[17]);
  UB1DCON_18 U10 (O[18], I[18]);
  UB1DCON_19 U11 (O[19], I[19]);
  UB1DCON_20 U12 (O[20], I[20]);
  UB1DCON_21 U13 (O[21], I[21]);
  UB1DCON_22 U14 (O[22], I[22]);
  UB1DCON_23 U15 (O[23], I[23]);
  UB1DCON_24 U16 (O[24], I[24]);
  UB1DCON_25 U17 (O[25], I[25]);
  UB1DCON_26 U18 (O[26], I[26]);
  UB1DCON_27 U19 (O[27], I[27]);
  UB1DCON_28 U20 (O[28], I[28]);
  UB1DCON_29 U21 (O[29], I[29]);
  UB1DCON_30 U22 (O[30], I[30]);
  UB1DCON_31 U23 (O[31], I[31]);
  UB1DCON_32 U24 (O[32], I[32]);
  UB1DCON_33 U25 (O[33], I[33]);
  UB1DCON_34 U26 (O[34], I[34]);
  UB1DCON_35 U27 (O[35], I[35]);
  UB1DCON_36 U28 (O[36], I[36]);
  UB1DCON_37 U29 (O[37], I[37]);
  UB1DCON_38 U30 (O[38], I[38]);
  UB1DCON_39 U31 (O[39], I[39]);
  UB1DCON_40 U32 (O[40], I[40]);
  UB1DCON_41 U33 (O[41], I[41]);
endmodule

module UBCON_43_10 (O, I);
  output [43:10] O;
  input [43:10] I;
  UB1DCON_10 U0 (O[10], I[10]);
  UB1DCON_11 U1 (O[11], I[11]);
  UB1DCON_12 U2 (O[12], I[12]);
  UB1DCON_13 U3 (O[13], I[13]);
  UB1DCON_14 U4 (O[14], I[14]);
  UB1DCON_15 U5 (O[15], I[15]);
  UB1DCON_16 U6 (O[16], I[16]);
  UB1DCON_17 U7 (O[17], I[17]);
  UB1DCON_18 U8 (O[18], I[18]);
  UB1DCON_19 U9 (O[19], I[19]);
  UB1DCON_20 U10 (O[20], I[20]);
  UB1DCON_21 U11 (O[21], I[21]);
  UB1DCON_22 U12 (O[22], I[22]);
  UB1DCON_23 U13 (O[23], I[23]);
  UB1DCON_24 U14 (O[24], I[24]);
  UB1DCON_25 U15 (O[25], I[25]);
  UB1DCON_26 U16 (O[26], I[26]);
  UB1DCON_27 U17 (O[27], I[27]);
  UB1DCON_28 U18 (O[28], I[28]);
  UB1DCON_29 U19 (O[29], I[29]);
  UB1DCON_30 U20 (O[30], I[30]);
  UB1DCON_31 U21 (O[31], I[31]);
  UB1DCON_32 U22 (O[32], I[32]);
  UB1DCON_33 U23 (O[33], I[33]);
  UB1DCON_34 U24 (O[34], I[34]);
  UB1DCON_35 U25 (O[35], I[35]);
  UB1DCON_36 U26 (O[36], I[36]);
  UB1DCON_37 U27 (O[37], I[37]);
  UB1DCON_38 U28 (O[38], I[38]);
  UB1DCON_39 U29 (O[39], I[39]);
  UB1DCON_40 U30 (O[40], I[40]);
  UB1DCON_41 U31 (O[41], I[41]);
  UB1DCON_42 U32 (O[42], I[42]);
  UB1DCON_43 U33 (O[43], I[43]);
endmodule

module UBCON_44_39 (O, I);
  output [44:39] O;
  input [44:39] I;
  UB1DCON_39 U0 (O[39], I[39]);
  UB1DCON_40 U1 (O[40], I[40]);
  UB1DCON_41 U2 (O[41], I[41]);
  UB1DCON_42 U3 (O[42], I[42]);
  UB1DCON_43 U4 (O[43], I[43]);
  UB1DCON_44 U5 (O[44], I[44]);
endmodule

module UBCON_44_43 (O, I);
  output [44:43] O;
  input [44:43] I;
  UB1DCON_43 U0 (O[43], I[43]);
  UB1DCON_44 U1 (O[44], I[44]);
endmodule

module UBCON_45_12 (O, I);
  output [45:12] O;
  input [45:12] I;
  UB1DCON_12 U0 (O[12], I[12]);
  UB1DCON_13 U1 (O[13], I[13]);
  UB1DCON_14 U2 (O[14], I[14]);
  UB1DCON_15 U3 (O[15], I[15]);
  UB1DCON_16 U4 (O[16], I[16]);
  UB1DCON_17 U5 (O[17], I[17]);
  UB1DCON_18 U6 (O[18], I[18]);
  UB1DCON_19 U7 (O[19], I[19]);
  UB1DCON_20 U8 (O[20], I[20]);
  UB1DCON_21 U9 (O[21], I[21]);
  UB1DCON_22 U10 (O[22], I[22]);
  UB1DCON_23 U11 (O[23], I[23]);
  UB1DCON_24 U12 (O[24], I[24]);
  UB1DCON_25 U13 (O[25], I[25]);
  UB1DCON_26 U14 (O[26], I[26]);
  UB1DCON_27 U15 (O[27], I[27]);
  UB1DCON_28 U16 (O[28], I[28]);
  UB1DCON_29 U17 (O[29], I[29]);
  UB1DCON_30 U18 (O[30], I[30]);
  UB1DCON_31 U19 (O[31], I[31]);
  UB1DCON_32 U20 (O[32], I[32]);
  UB1DCON_33 U21 (O[33], I[33]);
  UB1DCON_34 U22 (O[34], I[34]);
  UB1DCON_35 U23 (O[35], I[35]);
  UB1DCON_36 U24 (O[36], I[36]);
  UB1DCON_37 U25 (O[37], I[37]);
  UB1DCON_38 U26 (O[38], I[38]);
  UB1DCON_39 U27 (O[39], I[39]);
  UB1DCON_40 U28 (O[40], I[40]);
  UB1DCON_41 U29 (O[41], I[41]);
  UB1DCON_42 U30 (O[42], I[42]);
  UB1DCON_43 U31 (O[43], I[43]);
  UB1DCON_44 U32 (O[44], I[44]);
  UB1DCON_45 U33 (O[45], I[45]);
endmodule

module UBCON_47_14 (O, I);
  output [47:14] O;
  input [47:14] I;
  UB1DCON_14 U0 (O[14], I[14]);
  UB1DCON_15 U1 (O[15], I[15]);
  UB1DCON_16 U2 (O[16], I[16]);
  UB1DCON_17 U3 (O[17], I[17]);
  UB1DCON_18 U4 (O[18], I[18]);
  UB1DCON_19 U5 (O[19], I[19]);
  UB1DCON_20 U6 (O[20], I[20]);
  UB1DCON_21 U7 (O[21], I[21]);
  UB1DCON_22 U8 (O[22], I[22]);
  UB1DCON_23 U9 (O[23], I[23]);
  UB1DCON_24 U10 (O[24], I[24]);
  UB1DCON_25 U11 (O[25], I[25]);
  UB1DCON_26 U12 (O[26], I[26]);
  UB1DCON_27 U13 (O[27], I[27]);
  UB1DCON_28 U14 (O[28], I[28]);
  UB1DCON_29 U15 (O[29], I[29]);
  UB1DCON_30 U16 (O[30], I[30]);
  UB1DCON_31 U17 (O[31], I[31]);
  UB1DCON_32 U18 (O[32], I[32]);
  UB1DCON_33 U19 (O[33], I[33]);
  UB1DCON_34 U20 (O[34], I[34]);
  UB1DCON_35 U21 (O[35], I[35]);
  UB1DCON_36 U22 (O[36], I[36]);
  UB1DCON_37 U23 (O[37], I[37]);
  UB1DCON_38 U24 (O[38], I[38]);
  UB1DCON_39 U25 (O[39], I[39]);
  UB1DCON_40 U26 (O[40], I[40]);
  UB1DCON_41 U27 (O[41], I[41]);
  UB1DCON_42 U28 (O[42], I[42]);
  UB1DCON_43 U29 (O[43], I[43]);
  UB1DCON_44 U30 (O[44], I[44]);
  UB1DCON_45 U31 (O[45], I[45]);
  UB1DCON_46 U32 (O[46], I[46]);
  UB1DCON_47 U33 (O[47], I[47]);
endmodule

module UBCON_49_16 (O, I);
  output [49:16] O;
  input [49:16] I;
  UB1DCON_16 U0 (O[16], I[16]);
  UB1DCON_17 U1 (O[17], I[17]);
  UB1DCON_18 U2 (O[18], I[18]);
  UB1DCON_19 U3 (O[19], I[19]);
  UB1DCON_20 U4 (O[20], I[20]);
  UB1DCON_21 U5 (O[21], I[21]);
  UB1DCON_22 U6 (O[22], I[22]);
  UB1DCON_23 U7 (O[23], I[23]);
  UB1DCON_24 U8 (O[24], I[24]);
  UB1DCON_25 U9 (O[25], I[25]);
  UB1DCON_26 U10 (O[26], I[26]);
  UB1DCON_27 U11 (O[27], I[27]);
  UB1DCON_28 U12 (O[28], I[28]);
  UB1DCON_29 U13 (O[29], I[29]);
  UB1DCON_30 U14 (O[30], I[30]);
  UB1DCON_31 U15 (O[31], I[31]);
  UB1DCON_32 U16 (O[32], I[32]);
  UB1DCON_33 U17 (O[33], I[33]);
  UB1DCON_34 U18 (O[34], I[34]);
  UB1DCON_35 U19 (O[35], I[35]);
  UB1DCON_36 U20 (O[36], I[36]);
  UB1DCON_37 U21 (O[37], I[37]);
  UB1DCON_38 U22 (O[38], I[38]);
  UB1DCON_39 U23 (O[39], I[39]);
  UB1DCON_40 U24 (O[40], I[40]);
  UB1DCON_41 U25 (O[41], I[41]);
  UB1DCON_42 U26 (O[42], I[42]);
  UB1DCON_43 U27 (O[43], I[43]);
  UB1DCON_44 U28 (O[44], I[44]);
  UB1DCON_45 U29 (O[45], I[45]);
  UB1DCON_46 U30 (O[46], I[46]);
  UB1DCON_47 U31 (O[47], I[47]);
  UB1DCON_48 U32 (O[48], I[48]);
  UB1DCON_49 U33 (O[49], I[49]);
endmodule

module UBCON_4_0 (O, I);
  output [4:0] O;
  input [4:0] I;
  UB1DCON_0 U0 (O[0], I[0]);
  UB1DCON_1 U1 (O[1], I[1]);
  UB1DCON_2 U2 (O[2], I[2]);
  UB1DCON_3 U3 (O[3], I[3]);
  UB1DCON_4 U4 (O[4], I[4]);
endmodule

module UBCON_50_45 (O, I);
  output [50:45] O;
  input [50:45] I;
  UB1DCON_45 U0 (O[45], I[45]);
  UB1DCON_46 U1 (O[46], I[46]);
  UB1DCON_47 U2 (O[47], I[47]);
  UB1DCON_48 U3 (O[48], I[48]);
  UB1DCON_49 U4 (O[49], I[49]);
  UB1DCON_50 U5 (O[50], I[50]);
endmodule

module UBCON_50_49 (O, I);
  output [50:49] O;
  input [50:49] I;
  UB1DCON_49 U0 (O[49], I[49]);
  UB1DCON_50 U1 (O[50], I[50]);
endmodule

module UBCON_51_18 (O, I);
  output [51:18] O;
  input [51:18] I;
  UB1DCON_18 U0 (O[18], I[18]);
  UB1DCON_19 U1 (O[19], I[19]);
  UB1DCON_20 U2 (O[20], I[20]);
  UB1DCON_21 U3 (O[21], I[21]);
  UB1DCON_22 U4 (O[22], I[22]);
  UB1DCON_23 U5 (O[23], I[23]);
  UB1DCON_24 U6 (O[24], I[24]);
  UB1DCON_25 U7 (O[25], I[25]);
  UB1DCON_26 U8 (O[26], I[26]);
  UB1DCON_27 U9 (O[27], I[27]);
  UB1DCON_28 U10 (O[28], I[28]);
  UB1DCON_29 U11 (O[29], I[29]);
  UB1DCON_30 U12 (O[30], I[30]);
  UB1DCON_31 U13 (O[31], I[31]);
  UB1DCON_32 U14 (O[32], I[32]);
  UB1DCON_33 U15 (O[33], I[33]);
  UB1DCON_34 U16 (O[34], I[34]);
  UB1DCON_35 U17 (O[35], I[35]);
  UB1DCON_36 U18 (O[36], I[36]);
  UB1DCON_37 U19 (O[37], I[37]);
  UB1DCON_38 U20 (O[38], I[38]);
  UB1DCON_39 U21 (O[39], I[39]);
  UB1DCON_40 U22 (O[40], I[40]);
  UB1DCON_41 U23 (O[41], I[41]);
  UB1DCON_42 U24 (O[42], I[42]);
  UB1DCON_43 U25 (O[43], I[43]);
  UB1DCON_44 U26 (O[44], I[44]);
  UB1DCON_45 U27 (O[45], I[45]);
  UB1DCON_46 U28 (O[46], I[46]);
  UB1DCON_47 U29 (O[47], I[47]);
  UB1DCON_48 U30 (O[48], I[48]);
  UB1DCON_49 U31 (O[49], I[49]);
  UB1DCON_50 U32 (O[50], I[50]);
  UB1DCON_51 U33 (O[51], I[51]);
endmodule

module UBCON_53_20 (O, I);
  output [53:20] O;
  input [53:20] I;
  UB1DCON_20 U0 (O[20], I[20]);
  UB1DCON_21 U1 (O[21], I[21]);
  UB1DCON_22 U2 (O[22], I[22]);
  UB1DCON_23 U3 (O[23], I[23]);
  UB1DCON_24 U4 (O[24], I[24]);
  UB1DCON_25 U5 (O[25], I[25]);
  UB1DCON_26 U6 (O[26], I[26]);
  UB1DCON_27 U7 (O[27], I[27]);
  UB1DCON_28 U8 (O[28], I[28]);
  UB1DCON_29 U9 (O[29], I[29]);
  UB1DCON_30 U10 (O[30], I[30]);
  UB1DCON_31 U11 (O[31], I[31]);
  UB1DCON_32 U12 (O[32], I[32]);
  UB1DCON_33 U13 (O[33], I[33]);
  UB1DCON_34 U14 (O[34], I[34]);
  UB1DCON_35 U15 (O[35], I[35]);
  UB1DCON_36 U16 (O[36], I[36]);
  UB1DCON_37 U17 (O[37], I[37]);
  UB1DCON_38 U18 (O[38], I[38]);
  UB1DCON_39 U19 (O[39], I[39]);
  UB1DCON_40 U20 (O[40], I[40]);
  UB1DCON_41 U21 (O[41], I[41]);
  UB1DCON_42 U22 (O[42], I[42]);
  UB1DCON_43 U23 (O[43], I[43]);
  UB1DCON_44 U24 (O[44], I[44]);
  UB1DCON_45 U25 (O[45], I[45]);
  UB1DCON_46 U26 (O[46], I[46]);
  UB1DCON_47 U27 (O[47], I[47]);
  UB1DCON_48 U28 (O[48], I[48]);
  UB1DCON_49 U29 (O[49], I[49]);
  UB1DCON_50 U30 (O[50], I[50]);
  UB1DCON_51 U31 (O[51], I[51]);
  UB1DCON_52 U32 (O[52], I[52]);
  UB1DCON_53 U33 (O[53], I[53]);
endmodule

module UBCON_55_22 (O, I);
  output [55:22] O;
  input [55:22] I;
  UB1DCON_22 U0 (O[22], I[22]);
  UB1DCON_23 U1 (O[23], I[23]);
  UB1DCON_24 U2 (O[24], I[24]);
  UB1DCON_25 U3 (O[25], I[25]);
  UB1DCON_26 U4 (O[26], I[26]);
  UB1DCON_27 U5 (O[27], I[27]);
  UB1DCON_28 U6 (O[28], I[28]);
  UB1DCON_29 U7 (O[29], I[29]);
  UB1DCON_30 U8 (O[30], I[30]);
  UB1DCON_31 U9 (O[31], I[31]);
  UB1DCON_32 U10 (O[32], I[32]);
  UB1DCON_33 U11 (O[33], I[33]);
  UB1DCON_34 U12 (O[34], I[34]);
  UB1DCON_35 U13 (O[35], I[35]);
  UB1DCON_36 U14 (O[36], I[36]);
  UB1DCON_37 U15 (O[37], I[37]);
  UB1DCON_38 U16 (O[38], I[38]);
  UB1DCON_39 U17 (O[39], I[39]);
  UB1DCON_40 U18 (O[40], I[40]);
  UB1DCON_41 U19 (O[41], I[41]);
  UB1DCON_42 U20 (O[42], I[42]);
  UB1DCON_43 U21 (O[43], I[43]);
  UB1DCON_44 U22 (O[44], I[44]);
  UB1DCON_45 U23 (O[45], I[45]);
  UB1DCON_46 U24 (O[46], I[46]);
  UB1DCON_47 U25 (O[47], I[47]);
  UB1DCON_48 U26 (O[48], I[48]);
  UB1DCON_49 U27 (O[49], I[49]);
  UB1DCON_50 U28 (O[50], I[50]);
  UB1DCON_51 U29 (O[51], I[51]);
  UB1DCON_52 U30 (O[52], I[52]);
  UB1DCON_53 U31 (O[53], I[53]);
  UB1DCON_54 U32 (O[54], I[54]);
  UB1DCON_55 U33 (O[55], I[55]);
endmodule

module UBCON_56_55 (O, I);
  output [56:55] O;
  input [56:55] I;
  UB1DCON_55 U0 (O[55], I[55]);
  UB1DCON_56 U1 (O[56], I[56]);
endmodule

module UBCON_57_24 (O, I);
  output [57:24] O;
  input [57:24] I;
  UB1DCON_24 U0 (O[24], I[24]);
  UB1DCON_25 U1 (O[25], I[25]);
  UB1DCON_26 U2 (O[26], I[26]);
  UB1DCON_27 U3 (O[27], I[27]);
  UB1DCON_28 U4 (O[28], I[28]);
  UB1DCON_29 U5 (O[29], I[29]);
  UB1DCON_30 U6 (O[30], I[30]);
  UB1DCON_31 U7 (O[31], I[31]);
  UB1DCON_32 U8 (O[32], I[32]);
  UB1DCON_33 U9 (O[33], I[33]);
  UB1DCON_34 U10 (O[34], I[34]);
  UB1DCON_35 U11 (O[35], I[35]);
  UB1DCON_36 U12 (O[36], I[36]);
  UB1DCON_37 U13 (O[37], I[37]);
  UB1DCON_38 U14 (O[38], I[38]);
  UB1DCON_39 U15 (O[39], I[39]);
  UB1DCON_40 U16 (O[40], I[40]);
  UB1DCON_41 U17 (O[41], I[41]);
  UB1DCON_42 U18 (O[42], I[42]);
  UB1DCON_43 U19 (O[43], I[43]);
  UB1DCON_44 U20 (O[44], I[44]);
  UB1DCON_45 U21 (O[45], I[45]);
  UB1DCON_46 U22 (O[46], I[46]);
  UB1DCON_47 U23 (O[47], I[47]);
  UB1DCON_48 U24 (O[48], I[48]);
  UB1DCON_49 U25 (O[49], I[49]);
  UB1DCON_50 U26 (O[50], I[50]);
  UB1DCON_51 U27 (O[51], I[51]);
  UB1DCON_52 U28 (O[52], I[52]);
  UB1DCON_53 U29 (O[53], I[53]);
  UB1DCON_54 U30 (O[54], I[54]);
  UB1DCON_55 U31 (O[55], I[55]);
  UB1DCON_56 U32 (O[56], I[56]);
  UB1DCON_57 U33 (O[57], I[57]);
endmodule

module UBCON_59_26 (O, I);
  output [59:26] O;
  input [59:26] I;
  UB1DCON_26 U0 (O[26], I[26]);
  UB1DCON_27 U1 (O[27], I[27]);
  UB1DCON_28 U2 (O[28], I[28]);
  UB1DCON_29 U3 (O[29], I[29]);
  UB1DCON_30 U4 (O[30], I[30]);
  UB1DCON_31 U5 (O[31], I[31]);
  UB1DCON_32 U6 (O[32], I[32]);
  UB1DCON_33 U7 (O[33], I[33]);
  UB1DCON_34 U8 (O[34], I[34]);
  UB1DCON_35 U9 (O[35], I[35]);
  UB1DCON_36 U10 (O[36], I[36]);
  UB1DCON_37 U11 (O[37], I[37]);
  UB1DCON_38 U12 (O[38], I[38]);
  UB1DCON_39 U13 (O[39], I[39]);
  UB1DCON_40 U14 (O[40], I[40]);
  UB1DCON_41 U15 (O[41], I[41]);
  UB1DCON_42 U16 (O[42], I[42]);
  UB1DCON_43 U17 (O[43], I[43]);
  UB1DCON_44 U18 (O[44], I[44]);
  UB1DCON_45 U19 (O[45], I[45]);
  UB1DCON_46 U20 (O[46], I[46]);
  UB1DCON_47 U21 (O[47], I[47]);
  UB1DCON_48 U22 (O[48], I[48]);
  UB1DCON_49 U23 (O[49], I[49]);
  UB1DCON_50 U24 (O[50], I[50]);
  UB1DCON_51 U25 (O[51], I[51]);
  UB1DCON_52 U26 (O[52], I[52]);
  UB1DCON_53 U27 (O[53], I[53]);
  UB1DCON_54 U28 (O[54], I[54]);
  UB1DCON_55 U29 (O[55], I[55]);
  UB1DCON_56 U30 (O[56], I[56]);
  UB1DCON_57 U31 (O[57], I[57]);
  UB1DCON_58 U32 (O[58], I[58]);
  UB1DCON_59 U33 (O[59], I[59]);
endmodule

module UBCON_5_0 (O, I);
  output [5:0] O;
  input [5:0] I;
  UB1DCON_0 U0 (O[0], I[0]);
  UB1DCON_1 U1 (O[1], I[1]);
  UB1DCON_2 U2 (O[2], I[2]);
  UB1DCON_3 U3 (O[3], I[3]);
  UB1DCON_4 U4 (O[4], I[4]);
  UB1DCON_5 U5 (O[5], I[5]);
endmodule

module UBCON_5_4 (O, I);
  output [5:4] O;
  input [5:4] I;
  UB1DCON_4 U0 (O[4], I[4]);
  UB1DCON_5 U1 (O[5], I[5]);
endmodule

module UBCON_61_28 (O, I);
  output [61:28] O;
  input [61:28] I;
  UB1DCON_28 U0 (O[28], I[28]);
  UB1DCON_29 U1 (O[29], I[29]);
  UB1DCON_30 U2 (O[30], I[30]);
  UB1DCON_31 U3 (O[31], I[31]);
  UB1DCON_32 U4 (O[32], I[32]);
  UB1DCON_33 U5 (O[33], I[33]);
  UB1DCON_34 U6 (O[34], I[34]);
  UB1DCON_35 U7 (O[35], I[35]);
  UB1DCON_36 U8 (O[36], I[36]);
  UB1DCON_37 U9 (O[37], I[37]);
  UB1DCON_38 U10 (O[38], I[38]);
  UB1DCON_39 U11 (O[39], I[39]);
  UB1DCON_40 U12 (O[40], I[40]);
  UB1DCON_41 U13 (O[41], I[41]);
  UB1DCON_42 U14 (O[42], I[42]);
  UB1DCON_43 U15 (O[43], I[43]);
  UB1DCON_44 U16 (O[44], I[44]);
  UB1DCON_45 U17 (O[45], I[45]);
  UB1DCON_46 U18 (O[46], I[46]);
  UB1DCON_47 U19 (O[47], I[47]);
  UB1DCON_48 U20 (O[48], I[48]);
  UB1DCON_49 U21 (O[49], I[49]);
  UB1DCON_50 U22 (O[50], I[50]);
  UB1DCON_51 U23 (O[51], I[51]);
  UB1DCON_52 U24 (O[52], I[52]);
  UB1DCON_53 U25 (O[53], I[53]);
  UB1DCON_54 U26 (O[54], I[54]);
  UB1DCON_55 U27 (O[55], I[55]);
  UB1DCON_56 U28 (O[56], I[56]);
  UB1DCON_57 U29 (O[57], I[57]);
  UB1DCON_58 U30 (O[58], I[58]);
  UB1DCON_59 U31 (O[59], I[59]);
  UB1DCON_60 U32 (O[60], I[60]);
  UB1DCON_61 U33 (O[61], I[61]);
endmodule

module UBCON_62_51 (O, I);
  output [62:51] O;
  input [62:51] I;
  UB1DCON_51 U0 (O[51], I[51]);
  UB1DCON_52 U1 (O[52], I[52]);
  UB1DCON_53 U2 (O[53], I[53]);
  UB1DCON_54 U3 (O[54], I[54]);
  UB1DCON_55 U4 (O[55], I[55]);
  UB1DCON_56 U5 (O[56], I[56]);
  UB1DCON_57 U6 (O[57], I[57]);
  UB1DCON_58 U7 (O[58], I[58]);
  UB1DCON_59 U8 (O[59], I[59]);
  UB1DCON_60 U9 (O[60], I[60]);
  UB1DCON_61 U10 (O[61], I[61]);
  UB1DCON_62 U11 (O[62], I[62]);
endmodule

module UBCON_62_57 (O, I);
  output [62:57] O;
  input [62:57] I;
  UB1DCON_57 U0 (O[57], I[57]);
  UB1DCON_58 U1 (O[58], I[58]);
  UB1DCON_59 U2 (O[59], I[59]);
  UB1DCON_60 U3 (O[60], I[60]);
  UB1DCON_61 U4 (O[61], I[61]);
  UB1DCON_62 U5 (O[62], I[62]);
endmodule

module UBCON_62_58 (O, I);
  output [62:58] O;
  input [62:58] I;
  UB1DCON_58 U0 (O[58], I[58]);
  UB1DCON_59 U1 (O[59], I[59]);
  UB1DCON_60 U2 (O[60], I[60]);
  UB1DCON_61 U3 (O[61], I[61]);
  UB1DCON_62 U4 (O[62], I[62]);
endmodule

module UBCON_62_61 (O, I);
  output [62:61] O;
  input [62:61] I;
  UB1DCON_61 U0 (O[61], I[61]);
  UB1DCON_62 U1 (O[62], I[62]);
endmodule

module UBCON_63_30 (O, I);
  output [63:30] O;
  input [63:30] I;
  UB1DCON_30 U0 (O[30], I[30]);
  UB1DCON_31 U1 (O[31], I[31]);
  UB1DCON_32 U2 (O[32], I[32]);
  UB1DCON_33 U3 (O[33], I[33]);
  UB1DCON_34 U4 (O[34], I[34]);
  UB1DCON_35 U5 (O[35], I[35]);
  UB1DCON_36 U6 (O[36], I[36]);
  UB1DCON_37 U7 (O[37], I[37]);
  UB1DCON_38 U8 (O[38], I[38]);
  UB1DCON_39 U9 (O[39], I[39]);
  UB1DCON_40 U10 (O[40], I[40]);
  UB1DCON_41 U11 (O[41], I[41]);
  UB1DCON_42 U12 (O[42], I[42]);
  UB1DCON_43 U13 (O[43], I[43]);
  UB1DCON_44 U14 (O[44], I[44]);
  UB1DCON_45 U15 (O[45], I[45]);
  UB1DCON_46 U16 (O[46], I[46]);
  UB1DCON_47 U17 (O[47], I[47]);
  UB1DCON_48 U18 (O[48], I[48]);
  UB1DCON_49 U19 (O[49], I[49]);
  UB1DCON_50 U20 (O[50], I[50]);
  UB1DCON_51 U21 (O[51], I[51]);
  UB1DCON_52 U22 (O[52], I[52]);
  UB1DCON_53 U23 (O[53], I[53]);
  UB1DCON_54 U24 (O[54], I[54]);
  UB1DCON_55 U25 (O[55], I[55]);
  UB1DCON_56 U26 (O[56], I[56]);
  UB1DCON_57 U27 (O[57], I[57]);
  UB1DCON_58 U28 (O[58], I[58]);
  UB1DCON_59 U29 (O[59], I[59]);
  UB1DCON_60 U30 (O[60], I[60]);
  UB1DCON_61 U31 (O[61], I[61]);
  UB1DCON_62 U32 (O[62], I[62]);
  UB1DCON_63 U33 (O[63], I[63]);
endmodule

module UBCON_65_32 (O, I);
  output [65:32] O;
  input [65:32] I;
  UB1DCON_32 U0 (O[32], I[32]);
  UB1DCON_33 U1 (O[33], I[33]);
  UB1DCON_34 U2 (O[34], I[34]);
  UB1DCON_35 U3 (O[35], I[35]);
  UB1DCON_36 U4 (O[36], I[36]);
  UB1DCON_37 U5 (O[37], I[37]);
  UB1DCON_38 U6 (O[38], I[38]);
  UB1DCON_39 U7 (O[39], I[39]);
  UB1DCON_40 U8 (O[40], I[40]);
  UB1DCON_41 U9 (O[41], I[41]);
  UB1DCON_42 U10 (O[42], I[42]);
  UB1DCON_43 U11 (O[43], I[43]);
  UB1DCON_44 U12 (O[44], I[44]);
  UB1DCON_45 U13 (O[45], I[45]);
  UB1DCON_46 U14 (O[46], I[46]);
  UB1DCON_47 U15 (O[47], I[47]);
  UB1DCON_48 U16 (O[48], I[48]);
  UB1DCON_49 U17 (O[49], I[49]);
  UB1DCON_50 U18 (O[50], I[50]);
  UB1DCON_51 U19 (O[51], I[51]);
  UB1DCON_52 U20 (O[52], I[52]);
  UB1DCON_53 U21 (O[53], I[53]);
  UB1DCON_54 U22 (O[54], I[54]);
  UB1DCON_55 U23 (O[55], I[55]);
  UB1DCON_56 U24 (O[56], I[56]);
  UB1DCON_57 U25 (O[57], I[57]);
  UB1DCON_58 U26 (O[58], I[58]);
  UB1DCON_59 U27 (O[59], I[59]);
  UB1DCON_60 U28 (O[60], I[60]);
  UB1DCON_61 U29 (O[61], I[61]);
  UB1DCON_62 U30 (O[62], I[62]);
  UB1DCON_63 U31 (O[63], I[63]);
  UB1DCON_64 U32 (O[64], I[64]);
  UB1DCON_65 U33 (O[65], I[65]);
endmodule

module UBCON_66_6 (O, I);
  output [66:6] O;
  input [66:6] I;
  UB1DCON_6 U0 (O[6], I[6]);
  UB1DCON_7 U1 (O[7], I[7]);
  UB1DCON_8 U2 (O[8], I[8]);
  UB1DCON_9 U3 (O[9], I[9]);
  UB1DCON_10 U4 (O[10], I[10]);
  UB1DCON_11 U5 (O[11], I[11]);
  UB1DCON_12 U6 (O[12], I[12]);
  UB1DCON_13 U7 (O[13], I[13]);
  UB1DCON_14 U8 (O[14], I[14]);
  UB1DCON_15 U9 (O[15], I[15]);
  UB1DCON_16 U10 (O[16], I[16]);
  UB1DCON_17 U11 (O[17], I[17]);
  UB1DCON_18 U12 (O[18], I[18]);
  UB1DCON_19 U13 (O[19], I[19]);
  UB1DCON_20 U14 (O[20], I[20]);
  UB1DCON_21 U15 (O[21], I[21]);
  UB1DCON_22 U16 (O[22], I[22]);
  UB1DCON_23 U17 (O[23], I[23]);
  UB1DCON_24 U18 (O[24], I[24]);
  UB1DCON_25 U19 (O[25], I[25]);
  UB1DCON_26 U20 (O[26], I[26]);
  UB1DCON_27 U21 (O[27], I[27]);
  UB1DCON_28 U22 (O[28], I[28]);
  UB1DCON_29 U23 (O[29], I[29]);
  UB1DCON_30 U24 (O[30], I[30]);
  UB1DCON_31 U25 (O[31], I[31]);
  UB1DCON_32 U26 (O[32], I[32]);
  UB1DCON_33 U27 (O[33], I[33]);
  UB1DCON_34 U28 (O[34], I[34]);
  UB1DCON_35 U29 (O[35], I[35]);
  UB1DCON_36 U30 (O[36], I[36]);
  UB1DCON_37 U31 (O[37], I[37]);
  UB1DCON_38 U32 (O[38], I[38]);
  UB1DCON_39 U33 (O[39], I[39]);
  UB1DCON_40 U34 (O[40], I[40]);
  UB1DCON_41 U35 (O[41], I[41]);
  UB1DCON_42 U36 (O[42], I[42]);
  UB1DCON_43 U37 (O[43], I[43]);
  UB1DCON_44 U38 (O[44], I[44]);
  UB1DCON_45 U39 (O[45], I[45]);
  UB1DCON_46 U40 (O[46], I[46]);
  UB1DCON_47 U41 (O[47], I[47]);
  UB1DCON_48 U42 (O[48], I[48]);
  UB1DCON_49 U43 (O[49], I[49]);
  UB1DCON_50 U44 (O[50], I[50]);
  UB1DCON_51 U45 (O[51], I[51]);
  UB1DCON_52 U46 (O[52], I[52]);
  UB1DCON_53 U47 (O[53], I[53]);
  UB1DCON_54 U48 (O[54], I[54]);
  UB1DCON_55 U49 (O[55], I[55]);
  UB1DCON_56 U50 (O[56], I[56]);
  UB1DCON_57 U51 (O[57], I[57]);
  UB1DCON_58 U52 (O[58], I[58]);
  UB1DCON_59 U53 (O[59], I[59]);
  UB1DCON_60 U54 (O[60], I[60]);
  UB1DCON_61 U55 (O[61], I[61]);
  UB1DCON_62 U56 (O[62], I[62]);
  UB1DCON_63 U57 (O[63], I[63]);
  UB1DCON_64 U58 (O[64], I[64]);
  UB1DCON_65 U59 (O[65], I[65]);
  UB1DCON_66 U60 (O[66], I[66]);
endmodule

module UBCON_66_63 (O, I);
  output [66:63] O;
  input [66:63] I;
  UB1DCON_63 U0 (O[63], I[63]);
  UB1DCON_64 U1 (O[64], I[64]);
  UB1DCON_65 U2 (O[65], I[65]);
  UB1DCON_66 U3 (O[66], I[66]);
endmodule

module UBCON_9_7 (O, I);
  output [9:7] O;
  input [9:7] I;
  UB1DCON_7 U0 (O[7], I[7]);
  UB1DCON_8 U1 (O[8], I[8]);
  UB1DCON_9 U2 (O[9], I[9]);
endmodule

module UBExtender_66_6_6000 (O, I);
  output [67:6] O;
  input [66:6] I;
  UBCON_66_6 U0 (O[66:6], I[66:6]);
  UBZero_67_67 U1 (O[67]);
endmodule

module UBPureCLA_67_6 (S, X, Y);
  output [68:6] S;
  input [67:6] X;
  input [67:6] Y;
  wire C;
  UBPriCLA_67_6 U0 (S, X, Y, C);
  UBZero_6_6 U1 (C);
endmodule

module UBR4BE_31_0 (O__ds, O__d1, O__d0, I);
  output [16:0] O__ds, O__d1, O__d0;
  input [31:0] I;
  wire T;
  NUBZero_32_32 U0 (T);
  R4BEEL_0_2 U1 (O__ds[0], O__d1[0], O__d0[0], I[1], I[0]);
  R4BEE_1 U2 (O__ds[1], O__d1[1], O__d0[1], I[3], I[2], I[1]);
  R4BEE_2 U3 (O__ds[2], O__d1[2], O__d0[2], I[5], I[4], I[3]);
  R4BEE_3 U4 (O__ds[3], O__d1[3], O__d0[3], I[7], I[6], I[5]);
  R4BEE_4 U5 (O__ds[4], O__d1[4], O__d0[4], I[9], I[8], I[7]);
  R4BEE_5 U6 (O__ds[5], O__d1[5], O__d0[5], I[11], I[10], I[9]);
  R4BEE_6 U7 (O__ds[6], O__d1[6], O__d0[6], I[13], I[12], I[11]);
  R4BEE_7 U8 (O__ds[7], O__d1[7], O__d0[7], I[15], I[14], I[13]);
  R4BEE_8 U9 (O__ds[8], O__d1[8], O__d0[8], I[17], I[16], I[15]);
  R4BEE_9 U10 (O__ds[9], O__d1[9], O__d0[9], I[19], I[18], I[17]);
  R4BEE_10 U11 (O__ds[10], O__d1[10], O__d0[10], I[21], I[20], I[19]);
  R4BEE_11 U12 (O__ds[11], O__d1[11], O__d0[11], I[23], I[22], I[21]);
  R4BEE_12 U13 (O__ds[12], O__d1[12], O__d0[12], I[25], I[24], I[23]);
  R4BEE_13 U14 (O__ds[13], O__d1[13], O__d0[13], I[27], I[26], I[25]);
  R4BEE_14 U15 (O__ds[14], O__d1[14], O__d0[14], I[29], I[28], I[27]);
  R4BEE_15 U16 (O__ds[15], O__d1[15], O__d0[15], I[31], I[30], I[29]);
  R4BEEH_16_2 U17 (O__ds[16], O__d1[16], O__d0[16], T, I[31]);
endmodule

module UBR4BPPG_31_0_31_000 (PP0, PP1, PP2, PP3, PP4, PP5, PP6, PP7, PP8, PP9, PP10, PP11, PP12, PP13, PP14, PP15, PP16, PP17, IN1, IN2);
  output [34:0] PP0;
  output [36:0] PP1;
  output [54:18] PP10;
  output [56:20] PP11;
  output [58:22] PP12;
  output [60:24] PP13;
  output [62:26] PP14;
  output [64:28] PP15;
  output [65:30] PP16;
  output [33:32] PP17;
  output [38:2] PP2;
  output [40:4] PP3;
  output [42:6] PP4;
  output [44:8] PP5;
  output [46:10] PP6;
  output [48:12] PP7;
  output [50:14] PP8;
  output [52:16] PP9;
  input [31:0] IN1;
  input [31:0] IN2;
  wire B0;
  wire B1;
  wire B10;
  wire B11;
  wire B12;
  wire B13;
  wire B14;
  wire B15;
  wire B16;
  wire B2;
  wire B3;
  wire B4;
  wire B5;
  wire B6;
  wire B7;
  wire B8;
  wire B9;
  wire [16:0] IN2SD__ds, IN2SD__d1, IN2SD__d0;
  wire [33:0] PPT0;
  wire [35:2] PPT1;
  wire [53:20] PPT10;
  wire [55:22] PPT11;
  wire [57:24] PPT12;
  wire [59:26] PPT13;
  wire [61:28] PPT14;
  wire [63:30] PPT15;
  wire [65:32] PPT16;
  wire [37:4] PPT2;
  wire [39:6] PPT3;
  wire [41:8] PPT4;
  wire [43:10] PPT5;
  wire [45:12] PPT6;
  wire [47:14] PPT7;
  wire [49:16] PPT8;
  wire [51:18] PPT9;
  wire S0;
  wire S1;
  wire S10;
  wire S11;
  wire S12;
  wire S13;
  wire S14;
  wire S15;
  wire S16;
  wire S2;
  wire S3;
  wire S4;
  wire S5;
  wire S6;
  wire S7;
  wire S8;
  wire S9;
  UBR4BE_31_0 U0 (IN2SD__ds, IN2SD__d1, IN2SD__d0, IN2);
  UBSD4VPPG_31_0_0 U1 (PPT0, S0, IN1, IN2SD__ds[0], IN2SD__d1[0], IN2SD__d0[0]);
  UBSD4VPPG_31_0_1 U2 (PPT1, S1, IN1, IN2SD__ds[1], IN2SD__d1[1], IN2SD__d0[1]);
  UBSD4VPPG_31_0_2 U3 (PPT2, S2, IN1, IN2SD__ds[2], IN2SD__d1[2], IN2SD__d0[2]);
  UBSD4VPPG_31_0_3 U4 (PPT3, S3, IN1, IN2SD__ds[3], IN2SD__d1[3], IN2SD__d0[3]);
  UBSD4VPPG_31_0_4 U5 (PPT4, S4, IN1, IN2SD__ds[4], IN2SD__d1[4], IN2SD__d0[4]);
  UBSD4VPPG_31_0_5 U6 (PPT5, S5, IN1, IN2SD__ds[5], IN2SD__d1[5], IN2SD__d0[5]);
  UBSD4VPPG_31_0_6 U7 (PPT6, S6, IN1, IN2SD__ds[6], IN2SD__d1[6], IN2SD__d0[6]);
  UBSD4VPPG_31_0_7 U8 (PPT7, S7, IN1, IN2SD__ds[7], IN2SD__d1[7], IN2SD__d0[7]);
  UBSD4VPPG_31_0_8 U9 (PPT8, S8, IN1, IN2SD__ds[8], IN2SD__d1[8], IN2SD__d0[8]);
  UBSD4VPPG_31_0_9 U10 (PPT9, S9, IN1, IN2SD__ds[9], IN2SD__d1[9], IN2SD__d0[9]);
  UBSD4VPPG_31_0_10 U11 (PPT10, S10, IN1, IN2SD__ds[10], IN2SD__d1[10], IN2SD__d0[10]);
  UBSD4VPPG_31_0_11 U12 (PPT11, S11, IN1, IN2SD__ds[11], IN2SD__d1[11], IN2SD__d0[11]);
  UBSD4VPPG_31_0_12 U13 (PPT12, S12, IN1, IN2SD__ds[12], IN2SD__d1[12], IN2SD__d0[12]);
  UBSD4VPPG_31_0_13 U14 (PPT13, S13, IN1, IN2SD__ds[13], IN2SD__d1[13], IN2SD__d0[13]);
  UBSD4VPPG_31_0_14 U15 (PPT14, S14, IN1, IN2SD__ds[14], IN2SD__d1[14], IN2SD__d0[14]);
  UBSD4VPPG_31_0_15 U16 (PPT15, S15, IN1, IN2SD__ds[15], IN2SD__d1[15], IN2SD__d0[15]);
  UBSD4VPPG_31_0_16 U17 (PPT16, S16, IN1, IN2SD__ds[16], IN2SD__d1[16], IN2SD__d0[16]);
  UBOne_34 U18 (B0);
  UBCMBIN_34_34_33_000 U19 (PP0, B0, PPT0);
  UBOne_36 U20 (B1);
  UBCMBIN_36_36_35_000 U21 (PP1, B1, PPT1, S0);
  UBOne_38 U22 (B2);
  UBCMBIN_38_38_37_000 U23 (PP2, B2, PPT2, S1);
  UBOne_40 U24 (B3);
  UBCMBIN_40_40_39_000 U25 (PP3, B3, PPT3, S2);
  UBOne_42 U26 (B4);
  UBCMBIN_42_42_41_000 U27 (PP4, B4, PPT4, S3);
  UBOne_44 U28 (B5);
  UBCMBIN_44_44_43_000 U29 (PP5, B5, PPT5, S4);
  UBOne_46 U30 (B6);
  UBCMBIN_46_46_45_000 U31 (PP6, B6, PPT6, S5);
  UBOne_48 U32 (B7);
  UBCMBIN_48_48_47_000 U33 (PP7, B7, PPT7, S6);
  UBOne_50 U34 (B8);
  UBCMBIN_50_50_49_000 U35 (PP8, B8, PPT8, S7);
  UBOne_52 U36 (B9);
  UBCMBIN_52_52_51_000 U37 (PP9, B9, PPT9, S8);
  UBOne_54 U38 (B10);
  UBCMBIN_54_54_53_000 U39 (PP10, B10, PPT10, S9);
  UBOne_56 U40 (B11);
  UBCMBIN_56_56_55_000 U41 (PP11, B11, PPT11, S10);
  UBOne_58 U42 (B12);
  UBCMBIN_58_58_57_000 U43 (PP12, B12, PPT12, S11);
  UBOne_60 U44 (B13);
  UBCMBIN_60_60_59_000 U45 (PP13, B13, PPT13, S12);
  UBOne_62 U46 (B14);
  UBCMBIN_62_62_61_000 U47 (PP14, B14, PPT14, S13);
  UBOne_64 U48 (B15);
  UBCMBIN_64_64_63_000 U49 (PP15, B15, PPT15, S14);
  UBCMBIN_65_32_30_000 U50 (PP16, PPT16, S15);
  UBOne_33 U51 (B16);
  UBCMBIN_33_33_32_000 U52 (PP17, B16, S16);
endmodule

module UBSD4VPPG_31_0_0 (PP, C, IN1, IN2__ds, IN2__d1, IN2__d0);
  output C;
  output [33:0] PP;
  input [31:0] IN1;
  input IN2__ds, IN2__d1, IN2__d0;
  wire NZ;
  wire S;
  wire U__d1, U__d0;
  wire [32:0] W_R;
  wire W_T;
  SD41DDECON_0 U0 (S, U__d1, U__d0, IN2__ds, IN2__d1, IN2__d0);
  NUBZero_32_32 U1 (NZ);
  TCU4VPPG_32_0_0 U2 (W_T, W_R, NZ, IN1, U__d1, U__d0);
  TUBWCON_33_0 U3 (PP, W_T, W_R, S);
  UBBBG_0 U4 (C, S);
endmodule

module UBSD4VPPG_31_0_1 (PP, C, IN1, IN2__ds, IN2__d1, IN2__d0);
  output C;
  output [35:2] PP;
  input [31:0] IN1;
  input IN2__ds, IN2__d1, IN2__d0;
  wire NZ;
  wire S;
  wire U__d1, U__d0;
  wire [34:2] W_R;
  wire W_T;
  SD41DDECON_1 U0 (S, U__d1, U__d0, IN2__ds, IN2__d1, IN2__d0);
  NUBZero_32_32 U1 (NZ);
  TCU4VPPG_32_0_1 U2 (W_T, W_R, NZ, IN1, U__d1, U__d0);
  TUBWCON_35_2 U3 (PP, W_T, W_R, S);
  UBBBG_2 U4 (C, S);
endmodule

module UBSD4VPPG_31_0_10 (PP, C, IN1, IN2__ds, IN2__d1, IN2__d0);
  output C;
  output [53:20] PP;
  input [31:0] IN1;
  input IN2__ds, IN2__d1, IN2__d0;
  wire NZ;
  wire S;
  wire U__d1, U__d0;
  wire [52:20] W_R;
  wire W_T;
  SD41DDECON_10 U0 (S, U__d1, U__d0, IN2__ds, IN2__d1, IN2__d0);
  NUBZero_32_32 U1 (NZ);
  TCU4VPPG_32_0_10 U2 (W_T, W_R, NZ, IN1, U__d1, U__d0);
  TUBWCON_53_20 U3 (PP, W_T, W_R, S);
  UBBBG_20 U4 (C, S);
endmodule

module UBSD4VPPG_31_0_11 (PP, C, IN1, IN2__ds, IN2__d1, IN2__d0);
  output C;
  output [55:22] PP;
  input [31:0] IN1;
  input IN2__ds, IN2__d1, IN2__d0;
  wire NZ;
  wire S;
  wire U__d1, U__d0;
  wire [54:22] W_R;
  wire W_T;
  SD41DDECON_11 U0 (S, U__d1, U__d0, IN2__ds, IN2__d1, IN2__d0);
  NUBZero_32_32 U1 (NZ);
  TCU4VPPG_32_0_11 U2 (W_T, W_R, NZ, IN1, U__d1, U__d0);
  TUBWCON_55_22 U3 (PP, W_T, W_R, S);
  UBBBG_22 U4 (C, S);
endmodule

module UBSD4VPPG_31_0_12 (PP, C, IN1, IN2__ds, IN2__d1, IN2__d0);
  output C;
  output [57:24] PP;
  input [31:0] IN1;
  input IN2__ds, IN2__d1, IN2__d0;
  wire NZ;
  wire S;
  wire U__d1, U__d0;
  wire [56:24] W_R;
  wire W_T;
  SD41DDECON_12 U0 (S, U__d1, U__d0, IN2__ds, IN2__d1, IN2__d0);
  NUBZero_32_32 U1 (NZ);
  TCU4VPPG_32_0_12 U2 (W_T, W_R, NZ, IN1, U__d1, U__d0);
  TUBWCON_57_24 U3 (PP, W_T, W_R, S);
  UBBBG_24 U4 (C, S);
endmodule

module UBSD4VPPG_31_0_13 (PP, C, IN1, IN2__ds, IN2__d1, IN2__d0);
  output C;
  output [59:26] PP;
  input [31:0] IN1;
  input IN2__ds, IN2__d1, IN2__d0;
  wire NZ;
  wire S;
  wire U__d1, U__d0;
  wire [58:26] W_R;
  wire W_T;
  SD41DDECON_13 U0 (S, U__d1, U__d0, IN2__ds, IN2__d1, IN2__d0);
  NUBZero_32_32 U1 (NZ);
  TCU4VPPG_32_0_13 U2 (W_T, W_R, NZ, IN1, U__d1, U__d0);
  TUBWCON_59_26 U3 (PP, W_T, W_R, S);
  UBBBG_26 U4 (C, S);
endmodule

module UBSD4VPPG_31_0_14 (PP, C, IN1, IN2__ds, IN2__d1, IN2__d0);
  output C;
  output [61:28] PP;
  input [31:0] IN1;
  input IN2__ds, IN2__d1, IN2__d0;
  wire NZ;
  wire S;
  wire U__d1, U__d0;
  wire [60:28] W_R;
  wire W_T;
  SD41DDECON_14 U0 (S, U__d1, U__d0, IN2__ds, IN2__d1, IN2__d0);
  NUBZero_32_32 U1 (NZ);
  TCU4VPPG_32_0_14 U2 (W_T, W_R, NZ, IN1, U__d1, U__d0);
  TUBWCON_61_28 U3 (PP, W_T, W_R, S);
  UBBBG_28 U4 (C, S);
endmodule

module UBSD4VPPG_31_0_15 (PP, C, IN1, IN2__ds, IN2__d1, IN2__d0);
  output C;
  output [63:30] PP;
  input [31:0] IN1;
  input IN2__ds, IN2__d1, IN2__d0;
  wire NZ;
  wire S;
  wire U__d1, U__d0;
  wire [62:30] W_R;
  wire W_T;
  SD41DDECON_15 U0 (S, U__d1, U__d0, IN2__ds, IN2__d1, IN2__d0);
  NUBZero_32_32 U1 (NZ);
  TCU4VPPG_32_0_15 U2 (W_T, W_R, NZ, IN1, U__d1, U__d0);
  TUBWCON_63_30 U3 (PP, W_T, W_R, S);
  UBBBG_30 U4 (C, S);
endmodule

module UBSD4VPPG_31_0_16 (PP, C, IN1, IN2__ds, IN2__d1, IN2__d0);
  output C;
  output [65:32] PP;
  input [31:0] IN1;
  input IN2__ds, IN2__d1, IN2__d0;
  wire NZ;
  wire S;
  wire U__d1, U__d0;
  wire [64:32] W_R;
  wire W_T;
  SD41DDECON_16 U0 (S, U__d1, U__d0, IN2__ds, IN2__d1, IN2__d0);
  NUBZero_32_32 U1 (NZ);
  TCU4VPPG_32_0_16 U2 (W_T, W_R, NZ, IN1, U__d1, U__d0);
  TUBWCON_65_32 U3 (PP, W_T, W_R, S);
  UBBBG_32 U4 (C, S);
endmodule

module UBSD4VPPG_31_0_2 (PP, C, IN1, IN2__ds, IN2__d1, IN2__d0);
  output C;
  output [37:4] PP;
  input [31:0] IN1;
  input IN2__ds, IN2__d1, IN2__d0;
  wire NZ;
  wire S;
  wire U__d1, U__d0;
  wire [36:4] W_R;
  wire W_T;
  SD41DDECON_2 U0 (S, U__d1, U__d0, IN2__ds, IN2__d1, IN2__d0);
  NUBZero_32_32 U1 (NZ);
  TCU4VPPG_32_0_2 U2 (W_T, W_R, NZ, IN1, U__d1, U__d0);
  TUBWCON_37_4 U3 (PP, W_T, W_R, S);
  UBBBG_4 U4 (C, S);
endmodule

module UBSD4VPPG_31_0_3 (PP, C, IN1, IN2__ds, IN2__d1, IN2__d0);
  output C;
  output [39:6] PP;
  input [31:0] IN1;
  input IN2__ds, IN2__d1, IN2__d0;
  wire NZ;
  wire S;
  wire U__d1, U__d0;
  wire [38:6] W_R;
  wire W_T;
  SD41DDECON_3 U0 (S, U__d1, U__d0, IN2__ds, IN2__d1, IN2__d0);
  NUBZero_32_32 U1 (NZ);
  TCU4VPPG_32_0_3 U2 (W_T, W_R, NZ, IN1, U__d1, U__d0);
  TUBWCON_39_6 U3 (PP, W_T, W_R, S);
  UBBBG_6 U4 (C, S);
endmodule

module UBSD4VPPG_31_0_4 (PP, C, IN1, IN2__ds, IN2__d1, IN2__d0);
  output C;
  output [41:8] PP;
  input [31:0] IN1;
  input IN2__ds, IN2__d1, IN2__d0;
  wire NZ;
  wire S;
  wire U__d1, U__d0;
  wire [40:8] W_R;
  wire W_T;
  SD41DDECON_4 U0 (S, U__d1, U__d0, IN2__ds, IN2__d1, IN2__d0);
  NUBZero_32_32 U1 (NZ);
  TCU4VPPG_32_0_4 U2 (W_T, W_R, NZ, IN1, U__d1, U__d0);
  TUBWCON_41_8 U3 (PP, W_T, W_R, S);
  UBBBG_8 U4 (C, S);
endmodule

module UBSD4VPPG_31_0_5 (PP, C, IN1, IN2__ds, IN2__d1, IN2__d0);
  output C;
  output [43:10] PP;
  input [31:0] IN1;
  input IN2__ds, IN2__d1, IN2__d0;
  wire NZ;
  wire S;
  wire U__d1, U__d0;
  wire [42:10] W_R;
  wire W_T;
  SD41DDECON_5 U0 (S, U__d1, U__d0, IN2__ds, IN2__d1, IN2__d0);
  NUBZero_32_32 U1 (NZ);
  TCU4VPPG_32_0_5 U2 (W_T, W_R, NZ, IN1, U__d1, U__d0);
  TUBWCON_43_10 U3 (PP, W_T, W_R, S);
  UBBBG_10 U4 (C, S);
endmodule

module UBSD4VPPG_31_0_6 (PP, C, IN1, IN2__ds, IN2__d1, IN2__d0);
  output C;
  output [45:12] PP;
  input [31:0] IN1;
  input IN2__ds, IN2__d1, IN2__d0;
  wire NZ;
  wire S;
  wire U__d1, U__d0;
  wire [44:12] W_R;
  wire W_T;
  SD41DDECON_6 U0 (S, U__d1, U__d0, IN2__ds, IN2__d1, IN2__d0);
  NUBZero_32_32 U1 (NZ);
  TCU4VPPG_32_0_6 U2 (W_T, W_R, NZ, IN1, U__d1, U__d0);
  TUBWCON_45_12 U3 (PP, W_T, W_R, S);
  UBBBG_12 U4 (C, S);
endmodule

module UBSD4VPPG_31_0_7 (PP, C, IN1, IN2__ds, IN2__d1, IN2__d0);
  output C;
  output [47:14] PP;
  input [31:0] IN1;
  input IN2__ds, IN2__d1, IN2__d0;
  wire NZ;
  wire S;
  wire U__d1, U__d0;
  wire [46:14] W_R;
  wire W_T;
  SD41DDECON_7 U0 (S, U__d1, U__d0, IN2__ds, IN2__d1, IN2__d0);
  NUBZero_32_32 U1 (NZ);
  TCU4VPPG_32_0_7 U2 (W_T, W_R, NZ, IN1, U__d1, U__d0);
  TUBWCON_47_14 U3 (PP, W_T, W_R, S);
  UBBBG_14 U4 (C, S);
endmodule

module UBSD4VPPG_31_0_8 (PP, C, IN1, IN2__ds, IN2__d1, IN2__d0);
  output C;
  output [49:16] PP;
  input [31:0] IN1;
  input IN2__ds, IN2__d1, IN2__d0;
  wire NZ;
  wire S;
  wire U__d1, U__d0;
  wire [48:16] W_R;
  wire W_T;
  SD41DDECON_8 U0 (S, U__d1, U__d0, IN2__ds, IN2__d1, IN2__d0);
  NUBZero_32_32 U1 (NZ);
  TCU4VPPG_32_0_8 U2 (W_T, W_R, NZ, IN1, U__d1, U__d0);
  TUBWCON_49_16 U3 (PP, W_T, W_R, S);
  UBBBG_16 U4 (C, S);
endmodule

module UBSD4VPPG_31_0_9 (PP, C, IN1, IN2__ds, IN2__d1, IN2__d0);
  output C;
  output [51:18] PP;
  input [31:0] IN1;
  input IN2__ds, IN2__d1, IN2__d0;
  wire NZ;
  wire S;
  wire U__d1, U__d0;
  wire [50:18] W_R;
  wire W_T;
  SD41DDECON_9 U0 (S, U__d1, U__d0, IN2__ds, IN2__d1, IN2__d0);
  NUBZero_32_32 U1 (NZ);
  TCU4VPPG_32_0_9 U2 (W_T, W_R, NZ, IN1, U__d1, U__d0);
  TUBWCON_51_18 U3 (PP, W_T, W_R, S);
  UBBBG_18 U4 (C, S);
endmodule

module UBTCCONV66_68_0 (O, I);
  output [69:0] O;
  input [68:0] I;
  UBTC1CON69_0 U0 (O[0], I[0]);
  UBTC1CON69_1 U1 (O[1], I[1]);
  UBTC1CON69_2 U2 (O[2], I[2]);
  UBTC1CON69_3 U3 (O[3], I[3]);
  UBTC1CON69_4 U4 (O[4], I[4]);
  UBTC1CON69_5 U5 (O[5], I[5]);
  UBTC1CON69_6 U6 (O[6], I[6]);
  UBTC1CON69_7 U7 (O[7], I[7]);
  UBTC1CON69_8 U8 (O[8], I[8]);
  UBTC1CON69_9 U9 (O[9], I[9]);
  UBTC1CON69_10 U10 (O[10], I[10]);
  UBTC1CON69_11 U11 (O[11], I[11]);
  UBTC1CON69_12 U12 (O[12], I[12]);
  UBTC1CON69_13 U13 (O[13], I[13]);
  UBTC1CON69_14 U14 (O[14], I[14]);
  UBTC1CON69_15 U15 (O[15], I[15]);
  UBTC1CON69_16 U16 (O[16], I[16]);
  UBTC1CON69_17 U17 (O[17], I[17]);
  UBTC1CON69_18 U18 (O[18], I[18]);
  UBTC1CON69_19 U19 (O[19], I[19]);
  UBTC1CON69_20 U20 (O[20], I[20]);
  UBTC1CON69_21 U21 (O[21], I[21]);
  UBTC1CON69_22 U22 (O[22], I[22]);
  UBTC1CON69_23 U23 (O[23], I[23]);
  UBTC1CON69_24 U24 (O[24], I[24]);
  UBTC1CON69_25 U25 (O[25], I[25]);
  UBTC1CON69_26 U26 (O[26], I[26]);
  UBTC1CON69_27 U27 (O[27], I[27]);
  UBTC1CON69_28 U28 (O[28], I[28]);
  UBTC1CON69_29 U29 (O[29], I[29]);
  UBTC1CON69_30 U30 (O[30], I[30]);
  UBTC1CON69_31 U31 (O[31], I[31]);
  UBTC1CON69_32 U32 (O[32], I[32]);
  UBTC1CON69_33 U33 (O[33], I[33]);
  UBTC1CON69_34 U34 (O[34], I[34]);
  UBTC1CON69_35 U35 (O[35], I[35]);
  UBTC1CON69_36 U36 (O[36], I[36]);
  UBTC1CON69_37 U37 (O[37], I[37]);
  UBTC1CON69_38 U38 (O[38], I[38]);
  UBTC1CON69_39 U39 (O[39], I[39]);
  UBTC1CON69_40 U40 (O[40], I[40]);
  UBTC1CON69_41 U41 (O[41], I[41]);
  UBTC1CON69_42 U42 (O[42], I[42]);
  UBTC1CON69_43 U43 (O[43], I[43]);
  UBTC1CON69_44 U44 (O[44], I[44]);
  UBTC1CON69_45 U45 (O[45], I[45]);
  UBTC1CON69_46 U46 (O[46], I[46]);
  UBTC1CON69_47 U47 (O[47], I[47]);
  UBTC1CON69_48 U48 (O[48], I[48]);
  UBTC1CON69_49 U49 (O[49], I[49]);
  UBTC1CON69_50 U50 (O[50], I[50]);
  UBTC1CON69_51 U51 (O[51], I[51]);
  UBTC1CON69_52 U52 (O[52], I[52]);
  UBTC1CON69_53 U53 (O[53], I[53]);
  UBTC1CON69_54 U54 (O[54], I[54]);
  UBTC1CON69_55 U55 (O[55], I[55]);
  UBTC1CON69_56 U56 (O[56], I[56]);
  UBTC1CON69_57 U57 (O[57], I[57]);
  UBTC1CON69_58 U58 (O[58], I[58]);
  UBTC1CON69_59 U59 (O[59], I[59]);
  UBTC1CON69_60 U60 (O[60], I[60]);
  UBTC1CON69_61 U61 (O[61], I[61]);
  UBTC1CON69_62 U62 (O[62], I[62]);
  UBTC1CON69_63 U63 (O[63], I[63]);
  UBTC1CON69_64 U64 (O[64], I[64]);
  UBTC1CON69_65 U65 (O[65], I[65]);
  UBTCTCONV_68_66 U66 (O[69:66], I[68:66]);
endmodule

module WLCTR_34_0_36_0_3000 (S1, S2, PP0, PP1, PP2, PP3, PP4, PP5, PP6, PP7, PP8, PP9, PP10, PP11, PP12, PP13, PP14, PP15, PP16, PP17);
  output [67:6] S1;
  output [66:0] S2;
  input [34:0] PP0;
  input [36:0] PP1;
  input [54:18] PP10;
  input [56:20] PP11;
  input [58:22] PP12;
  input [60:24] PP13;
  input [62:26] PP14;
  input [64:28] PP15;
  input [65:30] PP16;
  input [33:32] PP17;
  input [38:2] PP2;
  input [40:4] PP3;
  input [42:6] PP4;
  input [44:8] PP5;
  input [46:10] PP6;
  input [48:12] PP7;
  input [50:14] PP8;
  input [52:16] PP9;
  wire [37:1] IC0;
  wire [43:7] IC1;
  wire [58:17] IC10;
  wire [66:30] IC11;
  wire [51:4] IC12;
  wire [66:26] IC13;
  wire [63:5] IC14;
  wire [49:13] IC2;
  wire [55:19] IC3;
  wire [61:25] IC4;
  wire [39:2] IC5;
  wire [50:11] IC6;
  wire [57:20] IC7;
  wire [65:29] IC8;
  wire [45:3] IC9;
  wire [38:0] IS0;
  wire [44:4] IS1;
  wire [62:11] IS10;
  wire [65:25] IS11;
  wire [62:0] IS12;
  wire [66:17] IS13;
  wire [66:0] IS14;
  wire [50:10] IS2;
  wire [56:16] IS3;
  wire [62:22] IS4;
  wire [44:0] IS5;
  wire [50:7] IS6;
  wire [62:16] IS7;
  wire [65:25] IS8;
  wire [50:0] IS9;
  CSA_34_0_36_0_38_000 U0 (IC0, IS0, PP0, PP1, PP2);
  CSA_40_4_42_6_44_000 U1 (IC1, IS1, PP3, PP4, PP5);
  CSA_46_10_48_12_5000 U2 (IC2, IS2, PP6, PP7, PP8);
  CSA_52_16_54_18_5000 U3 (IC3, IS3, PP9, PP10, PP11);
  CSA_58_22_60_24_6000 U4 (IC4, IS4, PP12, PP13, PP14);
  CSA_38_0_37_1_44_000 U5 (IC5, IS5, IS0, IC0, IS1);
  CSA_43_7_50_10_49000 U6 (IC6, IS6, IC1, IS2, IC2);
  CSA_56_16_55_19_6000 U7 (IC7, IS7, IS3, IC3, IS4);
  CSA_61_25_64_28_6000 U8 (IC8, IS8, IC4, PP15, PP16);
  CSA_44_0_39_2_50_000 U9 (IC9, IS9, IS5, IC5, IS6);
  CSA_50_11_62_16_5000 U10 (IC10, IS10, IC6, IS7, IC7);
  CSA_65_25_65_29_3000 U11 (IC11, IS11, IS8, IC8, PP17);
  CSA_50_0_45_3_62_000 U12 (IC12, IS12, IS9, IC9, IS10);
  CSA_58_17_65_25_6000 U13 (IC13, IS13, IC10, IS11, IC11);
  CSA_62_0_51_4_66_000 U14 (IC14, IS14, IS12, IC12, IS13);
  CSA_66_0_63_5_66_000 U15 (S1, S2, IS14, IC14, IC13);
endmodule

