#!/bin/sh

echo "*** Starting run:" 


FILES=scripts/*.sh

for f in $FILES
do
	echo "\n\nRun script $f:" 
	( ./../SC4AV -f $f ) 
done

echo "*** Finished." 

