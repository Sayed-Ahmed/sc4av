/**CPPFile****************************************************************

  FileName    [aig2polyn.cpp]

  SystemName  [SC4AV.]

  PackageName [ Build Polynomial.]

  Synopsis    [Polynomial extraction.]

  Author      [Amr Sayed Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]

  Revision    []

***********************************************************************/

#include"aig2polyn.h"
#include "misc/map.h"
////////////////////////////////////////////////////////////////////////
///                     FUNCTION DEFINITIONS                         ///
////////////////////////////////////////////////////////////////////////


/**Function*************************************************************

  Synopsis    [give new Ids to variables.]

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/

void givePolynId( GiaNtk * pGia, PolynMgr * pMgr, OrderedIdTable * aig2polynId){
    
    IdTable tempTable;
    MultiOrderedIdTable mTempTable = flipMapWithMultimap(*pMgr->_polynId2Aig);
    variableId nextElem=1;
    for (auto &it: mTempTable){
        auto lhs=it.second;
        auto rhs=it.first;        
        rhs=std::max(nextElem,rhs);
        while(pMgr->_inputVars->find(rhs)!=pMgr->_inputVars->end()
                || tempTable.find(rhs)!=tempTable.end()){
                rhs++;
        }
        tempTable.insert(std::make_pair(rhs,lhs));
        aig2polynId->insert(std::make_pair(lhs,rhs));
        nextElem=rhs+1;
    }
    
    
    pMgr->_polynId2Aig->swap(tempTable);
    
    return;
}


/**Function*************************************************************

  Synopsis    [add one Polynomial.]

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/


void addPolynomial(GiaNtk * pGia, PolynMgr * pMgr, int iMono_aig, 
                                                   OrderedIdTable * aig2polynId)
{
    int iFan0_aig, iFan1_aig;
    variableId iFan0, iFan1;
    GiaObj * pObj = (GiaObj *) Gia_ManObj( pGia, iMono_aig );
    

    iFan0_aig = Gia_ObjFaninId0p(pGia, pObj);
    iFan1_aig = Gia_ObjFaninId1p(pGia, pObj);
     
    variableId iMono=aig2polynId->at(iMono_aig);
    pMgr->_depVars->insert(iMono);
    
    
    auto iFan0_it= aig2polynId->find(iFan0_aig);
    if(iFan0_it!=aig2polynId->end()){
        iFan0=iFan0_it->second;
    }
    else iFan0=iFan0_aig;
    
    auto iFan1_it= aig2polynId->find(iFan1_aig);
    if(iFan1_it!=aig2polynId->end()){
        iFan1=iFan1_it->second;
    }
    else iFan1=iFan1_aig;
    
    pPolynomialData _polynData = pMgr->allocatePolynomialData();
    
    if ( Gia_ObjIsXor(pObj) )
    {
        Coefficient coeffx=1, coeffy=1, coeffxy=-2;
        pMonomial monlx= pMgr->allocateMonomial(iFan0);
        pMonomial monly= pMgr->allocateMonomial(iFan1);
        pMonomial monlxy= pMgr->allocateMonomial(iFan0,iFan1);
        pTerm termx= pMgr->allocateTerm(coeffx, monlx);
        pTerm termy= pMgr->allocateTerm(coeffy, monly);
        pTerm termxy= pMgr->allocateTerm(coeffxy, monlxy);
        pMgr->insertTerm(_polynData->leadTerms, termx);
        pMgr->insertTerm(_polynData->leadTerms, termy);
        pMgr->insertTerm(_polynData->leadTerms, termxy);        
        pMgr->updateMonomialVariableTables(monlxy, iMono);
        __Byte assign1 (std::string("00000001"));
        __Byte assign2 (std::string("00000010"));
        _polynData->lut->insert(assign1);
        _polynData->lut->insert(assign2);
    }
    else if ( Gia_ObjFaninC0(pObj) && Gia_ObjFaninC1(pObj) )  //  C * (1 - x) * (1 - y)
    {
        Coefficient coeff1=1, coeffx=-1, coeffy=-1, coeffxy=1;
        pMonomial monl1= pMgr->allocateMonomial();
        pMonomial monlx= pMgr->allocateMonomial(iFan0);
        pMonomial monly= pMgr->allocateMonomial(iFan1);
        pMonomial monlxy= pMgr->allocateMonomial(iFan0, iFan1);
        pTerm term1= pMgr->allocateTerm(coeff1, monl1);
        pTerm termx= pMgr->allocateTerm(coeffx, monlx);
        pTerm termy= pMgr->allocateTerm(coeffy, monly);
        pTerm termxy= pMgr->allocateTerm(coeffxy, monlxy);
        pMgr->insertTerm(_polynData->leadTerms, term1);
        pMgr->insertTerm(_polynData->leadTerms, termx);
        pMgr->insertTerm(_polynData->leadTerms, termy);
        pMgr->insertTerm(_polynData->leadTerms, termxy);
        
        pMgr->updateMonomialVariableTables(monlxy, iMono);
         __Byte assign (std::string("00000000"));
         _polynData->lut->insert(assign);

    }
    else if ( Gia_ObjFaninC0(pObj) && !Gia_ObjFaninC1(pObj) ) //  C * (1 - x) * y
    {
        Coefficient coeffy=1, coeffxy=-1;
        
        pMonomial monly= pMgr->allocateMonomial(iFan1);
        pMonomial monlxy= pMgr->allocateMonomial(iFan0, iFan1);
        pTerm termy= pMgr->allocateTerm(coeffy, monly);
        pTerm termxy= pMgr->allocateTerm(coeffxy, monlxy);
        pMgr->insertTerm(_polynData->leadTerms, termy);
        pMgr->insertTerm(_polynData->leadTerms, termxy);
        
        pMgr->updateMonomialVariableTables(monlxy, iMono);
        
        if(iFan0<iFan1){
            __Byte assign (std::string("00000001"));
            _polynData->lut->insert(assign);
        }
        else{
            __Byte assign (std::string("00000010"));
            _polynData->lut->insert(assign);
        }
    }
    else if ( !Gia_ObjFaninC0(pObj) && Gia_ObjFaninC1(pObj) ) //  C * x * (1 - y)
    {
        Coefficient coeffx=1, coeffxy=-1;
        
        pMonomial monlx= pMgr->allocateMonomial(iFan0);
        pMonomial monlxy= pMgr->allocateMonomial(iFan0, iFan1);
        pTerm termx= pMgr->allocateTerm(coeffx, monlx);
        pTerm termxy= pMgr->allocateTerm(coeffxy, monlxy);
        //_polynData->leadTerms->insert(termx); //x
        //_polynData->leadTerms->insert(termxy); // -1*x*y
        pMgr->insertTerm(_polynData->leadTerms, termx);
        pMgr->insertTerm(_polynData->leadTerms, termxy);
        
        pMgr->updateMonomialVariableTables(monlxy, iMono);
        if(iFan0<iFan1){
            std::bitset<8> assign (std::string("00000010"));
            _polynData->lut->insert(assign);
        }
        else{
            std::bitset<8> assign (std::string("00000001"));
            _polynData->lut->insert(assign);
        }
    }
    else
    {   
        Coefficient  coeffxy=1;
        pMonomial monlxy= pMgr->allocateMonomial(iFan0, iFan1);
        pTerm termxy= pMgr->allocateTerm(coeffxy, monlxy);
        //_polynData->leadTerms->insert(termxy); // x*y
        pMgr->insertTerm(_polynData->leadTerms, termxy);
        
        pMgr->updateMonomialVariableTables(monlxy, iMono);
        
        __Byte assign (std::string("00000011"));
        _polynData->lut->insert(assign);
    }
    
    _polynData->inVars->insert(iFan0);
    _polynData->inVars->insert(iFan1);
    
    pMgr->addPolyn2Ideal(iMono, _polynData);
    pMgr->updateVariableSubstitutionSize(iMono, _polynData->leadTerms->size());
}


/**Function*************************************************************

  Synopsis    [Assign Weights to Variables.]

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
void assignWeight_rec(GiaNtk * pGia, PolynMgr * pMgr, int iMono )
{
    int iFan0, iFan1;

    GiaObj * pObj = (GiaObj *) Gia_ManObj( pGia, iMono );
    
    if ( !Gia_ObjIsAnd(pObj) )
        return;
    
    assert( !Gia_ObjIsMux(pGia, pObj) );
    
    if( pMgr->isNotInIdTable(pMgr->_polynId2Aig,iMono) ){
        
        iFan0 = Gia_ObjFaninId0p(pGia, pObj);
        iFan1 = Gia_ObjFaninId1p(pGia, pObj);
        assignWeight_rec (pGia, pMgr, iFan0);
        assignWeight_rec (pGia, pMgr, iFan1);
        
        auto iFan0_it= pMgr->_polynId2Aig->find(iFan0);
        variableId iFan0_weight=0;
        if (iFan0_it!= pMgr->_polynId2Aig->end()){
            iFan0_weight= iFan0_it->second;   
        }
        
        auto iFan1_it= pMgr->_polynId2Aig->find(iFan1);
        variableId iFan1_weight=0;
        if (iFan1_it!= pMgr->_polynId2Aig->end()){
            iFan1_weight= iFan1_it->second;   
        }
        
        variableId iMono_weight= std::max(iFan0_weight, iFan1_weight)+1;
        
        pMgr->_polynId2Aig->insert(std::make_pair(iMono,iMono_weight));
    }
    else return;  
}



/**Function*************************************************************

  Synopsis    [Build Set of Polynomials from AIG.]

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
PolynMgr * buildPolynFromAig( GiaNtk * pGia, int fVerbose, vecStr* logFileName )
{

    PolynMgr * pMgr = new PolynMgr( );
    abctime clk = Abc_Clock();
    GiaObj * pObj; 
    int i, j,k, iDriver, jDriver, kDriver;
    OrderedIdTable aig2polyn;
    
    pMgr->openLogFile(logFileName);
    fprintf(pMgr->logFile, "***c Log of Circuit \"%s.aig\" Generated by SC4AV Tool:\n\n\n", pGia->pName);
    fprintf(pMgr->logFile, "***c Building Algebraic Model\n\n");
    
    GiaNtkForEachCiId( pGia, iDriver, i )
    {
        pMgr->_inputVars->insert(iDriver);
    }
    
    auto inputsSize=pMgr->_inputVars->size();
    GiaNtkForEachCiIdFromTo( pGia, jDriver, j, 0, inputsSize/2){
        GiaNtkForEachCiIdFromTo( pGia, kDriver, k, inputsSize/2, inputsSize){
            auto pMonl=pMgr->allocateMonomial(jDriver, kDriver);
            EXPCoefficient pow;
            mpz_ui_pow_ui (pow.get_mpz_t(), 2, j+(k-inputsSize/2));
            auto pTerm=pMgr->allocateEXPTerm( pow,pMonl);
            pMgr->_specPolyn->insert(pTerm);
        }
    }
        
    GiaNtkForEachCoReverse( pGia, pObj, i )
    {
        iDriver = Gia_ObjFaninId0p( pGia, pObj );
        int id = Gia_ObjId( pGia, pObj );
        assignWeight_rec( pGia,  pMgr, iDriver );
        assert(!pMgr->isPrimaryInput(iDriver));
        pMgr->_outputVars->insert(id);
        auto pMonl=pMgr->allocateMonomial(id);
        int maxSize=pMgr->_inputVars->size();
        EXPCoefficient pow;
        //maxSize=128;
        if(i<maxSize){
            mpz_ui_pow_ui (pow.get_mpz_t(), 2, i);
            auto pTerm=pMgr->allocateEXPTerm( -1*pow,pMonl);
            pMgr->_specPolyn->insert(pTerm);
        }
        else{
            mpz_ui_pow_ui (pow.get_mpz_t(), 2, i-maxSize);
            auto pTerm=pMgr->allocateEXPTerm(pow,pMonl);
            //pMgr->_specPolyn->insert(pTerm);
        }
    }
    //pMgr->printEXPPolynomial(stdout, pMgr->_specPolyn);
    //exit(0);
    givePolynId(pGia, pMgr, &aig2polyn);
    
    for(auto &it : aig2polyn)
    {
        addPolynomial( pGia,  pMgr, it.first, &aig2polyn);
    }
    
    GiaNtkForEachCoReverse( pGia, pObj, i )
    {
        iDriver = Gia_ObjFaninId0p( pGia, pObj );
        int id =Gia_ObjId( pGia, pObj );
        if(pMgr->isNotInIdeal(id)){
            auto monl= pMgr->allocateMonomial( aig2polyn.at(iDriver));
            auto _polynData = pMgr->allocatePolynomialData();
            _polynData->inVars->insert(aig2polyn.at(iDriver));
            if ( Gia_ObjFaninC0(pObj) ){
              auto term= pMgr->allocateTerm(-1, monl);
              pMgr->insertTerm(_polynData->leadTerms,term ); 
              auto monl1= pMgr->allocateMonomial();
              auto term1= pMgr->allocateTerm(1, monl1);
              pMgr->insertTerm(_polynData->leadTerms,term1 ); 
            }
            else{        
              auto term= pMgr->allocateTerm(1, monl);
              pMgr->insertTerm(_polynData->leadTerms,term );
            }
            pMgr->updateMonomialVariableTables(monl, id);
            pMgr->addPolyn2Ideal(id, _polynData);
            pMgr->updateVariableSubstitutionSize(id, 1);
        }
    }
    //if(fVerbose>0)
    //    pMgr->printIdeal(stdout, fVerbose );
    
    printSC4AVTime(pMgr->logFile, "***c Build Time", Abc_Clock() - clk);

    return pMgr;
}




/**Function*************************************************************

  Synopsis    [give new Ids to variables.]

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/

void giveGFPolynId( GiaNtk * pGia, GFPolynMgr * pMgr, OrderedIdTable * aig2polynId){
    
    IdTable tempTable;
    MultiOrderedIdTable mTempTable = flipMapWithMultimap(*pMgr->_polynId2Aig);
    variableId nextElem=1;
    for (auto &it: mTempTable){
        auto lhs=it.second;
        auto rhs=it.first;        
        rhs=std::max(nextElem,rhs);
        while(pMgr->_inputVars->find(rhs)!=pMgr->_inputVars->end()
                || tempTable.find(rhs)!=tempTable.end()){
                rhs++;
        }
        tempTable.insert(std::make_pair(rhs,lhs));
        aig2polynId->insert(std::make_pair(lhs,rhs));
        nextElem=rhs+1;
    }
    
    
    pMgr->_polynId2Aig->swap(tempTable);
    return;
}


/**Function*************************************************************

  Synopsis    [add one Polynomial.]

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/


void addGFPolynomial(GiaNtk * pGia, GFPolynMgr * pMgr, int iMono_aig, 
                                                   OrderedIdTable * aig2polynId)
{
    int iFan0_aig, iFan1_aig;
    variableId iFan0, iFan1;
    GiaObj * pObj = (GiaObj *) Gia_ManObj( pGia, iMono_aig );
    

    iFan0_aig = Gia_ObjFaninId0p(pGia, pObj);
    iFan1_aig = Gia_ObjFaninId1p(pGia, pObj);
     
    variableId iMono=aig2polynId->at(iMono_aig);
    pMgr->_depVars->insert(iMono);
    
    
    auto iFan0_it= aig2polynId->find(iFan0_aig);
    if(iFan0_it!=aig2polynId->end()){
        iFan0=iFan0_it->second;
    }
    else iFan0=iFan0_aig;
    
    auto iFan1_it= aig2polynId->find(iFan1_aig);
    if(iFan1_it!=aig2polynId->end()){
        iFan1=iFan1_it->second;
    }
    else iFan1=iFan1_aig;
    
    pGFPolynomialData _polynData = pMgr->allocateGFPolynomialData();
    
    if ( Gia_ObjIsXor(pObj) )
    {
        pMonomial monlx= pMgr->allocateMonomial(iFan0);
        pMonomial monly= pMgr->allocateMonomial(iFan1);
        pMgr->insertMonl(_polynData->leadMonls, monlx);
        pMgr->insertMonl(_polynData->leadMonls, monly);
        __Byte assign1 (std::string("00000001"));
        __Byte assign2 (std::string("00000010"));
        _polynData->lut->insert(assign1);
        _polynData->lut->insert(assign2);
    }
    else if ( Gia_ObjFaninC0(pObj) && Gia_ObjFaninC1(pObj) )  //  C * (1 - x) * (1 - y)
    {
        pMonomial monl1= pMgr->allocateMonomial();
        pMonomial monlx= pMgr->allocateMonomial(iFan0);
        pMonomial monly= pMgr->allocateMonomial(iFan1);
        pMonomial monlxy= pMgr->allocateMonomial(iFan0, iFan1);
        pMgr->insertMonl(_polynData->leadMonls, monl1);
        pMgr->insertMonl(_polynData->leadMonls, monlx);
        pMgr->insertMonl(_polynData->leadMonls, monly);
        pMgr->insertMonl(_polynData->leadMonls, monlxy);
        
        pMgr->updateMonomialVariableTables(monlxy, iMono);
         __Byte assign (std::string("00000000"));
         _polynData->lut->insert(assign);

    }
    else if ( Gia_ObjFaninC0(pObj) && !Gia_ObjFaninC1(pObj) ) //  C * (1 - x) * y
    {
        pMonomial monly= pMgr->allocateMonomial(iFan1);
        pMonomial monlxy= pMgr->allocateMonomial(iFan0, iFan1);
        pMgr->insertMonl(_polynData->leadMonls, monly);
        pMgr->insertMonl(_polynData->leadMonls, monlxy);
        
        pMgr->updateMonomialVariableTables(monlxy, iMono);
        
        if(iFan0<iFan1){
            __Byte assign (std::string("00000001"));
            _polynData->lut->insert(assign);
        }
        else{
            __Byte assign (std::string("00000010"));
            _polynData->lut->insert(assign);
        }
    }
    else if ( !Gia_ObjFaninC0(pObj) && Gia_ObjFaninC1(pObj) ) //  C * x * (1 - y)
    {   
        pMonomial monlx= pMgr->allocateMonomial(iFan0);
        pMonomial monlxy= pMgr->allocateMonomial(iFan0, iFan1);
        pMgr->insertMonl(_polynData->leadMonls, monlx);
        pMgr->insertMonl(_polynData->leadMonls, monlxy);
        
        pMgr->updateMonomialVariableTables(monlxy, iMono);
        if(iFan0<iFan1){
            std::bitset<8> assign (std::string("00000010"));
            _polynData->lut->insert(assign);
        }
        else{
            std::bitset<8> assign (std::string("00000001"));
            _polynData->lut->insert(assign);
        }
    }
    else
    {   
        pMonomial monlxy= pMgr->allocateMonomial(iFan0, iFan1);
        pMgr->insertMonl(_polynData->leadMonls, monlxy);
        
        pMgr->updateMonomialVariableTables(monlxy, iMono);
        
        __Byte assign (std::string("00000011"));
        _polynData->lut->insert(assign);
    }
    
    _polynData->inVars->insert(iFan0);
    _polynData->inVars->insert(iFan1);
    
    pMgr->addPolyn2GFIdeal(iMono, _polynData);
    pMgr->updateVariableSubstitutionSize(iMono, _polynData->leadMonls->size());
}


/**Function*************************************************************

  Synopsis    [Assign Weights to Variables.]

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
void assignGFWeight_rec(GiaNtk * pGia, GFPolynMgr * pMgr, int iMono )
{
    int iFan0, iFan1;

    GiaObj * pObj = (GiaObj *) Gia_ManObj( pGia, iMono );
    
    if ( !Gia_ObjIsAnd(pObj) )
        return;
    
    assert( !Gia_ObjIsMux(pGia, pObj) );
    
    if( pMgr->isNotInIdTable(pMgr->_polynId2Aig,iMono) ){
        
        iFan0 = Gia_ObjFaninId0p(pGia, pObj);
        iFan1 = Gia_ObjFaninId1p(pGia, pObj);
        assignGFWeight_rec (pGia, pMgr, iFan0);
        assignGFWeight_rec (pGia, pMgr, iFan1);
        
        auto iFan0_it= pMgr->_polynId2Aig->find(iFan0);
        variableId iFan0_weight=0;
        if (iFan0_it!= pMgr->_polynId2Aig->end()){
            iFan0_weight= iFan0_it->second;   
        }
        
        auto iFan1_it= pMgr->_polynId2Aig->find(iFan1);
        variableId iFan1_weight=0;
        if (iFan1_it!= pMgr->_polynId2Aig->end()){
            iFan1_weight= iFan1_it->second;   
        }
        
        variableId iMono_weight= std::max(iFan0_weight, iFan1_weight)+1;
        
        pMgr->_polynId2Aig->insert(std::make_pair(iMono,iMono_weight));
    }
    else return;  
}


/**Function*************************************************************

  Synopsis    [Build Set of Polynomials over Galois Field (GF2)from AIG.]

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
GFPolynMgr * buildGFPolynFromAig( GiaNtk * pGia, int fVerbose )
{
    GFPolynMgr * pMgr = new GFPolynMgr( );
    abctime clk = Abc_Clock();
    GiaObj * pObj; 
    int i, iDriver;
    OrderedIdTable aig2polyn;
    
    pMgr->openLogFile(pGia->pName);
    
    fprintf(pMgr->logFile, "***c Building Algebraic Model\n\n");
    
    GiaNtkForEachCiId( pGia, iDriver, i )
    {
        pMgr->_inputVars->insert(iDriver);
    }
        
    GiaNtkForEachCoReverse( pGia, pObj, i )
    {
        iDriver = Gia_ObjFaninId0p( pGia, pObj );
        int id = Gia_ObjId( pGia, pObj );
        assignGFWeight_rec( pGia,  pMgr, iDriver );
        assert(!pMgr->isPrimaryInput(iDriver));
        pMgr->_outputVars->insert(id);
        auto pMonl=pMgr->allocateMonomial(id);
        pMgr->_specPolyn->insert(pMonl);
    }
    
    giveGFPolynId(pGia, pMgr, &aig2polyn);
    
    for(auto &it : aig2polyn)
    {
        addGFPolynomial( pGia,  pMgr, it.first, &aig2polyn);
    }
    GiaNtkForEachCoReverse( pGia, pObj, i )
    {
        iDriver = Gia_ObjFaninId0p( pGia, pObj );
        int id =Gia_ObjId( pGia, pObj );
        if(pMgr->isNotInGFIdeal(id)){
            auto monl= pMgr->allocateMonomial( aig2polyn.at(iDriver));
            auto _polynData = pMgr->allocateGFPolynomialData();
            _polynData->inVars->insert(aig2polyn.at(iDriver));
            if ( Gia_ObjFaninC0(pObj) ){
                pMgr->insertMonl(_polynData->leadMonls,monl );
                auto monl1= pMgr->allocateMonomial();
                pMgr->insertMonl(_polynData->leadMonls,monl1 );
            }
            else{        
                pMgr->insertMonl(_polynData->leadMonls,monl );
            }
            pMgr->updateMonomialVariableTables(monl, id);
            pMgr->addPolyn2GFIdeal(id, _polynData);
            pMgr->updateVariableSubstitutionSize(id, 1);
        }
    }
    if(fVerbose>0)
        pMgr->printGFIdeal(stdout, fVerbose );
    
    printSC4AVTime(pMgr->logFile, "***c Build Time", Abc_Clock() - clk);

    return pMgr;
}



