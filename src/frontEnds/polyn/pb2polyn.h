/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   pb2polyn.h
 * Author: amr
 *
 * Created on October 26, 2017, 10:00 AM
 */
#include"INTprover/polyn.h"
#ifndef PB2POLYN_H
#define PB2POLYN_H



class FileBuffer {
    string    str;
    int     next;
public:
    int     line;
    int     i;
    FileBuffer(const char * buf) {
        str=buf;
        i=0;
        next = buf[i];
        line = 1; }
   ~FileBuffer() {}
    int  operator * () { return next; }
    void operator ++ () { if (next == '\n') line++;  i++; next =str[i] ;} 
    const char * fileStr(){return str.c_str();}
};

void parseSpecPBfile(vecStr* filename, PolynMgr * pMgr); 

#endif /* PB2POLYN_H */

