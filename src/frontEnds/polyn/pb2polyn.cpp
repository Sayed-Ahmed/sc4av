/**CPPFile****************************************************************

  FileName    [pb2polyn.cpp]

  SystemName  [SC4AV.]

  PackageName [ Build Polynomial from pseudo boolean constriants.]

  Synopsis    [Polynomial extraction.]

  Author      [Amr Sayed Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]

  Revision    []

***********************************************************************/

// Parser buffers (streams):


//==============================================================================
// PB Parser:
#include"pb2polyn.h"

template<class B>
static void skipWhitespace(B& in) {     // not including newline
    while (*in == ' ' || *in == '\t')
        ++in; }

template<class B>
static void skipLine(B& in) {
    for (;;){
        if (*in == EOF) return;
        if (*in == '\n') { ++in; return; }
        ++in; } }

template<class B>
static void skipComments(B& in) {      // skip comment and empty lines (assuming we are at beginning of line)
    while (*in == '*' || *in == '\n') skipLine(in); }

template<class B>
static bool skipEndOfLine(B& in) {     // skip newline AND trailing comment/empty lines
    if (*in == '\n') ++in;
    else             return false;
    skipComments(in);
    return true; }

template<class B>
static bool skipText(B& in, char* text) {
    while (*text != 0){
        if (*in != *text) return false;
        ++in, ++text; }
    return true; }


static int parseInt(const char* tmp) {
    char in[1000];
    strcpy(in, tmp);
    int val=0;
    int i=0;
    while (in[i] >= '0' && in[i] <= '9'){
        val *= 10;
        val += (in[i] - '0');
        i++; }
    return  val; }

template<class B>
static EXPCoefficient parseLargeInt(B& in) {
    EXPCoefficient    val;
    bool    neg = false;
    skipWhitespace(in);
    if      (*in == '-') neg = true, ++in;
    else if (*in == '+') ++in;
    skipWhitespace(in);     // BE NICE: allow "- 3" and "+  4" etc.
    if (*in < '0' || *in > '9'){
        printf("***e Error! Expected digit, not: %c\n\n", *in);
        throw ;
    }
    while (*in >= '0' && *in <= '9'){
        val *= 10;
        val += (*in - '0');
        ++in; }
    return neg ? -val : val; }

template<class B>
static int parseIdent(B& in) {   // 'tmp' is cleared, then filled with the parsed string. '(char*)tmp' is returned for convenience.
   std::string tmp;   
    skipWhitespace(in);
    if ((*in < 'a' || *in > 'z') && (*in < 'A' || *in > 'Z') && *in != '_'){
        printf("***e Error! Expected start of identifier, not: %c\n\n", *in);
        throw;
    }
    tmp.clear();
    while ((*in >= 'a' && *in <= 'z') || (*in >= 'A' && *in <= 'Z') || (*in >= '0' && *in <= '9') || *in == '_'){
        if (*in >= '0' && *in <= '9') tmp+=*in;
        ++in;
    }

    return parseInt(tmp.c_str());}

template<class B, class S>
void parseExpr(B& in, S& solver)
{
    bool empty = true;
    for(;;){
        skipWhitespace(in);
        if ((*in < '0' || *in > '9') && *in != '+' && *in != '-'){
            break;
        }
        auto term=solver->createExpTermConst(parseLargeInt(in));
        skipWhitespace(in);

        while(*in != '+' && *in != '-'&& *in != '='&& *in != '<' && *in != '>'){
            if (*in != '*'){
                printf("***e Error! Missing '*' after coefficient not %c %d\n\n", *in, *in);
                throw;
            }
            else ++in;
            variableId var=parseIdent(in);
            term->second->insert(var);
        }
        solver->insertEXPTerm(solver->_specPolyn, term);
        empty = false;
    }
    if (empty){
            printf("***e Error! Empty expression\n");
            throw;
    }
}

template<class B, class S>
static void parseSize(B& in, S& solver)
{
    int n_vars, n_constrs;

    if (*in != '*') return;
    ++in;
    skipWhitespace(in);

    if (!skipText(in, "#variable=")) goto Abort;

    skipWhitespace(in);
    if (!skipText(in, "#constraint=")) goto Abort;

  Abort:
    skipLine(in);
    skipComments(in);
}


template<class B>
int parseInequality(B& in)
{
    int ineq;
    skipWhitespace(in);
    if (*in == '<'){
        ++in;
        if (*in == '=') ineq = -1, ++in;
        else            ineq = -2;
    }else if (*in == '>'){
        ++in;
        if (*in == '=') ineq = +1, ++in;
        else            ineq = +2;
    }else{
        if (*in == '='){
            ++in;
            if (*in == '=') ++in;
            ineq = 0;
        }else{
            printf("***e Error! Expected inequality, not: %c in line %d\n\n", *in, in.line);
            throw;
        }
    }
    return ineq;
}

template<class B, class S>
bool parseConstrs(B& in, S& solver)
{
    int     ineq;
    EXPCoefficient  rhs;
    while (*in != EOF && *in!=0){
        //if(i>0){
        //    printf("***e Error! The Tool accepts only one Equation as Specification\n\n");
        //    throw;
        //} 
        parseExpr(in, solver);
        ineq = parseInequality(in);
        if(ineq!=0){
            printf("***e Error! The Tool accepts only the Equal constriants \"=\"\n\n");
            throw;
        }
        rhs  = parseLargeInt(in);

        skipWhitespace(in);
        if (!skipText(in, ";")){
            printf("***e Error! Expecting ';' after constraint. in line %d\n\n", in.line);
            throw;
        }
        skipEndOfLine(in);
        if ((*in >= '0' && *in <= '9') || *in == '+' || *in == '-'){
            printf("***e Error! The Tool accepts only one Equation as Specification\n\n");
            throw;
        }
        break;
    }
    return true;
}

static void putInbuf(const char * filename, char * buffer){

  FILE *pFile=NULL;
  pFile=fopen (filename,"r");
    if (pFile==NULL){
        printf("***e Error! Cannot open Specification File %s.\n\n", filename);
        throw;
   }
  fgets( buffer, 10*BUFSIZ, pFile);
}
//=================================================================================================
// Main parser functions:


template<class B, class S>
static bool parsePBSpec(B& in, S& solver)
{
    try{
        parseSize(in, solver);
        return parseConstrs(in, solver);
    }catch (char* msg){
        //if (abort_on_error){
            printf("***e Error! PARSE ERROR! %s\n", msg);
            exit(5);
        //}else
         //   throw msg;
    }

}

void parseSpecPBfile(vecStr* filename, PolynMgr * pMgr) {
    if (filename->size()==0){
        printf("***w Warning! NO Functional Specification,"
                " Default Specification of n-bit Multiplier Will be Tested. \n\n");
        return;
    }
     char buffer[BUFSIZ*10];
    memset( buffer, '\0', BUFSIZ*10);
    putInbuf(filename->c_str(), buffer);
    FileBuffer in =  FileBuffer(buffer);
    pMgr->_specPolyn->clear();
    parsePBSpec(in, pMgr); 
}
