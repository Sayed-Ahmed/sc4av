/**HFile****************************************************************

  FileName    [aig2polyn.h]

  SystemName  [SC4AV.]

  PackageName [frontEnds.]

  Synopsis    [build polynomials from Aig.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/

#ifndef AIG2POLYN_H
#define AIG2POLYN_H

#include"INTprover/polyn.h"
#include"GFprover/GFpolyn.h"
#include <base/abc/abc.h>
#include <base/io/ioAbc.h>
#include <proof/acec/acecInt.h>
#include <extra/extra.h>

typedef struct _GiaNtk: ABC_NAMESPACE_PREFIX Gia_Man_t_{

} GiaNtk;

typedef struct _GiaObj: ABC_NAMESPACE_PREFIX Gia_Obj_t{
    
}GiaObj;

typedef std::pair<variableId, variableId> IdPair;
typedef std::map<variableId, variableId> OrderedIdTable;
typedef std::multimap<variableId, variableId> MultiOrderedIdTable;
typedef std::shared_ptr<OrderedIdTable> pOrderedIdTable;

#define GiaNtkForEachCoReverse( p, pObj, i )                           \
    for ( i = Vec_IntSize(p->vCos) - 1; (i >= 0) && ((pObj) = (GiaObj*)Gia_ManCo(p, i)); i-- )


#define GiaNtkForEachCiId( p, Id, i )                                  \
    for ( i = 0; (i < Vec_IntSize(p->vCis)) && ((Id) = Gia_ObjId(p, Gia_ManCi(p, i))); i++ )
#define GiaNtkForEachCiIdFromTo( p, Id, i, from, to )                                  \
    for ( i = from; (i < to) && ((Id) = Gia_ObjId(p, Gia_ManCi(p, i))); i++ )

#define GiaNtkForEachCoId( p, Id, i )                                  \
    for ( i = 0; (i < Vec_IntSize(p->vCos)) && ((Id) = Gia_ObjId(p, Gia_ManCo(p, i))); i++ )

PolynMgr * buildPolynFromAig( GiaNtk *, int, vecStr* );
GFPolynMgr * buildGFPolynFromAig( GiaNtk *, int );
#endif /* AIG2POLYN_H */

