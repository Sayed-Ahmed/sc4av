
#include "extra.h"

/**Function*************************************************************

  Synopsis    [Extra_UtilStrsav()]

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
char * utilStrsav( const char *s )
{
    if(s == NULL) {  /* added 7/95, for robustness */
       return NULL;
    }
    else {
       return strcpy(ALLOC_CHAR_ARRAY( strlen(s)+1), s);
    }
}

/**Function*************************************************************

  Synopsis    [Frees the previously allocated argv array.]

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
char ** addToCharsArray( int argc, char ** argv, const char * elem )
{
    char ** argv2;
    int i;
    *argv2 = ALLOC_CHAR_ARRAY( argc + 1 ); 
    argv2[0] = utilStrsav( elem ); 
    for ( i = 0; i < argc; i++ )
        argv2[i+1] = utilStrsav( argv[i] ); 
    return argv2;
}


/**Function*************************************************************

  Synopsis    [Frees the previously allocated argv array.]

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/

void freeCharsArray( int argc, char **argv )
{
    int i;
    for ( i = 0; i < argc; i++ )
        FREE_CHAR_ARRAY( argv[i] );
    FREE_CHAR_ARRAY( argv );
}