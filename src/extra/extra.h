/**HFile****************************************************************

  FileName    [sc4commands.h]

  SystemName  [SC4Arith.]

  PackageName [extra.]

  Synopsis    []

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/

#include <stdlib.h>
#include <cstring>

#ifndef EXTRA_H
#define EXTRA_H

extern const char * utilOptarg;        // Global argument pointer (util_optarg)
extern int    utilOptind;    // Global argv index (util_optind)
extern const char *scanStr;


#define ALLOC_CHAR_ARRAY(num)   ((char *) malloc(sizeof(char) * (num)))
#define FREE_CHAR_ARRAY(obj)    ((obj) ? (free((char *) (obj)), (obj) = 0) : 0)

/*=== functions ========================================================*/
extern int  utilGetopt( int argc, char *argv[], const char *optstring );
extern void  utilGetoptReset( );
char ** addToCharsArray( int argc, char ** argv, const char * elem );
void freeCharsArray( int argc, char **argv );
double cpuTimeDouble();
char * utilGetVersion();

#define printSC4AVTime(f,a,t)    (fprintf(f, "%s = %9.2f sec\n", (a), 1.0*(t)/(CLOCKS_PER_SEC)))
#define printSC4AVRealTime(f,a,t)    (fprintf(f, "%s = %9.3f sec\n", (a), (t)/(CLOCKS_PER_SEC)))

#endif /* EXTRA_H */






