/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include "extra/extra.h"

const char * utilOptarg;        // Global argument pointer (util_optarg)
int    utilOptind=0;    // Global argv index (util_optind)
const char *scanStr;
/**Function*************************************************************

  Synopsis    [util_getopt()]

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
int utilGetopt( int argc, char *argv[], const char *optstring )
{
    register int c;
    register const char *place;

    utilOptarg = NULL;

    if (scanStr == NULL || *scanStr == '\0') 
    {
        if (utilOptind == 0) 
            utilOptind++;
        if (utilOptind >= argc) 
            return EOF;
        place = argv[utilOptind];
        if (place[0] != '-' || place[1] == '\0') 
            return EOF;
        utilOptind++;
        if (place[1] == '-' && place[2] == '\0') 
            return EOF;
        scanStr = place+1;
    }

    c = *scanStr++;
    place = strchr(optstring, c);
    if (place == NULL || c == ':') {
        (void) fprintf(stderr, "%s: unknown option %c\n", argv[0], c);
        return '?';
    }
    if (*++place == ':') 
    {
        if (*scanStr != '\0') 
        {
            utilOptarg = scanStr;
            scanStr = NULL;
        } 
        else 
        {
            if (utilOptind >= argc) 
            {
                (void) fprintf(stderr, "%s: %c requires an argument\n", 
                    argv[0], c);
                return '?';
            }
            utilOptarg = argv[utilOptind];
            utilOptind++;
        }
    }
    return c;
}

/**Function*************************************************************

  Synopsis    [Extra_UtilGetoptReset()]

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/

void utilGetoptReset()
{
    utilOptarg = 0;
    utilOptind = 0;
    scanStr = 0;
}


/**Function*************************************************************

  Synopsis    [Extra_CpuTimeDouble()]

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/

double cpuTimeDouble()
{
    struct rusage ru;
    getrusage(RUSAGE_SELF, &ru);
    return (double)ru.ru_utime.tv_sec + (double)ru.ru_utime.tv_usec / 1000000; 
}


/**Function*************************************************************

  Synopsis    []

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
char * utilGetVersion()
{
    static char Version[1000];
    sprintf(Version, "NTU, SC4AV 1.01 (compiled %s %s)", __DATE__, __TIME__);
    return Version;
}

