/**HFile****************************************************************

  FileName    [poly2BPC.h]

  SystemName  [SC4Arith.]

  PackageName [pbsolver]

  Synopsis    [.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/
#include"INTprover/approxPolyn.h"
#include "PbSolver.h"
#include "PbParser.h"
#ifndef POLY2PBC_H
#define POLY2PBC_H

void getPBConstraints(SATMgr *,  Minisatp::PbSolver *);
//int solvePBC(SATMgr * _approxMgr, int);
#endif /* POLY2PBC_H */

