/**CPPFile****************************************************************

  FileName    [poly2pbc.cpp]

  SystemName  [Symbolic Computation for Automated Verification (SC4AV).]

  PackageName [Solving Pseude-Boolean Constraints.]

  Synopsis    []

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/
#include"poly2PBC.h"
#include <string>

using namespace Minisatp;


template<class B>
static char* parseIdent(B& var, vec<char>& tmp) { 
   
    tmp.clear();
    tmp.push('x');
    std::string varstr= std::to_string(var);
    for(auto c: varstr){
        tmp.push(c);   
    }
    tmp.push(0);
    return (char*)tmp; }

template<class B, class S>
void parseExpr(B& poly, S& solver, vec<Lit>& out_ps, vec<Int>& out_Cs, 
                                                                vec<char>& tmp)
{
    bool empty = true;
    auto leadterms=poly.second->leadTerms;
    for(auto term: *leadterms){
        auto coeff=term->first;
        auto monl=term->second;
        if(monl->empty()) continue;
        assert(monl->size()==1);
        out_Cs.push(coeff);
        out_ps.push(mkLit(solver->getVar(parseIdent(*monl->begin(), tmp))));
        empty = false;
    }
    if (empty) throw xstrdup("Empty polynomial.");
}

template<class B, class S>
void parseConsExpr(B& poly, S& solver, vec<Lit>& out_ps, vec<Int>& out_Cs, 
                                                                vec<char>& tmp)
{
    bool empty = true;
    auto leadterms=poly.second->leadTerms;
    for(auto term: *leadterms){
        auto monl=term->second;
        if(monl->empty()) continue;
        assert(monl->size()==1);
        Int coeff=Int((mpz_t*)term->first.get_mpz_t());
        out_Cs.push(coeff);
        out_ps.push(mkLit(solver->getVar(parseIdent(*monl->begin(), tmp))));
        empty = false;
    }
    if (empty) throw xstrdup("Empty polynomial.");
}

template<class B, class S>
bool parseNonlinearExpr(B& poly, S& solver, vec<Lit>& out_ps, vec<Int>& out_Cs, 
                                                                vec<char>& tmp)
{
    int     ineq;
    Int     rhs(0);
    auto lvar=poly.first;
    auto leadTerms=poly.second->leadTerms;

    assert(leadTerms->size()==1);
    auto fTerm=*leadTerms->begin();
    auto fMonl=fTerm->second;
    if (fMonl->empty()) throw xstrdup("Empty Term.");
    /*Insert x-a_1-a_2+...-a_n>=1-n*/
    out_Cs.push(Int(1));
    out_ps.push(mkLit(solver->getVar(parseIdent(lvar, tmp))));
    for (auto var: *fMonl){
        out_Cs.push(Int(-1));
        out_ps.push(mkLit(solver->getVar(parseIdent(var, tmp))));
    }
    ineq = +1; //>=
    rhs=  Int(1-fMonl->size());
    if (!solver->addConstr(out_ps, out_Cs, rhs, ineq))
        return false;
    out_ps.clear();
    out_Cs.clear();
     /*Insert -x+a_1>=0*/
     /*Insert -x+a_2>=0*/ 
    for(auto var: *fMonl){
        out_Cs.push(Int(-1));
        out_ps.push(mkLit(solver->getVar(parseIdent(lvar, tmp))));
        out_Cs.push(Int(1));
        out_ps.push(mkLit(solver->getVar(parseIdent(var, tmp))));
        rhs=  Int(0);
            if (!solver->addConstr(out_ps, out_Cs, rhs, ineq))
        return false;
        out_ps.clear();
        out_Cs.clear();
    }

}

template<class B, class S>
void parseSize(B& _approxMgr, S& solver)
{
    int n_vars, n_constrs;
    n_vars = _approxMgr->_varSATTable->size()+
            _approxMgr->_inputApproxVars->size();
    n_constrs = _approxMgr->linearIdeal->size()+
            _approxMgr->nonlinearIdeal->size()+
            _approxMgr->_constraintsIdeal->size();
    solver->allocConstrs(n_vars, n_constrs);
}

template<class B, class S>
bool parseConstrs(B& _approxMgr, S& solver)
{
    vec<Lit> ps; vec<Int> Cs; vec<char> tmp;
    int     ineq;
    Int     rhs(0);
    for (auto poly: *_approxMgr->linearIdeal){
        parseExpr(poly, solver, ps, Cs, tmp);
        ineq = 0; //==
        auto firstTerm=*poly.second->leadTerms->begin();
        if(firstTerm->second->empty()){
            rhs= Int((firstTerm->first)*-1);
        }
        else{
            rhs  = Int(0);
        }
        if (!solver->addConstr(ps, Cs, rhs, ineq))
            return false;
        ps.clear();
        Cs.clear();
    }
    
    for (auto poly: *_approxMgr->linearEnquiryIdeal){
        parseExpr(poly, solver, ps, Cs, tmp);
        ineq = 0; //==
        auto firstTerm=*poly.second->leadTerms->begin();
        if(firstTerm->second->empty()){
            rhs= Int((firstTerm->first)*-1);
        }
        else{
            rhs  = Int(0);
        }
        if (!solver->addConstr(ps, Cs, rhs, ineq))
            return false;
        ps.clear();
        Cs.clear();
    }
    
    for (auto poly: *_approxMgr->_constraintsIdeal){
        parseConsExpr(poly, solver, ps, Cs, tmp);
        auto constData=poly.second;
        ineq = constData->inEqu; //==
        auto firstTerm=*constData->leadTerms->begin();
        if(firstTerm->second->empty()){
           int coeff= mpz_get_si(firstTerm->first.get_mpz_t())*-1;
           rhs=Int(coeff);
        }
        else{
            rhs  = Int(0);
        }
        if (!solver->addConstr(ps, Cs, rhs, ineq))
            return false;
        ps.clear();
        Cs.clear();
    }
    
    for (auto poly: *_approxMgr->nonlinearIdeal){
        parseNonlinearExpr(poly, solver, ps, Cs, tmp);
    }
    return true;
}


//=================================================================================================
// Main parser functions:


template<class B, class S>
static bool parse_PB(B& _approxMgr, S& solver)
{
    try{
        parseSize(_approxMgr, solver);
        return parseConstrs(_approxMgr, solver);
    }catch (cchar* msg){
        throw msg;
        exit(5);
    }

}


void getPBConstraints(SATMgr * _approxMgr,  PbSolver* pb_solver){
    
    
    parse_PB(_approxMgr, pb_solver); 
    
    return;
}