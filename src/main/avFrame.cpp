/**CPPFile****************************************************************

  FileName    [sc4aFrame.cpp]

  SystemName  [AVrith.]

  PackageName [Commands Frame.]

  Synopsis    [Commands Frame.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/
#include "main/sc4av.h"


AVFrame::AVFrame(SC4AV* pSC4AV):
pSC4AV(pSC4AV){
    
     _commands= new Commands(pSC4AV);
    
}

/**Function*************************************************************

  Synopsis    []

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
AVFrame::~AVFrame(){
    
    delete _commands;
}

/**Function*************************************************************

  Synopsis    []

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
void AVFrame::init(char * name ){
    
    sBinary = name;
    fBatch      = INTERACTIVE;
    fInitSource = 1;
    fInitRead   = 0;
    fFinalWrite = 0;
    sInFile = sOutFile = NULL;
    sprintf( sReadCmd,  "read"  );
    sprintf( sWriteCmd, "write" );
    sVersion = utilGetVersion();
    tCommands = tableStrInit();
    tFlags    = tableStrInit();
    aHistory  = vecPtrAlloc();
    _commands->commandsInit( );
    Err = stderr;
    Out = stdout;
    Hst = NULL;
    nSteps     =  1;
    fBatchMode =  0;
}


/**Function*************************************************************

  Synopsis    []

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
void AVFrame::printHello( )
{
    fprintf( stdout, "***      Welcome to SC4AV, compiled on %s %s!\n",
                                                           __DATE__, __TIME__ );
    fprintf( Out,"*** Copyright (c) 2017-2018, Nanyang Technological University "
         "(NTU)\n\n");
    fprintf( Out,"*** This Version of SC4AV is linked to UC Berkeley, ABC\n");
    fprintf( Out,"*** Copyright (c) 2005-2017, The Regents of the University of "
            "California.\n\n");
    fprintf( Out,"*** This Version of SC4AV is linked to MiniSAT+\n");
    fprintf( Out,"*** Copyright (c) 2005-2012, Niklas Een, Niklas Sorensson.\n\n\n\n");
     
}

/**Function*************************************************************

  Synopsis    []

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
void AVFrame::printUsage( char * ProgName )
{
    fprintf( Err, "\n" );
    fprintf( Err,
             "usage: %s [-c cmd] [-C cmd] [-f script] [-f script] [-h] [-o file] \n",
             ProgName);
    fprintf( Err, "    -c cmd\texecute commands `cmd'\n");
    fprintf( Err, "    -C cmd\texecute commands `cmd', then continue in interactive mode\n");
    fprintf( Err, "    -F script\texecute commands from a script file and echo commands\n");
    fprintf( Err, "    -f script\texecute commands from a script file\n");
    fprintf( Err, "    -h\t\tprint the command usage\n");
    fprintf( Err, "    -o file\tspecify output filename to store the result\n");
    fprintf( Err, "\n" );
}

/**Function*************************************************************

  Synopsis    []

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
bool AVFrame::open( int argc, char * argv[])
{

    while ((c = utilGetopt(argc, argv, "c:C:h:f:F:o")) != EOF) {
        switch(c) {
            case 'c':
                if( vecStrSize(sCommandUsr) > 0 )
                {
                    vecStrAppend(sCommandUsr, " ; ");
                }
                vecStrAppend(sCommandUsr, utilOptarg );
                fBatch = BATCH;
                break;

            case 'C':
                if( vecStrSize(sCommandUsr) > 0 )
                {
                    vecStrAppend(sCommandUsr, " ; ");
                }
                vecStrAppend(sCommandUsr, utilOptarg );
                fBatch = BATCH_THEN_INTERACTIVE;
                break;

            case 'f':
                if( vecStrSize(sCommandUsr) > 0 )
                {
                    vecStrAppend(sCommandUsr, " ; ");
                }
                sprintf(sCommandTmp, "source %s\n", utilOptarg );
                vecStrAppend(sCommandUsr, sCommandTmp);
                fBatch = BATCH;
                break;

            case 'F':
                if( vecStrSize(sCommandUsr) > 0 )
                {
                    vecStrAppend(sCommandUsr, " ; ");
                }
                sprintf(sCommandTmp, "source -x %s\n", utilOptarg);
                vecStrAppend(sCommandUsr, sCommandTmp);
                fBatch = BATCH;
                break;

            case 'h':
                goto usage;
                break;

            case 'o':
                sOutFile = utilOptarg;
                fFinalWrite = 1;
                break;

            default:
                goto usage;
        }
    }

    vecStrAppend(sCommandUsr, "\0");

    if ( fBatch!=INTERACTIVE 
            && fBatch!=BATCH_QUIET 
            && fBatch!=BATCH_QUIET_THEN_INTERACTIVE 
            && vecStrSize(sCommandUsr)>0 ){
         printHello( );
        //printf( " If Not, Write the Command Within \" \" \n\n" );
         sprintf( sReadCmd, sCommandUsr->c_str());
    }

    if ( fBatch!=INTERACTIVE )
    {
        fBatchMode = 1;

        if (argc - utilOptind == 0)
        {
            sInFile = NULL;
        }
        else if (argc - utilOptind == 1)
        {
            fInitRead = 1;
            sInFile = argv[utilOptind];
        }
        else
        {
            printUsage(argv[0] );
        }


        fStatus = 0;
        if ( fInitRead && sInFile )
        {
            sprintf( sCommandTmp, "%s %s", sReadCmd, sInFile );
            fStatus = commandExecute( sCommandTmp );
        }

        if ( fStatus == 0 )
        {
            /* cmd line contains `source <file>' */
            fStatus = commandExecute ( sCommandUsr->c_str() );
            if ( (fStatus == 0 || fStatus == -1) && fFinalWrite && sOutFile )
            {
                sprintf( sCommandTmp, "%s %s", sWriteCmd, sOutFile );
                fStatus = commandExecute ( sCommandTmp );
            }
        }

        if (fBatch == BATCH_THEN_INTERACTIVE 
            || fBatch == BATCH_QUIET_THEN_INTERACTIVE)
        {
            fBatch = INTERACTIVE;
            fBatchMode = 0;
        }
    }

    vecStrFreeP(sCommandUsr);

    if ( fBatch== INTERACTIVE )
    {
        // start interactive mode

        // print the hello line
        printHello( );

        // execute commands given by the user
        while ( !feof(stdin) )
        {
            // print command line prompt and
            // get the command from the user
            sCommand = utilsGetUsersInput( );

            // execute the user's command
            fStatus = commandExecute( sCommand );

            // stop if the user quitted or an error occurred
            if ( fStatus == -1 || fStatus == -2 )
                break;
        }
    }


    return 0;

usage:
    printHello( );
    printUsage( argv[0] );
    return 1;
}

/**Function*************************************************************

  Synopsis    []

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
char * AVFrame::utilsGetUsersInput( )
{
    static char Prompt[5000];
    sprintf( Prompt, "sc4av %02d> ", nSteps );
    
#ifdef SC4AV_USE_READLINE
    {
    static char * line = NULL;
    if (line != NULL) arrStrFree(line);
    line = readline(Prompt);  
    if (line == NULL){ printf("***EOF***\n"); exit(0); }
    add_history(line);
    return line;
    }
#else
    {
    char * pRetValue;
    fprintf( Out, "%s", Prompt );
    pRetValue = fgets( Prompt, 5000, stdin );
    return Prompt;
    }
#endif
}

/**Function*************************************************************

  Synopsis    []

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
int AVFrame::commandExecute( const char * sCommand )
{
    int fStatus = 0, argc;
    const char * sCommandNext;
    char **argv;


    //if ( !fAutoexac && !fSource ) 
	//    Cmd_HistoryAddCommand(pAbc, sCommand);
    sCommandNext = sCommand;
    do {
       	sCommandNext = splitCommandLine(sCommandNext, argc, argv );
		//fStatus = CmdApplyAlias( pAbc, &argc, &argv, &loop );
        if ( argc != 0 ){
               printf( "***c Executing Command line: %s\n", sCommand ); 
        } 
        fStatus = dispatchCommand( argc, argv );
       	//CmdFreeArgv( argc, argv );
    } while ( fStatus == 0 && *sCommandNext != '\0' );
    return fStatus;
}

/**Function*************************************************************

  Synopsis    [Splits the command line string into individual commands.]

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
const char * AVFrame::splitCommandLine( const char *sCommand, 
                                                       int &argc, char ** &argv )
{
    const char *p, *start;
    char c;
    int i, j;
    char * new_arg;
    vecPtr * vArgs;
    int single_quote, double_quote;

    vArgs = vecPtrAlloc( );

    p = sCommand;

    for ( ;; )
    {
        // skip leading white space 
        while ( isspace( ( int ) *p ) )
        {
            p++;
        }

        // skip until end of this token 
        single_quote = double_quote = 0;
        for ( start = p; ( c = *p ) != '\0'; p++ )
        {
            if ( c == ';' || c == '#' || isspace( ( int ) c ) )
            {
                if ( !single_quote && !double_quote )
                {
                    break;
                }
            }
            if ( c == '\'' )

            {
                single_quote = !single_quote;
            }
            if ( c == '"' )
            {
                double_quote = !double_quote;
            }
        }
        if ( single_quote || double_quote )
        {
            ( void ) fprintf( Err, "** cmd warning: ignoring unbalanced quote ...\n" );
        }
        if ( start == p )
            break;

        //new_arg =  (char*) malloc ( sizeof(char) * (p - start + 1));
        new_arg = ALLOC_CHAR_ARRAY(p - start + 1);
        //new_arg = new char(p - start + 1);
        j = 0;
        for ( i = 0; i < p - start; i++ )
        {
            c = start[i];
            if ( ( c != '\'' ) && ( c != '\"' ) )
            {
                new_arg[j++] = isspace( ( int ) c ) ? ' ' : start[i];
            }
        }
        new_arg[j] = '\0';
        vecPtrPush( vArgs, new_arg );
    }

    argc = vArgs->size();
    argv = (char **)vecPtr2Array( vArgs );
    //vecPtrFree( vArgs );
    const char * ccc=  (const char *) argv[0];
    if ( *p == ';' )
    {
        p++;
    }
    else if ( *p == '#' )
    {
        for ( ; *p != 0; p++ ); // skip to end of line 
    }
    return p;
}



/**Function*************************************************************

  Synopsis    [Executes one command.]

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
int AVFrame::dispatchCommand(int &argc, char ** &argv )
{
    //int argc = *pargc;
    //char ** argv = *pargv;
    char ** argv2;
    int (*pFunc) ( SC4AV *, int, char ** );
    Commands::Command * pCommand;
    char * value;
    int fError;
    double clk;

    if ( argc == 0 )
        return 0;

    if ( cmdCheckShellEscape( argc, argv ) == 1 )
	return 0;
    // get the command
    string key= argv[0];
    if ( ! lookupStrTable( tCommands, key, (char **)&pCommand ) )
    {   // the command is not in the table
        // if there is only one word with an extension, assume this is file to be read
        if ( argc == 1 && findChar( argv[0], '.' ) )
        { 
            // add command 'read' assuming that this is the file name
            argv2 = addToCharsArray( argc, argv, "read" );
            freeCharsArray( argc, argv );
            argc = argc+1;
            argv = argv2;
           // *pargc = argc;
            //*pargv = argv;
            if ( ! lookupStrTable(tCommands, key, (char **)&pCommand ) )
                assert( 0 );
        }
        else
        {
            fprintf(Err, "** cmd error: unknown command '%s'\n", argv[0] );
            return 1;
        }
    }

    // get the backup network if the command is going to change the network
    if ( pCommand->fChange ) 
    {
        /*if ( pNtkCur && Abc_FrameIsFlagEnabled( "backup" ) )
        {
            pNetCopy = Abc_NtkDup( pNtkCur );
            setCurrentNetwork( pAbc, pNetCopy );
            // swap the current network and the backup network 
            // to prevent the effect of resetting the short names
            swapCurrentAndBackup( pAbc );
        }*/
    }
    // execute the command
    clk = cpuTimeDouble();
    pFunc = (int (*)(SC4AV *, int, char **))pCommand->pFunc;
    fError = (*pFunc)( this->pSC4AV, argc, argv );
    TimeCommand += cpuTimeDouble() - clk;

    // automatic execution of arbitrary command after each command 
    // usually this is a passive command ... 
    /*if ( fError == 0 && !fAutoexac )
    {
        if ( lookupStrTable( tFlags, "autoexec", &value ) )
        {
            fAutoexac = 1;
            fError = commandExecute( value );
            fAutoexac = 0;
        }
    }*/
    return fError;
}

/**Function*************************************************************

  Synopsis    []

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
int AVFrame::cmdCheckShellEscape(int& argc, char **& argv)
{
	if (argv[0][0]== '!'  ) 
	{
		const int size = 4096;
		int i;
		char * buffer= ALLOC_CHAR_ARRAY(10000);
		strncpy (buffer, &argv[0][1], size);
		for (i = 1; i < argc; ++i)
		{
				strncat (buffer, " ", size);
				strncat (buffer, argv[i], size);
		}
		if (buffer[0] == 0) 
			strncpy (buffer, "/bin/sh", size);
		int RetValue = system (buffer);
                FREE_CHAR_ARRAY(buffer);
		// NOTE: Since we reconstruct the cmdline by concatenating
		// the parts, we lose information. So a command like
		// `!ls "file name"` will be sent to the system as
		// `ls file name` which is a BUG

    	return 1;
	}
	else
	{
		return 0;
	}
}

