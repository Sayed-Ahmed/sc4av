/**HFile****************************************************************

  FileName    [avCommands.h]

  SystemName  [SC4AV.]

  PackageName [commands.]

  Synopsis    [define the commands of the tool.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/
//#ifndef AVCOMMANDS_H
//#define AVCOMMANDS_H

    class SC4AV;
    class Commands{
        public:
           Commands (SC4AV* );
           ~Commands();
            typedef int (* CommandFuncType) (SC4AV*, int, char**) ;
            virtual void commandsInit();
            void addCommand(const char * sGroup, const char * sName,  
                                bool fChanges, CommandFuncType pFunc);
            struct Command
            {
                string        sName;       // the command name  
                string        sGroup;      // the group name  
                CommandFuncType pFunc;       // the function to execute the command
                bool           fChange;     // set to 1 to mark that the network is changed
            };
            
            typedef map<const char*, Command*> tableCommand;
            
            // static functions
            static int commandTime(SC4AV*, int, char **);
            static int commandEcho(SC4AV*, int, char **);
            static int commandQuit(SC4AV*, int, char **);
            static int commandHistory(SC4AV*, int, char **);
            static int commandSource(SC4AV*, int, char **);
            static int commandPolyn( SC4AV*, int, char ** );
            static int commandSC4Multiplier( SC4AV*, int, char ** );
            static int commandSC4Crypto( SC4AV*, int, char ** );
            static int commandReadVerilog( SC4AV*, int, char ** );
            static int commandReadAIGER( SC4AV*, int, char ** );
            static FILE * CmdFileOpen( char *, char *, char **, int );
            static int printMultModel(  SC4AV*, const char* );
        protected:
        SC4AV* pSC4AV;
        static void updateAIGNtk( SC4AV* pSC4AV, char *pFileName  );
        static int SC4MultSolver( SC4AV*, int,  vecStr*, vecStr* ); 
        static int SC4CryptoAnalysis( SC4AV*, int  );
    };


//#endif /* AVCOMMANDS_H */

