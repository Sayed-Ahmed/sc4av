/**CPPFile****************************************************************

  FileName    [sc4av.cpp]

  SystemName  [Symbolic Computation for Automated Verification (SC4AV).]

  PackageName [The parent Class]

  Synopsis    []

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/

#include "main/sc4av.h"




SC4AV::SC4AV (){
    pFrame = new AVFrame(this);
}

SC4AV::~SC4AV (){
    
}



/**Function*************************************************************

  Synopsis    []

  Description []

  SideEffects []

  SeeAlso     []

***********************************************************************/
void SC4AV::updateGia( GiaNtk * pNew )
{
    if ( pNew == NULL )
    {
        Abc_Print( -1, "Abc_FrameUpdateGia(): Tranformation has failed.\n" );
        return;
    }
    if ( Gia_ManPoNum(pNew) == 0 )
        Abc_Print( 0, "The current GIA has no primary outputs. Some commands may not work correctly.\n" );
    if ( pNew == curGiaNtk )
        return;
    // transfer names
    if (!pNew->vNamesIn && curGiaNtk && curGiaNtk->vNamesIn && Gia_ManCiNum(pNew) == Vec_PtrSize(curGiaNtk->vNamesIn))
    {
        pNew->vNamesIn = curGiaNtk->vNamesIn;
        curGiaNtk->vNamesIn = NULL;
    }
    if (!pNew->vNamesOut && curGiaNtk && curGiaNtk->vNamesOut && Gia_ManCoNum(pNew) == Vec_PtrSize(curGiaNtk->vNamesOut))
    {
        pNew->vNamesOut = curGiaNtk->vNamesOut;
        curGiaNtk->vNamesOut = NULL;
    }
    // update
    curGiaNtk  = pNew;
}

/**Function*************************************************************

  Synopsis    []

  Description []

  SideEffects []

  SeeAlso     []

***********************************************************************/

void SC4AV::closeManagers( ){
    
    delete curGiaNtk;
    delete _polynMgr;
    delete _preprocMgr;
}

/**Function*************************************************************

  Synopsis    []

  Description []

  SideEffects []

  SeeAlso     []

***********************************************************************/

void SC4AV::closeFrame( ){
    
    delete pFrame;
}