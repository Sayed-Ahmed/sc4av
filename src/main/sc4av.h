/**HFile****************************************************************

  FileName    [sc4arith.h]

  SystemName  [SC4Arith.]

  PackageName [frame.]

  Synopsis    [main header file.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/
#include <base/abc/abc.h>
#include <base/io/ioAbc.h>
#include <proof/acec/acecInt.h>
#include "frontEnds/polyn/aig2polyn.h"
#include "frontEnds/polyn/pb2polyn.h"
#include "INTprover/IMT.h"
#include "INTprover/approxPolyn.h"
#include "GFprover/GFIMT.h"
#include "main/avFrame.h"
//#include "PBSAT/poly2PBC.h"


#ifndef SC4AV_H
#define SC4AV_H
  
    
#define MAX_STR     (1<<15)

    class SC4AV{
    public:
        SC4AV ();
        ~SC4AV ();
        GiaNtk * curGiaNtk;
        AVFrame* pFrame;
        PolynMgr* _polynMgr;
        PreprocessMgr* _preprocMgr;
        GFPolynMgr* _GFpolynMgr;
        GFPreprocessMgr* _GFpreprocMgr;
        SATMgr* _approxMgr;
        void updateGia(GiaNtk *);
        void closeManagers( );
        void closeFrame( );
    };

#endif /* SC4AV_H */

