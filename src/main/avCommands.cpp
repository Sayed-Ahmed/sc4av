/**CPPFile****************************************************************

  FileName    [sc4aCommands.cpp]

  SystemName  [SC4Arith.]

  PackageName [Tool Commands.]

  Synopsis    [Tool Commands.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/
#include "main/sc4av.h"


Commands::Commands(SC4AV* pSC4AV):
pSC4AV (pSC4AV)
{
    
    
}

Commands::~Commands(){
    
    
}





    
 /**Function*************************************************************

  Synopsis    [Add New Command]

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
void Commands::addCommand( const char * sGroup, const char * sName, 
        bool fChanges, CommandFuncType pFunc)
{
    AVFrame* frame= pSC4AV->pFrame;
    string key;
    Command * pCommand;
    key = sName;
    // create the new command
    pCommand = new Command();
    pCommand->sName   =  sName;
    pCommand->sGroup  =  sGroup;
    pCommand->pFunc   = pFunc;
    pCommand->fChange = fChanges;
    
    if(frame->tCommands->find(key)==frame->tCommands->end()){
        tableStr::iterator it = frame->tCommands->begin();
        frame->tCommands->insert(it, std::make_pair (key,(char *)pCommand));
    }
    //assert( !fStatus.second );  // the command should not be in the table
}

/**Function********************************************************************

  Synopsis    []

  Description []

  SideEffects []

  SeeAlso     []

******************************************************************************/
int Commands::commandTime(SC4AV* pSC4AV , int argc, char **argv )
{
    AVFrame* frame= pSC4AV->pFrame;
    int c;
    int fClear;

    fClear = 0;
    utilGetoptReset();
    while ( ( c = utilGetopt( argc, argv, "ch" ) ) != EOF )
    {
        switch ( c )
        {
        case 'c':
            fClear ^= 1;
            break;
        case 'h':
            goto usage;
        default:
            goto usage;
        }
    }

    if ( fClear )
    {
        frame->TimeTotal += frame->TimeCommand;
        frame->TimeCommand = 0.0;
        return 0;
    }

    if ( argc != utilOptind )
    {
        goto usage;
    }


    frame->TimeTotal += frame->TimeCommand;
    fprintf( frame->Out, "elapse: %3.2f seconds, total: %3.2f seconds\n",
        frame->TimeCommand, frame->TimeTotal );

    frame->TimeCommand = 0.0;
    return 0;

  usage:
    fprintf( frame->Err, "usage: time [-ch]\n" );
    fprintf( frame->Err, "   \t\tprint the runtime since the last call\n" );
    fprintf( frame->Err, "   -c \t\tclears the elapsed time without printing it\n" );
    fprintf( frame->Err, "   -h \t\tprint the command usage\n" );
    return 1;
}

/**Function********************************************************************

  Synopsis    []

  Description []

  SideEffects []

  SeeAlso     []

******************************************************************************/
int Commands::commandEcho(SC4AV* pSC4AV, int argc, char **argv )
{
    AVFrame* frame= pSC4AV->pFrame;
    int i;
    int c;
    int n = 1;

    utilGetoptReset();
    while ( ( c = utilGetopt( argc, argv, "hn" ) ) != EOF )
    {
        switch ( c )
        {
        case 'n':
            n = 0;
            break;
        case 'h':
            goto usage;
            break;
        default:
            goto usage;
        }
    }


    for ( i = utilOptind; i < argc; i++ )
        fprintf( frame->Out, "%s ", argv[i] );
    if ( n )
        fprintf( frame->Out, "\n" );
 
    fflush ( frame->Out );
    
    return 0;

  usage:
    fprintf( frame->Err, "usage: echo [-h] string \n" );
    fprintf( frame->Err, "   -n \t\tsuppress newline at the end\n" );
    fprintf( frame->Err, "   -h \t\tprint the command usage\n" );
    return ( 1 );
}

/**Function********************************************************************

  Synopsis    []

  Description []

  SideEffects []

  SeeAlso     []

******************************************************************************/
int Commands::commandQuit(SC4AV* pSC4AV, int argc, char **argv )
{
    AVFrame* frame= pSC4AV->pFrame;
    int c;

    utilGetoptReset();
    while ( ( c = utilGetopt( argc, argv, "hs" ) ) != EOF )
    {
        switch ( c )
        {
        case 'h':
            goto usage;
            break;
        case 's':
            return -2;
            break;
        default:
            goto usage;
        }
    }

    if ( argc != utilOptind )
        goto usage;
    return -1;

  usage:
    fprintf( frame->Err, "usage: quit [-sh]\n" );
    fprintf( frame->Err, "   -h  print the command usage\n" );
    fprintf( frame->Err, "   -s  frees all the memory before quitting\n" );
    return 1;
}


/**Function********************************************************************

  Synopsis    []

  Description []

  SideEffects []

  SeeAlso     []

******************************************************************************/
int Commands::commandHistory(SC4AV* pSC4AV, int argc, char **argv )
{
    AVFrame* frame= pSC4AV->pFrame;
    char * pName;
    int c;
    int nPrints = 20;
    utilGetoptReset();
    while ( ( c = utilGetopt( argc, argv, "h" ) ) != EOF )
    {
        switch ( c )
        {
            case 'h':
                goto usage;
            default :
                goto usage;
        }
    }
    if ( argc > utilOptind + 1 )
        goto usage;
    // get the number from the command line
    if ( argc == utilOptind + 1 )
        nPrints = atoi(argv[utilOptind]);
    // print the commands
    vecPtrForEachEntryStart( char *, frame->aHistory, pName, std::max(0, (int) (frame->aHistory->size()-nPrints)) )
    {   
        fprintf( frame->Out, "%2d : %s\n", (int)(frame->aHistory->size()-i), pName );
    }
    return 0;

usage:
    fprintf( frame->Err, "usage: history [-h] <num>\n" );
    fprintf( frame->Err, "\t        lists the last commands entered on the command line\n" );
    fprintf( frame->Err, "\t-h    : print the command usage\n" );
    fprintf( frame->Err, "\t<num> : the maximum number of entries to show [default = %d]\n", nPrints );
    return ( 1 );
}



/**Function********************************************************************

  Synopsis    []

  Description []

  SideEffects []

  SeeAlso     []

******************************************************************************/
int Commands::commandSource(SC4AV* pSC4AV, int argc, char **argv )
{
    AVFrame* frame= pSC4AV->pFrame;
    int c, echo, prompt, silent, interactive, quit_count, lp_count;
    int status = 0;             /* initialize so that lint doesn't complain */
    int lp_file_index, did_subst;
    char *prompt_string, *real_filename, line[1000], *command;
    FILE *fp;

    interactive = silent = prompt = echo = 0;

    Extra_UtilGetoptReset();
    while ( ( c = utilGetopt( argc, argv, "ipsxh" ) ) != EOF )
    {
        switch ( c )
        {
        case 'i':               /* a hack to distinguish EOF from stdin */
            interactive = 1;
            break;
        case 'p':
            prompt ^= 1;
            break;
        case 's':
            silent ^= 1;
            break;
        case 'x':
            echo ^= 1;
            break;
        case 'h':
            goto usage;
        default:
            goto usage;
        }
    }

    /* added to avoid core-dumping when no script file is specified */
    if ( argc == globalUtilOptind )
    {
        goto usage;
    }

    lp_file_index = globalUtilOptind;
    lp_count = 0;

    /*
     * FIX (Tom, 5/7/95):  I'm not sure what the purpose of this outer do loop
     * is. In particular, lp_file_index is never modified in the loop, so it
     * looks it would just read the same file over again.  Also, SIS had
     * lp_count initialized to -1, and hence, any file sourced by SIS (if -l or
     * -t options on "source" were used in SIS) would actually be executed twice.
     */
    frame->fSource = 1;
    //do
    //{
        char * pFileName, * pTemp;

        // get the input file name
        //pFileName = argv[lp_file_index];
        pFileName = argv[1];
        // fix the wrong symbol
        for ( pTemp = pFileName; *pTemp; pTemp++ )
            if ( *pTemp == '>' )
                *pTemp = '\\';

        lp_count++;             /* increment the loop counter */

        fp = CmdFileOpen( pFileName, "r", &real_filename, silent );
        if ( fp == NULL )
        {
            frame->fSource = 0;
            ABC_FREE( real_filename );
            return !silent;     /* error return if not silent */
        }

        
        quit_count = 0;
        do
        {
            /*if ( prompt )
            {
                prompt_string = Cmd_FlagReadByName( pAbc, "prompt" );
                if ( prompt_string == NULL )
                    prompt_string = "abc> ";

            }
            else
            {*/
                prompt_string = NULL;
            //}

            /* clear errors -- e.g., EOF reached from stdin */
            //clearerr( fp );
                
            /* read another command line */
            if ( fgets( line, 1000, fp ) == NULL )
            { 
                if ( interactive )
                {
                    if ( quit_count++ < 5 )
                    {
                        fprintf( frame->Err, "\nUse \"quit\" to leave SC4AV.\n" );
                        continue;
                    }
                    status = -1;    /* fake a 'quit' */
                }
                else
                {
                    status = 0; /* successful end of 'source' ; loop? */
                }
                break;
            }
            quit_count = 0;

            if ( echo )
            {
                fprintf( frame->Out, "sc4av - > %s", line );
            }
           /* command = CmdHistorySubstitution( pAbc, line, &did_subst );
            if ( command == NULL )
            {
                status = 1;
                break;
            }*/
            if ( did_subst )
            {
                if ( interactive )
                {
                    fprintf( frame->Out, "%s\n", command );
                }
            }
            /*if ( command != line )
            {
                strcpy( line, command );
            }*/
            /*if ( interactive && *line != '\0' )
            {
                Cmd_HistoryAddCommand( pAbc, line );
                if ( pAbc->Hst != NULL )
                {
                    fprintf( pAbc->Hst, "%s\n", line );
                    fflush( pAbc->Hst );
                }
            }*/

            fflush( frame->Out );
            status = frame->commandExecute(line);
        }
        while ( status == 0 );

        if ( fp != stdin )
        {
            if ( status > 0 )
            {
                fprintf( frame->Err,
                                  "** cmd error: aborting 'source %s'\n",
                                  real_filename );
            }
            fclose( fp );
        }
        ABC_FREE( real_filename );

    //}
    //while ( ( status == 0 ) && ( lp_count <= 0 ) );
    frame->fSource = 0;
    return status;

  usage:
    fprintf( frame->Err, "usage: source [-psxh] <file_name>\n" );
    fprintf( frame->Err, "\t-p     supply prompt before reading each line [default = %s]\n", prompt? "yes": "no" );
    fprintf( frame->Err, "\t-s     silently ignore nonexistant file [default = %s]\n", silent? "yes": "no" );
    fprintf( frame->Err, "\t-x     echo each line as it is executed [default = %s]\n", echo? "yes": "no" );
    fprintf( frame->Err, "\t-h     print the command usage\n" );
    return 1;
}


/**Function*************************************************************

  Synopsis    [Opens the file with path (now, disabled).]

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
FILE * Commands::CmdFileOpen( char *sFileName, char *sMode, char **pFileNameReal, int silent )
{
    char * sRealName, * sPathUsr, * sPathLib, * sPathAll;
    FILE * pFile;
    
    if (strcmp(sFileName, "-") == 0) {
        if (strcmp(sMode, "w") == 0) {
            sRealName = Extra_UtilStrsav( "stdout" );
            pFile = stdout;
        }
        else {
            sRealName = Extra_UtilStrsav( "stdin" );
            pFile = stdin;
        }
    }
    else {
        sRealName = NULL;
        if (strcmp(sMode, "r") == 0) {
            
            /* combine both pathes if exist */
         /*   sPathUsr = Cmd_FlagReadByName(pAbc,"open_path");
            sPathLib = Cmd_FlagReadByName(pAbc,"lib_path");
            
            if ( sPathUsr == NULL && sPathLib == NULL ) {
                sPathAll = NULL;
            }
            else if ( sPathUsr == NULL ) {
                sPathAll = Extra_UtilStrsav( sPathLib );
            }
            else if ( sPathLib == NULL ) {
                sPathAll = Extra_UtilStrsav( sPathUsr );
            }
            else {
                sPathAll = ABC_ALLOC( char, strlen(sPathLib)+strlen(sPathUsr)+5 );
                sprintf( sPathAll, "%s:%s",sPathUsr, sPathLib );
            }*/
            if ( sPathAll != NULL ) {
                sRealName = Extra_UtilFileSearch(sFileName, sPathAll, "r");
                ABC_FREE( sPathAll );
            }
        }
        if (sRealName == NULL) {
            sRealName = Extra_UtilTildeExpand(sFileName);
        }

        if ((pFile = fopen(sRealName, sMode)) == NULL) {
            if (! silent) {
//                perror(sRealName);
                Abc_Print( 1, "Cannot open file \"%s\".\n", sRealName );
            }
        }
        //else
        //{
            // print the path/name of the resource file 'abc.rc' that is being loaded
            //if ( !silent && strlen(sRealName) >= 6  )            
                //Abc_Print( 1, "***Loading resource file \"%s\".\n", sRealName );
        //}
    }
    if ( pFileNameReal )
        *pFileNameReal = sRealName;
    else
        ABC_FREE(sRealName);
    
    return pFile;
}

/**Function*************************************************************

  Synopsis    []

  Description []

  SideEffects []

  SeeAlso     []

***********************************************************************/
void Commands::updateAIGNtk( SC4AV* pSC4AV, char *pFileName  )
{
    Abc_Ntk_t * pNtk;
    Gia_Man_t * pAig;
    FILE * pFile;
    char ** pArgvNew;
    char * pTemp;
    int fVerbose = 0;
    // get the input file name
    pFileName = pArgvNew[0];
    // fix the wrong symbol
    for ( pTemp = pFileName; *pTemp; pTemp++ )
        if ( *pTemp == '>' )
            *pTemp = '\\';
    if ( (pFile = fopen( pFileName, "r" )) == NULL )
    {
        Abc_Print( -1, "Cannot open input file \"%s\". ", pFileName );
        if ( (pFileName = Extra_FileGetSimilarName( pFileName, ".v", ".blif", NULL, NULL, NULL )) )
            Abc_Print( 1, "Did you mean \"%s\"?", pFileName );
        Abc_Print( 1, "\n" );
        return;
    }
    fclose( pFile );
    // read hierarchical Verilog
    pNtk = Io_ReadNetlist( pFileName, Io_ReadFileType(pFileName), 0 );
    if ( pNtk == NULL )
    {
        Abc_Print( -1, "Reading hierarchical Verilog has failed.\n" );
        return;
    }
    pAig = Abc_NtkFlattenHierarchyGia( pNtk, NULL, fVerbose );
    Abc_NtkDelete( pNtk );
    pSC4AV->updateGia( (GiaNtk*) pAig );
    return;
    }

/**Function*************************************************************

  Synopsis    []

  Description []

  SideEffects []

  SeeAlso     []

***********************************************************************/
int Commands::commandPolyn( SC4AV* pSC4AV, int argc, char ** argv )
{
    AVFrame* frame= pSC4AV->pFrame;
    int c, fVerbose = 0;
    vecStr* logFileName=vecStrAlloc(100);
    utilGetoptReset();
    while ( ( c = utilGetopt( argc, argv, "Nvwh" ) ) != EOF )
    {
        switch ( c )
        {
        case 'v':
            fVerbose ^= 1;
            break;
        case 'w':
            fVerbose ^= 2;
            break;    
        case 'h':
            goto usage;
        default:
            goto usage;
        }
    }
    if ( pSC4AV->curGiaNtk == NULL )
    {
        fprintf( frame->Err, "There is no AIG.\n" );
        return 0;
    }


    buildPolynFromAig( pSC4AV->curGiaNtk, fVerbose, logFileName );
    return 0;

usage:
    fprintf( frame->Err, "usage: &polyn [-N num] [-oasvwh]\n" );
    fprintf( frame->Err, "\t         derives algebraic polynomial from AIG\n" );
    fprintf( frame->Err, "\t-v     : toggles printing verbose information [default = %s]\n",  fVerbose? "yes": "no" );
    fprintf( frame->Err, "\t-w     : toggles printing very verbose information [default = %s]\n",  fVerbose? "yes": "no" );
    fprintf( frame->Err, "\t-h     : print the command usage\n");
    return 1;
}


/**Function*************************************************************

  Synopsis    []

  Description []

  SideEffects []

  SeeAlso     []

***********************************************************************/

int Commands::printMultModel(  SC4AV* pSC4AV, const char* pbFileName ){
    
   int Status=0;
   FILE *pbFile=NULL;
   pbFile=fopen(pbFileName, "w");
   if(pbFile==NULL){
       printf ("***e Error! Cannot Open Log file %s\n\n", pbFileName);
       throw;
   }
   pSC4AV->_polynMgr->printIdeal(pbFile ,0);
   Status=fclose(pbFile);
   return Status;   
}

/**Function*************************************************************

  Synopsis    []

  Description []

  SideEffects []

  SeeAlso     []

***********************************************************************/
int Commands::SC4MultSolver(  SC4AV* pSC4AV, int fVerbose , vecStr* spFileName, 
                                                            vecStr* logFileName){
    
    abctime clk = Abc_Clock();
    
    pSC4AV->_polynMgr=buildPolynFromAig( pSC4AV->curGiaNtk, fVerbose, logFileName );
    
    parseSpecPBfile(spFileName, pSC4AV->_polynMgr); 
 
    pSC4AV->_preprocMgr=preprocessGroebnerIdeal(pSC4AV->_polynMgr,  fVerbose);
    
    int Status= IMTSolver(pSC4AV->_polynMgr, fVerbose);

    if(!Status){
        Status= SATPSolver(pSC4AV->_polynMgr, fVerbose);
        printSC4AVTime( pSC4AV->_polynMgr->logFile, "***c Total Time", Abc_Clock() - clk );
        if(pSC4AV->_polynMgr->logFile!=stdout)
            printSC4AVTime( stdout, "***c Total Time", Abc_Clock() - clk );
        //exit(0);
    }
    
    printSC4AVTime( pSC4AV->_polynMgr->logFile, "***c Total Time", Abc_Clock() - clk );
    if(pSC4AV->_polynMgr->logFile!=stdout){
        printSC4AVTime( stdout, "***c Total Time", Abc_Clock() - clk );
        fprintf(stdout, "\n\n");
    }
    
 return Status;   
}


/**Function*************************************************************

  Synopsis    []

  Description []

  SideEffects []

  SeeAlso     []

***********************************************************************/
int Commands::SC4CryptoAnalysis(  SC4AV* pSC4AV, int fVerbose ){
    
    abctime clk = Abc_Clock();
    
    pSC4AV->_GFpolynMgr=buildGFPolynFromAig( pSC4AV->curGiaNtk, fVerbose );
 
    pSC4AV->_GFpreprocMgr=preprocessGroebnerGFIdeal(pSC4AV->_GFpolynMgr,  fVerbose);
    
    int Status= IMTGFSolver(pSC4AV->_GFpolynMgr, fVerbose);
    //int Status=1;
    pSC4AV->closeManagers();
    
    Abc_PrintTime( 1, "Total Time", Abc_Clock() - clk );
 return Status;   
}

/**Function*************************************************************

  Synopsis    []

  Description []

  SideEffects []

  SeeAlso     []

***********************************************************************/
int Commands::commandSC4Crypto( SC4AV* pSC4AV, int argc, char ** argv )
{
    ABC_NAMESPACE_USING_NAMESPACE
    AVFrame* frame= pSC4AV->pFrame;
    char ** pArgvNew;
    int c, nArgcNew;
    int fVerbose=0;
    
    utilGetoptReset();
    while ( ( c = utilGetopt( argc, argv, "ovh" ) ) != EOF )
    {
        switch ( c )
        {
        case 'v':
            fVerbose ^= 1;
            break;
        case 'h':
            goto usage;
        default:
            goto usage;
        }
    }
   
    pArgvNew = argv + utilOptind;
    nArgcNew = argc - utilOptind;
    if ( nArgcNew == 0 || nArgcNew == 1 )
    {
        if ( nArgcNew == 1 )
        {
            updateAIGNtk(pSC4AV, pArgvNew[0] );
        }
        frame->fStatus = SC4CryptoAnalysis( pSC4AV, fVerbose );
    }
    else if ( nArgcNew == 2 )
    {
        updateAIGNtk(pSC4AV, pArgvNew[0] );
        frame->fStatus = SC4CryptoAnalysis( pSC4AV, fVerbose );
    }
    else
    {
        fprintf( frame->Err, "Too many command-line arguments.\n" );
        return 1;
    }
    return 0;

usage:
    fprintf( frame->Err, "usage: &sc4crypto [-tvh] <file1> <file2>\n" );
    fprintf( frame->Err, "\t         analyzing cipher circuit by symbolic computation\n" );
    fprintf( frame->Err, "\t-v     : toggle verbose output [default = %s]\n", fVerbose? "yes":"no");
    fprintf( frame->Err, "\t-h     : print the command usage\n");
    fprintf( frame->Err, "\tfile1  : (optional) the file with the cipher network\n");
    fprintf( frame->Err, "\tfile2  : (optional) the file with the specifcation polynomial\n");
    return 1;
}
/**Function*************************************************************

  Synopsis    []

  Description []

  SideEffects []

  SeeAlso     []

***********************************************************************/
int Commands::commandSC4Multiplier( SC4AV* pSC4AV, int argc, char ** argv )
{
    ABC_NAMESPACE_USING_NAMESPACE
    AVFrame* frame= pSC4AV->pFrame;
    char ** pArgvNew;
    int c, nArgcNew;
    int fVerbose=0;

    vecStr* folderName=vecStrAlloc(500);
    vecStr* pbFileName=vecStrAlloc(500);
    vecStr* logFileName=vecStrAlloc(500);
    vecStr* spFile=vecStrAlloc(500);

    utilGetoptReset();
    while ( ( c = utilGetopt( argc, argv, "osvh" ) ) != EOF )
    {
        switch ( c )
        {
        case 'o':
            if ( utilOptind >= argc )
            {
                Abc_Print( -1, "Command line switch \"-o\" should be followed by a file name.\n" );
                goto usage;
            }
            folderName->append(argv[utilOptind]);
            utilOptind++;
            break;
        case 's':
            if ( utilOptind >= argc )
            {
                Abc_Print( -1, "Command line switch \"-s\" should be followed by a file name.\n" );
                goto usage;
            }
            spFile->append(argv[utilOptind]);
            utilOptind++;
            break;
        case 'v':
            fVerbose ^= 1;
            break;
        case 'h':
            goto usage;
        default:
            goto usage;
        }
    }
   
    pArgvNew = argv + utilOptind;
    nArgcNew = argc - utilOptind;
    if ( nArgcNew == 0 || nArgcNew == 1 )
    {
        if ( nArgcNew == 1 )
        {
            updateAIGNtk(pSC4AV, pArgvNew[0] );
        }

        if(folderName->size()!=0){
            vecStr* ntkStrName=vecStrAlloc(sizeof(pSC4AV->curGiaNtk->pName));
            ntkStrName->append(getSubstring(pSC4AV->curGiaNtk->pName, '/'));
            *pbFileName=*folderName+'/'+*ntkStrName+".pb";
            *logFileName=*folderName+'/'+*ntkStrName+".log";
            vecStrFreeP(ntkStrName);
        }
        frame->fStatus = SC4MultSolver( pSC4AV, fVerbose, spFile, logFileName );
    }   
    else
    {
        fprintf( frame->Err, "Too many command-line arguments.\n" );
        return 1;
    }
    if(pbFileName->size()!=0){
        printMultModel(pSC4AV, pbFileName->c_str());
    }
    
    vecStrFreeP(folderName);
    vecStrFreeP(pbFileName);
    vecStrFreeP(logFileName);
    vecStrFreeP(spFile);
    
    pSC4AV->closeManagers();
    return 0;

usage:
    fprintf( frame->Err, "usage: &sc4mult [-o <directory>]  [-s <file>] [-vh] <file>\n" );
    fprintf( frame->Err, "\t              verify integer multiplier circuit \n" );
    fprintf( frame->Err, "\t-o <directory>: the directory to print the algebraic model after preprocessing\n" );
    fprintf( frame->Err, "\t-s <file>     : the file with the specifcation polynomial\n" );
    fprintf( frame->Err, "\t-v            : toggle verbose output [default = %s]\n", fVerbose? "yes":"no");
    fprintf( frame->Err, "\t-h            : print the command usage\n");
    fprintf( frame->Err, "\t file         : (optional) the file with the AIG Multiplier network\n");
    return 1;
}

/**Function*************************************************************

  Synopsis    []

  Description []

  SideEffects []

  SeeAlso     []

***********************************************************************/
int Commands::commandReadVerilog( SC4AV* pSC4AV, int argc, char ** argv )
{
    ABC_NAMESPACE_USING_NAMESPACE
    AVFrame* frame= pSC4AV->pFrame;
    char ** pArgvNew;
    int nArgcNew;
    int c, fVerbose = 0;
    utilGetoptReset();
    while ( ( c = utilGetopt( argc, argv, "vh" ) ) != EOF )
    {
        switch ( c )
        {
        case 'v':
            fVerbose ^= 1;
            break;
        default:
            goto usage;
        }
    }
    pArgvNew = argv + globalUtilOptind;
    nArgcNew = argc - globalUtilOptind;
    if ( nArgcNew != 1 )
    {
        Abc_Print( -1, "There is no file name.\n" );
        return 1;
    }


    updateAIGNtk(pSC4AV, pArgvNew[0]);

usage:
    fprintf( frame->Err, "usage: &read_ver [-vh] <file>\n" );
    fprintf( frame->Err, "\t        a specialized reader for hierarchical Verilog files\n" );
    fprintf( frame->Err, "\t-v     : toggles additional verbose output [default = %s]\n", fVerbose? "yes": "no" );
    fprintf( frame->Err, "\t-h     : print the command usage\n");
    fprintf( frame->Err, "\t<file> : the file name\n");
    return 1;
}


/**Function*************************************************************

  Synopsis    []

  Description []

  SideEffects []

  SeeAlso     []

***********************************************************************/
int Commands::commandReadAIGER( SC4AV* pSC4AV, int argc, char ** argv )
{
    ABC_NAMESPACE_USING_NAMESPACE
    AVFrame* frame= pSC4AV->pFrame;
    extern void Abc3_ReadShowHie( char * pFileName, int fFlat );
    Gia_Man_t * pAig = NULL;
    FILE * pFile;
    char ** pArgvNew;
    char * FileName, * pTemp;
    int c, nArgcNew;
    int fMiniAig = 0;
    int fMiniLut = 0;
    int fVerbose = 0;
    int fGiaSimple = 0;
    int fSkipStrash = 0;
    Extra_UtilGetoptReset();
    while ( ( c = Extra_UtilGetopt( argc, argv, "csmlvh" ) ) != EOF )
    {
        switch ( c )
        {
        case 'c':
            fGiaSimple ^= 1;
            break;
        case 's':
            fSkipStrash ^= 1;
            break;
        case 'm':
            fMiniAig ^= 1;
            break;
        case 'l':
            fMiniLut ^= 1;
            break;
        case 'v':
            fVerbose ^= 1;
            break;
        default:
            goto usage;
        }
    }
    pArgvNew = argv + globalUtilOptind;
    nArgcNew = argc - globalUtilOptind;
    if ( nArgcNew != 1 )
    {
        Abc_Print( -1, "There is no file name.\n" );
        return 1;
    }

    // get the input file name
    FileName = pArgvNew[0];
    // fix the wrong symbol
    for ( pTemp = FileName; *pTemp; pTemp++ )
        if ( *pTemp == '>' )
            *pTemp = '\\';
    if ( (pFile = fopen( FileName, "r" )) == NULL )
    {
        Abc_Print( -1, "Cannot open input file \"%s\". ", FileName );
        if ( (FileName = Extra_FileGetSimilarName( FileName, ".aig", ".blif", ".pla", ".eqn", ".bench" )) )
            Abc_Print( 1, "Did you mean \"%s\"?", FileName );
        Abc_Print( 1, "\n" );
        return 1;
    }
    fclose( pFile );

    if ( fMiniAig )
        pAig = Gia_ManReadMiniAig( FileName );
    else if ( fMiniLut )
        pAig = Gia_ManReadMiniLut( FileName );
    else
        pAig = Gia_AigerRead( FileName, fGiaSimple, fSkipStrash, 0 );
    
    if ( pAig )
        pSC4AV->updateGia((GiaNtk*) pAig);
    return 0;

usage:
    fprintf( frame->Err, "usage: &r [-csmlvh] <file>\n" );
    fprintf( frame->Err, "\t         reads the current AIG from the AIGER file\n" );
    fprintf( frame->Err, "\t-c     : toggles reading simple AIG [default = %s]\n", fGiaSimple? "yes": "no" );
    fprintf( frame->Err, "\t-s     : toggles structural hashing while reading [default = %s]\n", !fSkipStrash? "yes": "no" );
    fprintf( frame->Err, "\t-m     : toggles reading MiniAIG rather than AIGER file [default = %s]\n", fMiniAig? "yes": "no" );
    fprintf( frame->Err, "\t-l     : toggles reading MiniLUT rather than AIGER file [default = %s]\n", fMiniLut? "yes": "no" );
    fprintf( frame->Err, "\t-v     : toggles additional verbose output [default = %s]\n", fVerbose? "yes": "no" );
    fprintf( frame->Err, "\t-h     : print the command usage\n");
    fprintf( frame->Err, "\t<file> : the file name\n");
    return 1;
}

/**Function********************************************************************

  Synopsis    []

  Description []

  SideEffects []

  SeeAlso     []

******************************************************************************/


void Commands::commandsInit( ){
    
    addCommand( "Basic", "echo", 0,    commandEcho);
    addCommand( "Basic", "time", 0,    commandTime);
    addCommand( "Basic", "quit", 0,    commandQuit);
    addCommand( "Basic", "history", 0, commandHistory );
    addCommand( "Basic", "source", 0,    commandSource);
    addCommand( "Parser", "polyn", 0,   commandPolyn);
    addCommand( "Prover", "sc4mult",  0,   commandSC4Multiplier);
    addCommand( "Prover", "sc4crypto",  0,   commandSC4Crypto);
    addCommand( "Prover", "readVerilog",  0,   commandReadVerilog);
    addCommand( "Prover", "readAIGER",  0,   commandReadAIGER);
}


