/**HFile****************************************************************

  FileName    [frame.h]

  SystemName  [AVrith.]

  PackageName [frame.]

  Synopsis    [define the frame of the tool.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/

#ifndef AVFRAME_H
#define AVFRAME_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "extra/extra.h"
#include "misc/int.h"
#include "misc/str.h"
#include "misc/ptr.h"
#include "main/avCommands.h"

#ifdef SC4AV_USE_READLINE
#include <readline/readline.h>
#include <readline/history.h>
#endif


class AVFrame {
    
    public:
        AVFrame(SC4AV*);
        virtual void init(char * );
        virtual bool open (int argc, char * argv[]);
        virtual ~AVFrame();
        char * utilsGetUsersInput( );
        int commandExecute( const char * sCommand );
        const char * splitCommandLine( const char *, int&, char **&);
        int dispatchCommand( int&, char **&);
        int cmdCheckShellEscape(int& argc, char **&argv);
        void printUsage(char * ProgName );
        void printHello();
        
        
        vecStr* sCommandUsr= vecStrAlloc(1000);
        char sCommandTmp[1000], sReadCmd[1000], sWriteCmd[1000];
        const char * sOutFile, * sInFile;
        char * sCommand;
        int  fStatus = 0;
        int c, fInitSource, fInitRead, fFinalWrite;
        // the functionality
        int             nSteps;        // the counter of different network processed
        int             fSource;       // marks the source mode
        int             fAutoexac;     // marks the autoexec mode
        int             fBatchMode;    // batch mode flag

        enum {
            INTERACTIVE, // interactive mode
            BATCH, // batch mode, run a command and quit
            BATCH_THEN_INTERACTIVE, // run a command, then back to interactive mode
            BATCH_QUIET, // as in batch mode, but don't echo the command
            BATCH_QUIET_THEN_INTERACTIVE, // as in batch then interactive mode, but don't echo the command
        } fBatch;
        
        Commands* _commands ;

        // commands, aliases, etc
        tableStr *      tCommands;     // the command table
        tableStr *      tFlags;        // the flag table
        vecPtr *     aHistory;      // the command history
        // output streams
        FILE *          Out;
        FILE *          Err;
        FILE *          Hst;
        // used for runtime measurement
        double          TimeCommand;   // the runtime of the last command
        double          TimeTotal;     // the total runtime of all commands
    private:    
        // general info
        char *          sVersion;      // the name of the current version
        char *          sBinary;       // the name of the binary running
    protected:
        SC4AV* pSC4AV;
};


#endif /* AVFRAME_H */



///#include "main/sc4aCommands.h"
