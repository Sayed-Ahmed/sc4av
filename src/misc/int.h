/**HFile****************************************************************

  FileName    [str.h]

  SystemName  [SC4Arith.]

  PackageName [Strings.]

  Synopsis    [manibulate Srtings.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/

#ifndef INT_H
#define INT_H

#include <vector>
#include <algorithm>


using namespace std;

typedef vector<int> vecInt;

static inline vecInt* vecIntAlloc( int nCap )
{
    vecInt* p= new vecInt(nCap);
    return p;
}

static inline size_t vecIntSize( vecInt* p )
{
    return p->size();
}

//typedef std::set<unsigned long long int>::iterator Iterator;
template <typename Iterator>
inline bool next_combination( Iterator first, Iterator k, Iterator last)
{
   /* Credits: Thomas Draper */
   if ((first == last) || (first == k) || (last == k))
      return false;
   Iterator itr1 = first;
   Iterator itr2 = last;
   ++itr1;
   if (last == itr1)
      return false;
   itr1 = last;
   --itr1;
   itr1 = k;
   --itr2;
   while (first != itr1)
   {
      if (*--itr1 < *itr2)
      {
         Iterator j = k;
         while (!(*itr1 < *j)) ++j;
         std::iter_swap(itr1,j);
         ++itr1;
         ++j;
         itr2 = k;
         std::rotate(itr1,j,last);
         while (last != j)
         {
            ++j;
            ++itr2;
         }
         std::rotate(k,itr2,last);
         return true;
      }
   }
   std::rotate(first,k,last);
   return false;
}

#endif /* INT_H */

