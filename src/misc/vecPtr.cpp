/**CPPFile****************************************************************

  FileName    [vecPtr.cpp]

  SystemName  [SC4Arith.]

  PackageName [Resizable arrays.]

  Synopsis    [Resizable arrays of pointers.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/
