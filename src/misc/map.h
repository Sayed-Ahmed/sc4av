/**HFile****************************************************************

  FileName    [map.h]

  SystemName  [SC4Arith.]

  PackageName [maps.]

  Synopsis    [manipulate Maps.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <map>

#ifndef MAP_H
#define MAP_H


template<typename A, typename B>
static inline std::pair<B,A> flipPair(const std::pair<A,B> &p)
{
    return std::pair<B,A>(p.second, p.first);
}

 template<typename A, typename B, template<class,class,class...> class M, class... Args>
static inline std::multimap<B,A> flipMapWithMultimap(const M<A,B,Args...> &src)
{
    std::multimap<B,A> dst;
    std::transform(src.begin(), src.end(),
                   std::inserter(dst, dst.begin()),
                   flipPair<A,B>);
    return dst;
}


template<typename A, typename B>
static inline std::map<B,A> flipMap(const std::map<A,B> &src)
{
    std::map<B,A> dst;
    std::transform(src.begin(), src.end(), std::inserter(dst, dst.begin()), 
                   flipPair<A,B>);
    return dst;
}

#endif /* MAP_H */

