/**CPPFile****************************************************************

  FileName    [vecStr.cpp]

  SystemName  [SC4Arith.]

  PackageName [Resizable arrays.]

  Synopsis    [Resizable arrays of characters.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/

#include "str.h"


bool lookupStrTable( tableStr* table, string key, char **value){

    if (table->find(key)==table->end()) return false;
    else{
        *value=table->at(key);
    }
    return true;
}

