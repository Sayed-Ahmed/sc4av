/**CPPFile****************************************************************

  FileName    [vecInt.cpp]

  SystemName  [SC4Arith.]

  PackageName [Resizable arrays.]

  Synopsis    [Resizable arrays of integers.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/

#include "int.h"



