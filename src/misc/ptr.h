/**HFile****************************************************************

  FileName    [ptr.h]

  SystemName  [SC4Arith.]

  PackageName [Vector of Pointers.]

  Synopsis    [manibulate Pointers.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/

#ifndef PTR_H
#define PTR_H

#include <vector>
#include <assert.h>

using namespace std;

typedef vector<void*> vecPtr;


////////////////////////////////////////////////////////////////////////
///                      MACRO DEFINITIONS                           ///
////////////////////////////////////////////////////////////////////////

// iterators through entries
#define vecPtrForEachEntry( Type, vVec, pEntry )                                               \
    for (int i = 0; (i < vecPtrSize(vVec)) && (((pEntry) = (Type)vecPtrEntry(vVec, i)), 1); i++ )
#define vecPtrForEachEntryStart( Type, vVec, pEntry, Start )                                   \
    for (int i = Start; (i < vecPtrSize(vVec)) && (((pEntry) = (Type)vecPtrEntry(vVec, i)), 1); i++ )
#define vecPtrForEachEntryStop( Type, vVec, pEntry, Stop )                                     \
    for (int i = 0; (i < Stop) && (((pEntry) = (Type)vecPtrEntry(vVec, i)), 1); i++ )
#define vecPtrForEachEntryStartStop( Type, vVec, pEntry, Start, Stop )                         \
    for (int i = Start; (i < Stop) && (((pEntry) = (Type)vecPtrEntry(vVec, i)), 1); i++ )
#define vecPtrForEachEntryReverse( Type, vVec, pEntry )                                        \
    for (int i = vecPtrSize(vVec) - 1; (i >= 0) && (((pEntry) = (Type)vecPtrEntry(vVec, i)), 1); i-- )
#define vecPtrForEachEntryTwo( Type1, vVec1, Type2, vVec2, pEntry1, pEntry2 )                  \
    for (int i = 0; (i < vecPtrSize(vVec1)) && (((pEntry1) = (Type1)vecPtrEntry(vVec1, i)), 1) && (((pEntry2) = (Type2)vecPtrEntry(vVec2, i)), 1); i++ )
#define vecPtrForEachEntryDouble( Type1, Type2, vVec, Entry1, Entry2 )                                \
    for (int i = 0; (i+1 < vecPtrSize(vVec)) && (((Entry1) = (Type1)vecPtrEntry(vVec, i)), 1) && (((Entry2) = (Type2)vecPtrEntry(vVec, i+1)), 1); i += 2 )

/**Function*************************************************************

  Synopsis    []

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/

static inline vecPtr* vecPtrAlloc( )
{
    vecPtr* p= new vecPtr;
    return p;
}

/**Function*************************************************************

  Synopsis    []

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
static inline int vecPtrSize( vecPtr * p )
{
    return p->size();
}
#endif /* PTR_H */

/**Function*************************************************************

  Synopsis    []

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
static inline void * vecPtrEntry( vecPtr * p, int i )
{
    assert( i >= 0 && i < p->size() );
    return p->at(i);
}

/**Function*************************************************************

  Synopsis    []

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/

static inline void vecPtrFree( vecPtr * p )
{
    
    for ( auto it = p->begin(); it != p->end(); ++it ){
            delete *it;
    }
    
     delete p;
}


/**Function*************************************************************

  Synopsis    []

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/

static inline void ** vecPtr2Array( vecPtr * p )
{
    void ** pArray = p->data();
    return pArray;
}

/**Function*************************************************************

  Synopsis    []

  Description []
               
  SideEffects []

  SeeAlso     []

***********************************************************************/
static inline void vecPtrPush( vecPtr * p, void * Entry )
{
    p->push_back(Entry);
}