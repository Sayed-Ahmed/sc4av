/**HFile****************************************************************

  FileName    [str.h]

  SystemName  [SC4Arith.]

  PackageName [Strings.]

  Synopsis    [manibulate Srtings.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/

#include <map>
#include <vector>
#include <string.h>
#include <algorithm>
#include <stdio.h>
#include <assert.h>
using namespace std;

#ifndef STR_H
#define STR_H

typedef string vecStr;
typedef map<string, char*> tableStr;


static inline vecStr* vecStrAlloc( int nCap )
{
    vecStr* p= new vecStr();
    return p;
}

static inline void vecStrFreeP(vecStr* p)
{
    delete p;
}

static inline void arrStrFree(char* p){
    ((p) ? (free((char *) (p)), (p) = 0) : 0);
}
static inline size_t vecStrSize( vecStr* p )
{
    return p->size();
}

static inline void vecStrAppend( vecStr* p, const char* item )
{
    p->append(item);
}

static inline tableStr* tableStrInit( )
{
    tableStr* p= new tableStr();
    return p;
}

static inline void tableStrFree( tableStr* p )
{
    delete p;
}

bool lookupStrTable( tableStr* table, string key, char ** value);

typedef vector<string> vecString;

static inline vecString* vecStringAlloc( )
{
    vecString* p= new vecString;
    return p;
}

static inline size_t vecStringSize( vecString* p )
{
    return p->size();
}

static inline void vecStringFree( vecString* p )
{
    for ( auto it = p->begin(); it != p->end(); ++it ){
           delete &it;
    }
    
     delete p;
}

static inline bool findChar (const char* l, char c){ 
    const char* end = l + sizeof(l) / sizeof(l[0]);            
    const char* pos = find(l, end, c);
    return (pos != end); 
 }

static inline void printStrTableKeys (tableStr * p ){ 
    for(auto it : *p) {
        printf("%s\n", it.first.c_str());
    }
}

static inline const char* getSubstring(const char* l, char c){
    auto s=vecStrAlloc( sizeof(l) );
    s->append(l);
    auto cPos=s->find_last_of(c);
    if(cPos==s->size())return l;
    assert(cPos+1<s->size());
    *s=s->substr(cPos+1,s->size());
    return s->c_str();
}

#endif /* STR_H */

