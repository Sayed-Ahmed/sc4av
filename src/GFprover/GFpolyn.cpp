/**CPPFile****************************************************************

  FileName    [polynMgr.cpp]

  SystemName  [SC4AV.]

  PackageName [prover]

  Synopsis    [Construct and Destruct polynMgr.]

  Author      [Amr Sayed Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]

  Revision    []

***********************************************************************/

#include"GFprover/GFpolyn.h"
#include "misc/util/abc_global.h"


    GFPolynMgr::GFPolynMgr( ){
        _varTable = (pVariableTable) new VariableTable;
        _monlTable = (pMonomialTable) new MonomialTable;
        _ideal = (pGFIdeal) new GFIdeal;
        _depVars = (pMonomial) new Monomial;
        _inputVars = (pMonomial ) new Monomial ;
        _outputVars = (pMonomial) new Monomial;
        _polynId2Aig = (pIdTable) new IdTable;
        _specPolyn = (pGFPolynomial) new GFPolynomial;
        logFile=NULL;
     }


    GFPolynMgr::~GFPolynMgr( ){
    }

    
    pMonomial GFPolynMgr::allocateMonomial(){
        return (pMonomial) new Monomial;
    }
    
    pMonomial GFPolynMgr::allocateMonomial(const variableId& v){
        pMonomial m= (pMonomial) new Monomial;
        m->insert(v);
        return m;
    }
    
    pMonomial GFPolynMgr::allocateMonomial(const variableId& v1, 
                                                          const variableId& v2){
        pMonomial m= (pMonomial) new Monomial;
        m->insert(v1);
        m->insert(v2);
        return m;
    }
    
    
    void GFPolynMgr::printMonomial(FILE* &out,  pMonomial m)
    {
        if(m->empty()) {
             fprintf(out, "%ld", 1);
             return;
        }
        size_t i =0;
        for ( auto& var : *m){
                i++;
                if(i!=m->size())
                        fprintf(out,"v_%llu*", var);
                else
                        fprintf(out,"v_%llu", var);
        }
        return;
    }
    

    void GFPolynMgr::printGFPolynomial(FILE* &out, pGFPolynomial p )
    {
            if(p->empty()){
                    fprintf(out,"Zero GFPolynomial\n");
                    return;
            }
            size_t i=0;
            for ( auto& monl : *p){
                    i++;
                    printMonomial(out, monl);
                    if( i!=p->size()){ fprintf(out, "+"); }
            }
            return;
    }
    
    
    void GFPolynMgr::printPrimaryInputs(FILE* &out, Monomial* m )
    {
            if(m->empty()){
                    fprintf(out,"empty primary input vector\n");
                    return;
            }
            size_t i=0;
            fprintf(out, "\t{in: ");
            for ( auto& v : *m){
                    i++;
                    fprintf(out,"v_%llu", v);
                    if( i!=m->size()){ fprintf(out, ","); }
            }
            fprintf(out, "}");
            return;
    }

    void GFPolynMgr::printGFIdeal(FILE *&out, const int& fVerbose)
    {
        if(_ideal->empty()){
            fprintf(out, "GFIdeal is empty\n");
            return;
        }
        for ( auto& it : *_ideal){
            fprintf(out,"v_%llu = ", it.first);
            printGFPolynomial(out, it.second->leadMonls);
            if(fVerbose>0) 
                printPrimaryInputs(out, &it.second->primaryInVars);
            fprintf(out, "\n\n");
        }
        return;
    }
    
     void GFPolynMgr::openLogFile(char* networkName){
        std::string filename=networkName;
        filename+=".log";
        logFile=fopen(filename.c_str(),"w");
        assert(logFile!=NULL);
    }

    void GFPolynMgr::closeLogFile(){
        if(logFile!=NULL)
            fclose(logFile);
    }     
    void GFPolynMgr::updateMonomialVariableTables(pMonomial pmonl, 
                                                          const variableId& id){
        auto monl=*pmonl; 
        auto monl_it = _monlTable->find(monl);
        if(monl_it==_monlTable->end()){
            auto monlD= allocateMonomialData();
            monlD->inPolynIds.insert(id);
            _monlTable->insert(std::make_pair(monl,monlD));
        }
        else{
            auto monlD=monl_it->second;
            monlD->inPolynIds.insert(id);
        }
        
        for(auto var :monl){
            auto var_it = _varTable->find(var);
            if(var_it==_varTable->end()){
                auto varD= allocateVariableData();
                varD->inPolynIds->insert(id);
                varD->fanoutSize++;
                _varTable->insert(std::make_pair(var,varD));
            }
            else{
                auto varD=var_it->second;
                varD->inPolynIds->insert(id);
                varD->fanoutSize++;
            }
        }
    }
    
    void GFPolynMgr::addPolyn2GFIdeal(const variableId & key, 
                                                           pGFPolynomialData p){
        
        updatePrimaryInVariables(p);    
        _ideal->insert(std::make_pair(key, p));
    }

    void GFPolynMgr::updatePrimaryInVariables(pGFPolynomialData p){
        
        assert(!p->inVars->empty());
        
        for(auto var: *p->inVars){
            if(isPrimaryInput(var)){
                p->primaryInVars.insert(var);
                continue;
            }
            auto var_it = _ideal->find(var);
            assert(var_it!=_ideal->end());
            assert(!var_it->second->primaryInVars.empty());
            auto var_fanin= var_it->second->primaryInVars;
            p->primaryInVars.insert(var_fanin.begin(), var_fanin.end());
        }
        
    }
    
    void GFPolynMgr::updateVariableSubstitutionSize(const variableId& var, 
                                                            const size_t& size){
        auto var_it = _varTable->find(var);
        if(var_it==_varTable->end()){
            auto varD= allocateVariableData();
            varD->substSize=size;
            _varTable->insert(std::make_pair(var,varD));
        }
        else{
            auto varD=var_it->second;
            varD->substSize=size;
        }
        
    }