/**CPPFile****************************************************************

  FileName    [preprocess.cpp]

  SystemName  [SC4AV.]

  PackageName [prover.]

  Synopsis    [preprocessing Groebner bases polynomials.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/

#include "GFpreprocess.h"



GFPreprocessMgr::GFPreprocessMgr(GFPolynMgr * pMgr_new){
    pMgr=pMgr_new;
    _prInputsTable=(pSetsTable)   new SetsTable;
    _fanInsTable = (pSetsTable)   new SetsTable;
    _removedTable = (pRewriteTable) new  RewriteTable;
    _substituteTable =(pGFSubstitutionTable) new GFSubstitutionTable;
    _eliminatedVars =(pMonomial) new Monomial;
}

GFPreprocessMgr::~GFPreprocessMgr(){
    
}


pMonomial GFPreprocessMgr::copyNewMonl(pMonomial m){
    auto newMonl= pMgr->allocateMonomial();
    newMonl->insert(m->begin(), m->end());
    return newMonl;
}

void GFPreprocessMgr::printGFPolynomial(FILE*& out,pGFPolynomial p, const variableId & v )
{
    fprintf(out,"v_%llu = ", v);
    pMgr->printGFPolynomial(out,p);
    fprintf(out,"\n\n");
    return;
}

void GFPreprocessMgr::printSubstitutionGFPolynomial(FILE*& out,pGFPolynomial p,Monomial* m )
{
    printMonomial(m);
     fprintf(out," = ");
    pMgr->printGFPolynomial(out,p);
    fprintf(out,"\n\n");
    return;
}
    
void GFPreprocessMgr::printSet(pVariableSet m){
    size_t i=0;
    fprintf(stdout,"{");
    for ( auto& var : *m){
            i++;
            if(i!=m->size())
                    fprintf(stdout,"v_%llu,", var);
            else
                    fprintf(stdout,"v_%llu", var);
    }
        fprintf(stdout,"}\n");
    return;
}


void GFPreprocessMgr::printSet(pMonomial m){
    size_t i=0;
    fprintf(stdout,"{");
    for ( auto& var : *m){
            i++;
            if(i!=m->size())
                    fprintf(stdout,"v_%llu,", var);
            else
                    fprintf(stdout,"v_%llu", var);
    }
        fprintf(stdout,"}\n");
    return;
}


void GFPreprocessMgr::printMonomial(Monomial* m){
    size_t i=0;
    for ( auto& var : *m){
            i++;
            if(i!=m->size())
                    fprintf(stdout,"v_%llu*", var);
            else
                    fprintf(stdout,"v_%llu", var);
    }
    return;
}

void GFPreprocessMgr::printSet(Monomial* m){
    size_t i=0;
    fprintf(stdout,"{");
    for ( auto& var : *m){
            i++;
            if(i!=m->size())
                    fprintf(stdout,"v_%llu,", var);
            else
                    fprintf(stdout,"v_%llu", var);
    }
        fprintf(stdout,"}\n");
    return;
}

void GFPreprocessMgr::printLUT(pTruthTable t){
    size_t i=0;
    fprintf(stdout,"{");
    for ( auto& assign : *t){
            i++;
            if(i!=t->size())
                    fprintf(stdout,"%s,", assign.to_string().c_str());
            else
                    fprintf(stdout,"%s", assign.to_string().c_str());
    }
        fprintf(stdout,"}\n");
    return;
}


void GFPreprocessMgr::printMonomialTable(){
    auto monlTable=pMgr->_monlTable;
    for (auto monl_it: *monlTable){
        auto inPolynIds=monl_it.second->inPolynIds;
        auto monl=monl_it.first;
        if(inPolynIds.size()==1){           
            printSet(&monl);
            fprintf(stdout," id %llu\n", *(inPolynIds.begin()));
        }
    }
}


bool GFPreprocessMgr::isXNORGFPolynomial(const variableId& polynId){
    
    size_t numInputs;
    auto lut=pMgr->getGFPolynomialLUT(polynId, numInputs);
    if(lut==NULL) return false;
    
    auto xnorLut=twoInputsXNOR(numInputs);
    
    if(xnorLut==NULL) return false;
    
    
    if( lut->size()==xnorLut->size() && *lut==*xnorLut) return true;
    
    return false;
    
}

bool GFPreprocessMgr::isXORGFPolynomial(const variableId & polynId){
    
    size_t numInputs;
    auto lut=pMgr->getGFPolynomialLUT(polynId, numInputs);
    if(lut==NULL) return false;
    
    auto xorLut=twoInputsXOR(numInputs);
    auto xnorLut=twoInputsXNOR(numInputs);
    
    if(xorLut==NULL || xnorLut==NULL) return false;
    
    if(lut->size()==xorLut->size() && *lut==*xorLut) return true;
    
    if( lut->size()==xnorLut->size() && *lut==*xnorLut) return true;
    
    return false;
    
}
bool GFPreprocessMgr::simulateGFPolynomial(pGFPolynomialData p, __Byte stim){
    
    if(p->leadMonls->empty()) return false;
    auto stimPolyn=pMgr->allocateGFPolynomial();
    auto newPolyn=pMgr->allocateGFPolynomial();
    
    pMgr->insertPolyn(stimPolyn, p->leadMonls);
    
    int i=0;
    for(auto var: *p->inVars){
        bool varValue=stim[i];
        for(auto monl : *stimPolyn){ 
            if(isInPMonomial(monl,var)){
                if(varValue){
                    auto newMonl=copyNewMonl(monl);
                    newMonl->erase(var);
                    pMgr->insertMonl(newPolyn, newMonl);
                }   
            }
            else{
                auto newMonl=copyNewMonl(monl);
                //newPolyn->insert(newMonl);
                pMgr->insertMonl(newPolyn, newMonl);
            }
        }
        //addSimilarMonomials(newPolyn);
        stimPolyn->swap(*newPolyn);
        newPolyn->clear();
        i++;
    }
    
    if(stimPolyn->empty()) {
        return false;
    }
    else if(stimPolyn->size()==1){
        auto monl=*stimPolyn->begin();
        if(monl->empty()){
            return true; 
        }
        //else{
            //printf("simulation %s", stim.to_string().c_str());
            //printGFPolynomial(stdout,p->leadMonls,0);
            //printGFPolynomial(stdout,stimPolyn,0);
            //assert(0);
        //}
    }
    else{ 
        printGFPolynomial(stdout,p->leadMonls,0);
        printGFPolynomial(stdout, stimPolyn,0);
        assert(0);
    }
}

void  GFPreprocessMgr::updateFaninsTable(){
    
    if(!_fanInsTable->empty()) _fanInsTable->clear();
    
    assert(!pMgr->_ideal->empty());
    for(auto polyn_it: *pMgr->_ideal){
       auto polynId= polyn_it.first;
       auto varIns=polyn_it.second->inVars;
       assert(!varIns->empty());
       auto varIns_it=_fanInsTable->find( *varIns);
       if(varIns_it==_fanInsTable->end()){
           pMonomial sharedPolynIds=pMgr->allocateMonomial(polynId); 
           _fanInsTable->insert(std::make_pair(*varIns,sharedPolynIds));
       }
       else{
           varIns_it->second->insert(polynId);
       }
    }
   return; 
}

void  GFPreprocessMgr::updatePrimaryInputsTable(){
    
    if(!_prInputsTable->empty()) _prInputsTable->clear();
    
    assert(!pMgr->_ideal->empty());
    for(auto polyn_it: *pMgr->_ideal){
       auto polynId= polyn_it.first;
       auto prIns=polyn_it.second->primaryInVars;
       assert(!prIns.empty());
       auto prIns_it=_prInputsTable->find( prIns);
       if(prIns_it==_prInputsTable->end()){
           pMonomial prIns_polynIds=pMgr->allocateMonomial(polynId); 
           _prInputsTable->insert(std::make_pair(prIns,prIns_polynIds));
       }
       else{
           prIns_it->second->insert(polynId);
       }
    }
   return; 
}

void  GFPreprocessMgr::updateRemovedTable(const variableId& varId){
    
    auto idSet= pMgr->getGFPolynomialChilds(varId);
    if(idSet== NULL) return;
    for (auto polynId: *idSet){
        auto remvTable_it= _removedTable->find(polynId);

        if(remvTable_it==_removedTable->end()){
            auto _remvVars = pMgr->allocateMonomial(varId);
            _removedTable->insert(std::make_pair(polynId, _remvVars));
        }
        else{
            remvTable_it->second->insert(varId);
        }
    }
    
   return; 
}

MonomialsSet GFPreprocessMgr::getCombinations(pMonomial removedVars){
        

    MonomialsSet combinations;
    combinations.insert(*removedVars);
    for(size_t k=2; k<removedVars->size(); k++){  
        std::vector<variableId> s; 
        std::copy(removedVars->begin(),removedVars->end(),std::back_inserter(s));
        do{
            Monomial monl;
            monl.insert(s.begin(), std::next(s.begin() , k));
            combinations.insert(monl);
            
        }while(next_combination(s.begin(), std::next(s.begin() , k) ,s.end()));
    }
    
    return combinations;
}


void GFPreprocessMgr::addSimilarMonomials(pGFPolynomial polyn){
    
    if(polyn->size()<2) return;
    pGFPolynomial simplifiedPolyn=pMgr->allocateGFPolynomial();
    auto it= polyn->begin();
    auto it_next=std::next(it);
    while(it_next!=polyn->end()){
        auto monl=*it;
        auto monl_next=*it_next;
        
        if( *monl==*monl_next ){
            it_next=std::next(it_next);
            if(it_next==polyn->end()) break;
            monl_next=*it_next;
        }
        else {
            auto newMonl= pMgr->allocateMonomial();
            newMonl->insert(monl->begin(), monl->end());
            pMgr->insertMonl(simplifiedPolyn,newMonl);
        }
        
        if(it_next==polyn->end()) break;
        it=it_next;
        it_next=std::next(it_next);
        if(it_next==polyn->end()){
            auto newMonl= copyNewMonl(*it);
            //simplifiedPolyn->insert(newMonl);
            pMgr->insertMonl(simplifiedPolyn,newMonl);
            break;
        }
    }
    polyn->swap(*simplifiedPolyn);
    return;
}

pGFPolynomial GFPreprocessMgr::multiplyTwoGFPolynomials(pGFPolynomial polyn1, pGFPolynomial polyn2){
    pGFPolynomial product=pMgr->allocateGFPolynomial();
    
    for (auto monl1: *polyn1){
        for (auto monl2 :*polyn2){
            pMonomial monl_prod= pMgr->allocateMonomial();
            monl_prod->insert(monl1->begin(), monl1->end());
            monl_prod->insert(monl2->begin(), monl2->end());
            pMgr->insertMonl(product, monl_prod);
        }
        //addSimilarMonomials(product);
    }

    return product;
}

pGFPolynomial GFPreprocessMgr::multiplyGFPolynWithMonl(pMonomial monl1, pGFPolynomial polyn2 ){
    
    pGFPolynomial product=pMgr->allocateGFPolynomial();
    ;
    for (auto monl2 :*polyn2){
        pMonomial monl_prod= (pMonomial) new Monomial;
        monl_prod->insert(monl1->begin(), monl1->end());
        monl_prod->insert(monl2->begin(), monl2->end());
        pMgr->insertMonl(product, monl_prod);
    }
    return product;
}

void GFPreprocessMgr::insertSubstitutionGFPolynomial(
                          GFSubstitutionTable::iterator nearest_it, const Monomial& set){
    Monomial diff;
    pMonomial faninVars=pMgr->allocateMonomial();
    pGFPolynomial redPolyn=pMgr->allocateGFPolynomial();
    if(nearest_it!=_substituteTable->end()){
        auto pSet=copytoNewMonl(set);
        auto nearestSet=copytoNewMonl(nearest_it->first);
        diff=getDifferenceVariables(pSet, nearestSet);
        redPolyn=nearest_it->second;
    }
    else{
        diff=set;
        auto polynMonls=pMgr->getGFPolynomialLeadMonls(*diff.begin());
        pMgr->insertPolyn(redPolyn, polynMonls);
        diff.erase(diff.begin());
    }
    assert(!diff.empty());
    
    for(auto var:diff){
        pGFPolynomial varPolyn= pMgr->getGFPolynomialLeadMonls(var);
        redPolyn=multiplyTwoGFPolynomials(redPolyn, varPolyn);
    }
         
    _substituteTable->insert(std::make_pair(set, redPolyn));
    
    //if(fVerbose>0)
      //  printSubstitutionGFPolynomial(stdout, redPolyn, &set);

   return; 
}

void  GFPreprocessMgr::updateGFSubstitutionTable(const variableId & polynId, pMonomial removedVars){
    
    if(removedVars->size()<2)return;
    
    MonomialsSet combinations;

    auto currPolyn= pMgr->getGFPolynomialLeadMonls(polynId);
    
    for(auto monl: *currPolyn){
        auto inters=getIntersectedVariables(monl, removedVars);
        combinations.insert(inters);
    }
    
    Monomial nearestSubset;
    GFSubstitutionTable::iterator nearest_it;

    for(auto set: combinations){
        if(set.size()<2) continue;
        if(_substituteTable->size()<2){
            nearest_it=_substituteTable->end();
        }
        else{
            auto set_it= _substituteTable->lower_bound(set);
            if(set_it== _substituteTable->end()){
                nearest_it=std::prev(set_it);
                //--nearest_it;
                nearestSubset= nearest_it->first;
            }
            else if(set_it->first!=set){
                if(set_it== _substituteTable->begin()){
                    nearest_it=set_it;
                }else{
                    nearest_it=std::prev(set_it);
                }
                nearestSubset= nearest_it->first;
            }
            else{ continue;}

            if(!std::includes(set.begin(), set.end(),
                    nearestSubset.begin(), nearestSubset.end())){
                nearest_it=_substituteTable->end();
            }
        }
        
        insertSubstitutionGFPolynomial(nearest_it, set);
    }
    
   return; 
}

void GFPreprocessMgr::refineDepVarsPrimaryInputs(){
    
    updatePrimaryInputsTable();
    _removedTable->clear();
    for (auto table_it: *_prInputsTable){
        auto polynIds=table_it.second;
       /* if ( polynIds->size()>1){
            auto lastPolynId_it=std::prev(polynIds->end());
            Monomial erasedVars;
           while(lastPolynId_it!=polynIds->begin()){
                pMonomial remainVars=pMgr->allocateMonomial();
                remainVars->insert(polynIds->begin(), lastPolynId_it);
                auto faninVars=pMgr->getGFPolynomialFanins(*lastPolynId_it);
                auto intersectVars=getIntersectedVariables(remainVars, faninVars);
                for(auto var: intersectVars){
                    erasedVars.insert(var);
                    markEliminatedVariables(var);
                }
                /*if(intersectVars.size()>1){
                    erasedVars.insert(*lastPolynId_it);
                    markEliminatedVariables(*lastPolynId_it); 
                }*/
           /*     lastPolynId_it=std::prev(lastPolynId_it);
            }
            for(auto var: erasedVars){
                polynIds->erase(var);
            }
        }*/
        
        if (polynIds->size()==1){
            auto removedPolyn=*polynIds->begin();
            markEliminatedVariables(removedPolyn);
        }
    }
    return;
}

void GFPreprocessMgr::refineDepVarsFanIns(){
    updateFaninsTable();
    _removedTable->clear();
    for (auto table_it: *_fanInsTable){
        auto polynIds=table_it.second;
        auto inVar=table_it.first;
        if (polynIds->size()>1){
            
            auto lastPolynId_it=std::prev(polynIds->end());
            Monomial erasedVars;
            while(lastPolynId_it!=polynIds->begin()){
                pMonomial remainVars=pMgr->allocateMonomial();
                pMonomial brotherVars=pMgr->allocateMonomial();
                remainVars->insert(polynIds->begin(), lastPolynId_it);
                auto childPolyns=pMgr->getGFPolynomialChilds(*lastPolynId_it);
                if(childPolyns==NULL){
                    lastPolynId_it=std::prev(lastPolynId_it);
                    continue;
                }
                for (auto polyn_it:*childPolyns){
                   auto faninVars=pMgr->getGFPolynomialFanins(polyn_it);
                   brotherVars->insert(faninVars->begin(), faninVars->end());
                }
                auto intersectVars=getIntersectedVariables(remainVars, brotherVars);
                
                if(!intersectVars.empty()){ 
                   erasedVars.insert(intersectVars.begin(), intersectVars.end());
                   erasedVars.insert(*lastPolynId_it);
                }
                
                lastPolynId_it=std::prev(lastPolynId_it);
                
            }
            for(auto var: erasedVars){
                markEliminatedVariables(var);
            }
        }
    }
    
   return; 
}

void GFPreprocessMgr::refineDepVarsCoupling(){

    _removedTable->clear();
    //renewMonomialTable();
    auto monlTable=pMgr->_monlTable;
    MonomialsSet candidates;
    MonomialsSet groupCandidates;
    for (auto monl_it: *monlTable){
        auto inPolynIds=monl_it.second->inPolynIds;
        auto monl=monl_it.first;
        if(inPolynIds.size()==1){  
            auto set_it= candidates.find(monl);
            if(set_it== candidates.end()){
                    candidates.insert(monl);
            }
        }
    }
    if(candidates.empty()) return;
    auto lastMonl_it=std::prev(candidates.end());
    MonomialsSet visitedMonls;
    while(lastMonl_it!=candidates.begin()){
 
        auto pLastMonl=copytoNewMonl(*lastMonl_it);

        for (auto it =candidates.begin(); it!=lastMonl_it; it=std::next(it)){
            auto pMonl=copytoNewMonl(*it);
            auto intersVars= getIntersectedVariables(pMonl, pLastMonl);
            if(!intersVars.empty()){
                pLastMonl->insert(pMonl->begin(), pMonl->end());
                visitedMonls.insert(*pMonl);
            }
        }
        groupCandidates.insert(*pLastMonl);
        do{
            lastMonl_it=std::prev(lastMonl_it);
        }while(visitedMonls.find(*lastMonl_it)!= visitedMonls.end()&&
               lastMonl_it!=candidates.begin() );
    }
    
    for(auto group : groupCandidates){
        auto pGroup=copytoNewMonl(group);
        auto miniGroups=groupVariablesBasedFanin(pGroup);
        for (auto miniGroup: miniGroups){
           auto set=miniGroup.first;
           auto inVars=miniGroup.second;
           auto pSet=copytoNewMonl(set);
           auto diff=getDifferenceVariables(pSet, inVars);
           for(auto varId: diff){
               markEliminatedVariables(varId);
           }
        }
    }
  
    for (auto monl_it: *monlTable){
        auto inPolynIds=monl_it.second->inPolynIds;
        auto monl=monl_it.first;
        if(inPolynIds.size()!=1) continue;     
        
        auto inVars=pMgr->getGFPolynomialFanins(*inPolynIds.begin());
        //auto pMonl=copytoNewMonomial(monl);
        //auto groups=groupVariablesBasedFanin(pMonl);
        auto groups=groupVariablesBasedFanin(inVars);

        for (auto group: groups){
            auto set=group.first;
            auto inVars=group.second;
            auto pSet=copytoNewMonl(set);
            auto diff=getDifferenceVariables(pSet, inVars);
            for(auto varId: diff){
                markEliminatedVariables(varId);
            }
        }
       
    }
    
   return; 
}

void GFPreprocessMgr::refineDepVarsSingular(){

    _removedTable->clear();
    for (auto polynId_it: *pMgr->_varTable){
        auto varId=polynId_it.first;
        auto varData=polynId_it.second->inPolynIds;
        if(varData->size()<=1){
            markEliminatedVariables(varId);
        }
        
    }
    
    
   return; 
}


void GFPreprocessMgr::refineDepVarsLinear(){

    _removedTable->clear();
    
    pMonomial keepedVars=pMgr->allocateMonomial();
    auto monlTable=pMgr->_monlTable;
    for (auto monl_it: *monlTable){
        auto inPolynIds=monl_it.second->inPolynIds;
        keepedVars->insert(inPolynIds.begin(), inPolynIds.end());
    }
    auto erasedVars=getDifferenceVariables( pMgr->_depVars, keepedVars);
    auto pErasedVars=copytoNewMonl(erasedVars);
    
    for(auto var: erasedVars){
        auto childs=pMgr->getGFPolynomialChilds(var);
        if(isSubmonomial(childs, pErasedVars)){
            markEliminatedVariables(var);
        }
    }
    
   return; 
}


void GFPreprocessMgr::replaceXNORsWithXORs(){

    pMonomial xnorVars=pMgr->allocateMonomial();
    for (auto polynId_it: *pMgr->_ideal){
        auto polynId=polynId_it.first;
        auto polynData=polynId_it.second;
        //if(isXORGFPolynomial(polynId)){
            xnorVars->insert(polynId);
        //}
    }
    
    for(auto var_it=std::prev(xnorVars->end()); var_it!=std::prev(xnorVars->begin());
    var_it=std::prev(var_it)){
        auto var=*var_it;
       auto childs=pMgr->getGFPolynomialChilds(var);
       bool enableInversion=true;
       if(childs->size()<=1) continue;
       for(auto polynId: *childs){
            auto currPolyn=pMgr->getGFPolynomialLeadMonls(polynId);
            auto newPolyn=invertVariableWithinGFPolynomial(currPolyn, var);
            if(newPolyn->size()>=currPolyn->size()){
                enableInversion=false;
                break;
            }
        }
        if(enableInversion){
            for(auto polynId: *childs){
                auto currPolyn=pMgr->getGFPolynomialLeadMonls(polynId);
                auto newPolyn=invertVariableWithinGFPolynomial(currPolyn, var);          
                eraseFromMonomialTable(polynId);
                currPolyn->swap(*newPolyn);
                addToMonomialTable(polynId);
            }
            auto leadMonls=pMgr->getGFPolynomialLeadMonls(var);

            auto monl1=pMgr->allocateMonomial();
            pMgr->insertMonl(leadMonls, monl1);
        }
    }
    renewMonomialTable( );
    if(fVerbose>0)
        pMgr->printGFIdeal(stdout, fVerbose); 
   return; 
}

void GFPreprocessMgr::refineDepVarsXORs(){

    _removedTable->clear();
    pMonomial keepedVars=pMgr->allocateMonomial();
    for (auto polynId_it: *pMgr->_ideal){
        auto polynId=polynId_it.first;
        auto polynData=polynId_it.second;
        if(isXORGFPolynomial(polynId)){
            keepedVars->insert(polynId);
            for(auto var: *polynData->inVars){
                if(!pMgr->isPrimaryInput(var)){
                    keepedVars->insert(var);
                }
            }
        }
        /*else{
            auto inVars=pMgr->getGFPolynomialFanins(polynId);
            if(inVars->size()>2){
                if(fVerbose>0)
                        printf("  keepedvar %d\n", polynId);
                keepedVars->insert(polynId);
            }
        }*/
    }
    
    updateFaninsTable();
    for (auto table_it: *_fanInsTable){
        auto polynIds=table_it.second;
        auto inVar=table_it.first;
        if (polynIds->size()<=1) continue;
            
        for(auto var: *polynIds){
            keepedVars->insert(var);
        }
    }
    auto erasedVars=getDifferenceVariables( pMgr->_depVars, keepedVars);
    
    for(auto var: erasedVars){
        markEliminatedVariables(var);
    }
    
   return; 
}


pGFPolynomial GFPreprocessMgr::inferSimplerMonomials(pMonomial pSet, pMonomial inVars){
    
    pGFPolynomial redPolyn=pMgr->allocateGFPolynomial();
    auto diff=getDifferenceVariables(pSet, inVars);
    auto intersVars= getIntersectedVariables(pSet, inVars);
    
    while(pMgr->isPrimaryInput(*diff.begin())){
        intersVars.insert(*diff.begin());
        diff.erase(diff.begin());
    }
    
    if(!diff.empty()){
        auto polynMonls=pMgr->getGFPolynomialLeadMonls(*diff.begin());
        //redPolyn->insert(polynMonls->begin(), polynMonls->end());
        pMgr->insertPolyn(redPolyn, polynMonls);

        auto inputMonl= copytoNewMonl(intersVars);
        redPolyn=multiplyGFPolynWithMonl(inputMonl, redPolyn);

        for(auto var:diff){
            if(var==*diff.begin()) continue;
            pGFPolynomial varPolyn= pMgr->getGFPolynomialLeadMonls(var);
            redPolyn=multiplyTwoGFPolynomials(redPolyn, varPolyn);
        }

        if(redPolyn->empty()) return redPolyn;
        
        if(redPolyn->size()==1){
            auto firstMonl=*redPolyn->begin();
            if(isSubmonomial(firstMonl, pSet)) return redPolyn;
        }

        auto sortedRedPolyn=copytoSortedGFPolynomial(redPolyn);
        auto lastMonomial=*std::prev(sortedRedPolyn->end());

        auto lastMonomial_it= pMgr->_monlTable->find(*lastMonomial);
        if(lastMonomial_it!=pMgr->_monlTable->end()){
            auto potentialPolyn=lastMonomial_it->second->inPolynIds;
        
            for(auto var:potentialPolyn){
                auto polynMonls=pMgr->getGFPolynomialLeadMonls(var);
                auto newGFPolynomial= pMgr->allocateGFPolynomial();
                //newGFPolynomial->insert(redPolyn->begin(), redPolyn->end());
                pMgr->insertPolyn(newGFPolynomial, redPolyn);
                for(auto monl: *polynMonls){
                    auto newMonl=copyNewMonl(monl);
                    //newGFPolynomial->insert(newMonl);
                    pMgr->insertMonl(newGFPolynomial,newMonl);
                }
                //addSimilarMonomials(newGFPolynomial);
                //printGFPolynomial(stdout, newGFPolynomial, 0);
                if(newGFPolynomial->empty()){
                    auto newMonl= pMgr->allocateMonomial(var);
                    assert(!isSubmonomial( newMonl, inVars));
                    //newGFPolynomial->insert(newMonl);
                    pMgr->insertMonl(newGFPolynomial,newMonl);
                    return newGFPolynomial;
                }
            }
        }

        redPolyn->clear();
    }
    auto newMonl=copyNewMonl(pSet);
   
    pMgr->insertMonl(redPolyn,pSet);
    return redPolyn;
}


void GFPreprocessMgr::insertSimplifiedGFPolynomial (const Monomial & set, pMonomial invars,  
                                                pGFSubstitutionTable subsumedTable){
   
    
    auto pSet=copytoNewMonl(set);
    pGFPolynomial reducedPolyn= inferSimplerMonomials(pSet, invars);

    if(reducedPolyn->empty()){
        subsumedTable->insert(std::make_pair(set, reducedPolyn));
    }
    else if(reducedPolyn->size()==1){
        auto newMonl=(*reducedPolyn->begin());
        if(newMonl->empty()){
            reducedPolyn->clear();
            subsumedTable->insert(std::make_pair(set, reducedPolyn));
        }
        else if (*newMonl!=*pSet){
            subsumedTable->insert(std::make_pair(set, reducedPolyn));
        }

    }
    return;
}

pGFSubstitutionTable GFPreprocessMgr::getSharedSupportingVariables( pMonomial faninVars){
    pGFSubstitutionTable subsumedTable= (pGFSubstitutionTable)new GFSubstitutionTable;

    auto lastVar_it=std::prev(faninVars->end());
    
    while(lastVar_it!=faninVars->begin()){
        if(pMgr->isPrimaryInput(*lastVar_it)) break;
        auto inputsLastVar= pMgr->getGFPolynomialFanins(*lastVar_it);
        if(inputsLastVar->size()>4){ 
            lastVar_it=std::prev(lastVar_it);
            continue;
        }
        pMonomial remainVars=pMgr->allocateMonomial();
        remainVars->insert(faninVars->begin(), lastVar_it);
        auto intersVars= getIntersectedVariables(remainVars, inputsLastVar);
        
        Monomial subsumedSupported;
        Monomial totalIntersVars;
        if(!intersVars.empty()){
            subsumedSupported.insert(intersVars.begin(), intersVars.end());
            totalIntersVars.insert(intersVars.begin(), intersVars.begin());
            auto sharedVars= copytoNewMonl(intersVars);
            subsumedSupported.insert(*lastVar_it);
            insertSimplifiedGFPolynomial(subsumedSupported, sharedVars, subsumedTable);            
        }
        else{
            subsumedSupported.insert(*lastVar_it);
        }
 

        for (auto it =faninVars->begin(); it!=lastVar_it; it=std::next(it)){
            if(pMgr->isPrimaryInput(*it)) continue;
            auto inputs= pMgr->getGFPolynomialFanins(*it);
            if(inputs->size()>4) continue;
            auto intersVars= getIntersectedVariables(inputs, inputsLastVar);
            if(intersVars.size()>1){
                Monomial subsumedPair;
                subsumedPair.insert(*lastVar_it);
                subsumedPair.insert(*it);
                auto sharedVars=copytoNewMonl(intersVars);
                insertSimplifiedGFPolynomial(subsumedPair,sharedVars, subsumedTable);
                
                subsumedSupported.insert(*it);
                totalIntersVars.insert(intersVars.begin(), intersVars.begin());
                auto totalSharedVars= copytoNewMonl(totalIntersVars);
                insertSimplifiedGFPolynomial(subsumedSupported, totalSharedVars, subsumedTable);
            }
        }
        
        lastVar_it=std::prev(lastVar_it);
    }
    
    return subsumedTable;
}

SetsTable GFPreprocessMgr::groupVariablesBasedFanin( pMonomial faninVars){
    SetsTable subsumedGroups;

    auto lastVar_it=std::prev(faninVars->end());
    
    while(lastVar_it!=faninVars->begin()){
        if(pMgr->isPrimaryInput(*lastVar_it)) break;
        auto inputsLastVar= pMgr->getGFPolynomialFanins(*lastVar_it);
        pMonomial remainVars=pMgr->allocateMonomial();
        remainVars->insert(faninVars->begin(), lastVar_it);
        auto intersVars= getIntersectedVariables(remainVars, inputsLastVar);
        
        Monomial subsumedSupported;
        if(intersVars.size()>1){
            subsumedSupported.insert(intersVars.begin(), intersVars.end());
            auto sharedVars= copytoNewMonl(intersVars);
            subsumedSupported.insert(*lastVar_it);
            subsumedGroups.insert(std::make_pair(subsumedSupported, sharedVars));            
        }
        
        lastVar_it=std::prev(lastVar_it);
    }
    
    return subsumedGroups;
}


void GFPreprocessMgr::replaceWithSimplerMonomials(pGFPolynomial leadMonls,
                                                  pGFSubstitutionTable simplerMonomials ){
    for(auto set: *simplerMonomials){
        auto pSet=copytoNewMonl(set.first);
        pGFPolynomial reducedPolyn= set.second;
        //printSet(pSet);
        //pMgr->printGFPolynomial(stdout, reducedPolyn);
        GFPolynomial erasedMonls; 
        pGFPolynomial newMonls=pMgr->allocateGFPolynomial();
       for(auto monl: *leadMonls){
           if(monl->size()<=1) continue;
           auto intersVars= getIntersectedVariables(pSet, monl);
           if(intersVars.size()!=pSet->size()) continue;

            if(reducedPolyn->empty()){
                erasedMonls.insert(monl);
            }
            else if(reducedPolyn->size()==1){
                auto newMonl=(*reducedPolyn->begin());
                if (*newMonl!=*pSet){
                    erasedMonls.insert(monl);
                    auto diffVars=getDifferenceVariables(monl, pSet);
                    auto nmonl= copytoNewMonl(diffVars);
                    nmonl->insert(newMonl->begin(), newMonl->end());
                    pMgr->insertMonl(newMonls, nmonl);
                }
            }                           
        }
        for(auto eraseMonl: erasedMonls){
            leadMonls->erase(eraseMonl);
        }
        pMgr->insertPolyn(leadMonls, newMonls);
        //addSimilarMonomials(leadMonls);
    } 
    return;
}


void GFPreprocessMgr::applyInferenceRules(pGFPolynomial leadMonls ){
    abctime clk_infer = Abc_Clock();

   auto faninVars=pMgr->allocateMonomial();
   for(auto monl: *leadMonls){
       for(auto var:*monl){
           faninVars->insert(var);
       }
   }
    
   
   abctime clk_shared = Abc_Clock();
   auto subsumedGroups= getSharedSupportingVariables(faninVars);
   timeAnalysis.shared+= Abc_Clock() - clk_shared;
   abctime clk_replace = Abc_Clock();
   replaceWithSimplerMonomials(leadMonls, subsumedGroups);
   timeAnalysis.replace+= Abc_Clock() - clk_replace;
   //addSimilarMonomials(leadMonls);
   
    timeAnalysis.inference+= Abc_Clock() - clk_infer;
    return;    
}

void GFPreprocessMgr::applyInferenceRules(const variableId& polynId){
   
   auto polynData= pMgr->getGFPolynomialData(polynId);
   auto faninVars=polynData->inVars;
   
   abctime clk_shared = Abc_Clock();
   auto subsumedGroups= getSharedSupportingVariables(faninVars);
   timeAnalysis.shared+= Abc_Clock() - clk_shared;
   abctime clk_replace = Abc_Clock();
   replaceWithSimplerMonomials(polynData->leadMonls, subsumedGroups);
   timeAnalysis.replace+= Abc_Clock() - clk_replace;
   //addSimilarMonomials(polynData->leadMonls);


    if(fVerbose>0){
        printf("simplified GFPolynomial: ");
        printGFPolynomial(stdout, polynData->leadMonls, polynId);
    }
    //assert(polynData->leadMonls->size()<4096);
    return;    
}



pGFPolynomial GFPreprocessMgr::invertVariableWithinGFPolynomial(pGFPolynomial currPolyn,
                                                      const variableId& elimVar ){
    
    pGFPolynomial newPolyn=pMgr->allocateGFPolynomial();
    pGFPolynomial product;

    pGFPolynomial subsPolyn=pMgr->allocateGFPolynomial();
    auto monl1=pMgr->allocateMonomial();
    auto monlv=pMgr->allocateMonomial(elimVar);
    pMgr->insertMonl(subsPolyn, monl1);
    pMgr->insertMonl(subsPolyn, monlv);
    

    for(auto currMonl: *currPolyn){
        
        auto newMonl= pMgr->allocateMonomial();
        newMonl->insert(currMonl->begin(), currMonl->end());
        
        if(currMonl->empty()){ pMgr->insertMonl(newPolyn, newMonl); continue; }
        
        if(isInPMonomial(currMonl, elimVar)){
             newMonl->erase(elimVar);
        }
        else{   
            pMgr->insertMonl(newPolyn,newMonl); 
            continue; 
        }
           
        product=multiplyGFPolynWithMonl(newMonl, subsPolyn);
        assert(product!=NULL);
        
        pMgr->insertPolyn(newPolyn, product);
    }

    return newPolyn;
}


void GFPreprocessMgr::rewriteOneGFPolynomial(const variableId & polynId,
                                                        pMonomial removedVars ){
    
    pGFPolynomial newPolyn=pMgr->allocateGFPolynomial();
    pGFPolynomial product;

    auto currPolyn=pMgr->getGFPolynomialLeadMonls(polynId);

    for(auto currMonl: *currPolyn){
        
        auto newMonl= pMgr->allocateMonomial();
        newMonl->insert(currMonl->begin(), currMonl->end());
        
        if(currMonl->empty()){ pMgr->insertMonl(newPolyn, newMonl); continue; }
        
        auto intersectVars=getIntersectedVariables(removedVars, currMonl);
        
        if(intersectVars.empty()) { pMgr->insertMonl(newPolyn, newMonl); continue; }
        

        for (auto var: intersectVars){
            newMonl->erase(var);
        }
        
        if(intersectVars.size()==1){
            auto subsPolyn=pMgr->getGFPolynomialLeadMonls(*intersectVars.begin());
            product=multiplyGFPolynWithMonl(newMonl, subsPolyn);
        }
        else{
            auto subsPolyn_it=_substituteTable->find(intersectVars);
            assert(subsPolyn_it!=_substituteTable->end());
            auto subsPolyn=subsPolyn_it->second;
            if(subsPolyn->empty()) continue; // zero GFPolynomial
            product=multiplyGFPolynWithMonl(newMonl, subsPolyn);
        }
        assert(product!=NULL);
        
        pMgr->insertPolyn(newPolyn, product);
    }
    //addSimilarMonomials(newPolyn);
    currPolyn->swap(*newPolyn);
    
    return;
}


pSortedSizeTuples GFPreprocessMgr::buildSortedSizedSet(pMonomial remvVars){
    auto sortedSet = (pSortedSizeTuples) new sortedSizeTuples;
    for(auto var: *remvVars){
        auto varD=pMgr->getVariableData(var);
        auto totalSize=varD->fanoutSize*varD->substSize;
        sizeTuple newTuple= sizeTuple(var, totalSize);
        sortedSet->insert(newTuple);
    }
    return sortedSet;
}

void GFPreprocessMgr::rewriteGFPolynomialWithInference(const variableId & polynId,
                                pMonomial removedVars, const bool & enableInference ){
    
    pGFPolynomial newPolyn=pMgr->allocateGFPolynomial();
    pGFPolynomial product;
    auto currPolyn= pMgr->getGFPolynomialLeadMonls(polynId);
   
    if(fVerbose>0){
        printf("GFPolynomial: ");
        printGFPolynomial(stdout, currPolyn, polynId);
    }

   for(auto currMonl: *currPolyn){
        auto newMonl= pMgr->allocateMonomial();
        newMonl->insert(currMonl->begin(), currMonl->end());
        
        if(currMonl->size()!=1) { pMgr->insertMonl(newPolyn,newMonl); continue; }
        auto intersectVars=getIntersectedVariables(removedVars, currMonl);

        if(intersectVars.empty()) { pMgr->insertMonl(newPolyn,newMonl); continue; }


        for (auto var: intersectVars){
            newMonl->erase(var);
        }

        if(intersectVars.size()==1){
            auto subsPolyn=pMgr->getGFPolynomialLeadMonls(*intersectVars.begin());
            product=multiplyGFPolynWithMonl(newMonl, subsPolyn);
        }
        else{
            assert(0);
        }
        assert(product!=NULL);
        pMgr->insertPolyn(newPolyn, product);
    }
    currPolyn->swap(*newPolyn);
    newPolyn->clear();

    auto currRemvVars=buildSortedSizedSet(removedVars);

    //pMgr->printGFPolynomial(stdout,currPolyn);
    // eliminate nonlinear monls 
    while (!currRemvVars->empty()){
        auto firstElement_it=currRemvVars->begin();
        auto elimVar=firstElement_it->first;
        //printf(" var %llu\n", elimVar);
        currRemvVars->erase(firstElement_it);
        
        auto subsPolyn=pMgr->getGFPolynomialLeadMonls(elimVar);
        
        for(auto currMonl: *currPolyn){

            auto newMonl= pMgr->allocateMonomial();
            newMonl->insert(currMonl->begin(), currMonl->end());

            if(currMonl->empty()){ pMgr->insertMonl(newPolyn,newMonl); continue; }

            //auto intersectVars=getIntersectedVariables(removedVars, currMonl);

            if(isInPMonomial(currMonl, elimVar)){
                 newMonl->erase(elimVar);
            }
            else{   
                pMgr->insertMonl(newPolyn,newMonl); 
                continue; 
            }
           
            product=multiplyGFPolynWithMonl(newMonl, subsPolyn);
            
            assert(product!=NULL);

            pMgr->insertPolyn(newPolyn, product);
        }
        
        currPolyn->swap(*newPolyn);
        newPolyn->clear();
        if(enableInference){
            applyInferenceRules(currPolyn);
        }
    }
    
    return;
}

/*
void GFPreprocessMgr::insertNewMonomialData( pGFPolynomial newMonls, 
        variableId polynId ){
    
    auto monlTable= pMgr->_monlTable;
    for(auto monl: *newMonls){
        auto newMonl=monl->second;
        
        if(newMonl->size()<=1) continue;

        auto monl_it = monlTable->find(*newMonl);
        if(monl_it==monlTable->end()){
            auto monlD=pMgr->allocateMonomialData();
            monlD->inPolynIds.insert(polynId);
            monlTable->insert(std::make_pair(*newMonl,monlD));
        }
        else{
            auto monlD=monl_it->second;
            monlD->inPolynIds.insert(polynId);
        }
    }
    return;
}

void GFPreprocessMgr::eraseMonomialData(pMonomial oldMonomial, 
        variableId polynId ){
    
    if(oldMonomial->size()<=1) return;
    
    auto monlTable= pMgr->_monlTable;
    auto oldMonl_it= monlTable->find(*oldMonomial);
    if(oldMonl_it!=monlTable->end()){
        auto oldMonlD=oldMonl_it->second;
        auto oldPolynId_it=oldMonlD->inPolynIds.find(polynId);

        if(oldPolynId_it!=oldMonlD->inPolynIds.end())
            oldMonlD->inPolynIds.erase(oldPolynId_it);

        if(oldMonlD->inPolynIds.empty()) monlTable->erase(oldMonl_it);
    }
    return;
}

void GFPreprocessMgr::updateMonomialData(pMonomial oldMonomial, pGFPolynomial newMonls, 
        variableId polynId ){
    
    eraseMonomialData(oldMonomial, polynId);
    insertNewMonomialData(newMonls, polynId);
    return;
}

*/

void GFPreprocessMgr::eraseFromMonomialTable( const variableId & polynId ){
   
    
    auto polynData=pMgr->getGFPolynomialData(polynId);
    auto monlTable= pMgr->_monlTable;
    
    for(auto monl: *polynData->leadMonls){
        
        if(monl->size()<=1) continue;

        auto monl_it = monlTable->find(*monl);
        if(monl_it!=monlTable->end()){
            auto monlD=monl_it->second;
            auto polynId_it=monlD->inPolynIds.find(polynId);
    
            if(polynId_it!=monlD->inPolynIds.end()){
                monlD->inPolynIds.erase(polynId_it);
            }
            
            if(monlD->inPolynIds.empty()) monlTable->erase(monl_it);
        }
        for(auto var: *monl){
            //printf("Dvar %llu \n", var);
            auto varD=pMgr->getVariableData(var);
            if(varD==NULL) continue;
            assert(varD->fanoutSize!=0);
            varD->fanoutSize--;
        }
       
    }
   
    return;
}

void GFPreprocessMgr::addToMonomialTable( const variableId & polynId ){
    
    auto polynData=pMgr->getGFPolynomialData(polynId);
    
    auto monlTable= pMgr->_monlTable;
    
    for(auto monl: *polynData->leadMonls){

        if(monl->size()<=1) continue;

        auto monl_it = monlTable->find(*monl);
        if(monl_it==monlTable->end()){
            auto monlD=pMgr->allocateMonomialData();
            monlD->inPolynIds.insert(polynId);
            monlTable->insert(std::make_pair(*monl,monlD));
        }
        else{
            auto monlD=monl_it->second;
            monlD->inPolynIds.insert(polynId);
        }
       
        for(auto var: *monl){
            auto varD=pMgr->getVariableData(var);
            varD->fanoutSize++;
        }
    }
   
    return;
}

void GFPreprocessMgr::updateVariableData(pMonomial vars, const variableId & oldId, 
                                                    const variableId & newId ){
    for(auto var: *vars){
        auto var_it = pMgr->_varTable->find(var);
        if(var_it==pMgr->_varTable->end()){
            auto varD= pMgr->allocateVariableData();
            varD->inPolynIds->insert(newId);
            pMgr->_varTable->insert(std::make_pair(var,varD));
        }
        else{
            auto varD=var_it->second;
            auto oldId_it=varD->inPolynIds->find(oldId);
            if(oldId_it!=varD->inPolynIds->end()){ 
                varD->inPolynIds->erase(oldId_it);
            }
            varD->inPolynIds->insert(newId);
        }
    }
    return;
}

void GFPreprocessMgr::renewMonomialTable( ){
    
    auto monlTable= pMgr->_monlTable;
    auto varTable=pMgr->_varTable;
    monlTable->clear();
    varTable->clear();
    
    for(auto polyn_it: *pMgr->_ideal){
        auto id= polyn_it.first;
        auto leadMonls=polyn_it.second->leadMonls;
        auto inVars=polyn_it.second->inVars;
        inVars->clear();
        for(auto monl: *leadMonls){
            
            for(auto var :*monl){
                inVars->insert(var);
                auto var_it = varTable->find(var);
                if(var_it==varTable->end()){
                    auto varD= pMgr->allocateVariableData();
                    varD->inPolynIds->insert(id);
                    varD->fanoutSize++;
                    varTable->insert(std::make_pair(var,varD));
                }
                else{
                    auto varD=var_it->second;
                    varD->inPolynIds->insert(id);
                    varD->fanoutSize++;
                }
            }
            
            if(monl->size()<=1){
                continue;
            }

            auto monl_it = monlTable->find(*monl);
            if(monl_it==monlTable->end()){
                auto monlD=pMgr->allocateMonomialData();
                monlD->inPolynIds.insert(id);
                monlTable->insert(std::make_pair(*monl,monlD));
            }
            else{
                auto monlD=monl_it->second;
                monlD->inPolynIds.insert(id);
            }
        }
        pMgr->updateVariableSubstitutionSize(id, leadMonls->size());
    }
    
    return;
}

void GFPreprocessMgr::updateGFPolynomialData( const variableId &polynId,
                                                        pMonomial removedVars ){

    auto polynData=pMgr->getGFPolynomialData(polynId);
    
    if(fVerbose>0){
        printf("rewritten GFPolynomial: ");
        printGFPolynomial(stdout, polynData->leadMonls, polynId);
    }
    
    for(auto var: *removedVars){
       polynData->inVars->erase(var);
       auto newFanin=pMgr->getGFPolynomialFanins(var);
       polynData->inVars->insert(newFanin->begin(), newFanin->end());
       updateVariableData(newFanin, var, polynId);
    }
    
    if(polynData->inVars->size()<4&&  polynData->lut!=NULL){
        polynData->lut->clear();
        for (auto i=0; i<std::exp2(polynData->inVars->size()); i++){
            std::bitset<8> assign (i);
            if(simulateGFPolynomial(polynData, assign)){   
                polynData->lut->insert(assign);
            }
        }
    }
    else polynData->lut=NULL;
    
    auto varD=pMgr->getVariableData(polynId);
    varD->substSize=polynData->leadMonls->size();
    
    return;
}

void GFPreprocessMgr::rewriteGFIdeal( const bool & enableInference){
    if(_removedTable->empty()) return;
    
    for(auto remvTable_it: *_removedTable){
        auto rewritePolynId = remvTable_it.first;
        auto removedVars = remvTable_it.second;
        
        for(auto var: *removedVars){
            auto varD=pMgr->getVariableData(var);
        }
        
        abctime clk_sub = Abc_Clock();
        //updateSubstitutionTable(rewritePolynId , removedVars);
        timeAnalysis.substite+= Abc_Clock() - clk_sub;
        
        abctime clk_rewr = Abc_Clock();
        eraseFromMonomialTable(rewritePolynId);
        //rewriteOneGFPolynomial(rewritePolynId , removedVars);
        rewriteGFPolynomialWithInference(rewritePolynId , removedVars, enableInference);
        timeAnalysis.rewrite+= Abc_Clock() - clk_rewr;
        abctime clk_updateD = Abc_Clock();
        updateGFPolynomialData(rewritePolynId , removedVars);
        timeAnalysis.update+= Abc_Clock() - clk_updateD;
        abctime clk_infer = Abc_Clock();
        //applyInferenceRules(rewritePolynId);
        timeAnalysis.inference+= Abc_Clock() - clk_infer;
        
        addToMonomialTable(rewritePolynId);   
    }
    
    for(auto var: *_eliminatedVars){
        eraseFromMonomialTable(var);
        pMgr->_ideal->erase(var);
        pMgr->_varTable->erase(var);
    }
    
    renewMonomialTable( );
    _removedTable->clear();
    _substituteTable->clear();
    _eliminatedVars->clear();
    
    if(fVerbose>0)
    pMgr->printGFIdeal(stdout, fVerbose);
    return;
}


GFPreprocessMgr* preprocessGroebnerGFIdeal(GFPolynMgr * pMgr, const int &verbose){
    abctime clk = Abc_Clock();
    GFPreprocessMgr* prepMgr= new GFPreprocessMgr(pMgr);
    prepMgr->fVerbose=verbose;
 
   // printf("*** PrimaryInputs\n\n");
   // prepMgr->refineDepVarsPrimaryInputs();
   // prepMgr->rewriteGFIdeal();
    
    printf("*** Reveal XORs\n\n");
    prepMgr->refineDepVarsFanIns();
    prepMgr->rewriteGFIdeal(true);

    printf("*** GFPolynomials Factorization\n\n");
    prepMgr->replaceXNORsWithXORs();
   
    printf("*** XOR Rewritting\n\n");
    prepMgr->refineDepVarsXORs();
    prepMgr->rewriteGFIdeal(true);
      
    printf("*** Singular Rewritting\n\n");
    prepMgr->refineDepVarsSingular();
    prepMgr->rewriteGFIdeal(true);
    
        
    printf("*** Coupling Rewritting\n\n");
    prepMgr->refineDepVarsCoupling();
    prepMgr->rewriteGFIdeal(false);
    
    printf("*** Second Coupling Rewritting\n\n");
    prepMgr->refineDepVarsCoupling();
    prepMgr->rewriteGFIdeal(false);
    
    printf("*** Linear Rewritting\n\n");
    prepMgr->refineDepVarsLinear();
    prepMgr->rewriteGFIdeal(false);


    if(verbose>0)
        prepMgr->printMonomialTable();
    
    
    Abc_PrintTime( 1, "Preprocess Time", Abc_Clock() - clk );
    if(verbose>0){
        Abc_PrintTime( 1, "Substitution",  prepMgr->timeAnalysis.substite);
        Abc_PrintTime( 1, "Rewrite",  prepMgr->timeAnalysis.rewrite);
        Abc_PrintTime( 1, "update",  prepMgr->timeAnalysis.update);
        Abc_PrintTime( 1, "inference",  prepMgr->timeAnalysis.inference);
        Abc_PrintTime( 1, "shared",  prepMgr->timeAnalysis.shared);
        Abc_PrintTime( 1, "replace",  prepMgr->timeAnalysis.replace);
    }
    return prepMgr;
}