/**CPPFile****************************************************************

  FileName    [IMTSolver.cpp]

  SystemName  [SC4AV.]

  PackageName [prover.]

  Synopsis    [GFIdeal Membership Testing.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/

#include "GFprover/GFIMT.h"


GFIMTMgr::GFIMTMgr(GFPolynMgr * pMgr_new ): GFPreprocessMgr(pMgr_new) {
     pMgr=pMgr_new;
}

GFIMTMgr::~GFIMTMgr(){
    
}




variableId GFIMTMgr::selectEliminatedVariable(pGFPolynomial polyn){
    
    assert(!polyn->empty());
    pMonomial depVars=pMgr->allocateMonomial();
    for(auto monl: *polyn){
        for(auto var: *monl){
            if(!pMgr->isPrimaryInput(var)){
                auto varD=pMgr->getVariableData(var);
                if(isInPMonomial(depVars,var)){
                    varD->fanoutSize++;
                }
                else{
                    depVars->insert(var);
                    varD->fanoutSize=1;
                }
            }
        }
    }
    //auto lastMonl=*std::prev(polyn->end());
    //auto lastMonl=lastMonl->second;
    auto sortedSet = (pSortedSizeTuples) new sortedSizeTuples;
    for(auto var: *depVars){
        auto varD=pMgr->getVariableData(var);
        auto totalSize=varD->fanoutSize*varD->substSize;
        sizeTuple newTuple= sizeTuple(var, totalSize);
        sortedSet->insert(newTuple);
    }
    auto firstElement_it=sortedSet->begin();
     
    return firstElement_it->first;
}

pGFPolynomial GFIMTMgr::GFNormalForm(pGFPolynomial specPolyn,bool & giveup){
    
    if(specPolyn->empty()) return specPolyn;
    pGFPolynomial newRemainder=pMgr->allocateGFPolynomial();
    pGFPolynomial product;
    auto currRemainder= specPolyn;   

    auto lastMonl_it=std::prev(specPolyn->end());
    auto lastMonl=*lastMonl_it;

    int i=0;
    while(!pMgr->isPrimaryInputMonomial(lastMonl)){
        
        auto singleMonl_it=lastMonl_it;
        auto singleMonl=*singleMonl_it;
        
        while(singleMonl_it!=currRemainder->begin()&&singleMonl->size()!=1){
            
            singleMonl_it=std::prev(singleMonl_it);
            singleMonl=*singleMonl_it;
        }
        
        if(singleMonl->size()==1&& !pMgr->isPrimaryInputMonomial(singleMonl)){
            auto newMonl= pMgr->allocateMonomial();

            auto subsPolyn=pMgr->getGFPolynomialLeadMonls(*singleMonl->begin());
            product=multiplyGFPolynWithMonl(newMonl, subsPolyn);

            assert(product!=NULL);
            currRemainder->erase(singleMonl_it);
            pMgr->insertPolyn(currRemainder, product);
            
            if(currRemainder->empty()) return currRemainder;
            lastMonl_it=std::prev(currRemainder->end());       
            lastMonl=(*lastMonl_it);
            continue;
        }

        if(fVerbose>0){
            pMgr->printGFPolynomial(stdout, currRemainder);
            printf("\n\n");
        }
        auto elimVar=selectEliminatedVariable(currRemainder);

        auto subsPolyn=pMgr->getGFPolynomialLeadMonls(elimVar);

        for(auto currMonl: *currRemainder){

            auto newMonl= pMgr->allocateMonomial();
            newMonl->insert(currMonl->begin(), currMonl->end());

            if(currMonl->empty()){ 
                pMgr->insertMonl(newRemainder,newMonl); continue; }


            if(isInPMonomial(currMonl, elimVar)){
                 newMonl->erase(elimVar);
            }
            else{   
                pMgr->insertMonl(newRemainder,newMonl); 
                continue; 
            }

            product=multiplyGFPolynWithMonl(newMonl, subsPolyn);
            

            assert(product!=NULL);

            pMgr->insertPolyn(newRemainder, product);
        }

        currRemainder->swap(*newRemainder);
        i++;
        newRemainder->clear();

       
        if(currRemainder->empty()) return currRemainder;

        
        lastMonl_it=std::prev(currRemainder->end());       
        lastMonl=(*lastMonl_it);
        
        while(pMgr->isPrimaryInputMonomial(lastMonl)
                &&lastMonl_it!=currRemainder->begin()){           
            lastMonl_it=std::prev(lastMonl_it);       
            lastMonl=(*lastMonl_it);
        }

        if(currRemainder->size()>
                pMgr->_inputVars->size()*pMgr->_inputVars->size()){
            fprintf(stdout, "*** Cannot Find a Solution!");
            giveup=true;
            break;
        }
    }
    return currRemainder;
}



bool GFIMTMgr::testGFMultiplierMembership(){
    
    pGFPolynomial specPolyn=pMgr->_specPolyn;

    fprintf(stdout, "*** SPECGFPolynomial:\n");
    pMgr->printGFPolynomial(stdout, specPolyn);
 
    bool giveUp=false;
    auto remainder=GFNormalForm(specPolyn, giveUp);
    
    if(fVerbose>0)
        pMgr->printGFPolynomial(stdout, remainder);
    
    if (giveUp) return !giveUp;
    
    if(remainder->empty()){
        fprintf(stdout, "*** SPECGFPolynomial is consistent with Algebraic Model!\n\n");
    }
    else if(remainder->size()==1&& (*remainder->begin())->empty()){
        fprintf(stdout, "*** SPECGFPolynomial is consistent with Algebraic Model!\n\n");
    }
    else{
        fprintf(stdout, "*** Algebraic Model Violates SPECGFPolynomial!\n\n");
        fprintf(stdout, "*** Remainder:\n");
        pMgr->printGFPolynomial(stdout, remainder);
        fprintf(stdout, "\n\n");
    }
    
    return !giveUp;
}

bool IMTGFSolver(GFPolynMgr *_procMgr, int fVerbose){
    abctime clk = Abc_Clock();
 
    GFIMTMgr * _imtMgr=  new GFIMTMgr(_procMgr);   
     _imtMgr->fVerbose=fVerbose;
 
    bool status=_imtMgr->testGFMultiplierMembership();
     
    Abc_PrintTime( 1, "IMTTime", Abc_Clock() - clk );
   // delete _imtMgr;
    return status;
}
