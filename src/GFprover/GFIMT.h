/**HFile****************************************************************

  FileName    [IMT.h]

  SystemName  [SC4AV.]

  PackageName [Solver.]

  Synopsis    [GFIdeal Membership Testing.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/
#include"GFprover/GFpreprocess.h"

#ifndef GFIMT_H
#define GFIMT_H

struct WordGFPolynomial{
    pGFPolynomial inputPart;
    pGFPolynomial outputPart;

};


class GFIMTMgr: public GFPreprocessMgr{
public:
    GFIMTMgr(GFPolynMgr * );
    ~GFIMTMgr();
    bool testGFMultiplierMembership();

private:
    GFPolynMgr * pMgr;

    variableId selectEliminatedVariable( pGFPolynomial);
    pGFPolynomial GFNormalForm(pGFPolynomial, bool& );
    
    inline bool isPrimaryInputsMonomial(pMonomial monl){
        
        auto primaryInputs=pMgr->_inputVars;
        return std::includes(primaryInputs->begin(), primaryInputs->end(),
                    monl->begin(), monl->end()); 
    };
    
    inline WordGFPolynomial allocateWordGFPolynomial(){
        WordGFPolynomial wordPolyn;
        wordPolyn.outputPart=pMgr->allocateGFPolynomial();
        wordPolyn.inputPart=pMgr->allocateGFPolynomial();
        return wordPolyn;
    };
    
    bool getSharedTerms(pGFPolynomial ,GFPolynomial::iterator&,
                                    pGFPolynomial,GFPolynomial::iterator& );
    

    

};

bool IMTGFSolver(GFPolynMgr *, int);
#endif 

