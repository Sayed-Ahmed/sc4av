
/**HFile****************************************************************

  FileName    [GFpolyn.h]

  SystemName  [SC4Arith.]

  PackageName [Solver.]

  Synopsis    [Data Structure of GF2 GFPolynomial.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/

#ifndef GFPOLYN_H
#define GFPOLYN_H


#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <set>
#include <queue>
#include <vector>
#include <string>
#include <assert.h>
#include <stdarg.h>
#include <math.h>
#include <algorithm>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>
#include <boost/range/algorithm.hpp>
#include <list>
#include "INTprover/polyn.h"


 



struct monomialCompare:public Monomial{
    bool operator()(const pMonomial m1, const pMonomial m2) const{
            return *m1<*m2;
    }
};

typedef std::set<pMonomial, monomialCompare> GFPolynomial;
typedef std::shared_ptr<GFPolynomial> pGFPolynomial;

struct GFPolynomialData{
	pGFPolynomial leadMonls;
	pMonomial inVars;
        Monomial primaryInVars;
	pTruthTable lut;
};



typedef std::shared_ptr<GFPolynomialData> pGFPolynomialData;


typedef std::map<variableId, pGFPolynomialData> GFIdeal;
typedef std::shared_ptr<GFIdeal> pGFIdeal;


class GFPolynMgr{

public:
        GFPolynMgr();
        ~GFPolynMgr();
        
        FILE * logFile;
	pVariableTable  _varTable;
	pMonomialTable	_monlTable;
	pGFIdeal  _ideal;
	pMonomial _depVars;
        pMonomial  _inputVars;
        pMonomial _outputVars;
        pIdTable _polynId2Aig;
        pGFPolynomial _specPolyn;
        
        pMonomial allocateMonomial();
        pMonomial allocateMonomial(const variableId &);
        pMonomial allocateMonomial(const variableId&, const variableId&);
       

        
        inline pGFPolynomial allocateGFPolynomial(){
            return (pGFPolynomial) new GFPolynomial;
        };
        
        
        inline pGFPolynomialData allocateGFPolynomialData(){
            pGFPolynomialData polynD= (pGFPolynomialData) new GFPolynomialData;
            polynD->leadMonls= (pGFPolynomial) new GFPolynomial;
            polynD->inVars= allocateMonomial();
            polynD->lut= (pTruthTable) new TruthTable;
            return polynD;
        };
        
        inline pMonomialData allocateMonomialData(){
            pMonomialData monlD= (pMonomialData) new MonomialData;
            return monlD;
        };
        
        inline pVariableData allocateVariableData(){
            pVariableData varD= (pVariableData) new VariableData;
            varD->inPolynIds=allocateMonomial();
            return varD;
        };
    
        inline pGFIdeal allocateGFIdeal(){
            return (pGFIdeal) new GFIdeal;
        };
        
        inline bool isNotInGFIdeal( const variableId& key ){
            return (_ideal->find(key)==_ideal->end());
        };
        
        inline bool isNotInIdTable( pIdTable t, const variableId& key ){
            return (t->find(key)==t->end());
        };
        
        inline bool isPrimaryInput(const variableId& var){
            
            return (_inputVars->find(var)!= _inputVars->end() );
        };
        
        inline bool isPrimaryInputMonomial( pMonomial m){
            
            Monomial differenceVars;
            std::set_difference(m->begin(), m->end(),
                            _inputVars->begin(), _inputVars->end(),
                            std::inserter(differenceVars, differenceVars.begin()));
            if(differenceVars.empty()) return true;
            return false;
        };
        
        inline bool isPrimaryOutput(const variableId & var){
            
            return (_outputVars->find(var)!= _outputVars->end() );
        };
        
        inline pMonomial getGFPolynomialFanins(const variableId& id){
            auto id_it=_ideal->find(id);
            assert(id_it!=_ideal->end());
            return id_it->second->inVars;
        }
        
        inline pGFPolynomial getGFPolynomialLeadMonls(const variableId& id){
            auto id_it=_ideal->find(id);
            assert(id_it!=_ideal->end());
            return id_it->second->leadMonls;
        }
        
        inline pGFPolynomialData getGFPolynomialData(const variableId& id){
            auto id_it=_ideal->find(id);
            assert(id_it!=_ideal->end());
            return id_it->second;
        }
        
        inline pMonomial getGFPolynomialChilds(const variableId& Id){
            assert(!_varTable->empty());
            auto it= _varTable->find(Id);
            if(it==_varTable->end()){
                assert(_outputVars->find(Id)!=_outputVars->end());
                return NULL;
            }
            return it->second->inPolynIds;
        }
        
        inline pVariableData getVariableData(const variableId& Id){
            assert(!_varTable->empty());
            auto it= _varTable->find(Id);
            if(it==_varTable->end()){
                assert(_outputVars->find(Id)!=_outputVars->end());
                return NULL;
            }
            return it->second;
        }
        
        inline pTruthTable getGFPolynomialLUT(const variableId& Id, size_t& numInputs){
            auto id_it=_ideal->find(Id);
            assert(id_it!=_ideal->end());
            numInputs=id_it->second->inVars->size();
            return id_it->second->lut;
        } 
        
        inline void insertMonl(pGFPolynomial polyn, pMonomial m){
            auto it=polyn->find(m);
            if(it!=polyn->end()){
                polyn->erase(it);
            }
            else{
                polyn->insert(m);
            }
            return;
        }
        
        inline void insertPolyn(pGFPolynomial p1, pGFPolynomial p2){
            
            for(auto m: *p2){
                insertMonl(p1, m);
            }
            return;
        }
        
        void printGFIdeal(FILE *&, const int& fVerbose);
        void printGFPolynomial(FILE *&, pGFPolynomial);
        void updateMonomialVariableTables(pMonomial, const variableId&);
        void addPolyn2GFIdeal(const variableId &, pGFPolynomialData );
        void updatePrimaryInVariables(pGFPolynomialData );
        void updateVariableSubstitutionSize(const variableId&, const size_t&);
        void printMonomial(FILE*&,  pMonomial );
        void openLogFile(char* );
        void closeLogFile();
private:
        void printPrimaryInputs(FILE* &, Monomial* );
        
};





#endif
