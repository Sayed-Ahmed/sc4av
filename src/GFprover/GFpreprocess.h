
/**HFile****************************************************************

  FileName    [polyn.h]

  SystemName  [SC4Arith.]

  PackageName [Solver.]

  Synopsis    [Preprocess Set of GFPolynomials.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/
#include"GFprover/GFpolyn.h"
#include"INTprover/preprocess.h"
#include"misc/int.h"
#include "misc/util/abc_global.h"

#ifndef GFPREPROCESS_H
#define GFPREPROCESS_H

struct monomialSortedCompare:public Monomial{
    bool operator()(const pMonomial m1, const pMonomial m2) const{
         if(m1->size()==m2->size()){
            return *m1<*m2;
        }
        else{
            return m1->size()<m2->size();
        }
    }
};

typedef std::set<pMonomial, monomialSortedCompare> sortedGFPolynomial;
typedef std::shared_ptr<sortedGFPolynomial> pSortedGFPolynomial;
typedef std::map<Monomial, pGFPolynomial> GFSubstitutionTable;
typedef std::shared_ptr<GFSubstitutionTable> pGFSubstitutionTable;



class GFPreprocessMgr{
    public:
        GFPreprocessMgr( GFPolynMgr *);
        ~GFPreprocessMgr();
        void refineDepVarsPrimaryInputs();
        void refineDepVarsFanIns();
        void refineDepVarsXORs();
        void refineDepVarsSingular();
        void refineDepVarsLinear();
        void refineDepVarsCoupling();
        void replaceXNORsWithXORs();
        void rewriteGFIdeal(const bool &);
        void renewMonomialTable( );
        void printMonomialTable( );
        pGFPolynomial multiplyGFPolynWithMonl(pMonomial, pGFPolynomial);
        pSortedSizeTuples buildSortedSizedSet(pMonomial);
                
        inline Monomial getIntersectedVariables(pMonomial m1, pMonomial m2){
            Monomial intersectVars;
            std::set_intersection(m1->begin(), m1->end(),
                            m2->begin(), m2->end(),
                            std::inserter(intersectVars, intersectVars.begin()));
            return intersectVars;
        }
        
        inline Monomial getDifferenceVariables(pMonomial m1, 
                                                               pMonomial m2){
            Monomial differenceVars;
            std::set_difference(m1->begin(), m1->end(),
                            m2->begin(), m2->end(),
                            std::inserter(differenceVars, differenceVars.begin()));
            return differenceVars;
        }
        
        inline bool isSubmonomial(pMonomial m1, pMonomial m2){
        
            return std::includes(m2->begin(), m2->end(),
                    m1->begin(), m1->end());    // m1 is subset of m2
        };
        
        inline bool isNotInMonomial( Monomial* m, const variableId & key ){
            return (m->find(key)==m->end());
        };
        
        inline bool isInMonomial( Monomial* m, const variableId & key ){
            return (m->find(key)!=m->end());
        };
        inline bool isInPMonomial( pMonomial m, const variableId & key ){
            return (m->find(key)!=m->end());
        };
        
        inline void markEliminatedVariables(const variableId & v){
            if(pMgr->isPrimaryOutput(v)||pMgr->isPrimaryInput(v)) return;
            auto varD=pMgr->getVariableData(v);
            auto maxSize= pMgr->_inputVars->size()*pMgr->_inputVars->size();
            if(varD->fanoutSize*varD->substSize> maxSize) return;
            updateRemovedTable(v);
            pMgr->_depVars->erase(v);
            _eliminatedVars->insert(v);
            if(fVerbose>0)
                printf("  erasedvar %llu\n", v);
        };
        
        inline pMonomial copytoNewMonl(const Monomial & monl){
            auto newMonl=pMgr->allocateMonomial();
            newMonl->insert(monl.begin(), monl.end());
            return newMonl;
        }
        
        inline pSortedGFPolynomial copytoSortedGFPolynomial(pGFPolynomial polyn){
            auto sortedPolyn=  (pSortedGFPolynomial) new sortedGFPolynomial ;
            sortedPolyn->insert(polyn->begin(), polyn->end());
            return sortedPolyn;
        }
        
        pSetsTable _prInputsTable;
        pSetsTable _fanInsTable;
        pRewriteTable _removedTable;
        pGFSubstitutionTable _substituteTable;
        pMonomial _eliminatedVars;
        int fVerbose=0;
                
        
        struct TimeAnalysis {
            abctime rewrite=0;
            abctime inference=0; 
            abctime substite=0;
            abctime shared=0;
            abctime replace=0;
            abctime coupling=0;
            abctime update=0;
        } timeAnalysis;
        
        void printSet(pMonomial);
    private:
        GFPolynMgr * pMgr; 
        pMonomial copyNewMonl(pMonomial );
        
        void printLUT(pTruthTable );
        void printSet(Monomial *);
        void printSet(pVariableSet);
        void printMonomial(Monomial*);
        void printSubstitutionGFPolynomial(FILE*&, pGFPolynomial, Monomial* );
        void printGFPolynomial(FILE*&, pGFPolynomial, const variableId& );
        void updatePrimaryInputsTable();
        void updateFaninsTable();
        void updateRemovedTable(const variableId &);
        void updateGFSubstitutionTable( const variableId &, pMonomial);
        void rewriteOneGFPolynomial( const variableId &, pMonomial);
        void rewriteGFPolynomialWithInference(const variableId &,pMonomial, const bool & );
        pGFPolynomial invertVariableWithinGFPolynomial(pGFPolynomial, const variableId &);
        void updateGFPolynomialData(const variableId &,pMonomial);

        void updateVariableData(pMonomial, const variableId &, const variableId & );
        void eraseFromMonomialTable(const variableId &);
        void addToMonomialTable( const variableId &);

        void addSimilarMonomials(pGFPolynomial);
        pGFPolynomial multiplyTwoGFPolynomials(pGFPolynomial , pGFPolynomial);
        void addTwoPolyns();
        MonomialsSet getCombinations(pMonomial);
        void insertSubstitutionGFPolynomial(GFSubstitutionTable::iterator, const Monomial &);
        bool simulateGFPolynomial(pGFPolynomialData, __Byte);
        bool isXORGFPolynomial(const variableId &);
        bool isXNORGFPolynomial(const variableId &);
        void applyInferenceRules(const variableId &);
        void applyInferenceRules(pGFPolynomial);
        void replaceWithSimplerMonomials(pGFPolynomial, pGFSubstitutionTable  );
        pGFPolynomial inferSimplerMonomials(pMonomial, pMonomial);
        pGFSubstitutionTable getSharedSupportingVariables( pMonomial);
        SetsTable  groupVariablesBasedFanin( pMonomial);
        void insertSimplifiedGFPolynomial (const Monomial &, pMonomial,  
                                                pGFSubstitutionTable );

        
        inline pTruthTable twoInputsXOR(size_t numbits){

            auto lut= (pTruthTable) new TruthTable;
            if(numbits==2){
                __Byte assign1 (std::string("00000001"));
                __Byte assign2 (std::string("00000010"));
                lut->insert(assign1);
                lut->insert(assign2);
                return lut;
            }
            else if(numbits==3){
                __Byte assign1 (std::string("00000110"));
                __Byte assign2 (std::string("00000011"));
                __Byte assign3 (std::string("00000100"));
                __Byte assign4 (std::string("00000101"));
                lut->insert(assign1);
                lut->insert(assign2);
                lut->insert(assign3);
                lut->insert(assign4);
                return lut;
            }
           /* else if(numbits==4){
                Byte assign1 (std::string("00000011"));
                Byte assign2 (std::string("00000111"));
                Byte assign3 (std::string("00001011"));
                Byte assign4 (std::string("00001100"));
                Byte assign5 (std::string("00001101"));
                Byte assign6 (std::string("00001110"));
                lut->insert(assign1);
                lut->insert(assign2);
                lut->insert(assign3);
                lut->insert(assign4);
                lut->insert(assign5);
                lut->insert(assign6);
                return lut;
            }*/
            else{
                return NULL;
            }
        };
        
        inline pTruthTable twoInputsXNOR(size_t numbits){

            auto lut= (pTruthTable) new TruthTable;
            if(numbits==2){
                __Byte assign1 (std::string("00000000"));
                __Byte assign2 (std::string("00000011"));
                lut->insert(assign1);
                lut->insert(assign2);
                return lut;
            }
            else if(numbits==3){
                __Byte assign1 (std::string("00000000"));
                __Byte assign2 (std::string("00000010"));
                __Byte assign3 (std::string("00000001"));
                __Byte assign4 (std::string("00000111"));
                lut->insert(assign1);
                lut->insert(assign2);
                lut->insert(assign3);
                lut->insert(assign4);
                return lut;
            }
           /* else if(numbits==4){
                Byte assign1 (std::string("00000000"));
                Byte assign2 (std::string("00000100"));
                Byte assign3 (std::string("00001000"));
                Byte assign4 (std::string("00000001"));
                Byte assign5 (std::string("00000101"));
                Byte assign6 (std::string("00001001"));
                Byte assign7 (std::string("00000010"));
                Byte assign8 (std::string("00000110"));
                Byte assign9 (std::string("00001010"));
                Byte assign10 (std::string("00001111"));
                lut->insert(assign1);
                lut->insert(assign2);
                lut->insert(assign3);
                lut->insert(assign4);
                lut->insert(assign5);
                lut->insert(assign6);
                lut->insert(assign7);
                lut->insert(assign8);
                lut->insert(assign9);
                lut->insert(assign10);
                return lut;
            }*/
            else{
                return NULL;
            }
        };

        
};

GFPreprocessMgr* preprocessGroebnerGFIdeal(GFPolynMgr *, const int & );

#endif /* PREPROCESS_H */

