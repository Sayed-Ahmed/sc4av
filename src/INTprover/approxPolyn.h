/**HFile****************************************************************

  FileName    [Approximate.h]

  SystemName  [SC4AV.]

  PackageName [Solver.]

  Synopsis    [Ideal Membership Testing.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/
#include"INTprover/preprocess.h"
//#include "PBSAT/poly2PBC.h"

#ifndef Approximate_H
#define Approximate_H


typedef std::map<Monomial, pMonomialData> SortedMonomialTable;
typedef std::shared_ptr<SortedMonomialTable> pSortedMonomialTable;
typedef std::map<variableId, bool> Model;
typedef std::shared_ptr<Model> pModel;

struct ConstraintPolynomial{
	pEXPPolynomial leadTerms;
	int inEqu;
};

typedef std::shared_ptr<ConstraintPolynomial> pConstraintPolynomial;
typedef std::map<variableId, pConstraintPolynomial> ConstraintIdeal;
typedef std::shared_ptr<ConstraintIdeal> pConstraintIdeal;

class SATMgr: public PreprocessMgr{
public:
    SATMgr(PolynMgr * );
    ~SATMgr();
    void approximateIdeal();
    void verifyIntMultiplierBySAT();
    pIdeal linearIdeal;
    pIdeal linearEnquiryIdeal;
    pIdeal nonlinearIdeal;
    pConstraintIdeal _constraintsIdeal;
    pMonomial _inputApproxVars;
    pVariableTable  _varSATTable;
    pSortedMonomialTable  _monlSATTable;
    pSortedMonomialTable  _monlNonlinearTable;
    pModel satModel;
    
    inline variableId name2index(std:: string x){
        auto num= x.substr(x.find("x") + 1);
        return std::stoi(num);
    }
    
    void printSatModel(FILE *&out);

private:
    variableId LargestId;
    void buildApproxIdeals();
    void reduceIdealByGaussian();
    void getMostReducedIdealByGaussian();
    void reduceSetofPolynomials(pMonomial, Monomial* , pMonomial);
    void reduceTwoPolynomials(pPolynomial, variableId, 
                                            pPolynomial, variableId, pMonomial );
    void relinearization();
    void addNonlinearPolynomial(pMonomial);
    void linearizeGaussianPolynomial(variableId, pMonomial,variableId );
    void applyInferenceThroughSAT();
    void verifySpecificationThroughSAT();
    void solveConstraint();
    void purifyOriginalIdeal(pMonomial);
    void removeRedundantTerm(const variableId& var);
    void addOnOffConstraint(const variableId&, const bool&);
    void addSpecificationConstraint();
    void addXORConstraint(const variableId&, const variableId&,  pEXPPolynomial);
    void relinearizConstraints();
    void assignOnSetValue(const variableId&, const variableId&);
    void linearize(const variableId&, pMonomial);
    pPolynomial getGaussianPolynomial(const variableId& id);
    Coefficient getGaussianCoeff(pPolynomial, pMonomial);
    void updateVarMonlSATTables(pPolynomial, const variableId& );
    variableId updateMonlNonlinearTable(pMonomial );
    void updateMonlCandidateTable(pMonomialTable table);
    void updateVarCandidateTable(pVariableTable);
    void updateMonlNonlinearTable(pMonomial, const variableId&);
    void renewSATTables();
    void printNonlinearIdeal(FILE *&out, const int& fVerbose);
    void printApproximatedIdeal(FILE *&out, const int& fVerbose);
    void printMonomialSATTable(pSortedMonomialTable);
   
    inline bool existInGaussianTable(const variableId& id){
        auto id_it=linearIdeal->find(id);
        return (id_it!=linearIdeal->end());
    }
    
    inline variableId getGaussianPolynomialId( pMonomial monl){
        for(auto var: *monl){
            auto id_it=linearIdeal->find(var);
            if(id_it!=linearIdeal->end())return var;
        }
        
        return 0;
    }
    
    inline void insertGaussianPolynomial(const variableId& id, pPolynomial poly){
        
        auto id_it=linearIdeal->find(id);
        if(id_it==linearIdeal->end()){
            auto polyData=pMgr->allocatePolynomialData();
            polyData->leadTerms=poly;
            linearIdeal->insert(std::make_pair(id,polyData) );
        }
        else{
            assert(0);
        }
    }
    
    
    inline pPolynomial getGaussianPolynomialLeadTerms(const variableId& id){
            auto id_it=linearIdeal->find(id);
            assert(id_it!=linearIdeal->end());
            return id_it->second->leadTerms;
    }
    
    inline void eraseGaussianPolynomial(const variableId& id){
            auto id_it=linearIdeal->find(id);
            assert(id_it!=linearIdeal->end());
            linearIdeal->erase(id_it);
            return;
    }
    
    inline Coefficient absCoeff(const Coefficient c){
        if(c<0)  return c*-1;
        else return c;
    }
  
    
    inline pEXPTerm createEXPTermConst(int c){
            auto monl=pMgr->allocateMonomial();
            EXPCoefficient coeff;
            mpz_set_si(coeff.get_mpz_t(),c);
            auto term=pMgr->allocateEXPTerm(coeff,monl); 
            return term;
    } 
    inline pEXPTerm createEXPTerm(int c, const variableId& id){
            auto monl=pMgr->allocateMonomial(id);
            EXPCoefficient coeff;
            mpz_set_si(coeff.get_mpz_t(),c);
            auto term=pMgr->allocateEXPTerm(coeff,monl); 
            return term;
    }
    
    inline pEXPTerm createEXPTerm2Vars(int c, const variableId& id1, 
                                                         const variableId& id2){
            auto monl=pMgr->allocateMonomial(id1,id2);
            EXPCoefficient coeff;
            mpz_set_si(coeff.get_mpz_t(),c);
            auto term=pMgr->allocateEXPTerm(coeff,monl); 
            return term;
    }
    
    
    inline pConstraintPolynomial allocateConstraintPolynomial(){
            pConstraintPolynomial polynD= (pConstraintPolynomial) new ConstraintPolynomial;
            polynD->leadTerms= (pEXPPolynomial) new EXPPolynomial;
            return polynD;
    }

};

SATMgr * approximate(PolynMgr *, int);
int SATPSolver(PolynMgr *, int );
int solvePBC(SATMgr *, int);
#endif /* Approximate_H */

