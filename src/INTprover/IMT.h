/**HFile****************************************************************

  FileName    [IMT.h]

  SystemName  [SC4AV.]

  PackageName [Solver.]

  Synopsis    [Ideal Membership Testing.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/
#include"INTprover/preprocess.h"

#ifndef IMT_H
#define IMT_H

struct WordPolynomial{
    pPolynomial inputPart;
    pPolynomial outputPart;

};


class IMTMgr: public PreprocessMgr{
public:
    IMTMgr(PolynMgr * );
    ~IMTMgr();
    bool testIntMultiplierMembership();
    void getMultiplierByGaussian();
    pIdeal gaussianTable;

private:
    PolynMgr * pMgr;
    pEXPPolynomial NormalForm(pEXPPolynomial, const EXPCoefficient, bool&);
    pEXPPolynomial multiplyPolynWithEXPMonl(pEXPTerm, pPolynomial );
    variableId selectEliminatedVariable( pEXPPolynomial);
    void moduloCoefficient(pEXPTerm, const EXPCoefficient);
    EXPPolynomial::iterator getNonPrimaryInputsMonomial(pEXPPolynomial, pMonomial);
    void applyInferenceRulesEXP(pEXPPolynomial );
    void replaceWithSimplerMonomialsEXP(pEXPPolynomial,pSubstitutionTable);
    
    inline WordPolynomial allocateWordPolynomial(){
        WordPolynomial wordPolyn;
        wordPolyn.outputPart=pMgr->allocatePolynomial();
        wordPolyn.inputPart=pMgr->allocatePolynomial();
        return wordPolyn;
    };
    
    bool getSharedTerms(pPolynomial ,Polynomial::iterator&,
                                    pPolynomial,Polynomial::iterator& );
    
    inline variableId getGaussianPolynomialId( pMonomial monl){
        for(auto var: *monl){
            auto id_it=gaussianTable->find(var);
            if(id_it!=gaussianTable->end())return var;
        }
        
        return 0;
    }
    
    inline void buildGaussianTable(){
        
        gaussianTable->insert(pMgr->_ideal->begin(), pMgr->_ideal->end());
        return;
    }
    
    inline pPolynomial getGaussianPolynomialLeadTerms(const variableId& id){
            auto id_it=gaussianTable->find(id);
            assert(id_it!=gaussianTable->end());
            return id_it->second->leadTerms;
    }
    
    inline void eraseGaussianPolynomial(const variableId& id){
            auto id_it=gaussianTable->find(id);
            assert(id_it!=gaussianTable->end());
            gaussianTable->erase(id_it);
            return;
    }
    
    inline bool checkSingleCondition(pMonomial m){
        if(m->size()>=1){
            auto lastVar=*std::prev(m->end());
            if(pMgr->isPrimaryInput(lastVar)) return false;
        }
        
        if(m->size()==1) return true;
        else if(m->size()>1){
            auto var=*std::prev(std::prev(m->end()));
            if(pMgr->isPrimaryInput(var)) return true;
        }
        return false;
    }


};

bool IMTSolver(PolynMgr *, int);
#endif /* IMT_H */

