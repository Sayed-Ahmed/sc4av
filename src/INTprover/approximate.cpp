/**CPPFile****************************************************************

  FileName    [ApproximateSolver.cpp]

  SystemName  [SC4AV.]

  PackageName [prover.]

  Synopsis    [Ideal Membership Testing.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/

#include "INTprover/approxPolyn.h"


SATMgr::SATMgr(PolynMgr * pMgr_new ): PreprocessMgr(pMgr_new) {
    pMgr=pMgr_new;
    linearIdeal= (pIdeal) new Ideal;
    nonlinearIdeal= (pIdeal) new Ideal;
    linearEnquiryIdeal=(pIdeal) new Ideal;
    _constraintsIdeal= (pConstraintIdeal) new ConstraintIdeal;
    _inputApproxVars= (pMonomial) new Monomial;
    _varSATTable= (pVariableTable) new VariableTable;
    _monlSATTable= (pSortedMonomialTable) new SortedMonomialTable;
    _monlNonlinearTable= (pSortedMonomialTable) new SortedMonomialTable;
    satModel= (pModel) new Model;
    LargestId=*std::prev(pMgr->_outputVars->end());
}

SATMgr::~SATMgr(){
    
}


void SATMgr::printSatModel(FILE *&out){
    
    if(satModel->empty()){
        fprintf(out, "***s No Satisfiable Model, SPECPolynomial is Consistent with Algebraic Model!\n\n");
        return;
    }
    fprintf(out, "***s Algebraic Model Violates SPECPolynomial!\nSatisfiable Model:\n");
    for (auto var_it: *satModel){
        fprintf(out, " %sv_%d",  var_it.second?"":"~", var_it.first);
    }
    fprintf(out, "\n\n");
    return;
}

void SATMgr::printApproximatedIdeal(FILE *&out, const int& fVerbose)
{
    if(linearIdeal->empty()){
        fprintf(out, "Approximated ideal is empty\n");
        return;
    }
    for ( auto& it : *linearIdeal){
        pMgr->printPolynomial(out, it.second->leadTerms);
        fprintf(out," = 0");
        fprintf(out, "\n\n");
    }
    return;
}

void SATMgr::printNonlinearIdeal(FILE *&out, const int& fVerbose)
{
    if(nonlinearIdeal->empty()){
        fprintf(out, "Nonlinear Ideal is empty\n");
        return;
    }
    for ( auto& it : *nonlinearIdeal){
        fprintf(out,"v_%llu = ", it.first);
        pMgr->printPolynomial(out, it.second->leadTerms);
        fprintf(out, "\n\n");
    }
    return;
}
void SATMgr::printMonomialSATTable(pSortedMonomialTable monlTable){
    for (auto monl_it: *monlTable){
        auto inPolynIds=monl_it.second->inPolynIds;
        auto monl=monl_it.first;         
        printSet(&monl);
        for(auto id: inPolynIds)
            fprintf(stdout," id %llu", id);
        fprintf(stdout,"\n");
    }
}

    void SATMgr::renewSATTables(){
        _monlSATTable->clear();
        _varSATTable->clear();
        for(auto polyn_it: *linearIdeal){
            updateVarMonlSATTables(polyn_it.second->leadTerms, polyn_it.first);
        }
    }
    
    void SATMgr::updateVarCandidateTable(pVariableTable table){
     _varSATTable->clear();
     _monlSATTable->clear();
     for(auto polyn_it: *linearIdeal){
         updateVarMonlSATTables(polyn_it.second->leadTerms, polyn_it.first);
     }
     table->clear();
     for(auto varTable_it= _varSATTable->begin(); 
             varTable_it!= _varSATTable->end(); 
         varTable_it=std::next(varTable_it)){
         auto polynIds= varTable_it->second->inPolynIds;
         if(polynIds->size()>=2) 
             table->insert(varTable_it, std::next(varTable_it));
     }
    } 

void SATMgr::updateMonlCandidateTable(pMonomialTable table){
    _monlSATTable->clear();
    _varSATTable->clear();
    table->clear();
    for(auto polyn_it: *linearIdeal){
        updateVarMonlSATTables(polyn_it.second->leadTerms, polyn_it.first);
    }
    for(auto monlTable_it= _monlSATTable->begin(); 
            monlTable_it!= _monlSATTable->end(); 
        monlTable_it=std::next(monlTable_it)){
        auto polynIds= monlTable_it->second->inPolynIds;
        if(polynIds.size()>=2) 
            table->insert(monlTable_it, std::next(monlTable_it));
    }
}

void SATMgr::buildApproxIdeals(){

    for(auto polyn_it: *pMgr->_ideal){
        auto polyn_id=polyn_it.first;
        auto polyn_data=polyn_it.second;
        if(polyn_data->leadTerms->size()==1){
            auto monl=(*polyn_data->leadTerms->begin())->second;
            linearize(polyn_id, monl);
            continue;
        }
        auto gaussPolyn=getGaussianPolynomial(polyn_id);
        auto gaussPolyn_data=pMgr->allocatePolynomialData();
        gaussPolyn_data->leadTerms=gaussPolyn;
        gaussPolyn_data->inVars=polyn_data->inVars;
        linearIdeal->insert(std::make_pair(polyn_id,gaussPolyn_data));
    }
    _inputApproxVars->insert(pMgr->_inputVars->begin(), pMgr->_inputVars->end());
    renewSATTables();
    return;
}

    
void SATMgr::updateMonlNonlinearTable(pMonomial monl, const variableId& id){

    if(monl->size()>1){
        auto monl_it = _monlNonlinearTable->find(*monl);
        if(monl_it==_monlNonlinearTable->end()){
            auto monlD= pMgr->allocateMonomialData();
            monlD->inPolynIds.insert(id);
            _monlNonlinearTable->insert(std::make_pair(*monl,monlD));
            return;
        }
    }
    return;
}

variableId SATMgr::updateMonlNonlinearTable(pMonomial monl){

    if(monl->size()>1){
        auto monl_it = _monlNonlinearTable->find(*monl);
        if(monl_it==_monlNonlinearTable->end()){
            auto monlD= pMgr->allocateMonomialData();
            LargestId++;
            monlD->inPolynIds.insert(LargestId);
            _monlNonlinearTable->insert(std::make_pair(*monl,monlD));
            return LargestId;
        }
        else{
            auto monlD=monl_it->second;
            return *monlD->inPolynIds.begin();
        }
    }

}    
void SATMgr::updateVarMonlSATTables(pPolynomial pPolyn, 
                                                         const variableId& id){
    auto polyn=*pPolyn;
    for(auto term: polyn){
        auto monl=*term->second;
        if(monl.size()>1){
            auto monl_it = _monlSATTable->find(monl);
            if(monl_it==_monlSATTable->end()){
                auto monlD= pMgr->allocateMonomialData();
                monlD->inPolynIds.insert(id);
                _monlSATTable->insert(std::make_pair(monl,monlD));
            }
            else{
                auto monlD=monl_it->second;
                monlD->inPolynIds.insert(id);
            }
        }
        for(auto var :monl){
            auto var_it = _varSATTable->find(var);
            if(var_it==_varSATTable->end()){
                auto varD= pMgr->allocateVariableData();
                varD->inPolynIds->insert(id);
                varD->fanoutSize++;
                _varSATTable->insert(std::make_pair(var,varD));
            }
            else{
                auto varD=var_it->second;
                varD->inPolynIds->insert(id);
                varD->fanoutSize++;
            }
        }
    }
}
    
void SATMgr::linearize(const variableId& polyn_id, pMonomial monl){
    
    auto polyn_it=pMgr->_ideal->find(polyn_id);
    assert(polyn_it!=pMgr->_ideal->end());
    nonlinearIdeal->insert(polyn_it, std::next(polyn_it));
    updateMonlNonlinearTable(monl, polyn_id);
    _inputApproxVars->insert(polyn_id);
    return;
}

Coefficient SATMgr::getGaussianCoeff(pPolynomial polyn, pMonomial monl){
    
    for(auto term_it= polyn->begin(); term_it!= polyn->end(); 
            term_it=std::next(term_it)){
        
        auto term=*term_it;
        if(*term->second==*monl){
            return term->first;
        }
    }
    assert(0);
}

pPolynomial SATMgr::getGaussianPolynomial(const variableId& id){
    
    auto inPolyn= pMgr->allocatePolynomial();
    
    auto pMonl=pMgr->allocateMonomial(id);
    auto pTerm=pMgr->allocateTerm(-1,pMonl);
    inPolyn->insert(pTerm);
   
    auto leadTerms= pMgr->getPolynomialLeadTerms(id);
    pMgr->insertPolyn(inPolyn, leadTerms);
    //inPolyn->insert(leadTerms->begin(), leadTerms->end());
    //eraseGaussianPolynomial(id);    
     return inPolyn; 
}


void SATMgr::getMostReducedIdealByGaussian(){

    pVariableTable varCandidateTable=(pVariableTable) new VariableTable;;
    updateVarCandidateTable(varCandidateTable);
  
    
    while(!varCandidateTable->empty()){
        for(auto varTable_it: *varCandidateTable){
            auto polynIds= varTable_it.second->inPolynIds;
            if(polynIds->size()<2) continue;
            auto monl= pMgr->allocateMonomial(varTable_it.first);
            auto polynId1=*polynIds->begin();
            auto polynId2=*std::next(polynIds->begin());
            if(!existInGaussianTable(polynId1)||!existInGaussianTable(polynId2)){
                continue;
            }
            auto polyn1= getGaussianPolynomialLeadTerms(polynId1);
            auto polyn2= getGaussianPolynomialLeadTerms(polynId2);
            eraseGaussianPolynomial(polynId1);  
            eraseGaussianPolynomial(polynId2); 
            reduceTwoPolynomials( polyn1, polynId1, polyn2, polynId2, monl);
        } 
        //printVariableSATTable(_varSATTable);
        updateVarCandidateTable(varCandidateTable);
    }
    return;
}


void SATMgr::reduceTwoPolynomials(pPolynomial polyn1, variableId polynId1, 
                                            pPolynomial polyn2, variableId polynId2,
                                            pMonomial monl){
     if(polyn1==NULL ||polyn2==NULL) return;
    auto coeff1=getGaussianCoeff(polyn1, monl);
    auto coeff2=getGaussianCoeff(polyn2, monl);

    auto prdMonl= pMgr->allocateMonomial();
    if(absCoeff(coeff1)>=absCoeff(coeff2)){
        auto prdCoeff=coeff1/coeff2; 
        if(absCoeff(prdCoeff)%2==0||absCoeff(prdCoeff)==1){
            auto prdTerm = pMgr->allocateTerm(-1*prdCoeff, prdMonl);
            auto product=multiplyPolynWithMonl(prdTerm, polyn2);
            pMgr->insertPolyn(product, polyn1);
            if(product->size()<polyn2->size()){
                polyn2->swap(*product);
                eraseGaussianPolynomial(polynId1); 
            }
        }
    }
    else{
        auto prdCoeff=coeff2/coeff1; 
        if(absCoeff(prdCoeff)%2==0||absCoeff(prdCoeff)==1){
            auto prdTerm = pMgr->allocateTerm(-1*prdCoeff, prdMonl);
            auto product=multiplyPolynWithMonl(prdTerm, polyn1);
            pMgr->insertPolyn(product, polyn2);
            if(product->size()<polyn1->size()){
                polyn1->swap(*product);
                eraseGaussianPolynomial(polynId2); 
            }
        }

    }   
}

void SATMgr::reduceSetofPolynomials(pMonomial monl, Monomial* polynIds
                                                        , pMonomial visitedIds){
    
    if(polynIds->size()<2) return;
    auto polynId1=*polynIds->begin();
    auto polynId2=*std::next(polynIds->begin());

    if(!existInGaussianTable(polynId1)||
            !existInGaussianTable(polynId2)||
            isInPMonomial(visitedIds, polynId1)||
            isInPMonomial(visitedIds, polynId2)){
        return;
    }
    
    visitedIds->insert(polynId1);
    visitedIds->insert(polynId2);
    auto polyn1=getGaussianPolynomialLeadTerms(polynId1);
    auto polyn2=getGaussianPolynomialLeadTerms(polynId2);
    reduceTwoPolynomials( polyn1, polynId1, polyn2, polynId2, monl);
}

void SATMgr::reduceIdealByGaussian(){
    
    pMonomial visitedIds=pMgr->allocateMonomial();
    pMonomialTable monlCandidateTable=(pMonomialTable) new MonomialTable;;
    updateMonlCandidateTable(monlCandidateTable);
    int i=0;
    while(!monlCandidateTable->empty()){
        for(auto monlTable_it: *monlCandidateTable){
            auto polynIds= monlTable_it.second->inPolynIds;
            auto monl= copytoNewMonomial(monlTable_it.first);
            reduceSetofPolynomials(monl,& polynIds, visitedIds);
        } 
       /// printMonomialSATTable(_monlSATTable);
        updateMonlCandidateTable(monlCandidateTable);
        visitedIds->clear();
        //printApproximatedIdeal(stdout, 1);
        i++;
        if(i>6) break;
    }
    return;
}


void SATMgr::removeRedundantTerm(const variableId& var){
    auto var_it=_varSATTable->find(var);
    assert(var_it!=_varSATTable->end()); 
    auto parents =var_it->second->inPolynIds;
    auto newPolyn=pMgr->allocatePolynomial();
    for(auto polynId: *parents){
        auto polyn= getGaussianPolynomialLeadTerms(polynId);
        /*for(auto term : *polyn){
            auto monl = term->second; 
            if(isInPMonomial(monl,var)){
                polyn->erase(term);
            }
        }*/
        for(auto term : *polyn){
            auto monl = term->second; 
            if(!isInPMonomial(monl,var)){
                auto newTerm=copyNewTerm(term);
                //newPolyn->insert(newTerm);
                pMgr->insertTerm(newPolyn, newTerm);
            }
        }
        polyn->swap(*newPolyn);
        newPolyn->clear();
    }
    

}

void SATMgr::assignOnSetValue(const variableId& polynId, 
                                                        const variableId& var){
    auto polyn= getGaussianPolynomialLeadTerms(polynId);
    auto newPolyn=pMgr->allocatePolynomial();
    for(auto term : *polyn){
        auto monl = term->second; 
        if(isInPMonomial(monl,var)){
            auto newTerm=copyNewTerm(term);
            newTerm->second->erase(var);
            pMgr->insertTerm(newPolyn, newTerm);  
        }
        else{
            auto newTerm=copyNewTerm(term);
            //newPolyn->insert(newTerm);
            pMgr->insertTerm(newPolyn, newTerm);
        }
    }
    polyn->swap(*newPolyn);
    newPolyn->clear();
}


void SATMgr::relinearizConstraints(){
    
    for(auto polyn_it: * _constraintsIdeal){
        auto polyn=polyn_it.second->leadTerms;
        for(auto term_it: *polyn){
            auto monl=term_it->second;
            if(monl->size()<2) continue;
            auto varId=updateMonlNonlinearTable( monl);
            if(varId==LargestId){
                auto monlCopy=copytoNewMonomial(*monl);
                addNonlinearPolynomial(monlCopy);
            }
            monl->clear();
            monl->insert(varId);
        }
        
       /* if(fVerbose>0){
            pMgr->printEXPPolynomial(stdout,polyn);
            printf("\n\n");
        }*/
    }

    return;
}

void SATMgr::addOnOffConstraint(const variableId& id, const bool& value){
    auto term=pMgr->createTerm(-1,id);
    auto leadTerms=pMgr->allocatePolynomial();
    leadTerms->insert(term);
    if(value){
       auto term1=pMgr->createTermConst(1);
       leadTerms->insert(term1);
    }
    auto polyn_data=pMgr->allocatePolynomialData();
    polyn_data->leadTerms=leadTerms;
    polyn_data->inVars=pMgr->allocateMonomial(id);
    linearEnquiryIdeal->insert(std::make_pair(id,polyn_data));
}

void SATMgr::addXORConstraint(const variableId& v1, 
                                            const variableId& v2,
                                                        pEXPPolynomial XORPolyn){
    auto termv1=createEXPTerm(1,v1);
    XORPolyn->insert(termv1);
    auto termv2=createEXPTerm(1,v2);
    XORPolyn->insert(termv2);
    auto termv1v2=createEXPTerm2Vars(-2,v1,v2);
    XORPolyn->insert(termv1v2);
}

void SATMgr::addSpecificationConstraint(){

    auto specPolyn=pMgr->_specPolyn;
    auto constraintPolyn=pMgr->allocateEXPPolynomial();
    constraintPolyn->insert(specPolyn->begin(),specPolyn->end());
    auto XORPolyn=pMgr->allocateEXPPolynomial();
    for(auto term_it: *constraintPolyn){
        auto monl=term_it->second;
        auto coeff=term_it->first;
        if(monl->empty())continue;
        auto var=*monl->begin();
        if(pMgr->isPrimaryOutput(var)){
            LargestId++;
            variableId newVar=LargestId;
            monl->erase(var);
            monl->insert(newVar);
            addXORConstraint(var,newVar, XORPolyn);
        }
    }
    auto specConstData=allocateConstraintPolynomial();
    specConstData->leadTerms=constraintPolyn;
    specConstData->inEqu=0;
    _constraintsIdeal->insert(std::make_pair(0,specConstData));
    auto XORConstData=allocateConstraintPolynomial();
    XORConstData->leadTerms=XORPolyn;
    XORConstData->inEqu=2;
    _constraintsIdeal->insert(std::make_pair(LargestId, XORConstData));  
    return;
}

void SATMgr::applyInferenceThroughSAT(){
    int i=0;

    for(auto nonlinear_it: *nonlinearIdeal){
        auto nonlinearVar=nonlinear_it.first;
        auto nonlinearTerm=*nonlinear_it.second->leadTerms->begin();
        auto nonlinearMonl=nonlinearTerm->second;
        auto mostVar=*std::prev(nonlinearMonl->end());
        if(pMgr->isPrimaryInput(mostVar)) continue;
        i++;
        //if(i==1) continue;
        addOnOffConstraint(nonlinearVar, true);
        solveConstraint();
        if(satModel->empty()){
            removeRedundantTerm(nonlinearVar);
            purifyOriginalIdeal(nonlinearMonl);
        }
        linearEnquiryIdeal->clear();
        if(i>3) break;
    }
  /*  auto monlTable=pMgr->_monlTable;
    pMonomial visitedIds=pMgr->allocateMonomial();
    for(auto monlTable_it: *monlTable){
        auto polynIds= monlTable_it.second->inPolynIds;
        if(polynIds.size()>1) continue;
        auto monl= copytoNewMonomial(monlTable_it.first);
        auto mostVar=*std::prev(monl->end());
        if(pMgr->isPrimaryInput(mostVar)) continue;
        auto nonlinearVar=updateMonlNonlinearTable(monl);
        addOnOffConstraint(nonlinearVar, true);
        solveConstraint();
        if(satModel->empty()){
            removeRedundantTerm(nonlinearVar);
            purifyOriginalIdeal(monl);
        }
        _constraintsIdeal->clear();
    }*/
    
}


void SATMgr::verifySpecificationThroughSAT(){

    addSpecificationConstraint();
    relinearizConstraints();
    solveConstraint();
    
}

void SATMgr::solveConstraint(){
    
    satModel->clear();
    solvePBC(this, fVerbose);
    printSatModel(pMgr->logFile);
    if(pMgr->logFile!=stdout)
            printSatModel(stdout);
}

void SATMgr::purifyOriginalIdeal(pMonomial nonlinearMonl){
    auto orgMonlTable=pMgr->_monlTable;
    auto monl_it=orgMonlTable->find(*nonlinearMonl);
    assert(monl_it!=orgMonlTable->end());
    auto parents=monl_it->second->inPolynIds;
    auto newPolyn=pMgr->allocatePolynomial();
    for(auto polynId: parents){
      auto  polynTerms=pMgr->getPolynomialLeadTerms(polynId);
      auto oldSize=polynTerms->size();
        for(auto term : *polynTerms){
            auto monl = term->second; 
            if(*nonlinearMonl!=*monl){
                auto newTerm=copyNewTerm(term);
                pMgr->insertTerm(newPolyn, newTerm);
            }
        }
        polynTerms->swap(*newPolyn);
        newPolyn->clear();
        assert(oldSize>polynTerms->size());
    }
    orgMonlTable->erase(monl_it);
}

void SATMgr::linearizeGaussianPolynomial(variableId id, 
                                                    pMonomial linearizeMonl,
                                                    variableId linearVar){
    auto polynTerms=getGaussianPolynomialLeadTerms(id);
    for(auto term : *polynTerms){
        auto monl=term->second;
        if(*linearizeMonl== *monl){
            auto linearMonl=pMgr->allocateMonomial(linearVar);
            monl->swap(*linearMonl);
            break; ///each term has unique variable
        }
    }
    return;
}

void SATMgr::addNonlinearPolynomial(pMonomial monl){
    auto leadTerms=pMgr->allocatePolynomial();
    auto term=pMgr->allocateTerm(1,monl);
    leadTerms->insert(term);
    auto polynData=pMgr->allocatePolynomialData();
    polynData->leadTerms=leadTerms;
    polynData->inVars=monl;
    nonlinearIdeal->insert(std::make_pair(LargestId, polynData));
    return;
}

void SATMgr::relinearization(){
    
    //while(!_monlSATTable->empty()){
    for(auto monl_it: *_monlSATTable){
        auto monl=monl_it.first;
        auto parents=monl_it.second->inPolynIds;
        assert(monl.size()>1);
        auto pMonl=copytoNewMonomial(monl);
        auto varId=updateMonlNonlinearTable( pMonl);
        if(varId==LargestId){
            addNonlinearPolynomial(pMonl);
        }
        for(auto id: parents){
           linearizeGaussianPolynomial(id,pMonl,varId); 
        }
    }
    renewSATTables();
    //}
    return;
}

void SATMgr::approximateIdeal(){
    buildApproxIdeals();
    reduceIdealByGaussian();
    relinearization();
//    printApproximatedIdeal(stdout, 1);
//    printNonlinearIdeal(stdout,1);
    applyInferenceThroughSAT();
    //getMostReducedIdealByGaussian();
    //printApproximatedIdeal(stdout, 1);

    return;
}

void SATMgr::verifyIntMultiplierBySAT(){
    if(pMgr->logFile!=stdout)
        fprintf(stdout, "***c Solving The Miter Constriants by MiniSAT-Plus:\n\n");
    fprintf(pMgr->logFile, "***c Solving The Miter Constriants by MiniSAT-Plus:\n\n");
    buildApproxIdeals();
    //reduceIdealByGaussian();
    relinearization();
    verifySpecificationThroughSAT();
    /*if(fVerbose>0){
        printNonlinearIdeal(stdout,1);
        printApproximatedIdeal(stdout, 1);
    }*/
    return;
}


SATMgr * approximate(PolynMgr *_procMgr, int fVerbose){
    abctime clk = Abc_Clock();
 
    SATMgr * _approxMgr=  new SATMgr(_procMgr);   
    _approxMgr->fVerbose=fVerbose;
    _approxMgr->approximateIdeal();
     
    printSC4AVTime(_procMgr->logFile, "***c Approximate Time", Abc_Clock() - clk );
   // delete _imtMgr;
    return _approxMgr;
}


int SATPSolver(PolynMgr *_procMgr, int fVerbose){
    abctime clk = Abc_Clock();
       
    int Status=0;
    SATMgr * _approxMgr=  new SATMgr(_procMgr);   
    _approxMgr->fVerbose=fVerbose;
    _approxMgr->verifyIntMultiplierBySAT();   
    
    printSC4AVTime( _procMgr->logFile, "***c SATP Time", Abc_Clock() - clk );
    
    return Status;
}