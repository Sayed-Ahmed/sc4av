
/**HFile****************************************************************

  FileName    [polyn.h]

  SystemName  [SC4Arith.]

  PackageName [Solver.]

  Synopsis    [Data Structure of Polynomial.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/

#ifndef POLYN_H
#define POLYN_H


#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <set>
#include <queue>
#include <vector>
#include <string>
#include <assert.h>
#include <stdarg.h>
#include <math.h>
#include <algorithm>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>
#include <boost/range/algorithm.hpp>
#include <list> 
#include <gmpxx.h>
#include <extra/extra.h>
#include <misc/str.h>


 
typedef long long int Coefficient;
typedef mpz_class EXPCoefficient;
typedef unsigned long long int variableId;
typedef unsigned long long int monomialId;
typedef std::bitset<8> __Byte;
typedef std::set<variableId> Monomial;
typedef std::shared_ptr<Monomial> pMonomial;
typedef std::pair<Coefficient, pMonomial> Term;
typedef std::shared_ptr<Term> pTerm;
typedef std::pair<EXPCoefficient, pMonomial> EXPTerm;
typedef std::shared_ptr<EXPTerm> pEXPTerm;

struct bitCompare {
    bool operator()(const __Byte t1, const __Byte t2) const{
        
        return t1.to_ulong()<t2.to_ulong();
        
    }
};
typedef std::set<__Byte, bitCompare> TruthTable;
typedef std::shared_ptr<TruthTable> pTruthTable;



struct termCompare:public Term{
    bool operator()(const pTerm t1, const pTerm t2) const{
        //if(*t1->second==*t2->second){
        //    return t1<t2;
        //}
        //else {
            return *t1->second<*t2->second;
        //}
    }
};

struct EXPTermCompare:public EXPTerm{
    bool operator()(const pEXPTerm t1, const pEXPTerm t2) const{
        //if(*t1->second==*t2->second){
        //    return t1<t2;
        //}
        //else {
            return *t1->second<*t2->second;
        //}
    }
};

typedef std::set<pTerm, termCompare> Polynomial;
typedef std::shared_ptr<Polynomial> pPolynomial;

typedef std::set<pEXPTerm, EXPTermCompare> EXPPolynomial;
typedef std::shared_ptr<EXPPolynomial> pEXPPolynomial;

struct PolynomialData{
	pPolynomial leadTerms;
	pMonomial inVars;
        Monomial primaryInVars;
        Coefficient modulo=0;
	pTruthTable lut;
};

struct VariableData{
	pMonomial inPolynIds;
	size_t fanoutSize=0;
        size_t substSize=0;
};

struct  MonomialData{
	Monomial inPolynIds;
};

typedef std::shared_ptr<PolynomialData> pPolynomialData;
typedef std::shared_ptr<VariableData> pVariableData;
typedef std::shared_ptr<MonomialData> pMonomialData;
typedef std::map<variableId, pPolynomialData> Ideal;
typedef std::map<variableId, pVariableData> VariableTable;
typedef boost::unordered_map<Monomial, pMonomialData> MonomialTable;
typedef boost::unordered_set<variableId> VariableSet;
typedef boost::unordered_map<variableId, variableId> IdTable;
typedef std::shared_ptr<VariableTable> pVariableTable;
typedef std::shared_ptr<MonomialTable> pMonomialTable;
typedef std::shared_ptr<Ideal> pIdeal;
typedef std::shared_ptr<VariableSet> pVariableSet;
typedef std::shared_ptr<IdTable> pIdTable;


class PolynMgr{

public:
        PolynMgr();
        ~PolynMgr();
        
        FILE * logFile;
	pVariableTable  _varTable;
	pMonomialTable	_monlTable;
	pIdeal  _ideal;
	pMonomial _depVars;
        pMonomial  _inputVars;
        pMonomial _outputVars;
        pIdTable _polynId2Aig;
        pEXPPolynomial _specPolyn;
        EXPCoefficient moduloN;
        
        pMonomial allocateMonomial();
        pMonomial allocateMonomial(const variableId &);
        pMonomial allocateMonomial(const variableId&, const variableId&);
       
        inline pTerm allocateTerm(const Coefficient& c, pMonomial m){
            return (pTerm) new Term(c,m);
        };
        
        inline pEXPTerm allocateEXPTerm(const EXPCoefficient& c, pMonomial m){
            return (pEXPTerm) new EXPTerm(c,m);
        };
        
        inline pPolynomial allocatePolynomial(){
            return (pPolynomial) new Polynomial;
        };
        
        inline pEXPPolynomial allocateEXPPolynomial(){
            return (pEXPPolynomial) new EXPPolynomial;
        };
        
        inline pPolynomialData allocatePolynomialData(){
            pPolynomialData polynD= (pPolynomialData) new PolynomialData;
            polynD->leadTerms= (pPolynomial) new Polynomial;
            polynD->inVars= allocateMonomial();
            polynD->lut= (pTruthTable) new TruthTable;
            polynD->modulo=0;
            return polynD;
        };
        
        inline pMonomialData allocateMonomialData(){
            pMonomialData monlD= (pMonomialData) new MonomialData;
            return monlD;
        };
        
        inline pVariableData allocateVariableData(){
            pVariableData varD= (pVariableData) new VariableData;
            varD->inPolynIds=allocateMonomial();
            return varD;
        };
    
        inline pIdeal allocateIdeal(){
            return (pIdeal) new Ideal;
        };
        
        inline pTerm createTerm(const Coefficient& c, const variableId& v){
            auto m=allocateMonomial(v);
            return (pTerm) new Term(c,m);
        };
        
        inline pTerm createTermConst(const Coefficient& c){
            auto m=allocateMonomial();
            return (pTerm) new Term(c,m);
        };
        
        inline pEXPTerm createExpTerm(const EXPCoefficient& c, const variableId& v){
            auto m=allocateMonomial(v);
            return allocateEXPTerm(c,m);
        };
        
        inline pEXPTerm createExpTermConst(const EXPCoefficient& c){
            auto m=allocateMonomial();
            return allocateEXPTerm(c,m);
        };
        
        inline bool isNotInIdeal( const variableId& key ){
            return (_ideal->find(key)==_ideal->end());
        };
        
        inline bool isNotInIdTable( pIdTable t, const variableId& key ){
            return (t->find(key)==t->end());
        };
        
        inline bool isPrimaryInput(const variableId& var){
            
            return (_inputVars->find(var)!= _inputVars->end() );
        };
        
        inline bool isPrimaryInputMonomial( pMonomial m){
            
            Monomial differenceVars;
            std::set_difference(m->begin(), m->end(),
                            _inputVars->begin(), _inputVars->end(),
                            std::inserter(differenceVars, differenceVars.begin()));
            if(differenceVars.empty()) return true;
            return false;
        };
        
        inline bool isPrimaryOutput(const variableId & var){
            
            return (_outputVars->find(var)!= _outputVars->end() );
        };
        
        inline pMonomial getPolynomialFanins(const variableId& id){
            auto id_it=_ideal->find(id);
            assert(id_it!=_ideal->end());
            return id_it->second->inVars;
        }
        
        inline pPolynomial getPolynomialLeadTerms(const variableId& id){
            auto id_it=_ideal->find(id);
            assert(id_it!=_ideal->end());
            return id_it->second->leadTerms;
        }
        
        inline pPolynomialData getPolynomialData(const variableId& id){
            auto id_it=_ideal->find(id);
            assert(id_it!=_ideal->end());
            return id_it->second;
        }
        
        inline pMonomial getPolynomialChilds(const variableId& Id){
            assert(!_varTable->empty());
            auto it= _varTable->find(Id);
            if(it==_varTable->end()){
                assert(_outputVars->find(Id)!=_outputVars->end());
                return NULL;
            }
            return it->second->inPolynIds;
        }
        
        inline pVariableData getVariableData(const variableId& Id){
            assert(!_varTable->empty());
            auto it= _varTable->find(Id);
            if(it==_varTable->end()){
                assert(_outputVars->find(Id)!=_outputVars->end());
                return NULL;
            }
            return it->second;
        }
        
        inline pTruthTable getPolynomialLUT(const variableId& Id, size_t& numInputs){
            auto id_it=_ideal->find(Id);
            assert(id_it!=_ideal->end());
            numInputs=id_it->second->inVars->size();
            return id_it->second->lut;
        } 
        
        inline pTerm copyToNewTerm(pTerm t){
            auto m_copy=allocateMonomial();
            m_copy->insert(t->second->begin(),t->second->end());
            auto t_copy=allocateTerm(t->first,m_copy);
            return t_copy; 
        }
        
        inline void insertTerm(pPolynomial polyn, pTerm t){
            auto it=polyn->find(t);
            if(it!=polyn->end()){
                (*it)->first+=t->first;
                if((*it)->first==0){
                    polyn->erase(it);
                }
            }
            else if(t->first!=0){
                polyn->insert(t);
            }
            return;
        }
        
        inline void insertPolyn(pPolynomial p1, pPolynomial p2){
            
            for(auto t: *p2){
                auto t_copy=copyToNewTerm(t);
                insertTerm(p1, t_copy);
            }
            return;
        }
        
        inline void insertEXPTerm(pEXPPolynomial polyn, pEXPTerm t){
        auto it=polyn->find(t);
        if(it!=polyn->end()){
            (*it)->first=(*it)->first+t->first;
            if((*it)->first==0){
                polyn->erase(it);
            }
        }
        else if(t->first!=0){
            polyn->insert(t);
        }
        return;
    }
        
        void printIdeal(FILE *&, const int& fVerbose);
        void printPolynomial(FILE *&, pPolynomial);
        void printEXPPolynomial(FILE *&, pEXPPolynomial);
        void updateMonomialVariableTables(pMonomial, const variableId&);
        void addPolyn2Ideal(const variableId &, pPolynomialData );
        void updatePrimaryInVariables(pPolynomialData );
        void updateVariableSubstitutionSize(const variableId&, const size_t&);
        void printTerm(FILE*&,  pTerm );
        void printEXPTerm(FILE*&,  pEXPTerm );
        void openLogFile(vecStr*);
        void closeLogFile();

private:
        void printPrimaryInputs(FILE* &, Monomial* );
        
};





#endif
