/**CPPFile****************************************************************

  FileName    [IMTSolver.cpp]

  SystemName  [SC4AV.]

  PackageName [prover.]

  Synopsis    [Ideal Membership Testing.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/

#include "INTprover/IMT.h"


IMTMgr::IMTMgr(PolynMgr * pMgr_new ): PreprocessMgr(pMgr_new) {
     pMgr=pMgr_new;
     gaussianTable= (pIdeal) new Ideal;
}

IMTMgr::~IMTMgr(){
    
}


void IMTMgr::replaceWithSimplerMonomialsEXP(pEXPPolynomial leadTerms,
                                                  pSubstitutionTable simplerMonomials ){
    pEXPPolynomial newTerms=pMgr->allocateEXPPolynomial();
    for(auto set: *simplerMonomials){
        auto pSet=copytoNewMonomial(set.first);
        pPolynomial reducedPolyn= set.second;
        //printSet(pSet);
        //pMgr->printPolynomial(stdout, reducedPolyn);
        EXPPolynomial erasedTerms; 
       for(auto term: *leadTerms){
           auto monl=term->second;
           auto coeff=term->first;
           if(monl->size()<=1||coeff==0) continue;
           auto intersVars= getIntersectedVariables(pSet, monl);
           if(intersVars.size()!=pSet->size()) continue;

            if(reducedPolyn->empty()){
                term->first=0;
                erasedTerms.insert(term);
            }
            else if(reducedPolyn->size()==1){
                auto newMonl=(*reducedPolyn->begin())->second;
                if (*newMonl!=*pSet){
                    erasedTerms.insert(term);
                    auto diffVars=getDifferenceVariables(monl, pSet);
                    auto insertMonl= copytoNewMonomial(diffVars);
                    insertMonl->insert(newMonl->begin(), newMonl->end());
                    auto newterm=pMgr->allocateEXPTerm(term->first, insertMonl);
                    pMgr->insertEXPTerm(newTerms, newterm);
                }
            }                           
        }
        for(auto eraseTerm: erasedTerms){
            leadTerms->erase(eraseTerm);
        }
        //addSimilarMonomials(leadTerms);
    } 
    insertEXPPolyn(leadTerms, newTerms);
    return;
}

void IMTMgr::applyInferenceRulesEXP(pEXPPolynomial leadTerms ){

   abctime clk_infer = Abc_Clock();

   auto faninVars=pMgr->allocateMonomial();
   for(auto term: *leadTerms){
       auto monl=term->second;
       for(auto var:*monl){
           faninVars->insert(var);
       }
   }
    
   abctime clk_shared = Abc_Clock();
   auto subsumedGroups= getSharedSupportingVariables(faninVars);
   timeAnalysis.shared+= Abc_Clock() - clk_shared;
   abctime clk_replace = Abc_Clock();
   replaceWithSimplerMonomialsEXP(leadTerms, subsumedGroups);
   timeAnalysis.replace+= Abc_Clock() - clk_replace;
   //addSimilarMonomials(leadTerms);
   
   timeAnalysis.inference+= Abc_Clock() - clk_infer;
   return;    
}

EXPPolynomial::iterator IMTMgr::getNonPrimaryInputsMonomial(pEXPPolynomial polyn, 
                                             pMonomial lastMonl ){
    auto lastTerm_it=std::prev(polyn->end());       
    lastMonl=(*lastTerm_it)->second;
    while(pMgr->isPrimaryInputMonomial(lastMonl)
        &&lastTerm_it!=polyn->begin()){           
        lastTerm_it=std::prev(lastTerm_it);       
        lastMonl=(*lastTerm_it)->second;
    }
   return lastTerm_it;
}
    
void IMTMgr::moduloCoefficient(pEXPTerm term, const EXPCoefficient moduloN){
    auto Coeff=term->first;
    while(absEXPCoeff(Coeff)>=moduloN){
        if(Coeff>0)
           Coeff-=moduloN;
        else
           Coeff+=moduloN;    
    }
    term->first=Coeff;
    return;
}

pEXPPolynomial IMTMgr::multiplyPolynWithEXPMonl(pEXPTerm term1, 
        pPolynomial polyn2 ){
    
    pEXPPolynomial product=pMgr->allocateEXPPolynomial();
    
    
    EXPCoefficient coeff1=std::get<0>(*term1);
    auto monl1=std::get<1>(*term1);
    for (auto term2 :*polyn2){
        EXPCoefficient coeff2;
        mpz_set_si(coeff2.get_mpz_t(),std::get<0>(*term2));
        auto monl2=std::get<1>(*term2);
        EXPCoefficient coeff_prod=coeff1*coeff2;
        pMonomial monl_prod= (pMonomial) new Monomial;
        monl_prod->insert(monl1->begin(), monl1->end());
        monl_prod->insert(monl2->begin(), monl2->end());
        auto term_prod=pMgr->allocateEXPTerm(coeff_prod, monl_prod);
        //product->insert(term_prod);
        insertEXPTerm(product, term_prod);
    }
    //addSimilarMonomials(product);
    return product;
}



variableId IMTMgr::selectEliminatedVariable(pEXPPolynomial polyn){
    
    assert(!polyn->empty());
    pMonomial depVars=pMgr->allocateMonomial();
    for(auto term: *polyn){
        auto monl=term->second;
        for(auto var: *monl){
            if(!pMgr->isPrimaryInput(var)){
                auto varD=pMgr->getVariableData(var);
                if(isInPMonomial(depVars,var)){
                    varD->fanoutSize++;
                }
                else{
                    depVars->insert(var);
                    varD->fanoutSize=1;
                }
            }
        }
    }
    //auto lastTerm=*std::prev(polyn->end());
    //auto lastMonl=lastTerm->second;
    if(depVars->empty()) return 0;
    auto sortedSet = (pSortedSizeTuples) new sortedSizeTuples;
    for(auto var: *depVars){
        auto varD=pMgr->getVariableData(var);
        auto totalSize=varD->fanoutSize*varD->substSize;
        sizeTuple newTuple= sizeTuple(var, totalSize);
        sortedSet->insert(newTuple);
    }
    auto firstElement_it=sortedSet->begin();
     
    return firstElement_it->first;
}

pEXPPolynomial IMTMgr::NormalForm(pEXPPolynomial specPolyn, 
                                    const EXPCoefficient moduloN,
                                                        bool & giveup){
    
    if(specPolyn->empty()) return specPolyn;
    pEXPPolynomial newRemainder=pMgr->allocateEXPPolynomial();
    pEXPPolynomial currRemainder=pMgr->allocateEXPPolynomial();
    pEXPPolynomial product;
    insertEXPPolyn(currRemainder,specPolyn);   

    auto lastTerm_it=std::prev(currRemainder->end());
    auto lastMonl=(*lastTerm_it)->second;

    while(!pMgr->isPrimaryInputMonomial(lastMonl)){

        auto singleTerm_it=lastTerm_it;
        auto singleMonl=(*singleTerm_it)->second;
        
        while(singleTerm_it!=currRemainder->begin()&&!checkSingleCondition(singleMonl)){
            
            singleTerm_it=std::prev(singleTerm_it);
            singleMonl=(*singleTerm_it)->second;
        }

        if(checkSingleCondition(singleMonl)&& !pMgr->isPrimaryInputMonomial(singleMonl)){
            auto singleVar=*std::prev(singleMonl->end());
            auto subsPolyn=pMgr->getPolynomialLeadTerms(singleVar);
            
            moduloCoefficient(*singleTerm_it,moduloN);
            auto singleCoeff=(*singleTerm_it)->first;
            if(singleCoeff==0) {
                currRemainder->erase(singleTerm_it);
                lastTerm_it=getNonPrimaryInputsMonomial(currRemainder, lastMonl);
                continue; 
            }
            auto newMonl= pMgr->allocateMonomial();
            if(singleMonl->size()>1)
                newMonl->insert(singleMonl->begin(), std::prev(singleMonl->end()));
            auto newTerm = pMgr->allocateEXPTerm(singleCoeff, newMonl);

            product=multiplyPolynWithEXPMonl(newTerm, subsPolyn);

            assert(product!=NULL);
            currRemainder->erase(singleTerm_it);
            insertEXPPolyn(currRemainder, product);
            
            if(currRemainder->empty()) return currRemainder;
            
            lastTerm_it=std::prev(currRemainder->end());       
            lastMonl=(*lastTerm_it)->second;
            while(pMgr->isPrimaryInputMonomial(lastMonl)
                    &&lastTerm_it!=currRemainder->begin()){           
                lastTerm_it=std::prev(lastTerm_it);       
                lastMonl=(*lastTerm_it)->second;
            }
            continue;
        }
        
        auto elimVar=selectEliminatedVariable(currRemainder);

        if( elimVar!=0){
            /*if(fVerbose>0){
                pMgr->printEXPPolynomial(stdout, currRemainder);
                printf("\n\n");
            }*/
             auto subsPolyn=pMgr->getPolynomialLeadTerms(elimVar);
            for(auto currTerm: *currRemainder){        
                moduloCoefficient(currTerm,moduloN);
                auto currCoeff=std::get<0>(*currTerm); 
                auto currMonl=std::get<1>(*currTerm);
                if(currCoeff==0) { continue; }

                auto newMonl= pMgr->allocateMonomial();
                newMonl->insert(currMonl->begin(), currMonl->end());
                auto newTerm = pMgr->allocateEXPTerm(currCoeff, newMonl);

                if(currMonl->empty()||absEXPCoeff(currCoeff)>moduloN){ 
                    insertEXPTerm(newRemainder,newTerm); continue; }


                if(isInPMonomial(currMonl, elimVar)){
                     newMonl->erase(elimVar);
                }
                else{   
                    insertEXPTerm(newRemainder,newTerm); 
                    continue; 
                }

                product=multiplyPolynWithEXPMonl(newTerm, subsPolyn);
                assert(product!=NULL);
                insertEXPPolyn(newRemainder, product);
            }

            currRemainder->swap(*newRemainder);
            newRemainder->clear();
            if(currRemainder->empty()) return currRemainder;

        }
        lastTerm_it=std::prev(currRemainder->end());       
        lastMonl=(*lastTerm_it)->second;      
        while(pMgr->isPrimaryInputMonomial(lastMonl)
                &&lastTerm_it!=currRemainder->begin()){           
            lastTerm_it=std::prev(lastTerm_it);       
            lastMonl=(*lastTerm_it)->second;
        }

        if(currRemainder->size()> 2*maxSize){
            if(pMgr->logFile!=stdout){
                fprintf(stdout, "***s IMT Cannot Find a Solution!\n\n");
            }
            fprintf(pMgr->logFile, "***s IMT  Cannot Find a Solution!\n\n");
            giveup=true;
            break;
        }
    }

    for(auto currTerm: *currRemainder){        
        moduloCoefficient(currTerm,moduloN);
        auto currCoeff=std::get<0>(*currTerm); 
        auto currMonl=std::get<1>(*currTerm);
        if(currCoeff==0) { continue; }

        auto newMonl= pMgr->allocateMonomial();
        newMonl->insert(currMonl->begin(), currMonl->end());
        auto newTerm = pMgr->allocateEXPTerm(currCoeff, newMonl);
        insertEXPTerm(newRemainder,newTerm); continue; 
    }
    currRemainder->swap(*newRemainder);
    newRemainder->clear();
    return currRemainder;
}



bool IMTMgr::testIntMultiplierMembership(){
    
    auto specPolyn=pMgr->_specPolyn;
    EXPCoefficient moduloN=pMgr->moduloN;
    fprintf(pMgr->logFile, "***c Testing the SPECPolynomial:\n");
    fprintf(stdout, "***c Testing the SPECPolynomial:\n");
    pMgr->printEXPPolynomial(pMgr->logFile, specPolyn);
    gmp_fprintf(pMgr->logFile, "   modulo %Zd\n\n", moduloN);
 
    bool giveUp=false;
    auto remainder=NormalForm(specPolyn,moduloN, giveUp);
    
    if (giveUp) return !giveUp;
    
    if(remainder->empty()){
        fprintf(pMgr->logFile, "***s SPECPolynomial is Consistent with Algebraic Model!\n\n");
        if(pMgr->logFile!=stdout)
            fprintf(stdout, "***s SPECPolynomial is Consistent with Algebraic Model!\n\n");
    }
    else{
        if(pMgr->logFile!=stdout){
            fprintf(stdout, "***s Algebraic Model Violates SPECPolynomial!\n\n");
            fprintf(stdout, "***s Remainder:\n");
            pMgr->printEXPPolynomial(stdout, remainder);
            fprintf(stdout, "\n\n");
        }
        fprintf(pMgr->logFile, "***s Algebraic Model Violates SPECPolynomial!\n\n");
        fprintf(pMgr->logFile, "***s Remainder:\n");
        pMgr->printEXPPolynomial(pMgr->logFile, remainder);
        fprintf(pMgr->logFile, "\n\n");
    }
    
    return !giveUp;
}

bool IMTSolver(PolynMgr *_procMgr, int fVerbose){
    abctime clk = Abc_Clock();
 
    IMTMgr * _imtMgr=  new IMTMgr(_procMgr);   
    _imtMgr->fVerbose=fVerbose;
    bool status=_imtMgr->testIntMultiplierMembership();
     
    printSC4AVTime( _procMgr->logFile, "***c IMT Time", Abc_Clock() - clk );
 
    return status;
}
