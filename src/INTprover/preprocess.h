
/**HFile****************************************************************

  FileName    [polyn.h]

  SystemName  [SC4Arith.]

  PackageName [Solver.]

  Synopsis    [Preprocess Set of Polynomials.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/
#include"INTprover/polyn.h"
#include"misc/int.h"
#include "misc/util/abc_global.h"

#ifndef PREPROCESS_H
#define PREPROCESS_H

struct termSortedCompare:public Term{
    bool operator()(const pTerm t1, const pTerm t2) const{
         if(t1->second->size()==t2->second->size()){
            return *t1->second<*t2->second;
        }
        else{
            return t1->second->size()<t2->second->size();
        }
    }
};

typedef std::set<pTerm, termSortedCompare> sortedPolynomial;
typedef std::shared_ptr<sortedPolynomial> pSortedPolynomial;

typedef std::pair<variableId, size_t> sizeTuple;

struct tupleSortedCompare:public sizeTuple{
    bool operator()(const sizeTuple t1, const sizeTuple t2) const{
        if(t1.second==t2.second){
            return t2.first<t1.first;
        }
        else{
            return t1.second<t2.second;
        }
    }
};

typedef std::set<sizeTuple, tupleSortedCompare> sortedSizeTuples;
typedef std::shared_ptr<sortedSizeTuples> pSortedSizeTuples;

typedef boost::unordered_map<Monomial, pMonomial> SetsTable;
typedef std::shared_ptr<SetsTable> pSetsTable;
typedef std::map<variableId, pMonomial> RewriteTable;
typedef std::map<Monomial, pPolynomial> SubstitutionTable;
typedef std::shared_ptr<RewriteTable> pRewriteTable;
typedef std::shared_ptr<SubstitutionTable> pSubstitutionTable;
typedef std::set<Monomial> MonomialsSet;
typedef std::set<pMonomial> pMonomialsSet;
typedef std::shared_ptr<pMonomialsSet>ppMonomialsSet;
typedef std::map<variableId, bool> ValueTable;
typedef std::map<variableId,ppMonomialsSet> CutsTable;
typedef std::shared_ptr<CutsTable> pCutsTable;
typedef boost::unordered_map<Monomial,ppMonomialsSet> MonomialCutsTable;
typedef std::shared_ptr<MonomialCutsTable> pMonomialCutsTable;


class PreprocessMgr{
    public:
        PreprocessMgr( PolynMgr *);
        ~PreprocessMgr();
        void refineDepVarsPrimaryInputs();
        void refineDepVarsFanIns();
        void refineDepVarsXORs();
        void refineDepVarsSingular();
        void refineDepVarsCoupling();
        void refineDepCutsCoupling();
        void refineDepVarsCuts(const int);
        void polynomialFactorization();
        void rewriteIdeal(const bool &);
        void refineIdeal(const bool &);
        void renewMonomialTable( );
        void printMonomialTable( );
        void printCutsTable();
        pPolynomial multiplyPolynWithMonl(pTerm, pPolynomial);
        pSortedSizeTuples buildSortedSizedSet(pMonomial);
        void analyzeSpecPolynomial();
        void refineRemovedVariables(pMonomial);
        pSubstitutionTable getSharedSupportingVariables( pMonomial);
        pSubstitutionTable getSharedCutsForInference( pMonomial);
        void  findAllCuts(int k);
        
        const int inferenceDepth=6;
        int maxSize;
        
        inline Monomial getIntersectedVariables(pMonomial m1, pMonomial m2){
            Monomial intersectVars;
            std::set_intersection(m1->begin(), m1->end(),
                            m2->begin(), m2->end(),
                            std::inserter(intersectVars, intersectVars.begin()));
            return intersectVars;
        }
        
        inline Monomial getDifferenceVariables(pMonomial m1, 
                                                               pMonomial m2){
            Monomial differenceVars;
            std::set_difference(m1->begin(), m1->end(),
                            m2->begin(), m2->end(),
                            std::inserter(differenceVars, differenceVars.begin()));
            return differenceVars;
        }
        
        inline pMonomial getSymmetricDifferenceVariables(pMonomial m1, 
                                                               pMonomial m2){
            Monomial differenceVars;
            std::set_symmetric_difference(m1->begin(), m1->end(),
                            m2->begin(), m2->end(),
                            std::inserter(differenceVars, differenceVars.begin()));
            pMonomial pDifferenceVars=copytoNewMonomial(differenceVars);
            return pDifferenceVars;
        }
        
        inline bool isSubmonomial(pMonomial m1, pMonomial m2){
        
            return std::includes(m2->begin(), m2->end(),
                    m1->begin(), m1->end());    // m1 is subset of m2
        };
        
        inline bool isNotInMonomial( Monomial* m, const variableId & key ){
            return (m->find(key)==m->end());
        };
        
        inline bool isInMonomial( Monomial* m, const variableId & key ){
            return (m->find(key)!=m->end());
        };
        inline bool isInPMonomial( pMonomial m, const variableId & key ){
            return (m->find(key)!=m->end());
        };
        
        inline bool isPrimaryInputsMonomial(pMonomial monl){
            auto primaryInputs=pMgr->_inputVars;
            return std::includes(primaryInputs->begin(), primaryInputs->end(),
                    monl->begin(), monl->end()); 
        };
        
        inline bool isMostOutput(const variableId & var){
            
            return (_mostOutputVars->find(var)!= _mostOutputVars->end() );
        };
        
        /*inline void markEliminatedVariables(const variableId & v){
            if(pMgr->isPrimaryOutput(v)||pMgr->isPrimaryInput(v)) return;
            auto varD=pMgr->getVariableData(v);
            if(varD->fanoutSize*varD->substSize> 2*maxSize) return;
            updateRemovedTable(v);
            pMgr->_depVars->erase(v);
            _eliminatedVars->insert(v);
            //printf("elem:%llu\n",v);
        };*/
        
        inline void markEliminatedVariables(const variableId & v){
            if(pMgr->isPrimaryOutput(v)||pMgr->isPrimaryInput(v)) return;
            auto varD=pMgr->getVariableData(v);
            auto costVar=varD->fanoutSize*varD->substSize;
            if(costVar> 2*maxSize) return;
            if(costVar< pMgr->_inputVars->size()/4)
               _eliminatedVarsL->insert(v);
            else
               _eliminatedVarsH->insert(v); 
        };

        
        inline pMonomial copytoNewMonomial(const Monomial & monl){
            auto newMonl=pMgr->allocateMonomial();
            newMonl->insert(monl.begin(), monl.end());
            return newMonl;
        }
        
        inline pSortedPolynomial copytoSortedPolynomial(pPolynomial polyn){
            auto sortedPolyn=  (pSortedPolynomial) new sortedPolynomial ;
            sortedPolyn->insert(polyn->begin(), polyn->end());
            return sortedPolyn;
        }
        
        inline Coefficient absCoeff(const Coefficient c){
            if(c<0)  return c*-1;
            else return c;
        }

        inline EXPCoefficient absEXPCoeff(const EXPCoefficient c){
            if(c<0){
                EXPCoefficient minusC;
                mpz_set_si(minusC.get_mpz_t(),-1);
                minusC*=c;
                return minusC;
            }
            else return c;
        }

        inline void insertEXPTerm(pEXPPolynomial polyn, pEXPTerm t){
            auto it=polyn->find(t);
            if(it!=polyn->end()){
                (*it)->first=(*it)->first+t->first;
                if((*it)->first==0){
                    polyn->erase(it);
                }
            }
            else if(t->first!=0){
                polyn->insert(t);
            }
            return;
        }

        inline pEXPTerm copyToNewEXPTerm(pEXPTerm t){
            EXPCoefficient newc;
            mpz_init_set(newc.get_mpz_t(), t->first.get_mpz_t());
            return pMgr->allocateEXPTerm(newc, t->second);
        }

        inline void insertEXPPolyn(pEXPPolynomial p1, pEXPPolynomial p2){

            for(auto t: *p2){
                auto newt=copyToNewEXPTerm(t);
                insertEXPTerm(p1, newt);
            }
            return;
        }
        inline ppMonomialsSet getVariableCuts(variableId v){
            auto cuts_it=_cutsTable->find(v);
            if (cuts_it!=_cutsTable->end())
                return cuts_it->second;
            else
                assert(0); 
        }
        
        pSetsTable _prInputsTable;
        pSetsTable _fanInsTable;
        pSetsTable _sharedCutsTable;
        pRewriteTable _removedTable;
        pSubstitutionTable _substituteTable;
        pMonomial _eliminatedVars;
        pMonomial _eliminatedVarsL;
        pMonomial _eliminatedVarsH;
        int fVerbose=0;
        pMonomial _mostOutputVars;
	pCutsTable _cutsTable;
                
        
        struct TimeAnalysis {
            abctime rewrite=0;
            abctime inference=0; 
            abctime substite=0;
            abctime shared=0;
            abctime replace=0;
            abctime coupling=0;
            abctime update=0;
        } timeAnalysis;
        
        struct InferenceAnalysis {
            int nonIntersected=0;
            int subsumed=0; 
            int deep=0;
        } _inferenceAnalysis;
        

        
        void printSet(pMonomial);
        pTerm copyNewTerm(pTerm );
        void printSet(Monomial *);
        PolynMgr * pMgr; 
    private:
        void printLUT(pTruthTable );
        void printSet(pVariableSet);
        void printMonomial(Monomial*);
        void printSubstitutionPolynomial(FILE*&, pPolynomial, Monomial* );
        void printPolynomial(FILE*&, pPolynomial, const variableId& );
        void updatePrimaryInputsTable();
        void updateFaninsTable();
        void updateRemovedTable(const variableId &);
        void updateSubstitutionTable( const variableId &, pMonomial);
        void rewriteOnePolynomial( const variableId &, pMonomial);
        void rewritePolynomialWithInference(const variableId &,pMonomial, const bool & );
        pPolynomial invertVariableWithinPolynomial(pPolynomial, const variableId &);
        void updatePolynomialData(const variableId &,pMonomial);

        void updateVariableData(pMonomial, const variableId &, const variableId & );
        void eraseFromMonomialTable(const variableId &);
        void addToMonomialTable( const variableId &);
        void eraseFromCutsTable(const variableId &);
        void updateCutsTable( const variableId &);
        void updateSharedCutsTable();
        


        void addSimilarMonomials(pPolynomial);
        pPolynomial multiplyTwoPolynomials(pPolynomial , pPolynomial);
        void addTwoPolyns();
        MonomialsSet getCombinations(pMonomial);
        void insertSubstitutionPolynomial(SubstitutionTable::iterator, const Monomial &);
        bool simulatePolynomial(pPolynomialData, __Byte);
        bool isXORPolynomial(const variableId &);
        bool isXNORPolynomial(const variableId &);
        void applyInferenceRules(const variableId &);
        void applyInferenceRules(pPolynomial);
        void replaceWithSimplerMonomials(pPolynomial, pSubstitutionTable  );
        pPolynomial inferSimplerMonomials(pMonomial, pMonomial);
        pPolynomial inferSimplerMonomialsfromCuts(pMonomial, pMonomial);
        //pSubstitutionTable getSharedSupportingVariables( pMonomial);
        SetsTable  groupVariablesBasedFanin( pMonomial);
        void insertSimplifiedPolynomial (const Monomial &, pMonomial,  
                                                pSubstitutionTable );
        void applyModulo(const variableId&);

        ppMonomialsSet findVariableCuts(int k, variableId, pMonomial);
        
        inline pTruthTable twoInputsXOR(size_t numbits){

            auto lut= (pTruthTable) new TruthTable;
            if(numbits==2){
                __Byte assign1 (std::string("00000001"));
                __Byte assign2 (std::string("00000010"));
                lut->insert(assign1);
                lut->insert(assign2);
                return lut;
            }
            else if(numbits==3){
                __Byte assign1 (std::string("00000110"));
                __Byte assign2 (std::string("00000011"));
                __Byte assign3 (std::string("00000100"));
                __Byte assign4 (std::string("00000101"));
                lut->insert(assign1);
                lut->insert(assign2);
                lut->insert(assign3);
                lut->insert(assign4);
                return lut;
            }
           /* else if(numbits==4){
                Byte assign1 (std::string("00000011"));
                Byte assign2 (std::string("00000111"));
                Byte assign3 (std::string("00001011"));
                Byte assign4 (std::string("00001100"));
                Byte assign5 (std::string("00001101"));
                Byte assign6 (std::string("00001110"));
                lut->insert(assign1);
                lut->insert(assign2);
                lut->insert(assign3);
                lut->insert(assign4);
                lut->insert(assign5);
                lut->insert(assign6);
                return lut;
            }*/
            else{
                return NULL;
            }
        };
        
        inline pTruthTable twoInputsXNOR(size_t numbits){

            auto lut= (pTruthTable) new TruthTable;
            if(numbits==2){
                __Byte assign1 (std::string("00000000"));
                __Byte assign2 (std::string("00000011"));
                lut->insert(assign1);
                lut->insert(assign2);
                return lut;
            }
            else if(numbits==3){
                __Byte assign1 (std::string("00000000"));
                __Byte assign2 (std::string("00000010"));
                __Byte assign3 (std::string("00000001"));
                __Byte assign4 (std::string("00000111"));
                lut->insert(assign1);
                lut->insert(assign2);
                lut->insert(assign3);
                lut->insert(assign4);
                return lut;
            }
           /* else if(numbits==4){
                Byte assign1 (std::string("00000000"));
                Byte assign2 (std::string("00000100"));
                Byte assign3 (std::string("00001000"));
                Byte assign4 (std::string("00000001"));
                Byte assign5 (std::string("00000101"));
                Byte assign6 (std::string("00001001"));
                Byte assign7 (std::string("00000010"));
                Byte assign8 (std::string("00000110"));
                Byte assign9 (std::string("00001010"));
                Byte assign10 (std::string("00001111"));
                lut->insert(assign1);
                lut->insert(assign2);
                lut->insert(assign3);
                lut->insert(assign4);
                lut->insert(assign5);
                lut->insert(assign6);
                lut->insert(assign7);
                lut->insert(assign8);
                lut->insert(assign9);
                lut->insert(assign10);
                return lut;
            }*/
            else{
                return NULL;
            }
        };
        
        int currDepth=0;
        

        
};

PreprocessMgr* preprocessGroebnerIdeal(PolynMgr *, const int & );

#endif /* PREPROCESS_H */

