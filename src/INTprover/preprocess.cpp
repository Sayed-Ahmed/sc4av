/**CPPFile****************************************************************

  FileName    [preprocess.cpp]

  SystemName  [SC4AV.]

  PackageName [prover.]

  Synopsis    [preprocessing Groebner bases polynomials.]

  Author      [Amr Sayed-Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]


***********************************************************************/

#include "preprocess.h"


PreprocessMgr::PreprocessMgr(PolynMgr * pMgr_new){
    pMgr=pMgr_new;
    _prInputsTable=(pSetsTable)   new SetsTable;
    _fanInsTable = (pSetsTable)   new SetsTable;
    _sharedCutsTable= (pSetsTable)   new SetsTable;
    _removedTable = (pRewriteTable) new  RewriteTable;
    _substituteTable =(pSubstitutionTable) new SubstitutionTable;
    _eliminatedVars =(pMonomial) new Monomial;
    _eliminatedVarsL =(pMonomial) new Monomial;
    _eliminatedVarsH =(pMonomial) new Monomial;
    _mostOutputVars=(pMonomial) new Monomial;
    maxSize= pMgr->_inputVars->size()*pMgr->_inputVars->size();
  _cutsTable = (pCutsTable) new  CutsTable;
}

PreprocessMgr::~PreprocessMgr(){
    
}


pTerm PreprocessMgr::copyNewTerm(pTerm t){
    
    auto coeff=t->first;
    auto monl=t->second;
    auto newMonl= pMgr->allocateMonomial();
    newMonl->insert(monl->begin(), monl->end());
    auto newTerm=pMgr->allocateTerm(coeff, newMonl);
    return newTerm;
}

void PreprocessMgr::printPolynomial(FILE*& out,pPolynomial p, const variableId & v )
{
    fprintf(out,"v_%llu = ", v);
    pMgr->printPolynomial(out,p);
    fprintf(out,"\n\n");
    return;
}

void PreprocessMgr::printSubstitutionPolynomial(FILE*& out,pPolynomial p,Monomial* m )
{
    printMonomial(m);
     fprintf(out," = ");
    pMgr->printPolynomial(out,p);
    fprintf(out,"\n\n");
    return;
}
    
void PreprocessMgr::printSet(pVariableSet m){
    size_t i=0;
    fprintf(stdout,"{");
    for ( auto& var : *m){
            i++;
            if(i!=m->size())
                    fprintf(stdout,"v_%llu,", var);
            else
                    fprintf(stdout,"v_%llu", var);
    }
        fprintf(stdout,"}\n");
    return;
}


void PreprocessMgr::printSet(pMonomial m){
    size_t i=0;
    fprintf(stdout,"{");
    for ( auto& var : *m){
            i++;
            if(i!=m->size())
                    fprintf(stdout,"v_%llu,", var);
            else
                    fprintf(stdout,"v_%llu", var);
    }
        fprintf(stdout,"}\n");
    return;
}


void PreprocessMgr::printMonomial(Monomial* m){
    size_t i=0;
    for ( auto& var : *m){
            i++;
            if(i!=m->size())
                    fprintf(stdout,"v_%llu*", var);
            else
                    fprintf(stdout,"v_%llu", var);
    }
    return;
}

void PreprocessMgr::printSet(Monomial* m){
    size_t i=0;
    fprintf(stdout,"{");
    for ( auto& var : *m){
            i++;
            if(i!=m->size())
                    fprintf(stdout,"v_%llu,", var);
            else
                    fprintf(stdout,"v_%llu", var);
    }
        fprintf(stdout,"}");
    return;
}

void PreprocessMgr::printLUT(pTruthTable t){
    size_t i=0;
    fprintf(stdout,"{");
    for ( auto& assign : *t){
            i++;
            if(i!=t->size())
                    fprintf(stdout,"%s,", assign.to_string().c_str());
            else
                    fprintf(stdout,"%s", assign.to_string().c_str());
    }
        fprintf(stdout,"}\n");
    return;
}


void PreprocessMgr::printMonomialTable(){
    auto monlTable=pMgr->_monlTable;
    for (auto monl_it: *monlTable){
        auto inPolynIds=monl_it.second->inPolynIds;
        auto monl=monl_it.first;
        if(inPolynIds.size()==1){           
            printSet(&monl);
            fprintf(stdout," id %llu\n", *(inPolynIds.begin()));
        }
    }
}

void PreprocessMgr::printCutsTable(){
   
    for (auto cut_it: *_cutsTable){
        fprintf(stdout," v_%llu= ", cut_it.first);
        for (auto set: *cut_it.second){
            printSet(set);
        }
    }
    for (auto cut_it: *_sharedCutsTable){
        auto cut=cut_it.first;
        printSet(&cut);
        fprintf(stdout," = ");
        printSet(cut_it.second);
    }
}
void PreprocessMgr::analyzeSpecPolynomial(){
    auto specPolyn=pMgr->_specPolyn;
    if(specPolyn->empty()){
        printf("***e Error the specification equation is empty\n\n");
        throw;
    }
    EXPCoefficient largestCoeff;
    largestCoeff=absEXPCoeff((*std::prev(specPolyn->end()))->first);
    pMgr->moduloN=largestCoeff*2;
    
    for (auto term: *specPolyn){
        auto coeff=term->first;
        auto monl=term->second;
        if(monl->empty()) continue;
        assert(absEXPCoeff(coeff)<=largestCoeff);
        for(auto var:*monl){
            if(pMgr->isPrimaryOutput(var)){
                auto polynData=pMgr->getPolynomialData(var);
                EXPCoefficient modulo=(largestCoeff*2)/absEXPCoeff(coeff);
                if(modulo<64)
                    polynData->modulo=mpz_get_ui(modulo.get_mpz_t());
                else 
                    polynData->modulo=64;
                if(polynData->modulo<64)
                    _mostOutputVars->insert(var);
            }
        }
    }
    
    auto ideal=pMgr->_ideal;
    for(auto polyn_it=std::prev(ideal->end());
                polyn_it!=std::prev(ideal->begin());
                    polyn_it=std::prev(polyn_it)){
        auto polynData=(*polyn_it).second;
        auto moduloParent=polynData->modulo;
        auto childPolys=polynData->inVars;
        for(auto childId: *childPolys){
            if(pMgr->isPrimaryInput(childId)) continue;
            auto childPolynData=pMgr->getPolynomialData(childId);
            if(childPolynData->modulo<moduloParent){
                childPolynData->modulo=moduloParent;
                if(childPolynData->modulo<32){
                    _mostOutputVars->insert(childId );
                }
                else{
                    auto id_it=_mostOutputVars->find(childId);
                    if(id_it!=_mostOutputVars->end()){
                        _mostOutputVars->erase(id_it);
                    }
                }
            }
        }
                    
    }
            
    

}

bool PreprocessMgr::isXNORPolynomial(const variableId& polynId){
    
    size_t numInputs;
    auto lut=pMgr->getPolynomialLUT(polynId, numInputs);
    if(lut==NULL) return false;
    
    auto xnorLut=twoInputsXNOR(numInputs);
    
    if(xnorLut==NULL) return false;
    
    
    if( lut->size()==xnorLut->size() && *lut==*xnorLut) return true;
    
    return false;
    
}

bool PreprocessMgr::isXORPolynomial(const variableId & polynId){
    
    size_t numInputs;
    auto lut=pMgr->getPolynomialLUT(polynId, numInputs);
    if(lut==NULL) return false;
    
    auto xorLut=twoInputsXOR(numInputs);
    auto xnorLut=twoInputsXNOR(numInputs);
    
    if(xorLut==NULL || xnorLut==NULL) return false;
    
    if(lut->size()==xorLut->size() && *lut==*xorLut) return true;
    
    if( lut->size()==xnorLut->size() && *lut==*xnorLut) return true;
    
    return false;
    
}
bool PreprocessMgr::simulatePolynomial(pPolynomialData p, __Byte stim){
    
    if(p->leadTerms->empty()) return false;
    auto stimPolyn=pMgr->allocatePolynomial();
    auto newPolyn=pMgr->allocatePolynomial();
    
    pMgr->insertPolyn(stimPolyn, p->leadTerms);
    
    int i=0;
    for(auto var: *p->inVars){
        bool varValue=stim[i];
        for(auto term : *stimPolyn){
            auto monl = term->second; 
            if(isInPMonomial(monl,var)){
                if(varValue){
                    auto newTerm=copyNewTerm(term);
                    newTerm->second->erase(var);
                    pMgr->insertTerm(newPolyn, newTerm);
                }   
            }
            else{
                auto newTerm=copyNewTerm(term);
                //newPolyn->insert(newTerm);
                pMgr->insertTerm(newPolyn, newTerm);
            }
        }
        //addSimilarMonomials(newPolyn);
        stimPolyn->swap(*newPolyn);
        newPolyn->clear();
        i++;
    }
    
    if(stimPolyn->empty()) {
        return false;
    }
    else if(stimPolyn->size()==1){
        auto term=*stimPolyn->begin();
        auto coeff = term->first;
        auto monl = term->second;
        if(coeff==1&&monl->empty()){
            return true; 
        }
        //else{
            //printf("simulation %s", stim.to_string().c_str());
            //printPolynomial(stdout,p->leadTerms,0);
            //printPolynomial(stdout,stimPolyn,0);
            //assert(0);
        //}
    }
    else{ 
        printPolynomial(stdout,p->leadTerms,0);
        printPolynomial(stdout, stimPolyn,0);
        assert(0);
    }
}

void PreprocessMgr::refineRemovedVariables(pMonomial remVars){
    
    pMonomial nonremovedVars=pMgr->allocateMonomial();
    for(auto var: *remVars){
        auto varD=pMgr->getVariableData(var);
        if(varD->fanoutSize*varD->substSize> maxSize){
            nonremovedVars->insert(var);
        }
    }
    
    for(auto var: * nonremovedVars){
        remVars->erase(var);
        pMgr->_depVars->insert(var);
        _eliminatedVars->erase(var);
    }
    
    return;
}

void  PreprocessMgr::updateFaninsTable(){
    
    if(!_fanInsTable->empty()) _fanInsTable->clear();
    
    assert(!pMgr->_ideal->empty());
    for(auto polyn_it: *pMgr->_ideal){
       auto polynId= polyn_it.first;
       auto varIns=polyn_it.second->inVars;
       //fprintf(stdout,"v_%llu ", polynId);
       if(varIns->empty()) continue;
       auto varIns_it=_fanInsTable->find( *varIns);
       if(varIns_it==_fanInsTable->end()){
           pMonomial sharedPolynIds=pMgr->allocateMonomial(polynId); 
           _fanInsTable->insert(std::make_pair(*varIns,sharedPolynIds));
       }
       else{
           varIns_it->second->insert(polynId);
       }
    }
   return; 
}

void  PreprocessMgr::updatePrimaryInputsTable(){
    
    if(!_prInputsTable->empty()) _prInputsTable->clear();
    
    assert(!pMgr->_ideal->empty());
    for(auto polyn_it: *pMgr->_ideal){
       auto polynId= polyn_it.first;
       auto prIns=polyn_it.second->primaryInVars;
       assert(!prIns.empty());
       auto prIns_it=_prInputsTable->find( prIns);
       if(prIns_it==_prInputsTable->end()){
           pMonomial prIns_polynIds=pMgr->allocateMonomial(polynId); 
           _prInputsTable->insert(std::make_pair(prIns,prIns_polynIds));
       }
       else{
           prIns_it->second->insert(polynId);
       }
    }
   return; 
}

void  PreprocessMgr::updateRemovedTable(const variableId& varId){
    
    auto idSet= pMgr->getPolynomialChilds(varId);
    if(idSet== NULL) return;
    for (auto polynId: *idSet){
        auto remvTable_it= _removedTable->find(polynId);

        if(remvTable_it==_removedTable->end()){
            auto _remvVars = pMgr->allocateMonomial(varId);
            _removedTable->insert(std::make_pair(polynId, _remvVars));
        }
        else{
            remvTable_it->second->insert(varId);
        }
    }
    
   return; 
}

MonomialsSet PreprocessMgr::getCombinations(pMonomial removedVars){
        

    MonomialsSet combinations;
    combinations.insert(*removedVars);
    for(size_t k=2; k<removedVars->size(); k++){  
        std::vector<variableId> s; 
        std::copy(removedVars->begin(),removedVars->end(),std::back_inserter(s));
        do{
            Monomial monl;
            monl.insert(s.begin(), std::next(s.begin() , k));
            combinations.insert(monl);
            
        }while(next_combination(s.begin(), std::next(s.begin() , k) ,s.end()));
    }
    
    return combinations;
}


void PreprocessMgr::addSimilarMonomials(pPolynomial polyn){
    
    if(polyn->size()<2) return;
    pPolynomial simplifiedPolyn=pMgr->allocatePolynomial();
    auto it= polyn->begin();
    auto it_next=std::next(it);
    while(it_next!=polyn->end()){
        auto coeff=std::get<0>(**it);
        auto monl=std::get<1>(**it);
        auto coeff_next=std::get<0>(**it_next);
        auto monl_next=std::get<1>(**it_next);
        auto newCoeff=coeff;
        
        
        while(monl->empty()&&monl_next->empty()){
            newCoeff+=coeff_next;
            it_next=std::next(it_next);
            if(it_next==polyn->end()) break;
            coeff_next=std::get<0>(**it_next);
            monl_next=std::get<1>(**it_next);
        }
        
        while( *monl==*monl_next && (!monl->empty()|| !monl_next->empty())){
            newCoeff+=coeff_next;
            it_next=std::next(it_next);
            if(it_next==polyn->end()) break;
            coeff_next=std::get<0>(**it_next);
            monl_next=std::get<1>(**it_next);
        }
        
        if(newCoeff!=0){
            auto newMonl= pMgr->allocateMonomial();
            newMonl->insert(monl->begin(), monl->end());
            auto newTerm=pMgr->allocateTerm(newCoeff, newMonl);
            //simplifiedPolyn->insert(newTerm);
            pMgr->insertTerm(simplifiedPolyn,newTerm);
        }
        if(it_next==polyn->end()) break;
        it=it_next;
        it_next=std::next(it_next);
        if(it_next==polyn->end()){
            auto newTerm= copyNewTerm(*it);
            //simplifiedPolyn->insert(newTerm);
            pMgr->insertTerm(simplifiedPolyn,newTerm);
            break;
        }
    }
    polyn->swap(*simplifiedPolyn);
    return;
}

pPolynomial PreprocessMgr::multiplyTwoPolynomials(pPolynomial polyn1, pPolynomial polyn2){
    pPolynomial product=pMgr->allocatePolynomial();
    
    for (auto term1: *polyn1){
        auto coeff1=std::get<0>(*term1);
        auto monl1=std::get<1>(*term1);
        for (auto term2 :*polyn2){
            auto coeff2=std::get<0>(*term2);
            auto monl2=std::get<1>(*term2);
            Coefficient coeff_prod=coeff1*coeff2;
            pMonomial monl_prod= pMgr->allocateMonomial();
            monl_prod->insert(monl1->begin(), monl1->end());
            monl_prod->insert(monl2->begin(), monl2->end());
            auto term_prod=pMgr->allocateTerm(coeff_prod, monl_prod);
            //product->insert(term_prod);
            pMgr->insertTerm(product, term_prod);
        }
        //addSimilarMonomials(product);
    }

    return product;
}

pPolynomial PreprocessMgr::multiplyPolynWithMonl(pTerm term1, pPolynomial polyn2 ){
    
    pPolynomial product=pMgr->allocatePolynomial();
    
    auto coeff1=std::get<0>(*term1);
    auto monl1=std::get<1>(*term1);
    for (auto term2 :*polyn2){
        auto coeff2=std::get<0>(*term2);
        auto monl2=std::get<1>(*term2);
        Coefficient coeff_prod=coeff1*coeff2;
        pMonomial monl_prod= (pMonomial) new Monomial;
        monl_prod->insert(monl1->begin(), monl1->end());
        monl_prod->insert(monl2->begin(), monl2->end());
        auto term_prod=pMgr->allocateTerm(coeff_prod, monl_prod);
        //product->insert(term_prod);
        pMgr->insertTerm(product, term_prod);
    }
    //addSimilarMonomials(product);
    return product;
}

void PreprocessMgr::insertSubstitutionPolynomial(
                          SubstitutionTable::iterator nearest_it, const Monomial& set){
    Monomial diff;
    pMonomial faninVars=pMgr->allocateMonomial();
    pPolynomial redPolyn=pMgr->allocatePolynomial();
    if(nearest_it!=_substituteTable->end()){
        auto pSet=copytoNewMonomial(set);
        auto nearestSet=copytoNewMonomial(nearest_it->first);
        diff=getDifferenceVariables(pSet, nearestSet);
        redPolyn=nearest_it->second;
    }
    else{
        diff=set;
        auto polynTerms=pMgr->getPolynomialLeadTerms(*diff.begin());
        pMgr->insertPolyn(redPolyn, polynTerms);
        diff.erase(diff.begin());
    }
    assert(!diff.empty());
    
    for(auto var:diff){
        pPolynomial varPolyn= pMgr->getPolynomialLeadTerms(var);
        redPolyn=multiplyTwoPolynomials(redPolyn, varPolyn);
    }
         
    _substituteTable->insert(std::make_pair(set, redPolyn));
    
    //if(fVerbose>0)
      //  printSubstitutionPolynomial(stdout, redPolyn, &set);

   return; 
}

void  PreprocessMgr::updateSubstitutionTable(const variableId & polynId, pMonomial removedVars){
    
    if(removedVars->size()<2)return;
    
    MonomialsSet combinations;

    auto currPolyn= pMgr->getPolynomialLeadTerms(polynId);
    
    for(auto term: *currPolyn){
        auto monl= term->second;
        auto inters=getIntersectedVariables(monl, removedVars);
        combinations.insert(inters);
    }
    
    Monomial nearestSubset;
    SubstitutionTable::iterator nearest_it;

    for(auto set: combinations){
        if(set.size()<2) continue;
        if(_substituteTable->size()<2){
            nearest_it=_substituteTable->end();
        }
        else{
            auto set_it= _substituteTable->lower_bound(set);
            if(set_it== _substituteTable->end()){
                nearest_it=std::prev(set_it);
                //--nearest_it;
                nearestSubset= nearest_it->first;
            }
            else if(set_it->first!=set){
                if(set_it== _substituteTable->begin()){
                    nearest_it=set_it;
                }else{
                    nearest_it=std::prev(set_it);
                }
                nearestSubset= nearest_it->first;
            }
            else{ continue;}

            if(!std::includes(set.begin(), set.end(),
                    nearestSubset.begin(), nearestSubset.end())){
                nearest_it=_substituteTable->end();
            }
        }
        
        insertSubstitutionPolynomial(nearest_it, set);
    }
    
   return; 
}

ppMonomialsSet PreprocessMgr::findVariableCuts(int k, variableId polynId, 
                                                        pMonomial varIns ){
 
       auto selfCut= pMgr->allocateMonomial(polynId);
       auto kCuts = (ppMonomialsSet) new  pMonomialsSet;
       auto tmpCuts = (ppMonomialsSet) new  pMonomialsSet;
       assert(!varIns->empty());
       for (auto inVar: *varIns){
           ppMonomialsSet varCuts;
           if(pMgr->isPrimaryInput(inVar)){
                auto newCut=pMgr->allocateMonomial(inVar);
                varCuts = (ppMonomialsSet) new pMonomialsSet;
                varCuts->insert(newCut);
           }
           else{
               varCuts=getVariableCuts(inVar);
           }
           
            if(kCuts->empty()){
                kCuts->insert(varCuts->begin(),varCuts->end());
            }
            else{
                for (auto kcut: *kCuts){
                    for(auto vcut: *varCuts){
                        auto newCut=pMgr->allocateMonomial();
                        newCut->insert(kcut->begin(), kcut->end());
                        newCut->insert(vcut->begin(),vcut->end());
                        if(newCut->size()<=k) tmpCuts->insert(newCut);
                    }
                }
                kCuts->swap(*tmpCuts);
                tmpCuts->clear();
            }
       }
       kCuts->insert(selfCut);
       return kCuts;
    
}

void  PreprocessMgr::findAllCuts(int k){
    
    if(!_cutsTable->empty()) _cutsTable->clear();
    if(!_sharedCutsTable->empty()) _sharedCutsTable->clear();
    
    assert(!pMgr->_ideal->empty());
    for(auto polyn_it: *pMgr->_ideal){
       auto polynId= polyn_it.first;
       auto varIns=polyn_it.second->inVars;
        auto kCuts=findVariableCuts(k,polynId,varIns);
       _cutsTable->insert(std::make_pair(polynId,kCuts));
       
    }
    
   return; 
}

void PreprocessMgr::updateSharedCutsTable(){
    _sharedCutsTable->clear();
    for (auto kCuts_it:*_cutsTable){
        auto kCuts=kCuts_it.second;
        auto polynId=kCuts_it.first;
        for(auto cut: *kCuts){
           if(cut->size()<2) continue;
           auto cut_it=_sharedCutsTable->find( *cut);
           if(cut_it==_sharedCutsTable->end()){
               pMonomial sharedPolynIds=pMgr->allocateMonomial(polynId); 
               _sharedCutsTable->insert(std::make_pair(*cut,sharedPolynIds));
           }
           else{
               cut_it->second->insert(polynId);
           }
        }
    }
    return;
}

void PreprocessMgr::refineDepVarsPrimaryInputs(){
    
    updatePrimaryInputsTable();
    _removedTable->clear();
    for (auto table_it: *_prInputsTable){
        auto polynIds=table_it.second;
        
        if (polynIds->size()==1){
            auto removedPolyn=*polynIds->begin();
            markEliminatedVariables(removedPolyn);
        }
    }
    return;
}

void PreprocessMgr::refineDepVarsFanIns(){
    updateFaninsTable();
    _removedTable->clear();
    for (auto table_it: *_fanInsTable){
        auto polynIds=table_it.second;
        auto inVar=table_it.first;
        if (polynIds->size()>1){
            
            auto lastPolynId_it=std::prev(polynIds->end());
            Monomial erasedVars;
            while(lastPolynId_it!=polynIds->begin()){
                pMonomial remainVars=pMgr->allocateMonomial();
                pMonomial brotherVars=pMgr->allocateMonomial();
                remainVars->insert(polynIds->begin(), lastPolynId_it);
                auto childPolyns=pMgr->getPolynomialChilds(*lastPolynId_it);
                if(childPolyns==NULL){
                    lastPolynId_it=std::prev(lastPolynId_it);
                    continue;
                }
                for (auto polyn_it:*childPolyns){
                   auto faninVars=pMgr->getPolynomialFanins(polyn_it);
                   brotherVars->insert(faninVars->begin(), faninVars->end());
                }
                auto intersectVars=getIntersectedVariables(remainVars, brotherVars);
                
                if(!intersectVars.empty()){ 
                   erasedVars.insert(intersectVars.begin(), intersectVars.end());
                   erasedVars.insert(*lastPolynId_it);
                }
                
                lastPolynId_it=std::prev(lastPolynId_it);
                
            }
            for(auto var: erasedVars){
                markEliminatedVariables(var);
            }
        }
    }
    
   return; 
}

void PreprocessMgr::refineDepVarsCuts(const int k){
    //findAllCuts(k);
    //printCutsTable();
    updateSharedCutsTable();
    Monomial erasedVars;

    for (auto cut_it: *_sharedCutsTable){
        if(cut_it.first.size()>k) continue;
        auto polynIds=cut_it.second;
        if(polynIds->size()<2) continue;
        auto lastPolynId_it=std::prev(polynIds->end());
        while(lastPolynId_it!=polynIds->begin()){
            pMonomial remainVars=pMgr->allocateMonomial();
            remainVars->insert(polynIds->begin(), lastPolynId_it);
            auto fanins=pMgr->getPolynomialFanins(*lastPolynId_it);
            if(fanins->size()>1){
                auto intersectVars=getIntersectedVariables(remainVars, fanins);
                for(auto var : intersectVars){
                    if(pMgr->isPrimaryInput(var)) continue;
                    auto childs=pMgr->getPolynomialChilds(var);
                    if(childs->size()==1){ 
                        erasedVars.insert(var);
                    }
                }
            }
            lastPolynId_it=std::prev(lastPolynId_it);
        }
    }
    for(auto var: erasedVars){
        markEliminatedVariables(var);
    }
}
void PreprocessMgr::refineDepVarsCoupling(){

    _removedTable->clear();
    //renewMonomialTable();
    auto monlTable=pMgr->_monlTable;
    MonomialsSet candidates;
    MonomialsSet groupCandidates;
    for (auto monl_it: *monlTable){
        auto inPolynIds=monl_it.second->inPolynIds;
        auto monl=monl_it.first;
        if(inPolynIds.size()==1){  
            auto set_it= candidates.find(monl);
            if(set_it== candidates.end()){
                    candidates.insert(monl);
            }
        }
    }
    if(candidates.empty()) return;
    auto lastMonl_it=std::prev(candidates.end());
    MonomialsSet visitedMonls;
    while(lastMonl_it!=candidates.begin()){
 
        auto pLastMonl=copytoNewMonomial(*lastMonl_it);

        for (auto it =candidates.begin(); it!=lastMonl_it; it=std::next(it)){
            auto pMonl=copytoNewMonomial(*it);
            auto intersVars= getIntersectedVariables(pMonl, pLastMonl);
            if(!intersVars.empty()){
                pLastMonl->insert(pMonl->begin(), pMonl->end());
                visitedMonls.insert(*pMonl);
            }
        }
        groupCandidates.insert(*pLastMonl);
        do{
            lastMonl_it=std::prev(lastMonl_it);
        }while(visitedMonls.find(*lastMonl_it)!= visitedMonls.end()&&
               lastMonl_it!=candidates.begin() );
    }
    
    for(auto group : groupCandidates){
        auto pGroup=copytoNewMonomial(group);
        auto miniGroups=groupVariablesBasedFanin(pGroup);
        for (auto miniGroup: miniGroups){
           auto set=miniGroup.first;
           auto inVars=miniGroup.second;
           auto pSet=copytoNewMonomial(set);
           auto diff=getDifferenceVariables(pSet, inVars);
           for(auto varId: diff){
               markEliminatedVariables(varId);
           }
        }
    }
  
    //auto illPolyn=pMgr->allocateMonomial();
    for (auto monl_it: *monlTable){
        auto inPolynIds=monl_it.second->inPolynIds;
        auto monl=monl_it.first;
        if(inPolynIds.size()!=1) continue;     
        
        auto inVars=pMgr->getPolynomialFanins(*inPolynIds.begin());
        //auto pMonl=copytoNewMonomial(monl);
        //auto groups=groupVariablesBasedFanin(pMonl);
        auto groups=groupVariablesBasedFanin(inVars);

        for (auto group: groups){
            auto set=group.first;
            auto inVars=group.second;
            auto pSet=copytoNewMonomial(set);
            auto diff=getDifferenceVariables(pSet, inVars);
            for(auto varId: diff){
                markEliminatedVariables(varId);
            }
        }
       
    }
    
    /*for (auto polynId_it: *pMgr->_ideal){
    auto polynId=polynId_it.first;
    auto polynData=polynId_it.second;
        if(polynData->leadTerms->size()<=2){
             markEliminatedVariables(polynId);
        }
    }*/
    
   return; 
}


void PreprocessMgr::refineDepCutsCoupling(){

    _removedTable->clear();
    //renewMonomialTable();
    const int k=6;
    findAllCuts(k);
    
    MonomialCutsTable _revMonlCuts;
    auto monlTable=pMgr->_monlTable;
    pMonomialsSet candidates;
    Monomial erasedVars;
    //MonomialsSet groupCandidates;
    for (auto monl_it: *monlTable){
        auto inPolynIds=monl_it.second->inPolynIds;
        auto monl=monl_it.first;
        auto pMonl=copytoNewMonomial(monl);
        if(inPolynIds.size()==1){  
            //auto set_it= candidates.find(pMonl);
            //if(set_it== candidates.end()){
            candidates.insert(pMonl);
            //}
        }
    }
    if(candidates.empty()) return;
    
    for (auto monl: candidates){
       auto kCuts = (ppMonomialsSet) new  pMonomialsSet;
       auto tmpCuts = (ppMonomialsSet) new  pMonomialsSet;
       for (auto inVar: *monl){
           ppMonomialsSet varCuts;
           if(pMgr->isPrimaryInput(inVar)){
                auto newCut=pMgr->allocateMonomial(inVar);
                varCuts = (ppMonomialsSet) new pMonomialsSet;
                varCuts->insert(newCut);
           }
           else{
                auto inVar_it=_cutsTable->find(inVar);
                if(inVar_it!=_cutsTable->end()){
                    varCuts=inVar_it->second;
                }
                else{
                    assert(0);
                }
            }
            if(kCuts->empty()){
                kCuts->insert(varCuts->begin(),varCuts->end());
            }
            else{
                for (auto kcut: *kCuts){
                    for(auto vcut: *varCuts){
                        auto newCut=pMgr->allocateMonomial();
                        newCut->insert(kcut->begin(), kcut->end());
                        newCut->insert(vcut->begin(),vcut->end());
                        if(newCut->size()<=k) tmpCuts->insert(newCut);
                    }
                }
                kCuts->swap(*tmpCuts);
  
                tmpCuts->clear();
            }
       }
        printSet(monl);
        fprintf(stdout," = ");
        for(auto cut: *kCuts){
           if(cut->size()<2) continue;
           auto cut_it=_revMonlCuts.find( *cut);
           if(cut_it==_revMonlCuts.end()){
               ppMonomialsSet sharedMonls=(ppMonomialsSet) new pMonomialsSet;
               sharedMonls->insert(monl);
               _revMonlCuts.insert(std::make_pair(*cut,sharedMonls));
           }
           else{
               cut_it->second->insert(monl);
           }

            printSet(cut);
        }
         fprintf(stdout,"\n\n");
   
    }
    
   for (auto cut_it: _revMonlCuts){
        if(cut_it.second->size()<2) continue;
        auto cut=cut_it.first;
        printSet(&cut);
        fprintf(stdout," = ");
        for(auto monl: *cut_it.second){
            printSet(monl);
        }
        fprintf(stdout,"\n\n");
    }
    
   /* for (auto cut_it: _revMonlCuts){
        if(cut_it.second->size()<2) continue;
        auto cut=cut_it.first;
        pMonomial varIds= copytoNewMonomial(cut);
        //auto varIds= pMgr->allocateMonomial();
        pMonomial allVars=pMgr->allocateMonomial();
        for(auto monl: *cut_it.second){
           allVars->insert(monl->begin(), monl->end());
           varIds=getSymmetricDifferenceVariables(varIds,monl);
        }
        auto lastVar_it=std::prev(allVars->end());
        while(lastVar_it!=allVars->begin()){
            pMonomial remainVars=pMgr->allocateMonomial();
            remainVars->insert(allVars->begin(), lastVar_it);
            if(pMgr->isPrimaryInput(*lastVar_it)) break;
            auto fanins=pMgr->getPolynomialFanins(*lastVar_it);
            if(fanins->size()>1&&!isInMonomial(&cut,*lastVar_it)){
                auto intersectVars=getIntersectedVariables(varIds, fanins);
                if(!intersectVars.empty()){ 
                    erasedVars.insert(*lastVar_it);
                }
            }
            lastVar_it=std::prev(lastVar_it);
        }
        for (auto var:*varIds){
            if(!isInMonomial(&cut,var)){
              erasedVars.insert(var);  
            }
        }
    }*/
    for (auto cut_it: _revMonlCuts){
        if(cut_it.second->size()<2) continue;
        auto cut=cut_it.first;
        pMonomial varIds= copytoNewMonomial(cut);
        for(auto monl: *cut_it.second){
           auto diffVars=getDifferenceVariables(monl,varIds);
           if(!diffVars.empty()){ 
                erasedVars.insert(diffVars.begin(),diffVars.end());
            }
        }
    }
    for(auto var: erasedVars){
        markEliminatedVariables(var);
       // printf("elem:%llu\n",var);
    }
   return; 
}


void PreprocessMgr::refineDepVarsSingular(){

    _removedTable->clear();
    for (auto polynId_it: *pMgr->_varTable){
        auto varId=polynId_it.first;
        auto varData=polynId_it.second->inPolynIds;
        if(varData->size()<=1){
            markEliminatedVariables(varId);
        }
        
    }
    
    
   return; 
}


void PreprocessMgr::polynomialFactorization(){

    pMonomial xVars=pMgr->allocateMonomial();
    for (auto polynId_it: *pMgr->_ideal){
        auto polynId=polynId_it.first;
        auto polynData=polynId_it.second;
            xVars->insert(polynId);
    }
    
    for(auto var_it=std::prev(xVars->end()); var_it!=std::prev(xVars->begin());
    var_it=std::prev(var_it)){
        auto var=*var_it;
       auto childs=pMgr->getPolynomialChilds(var);
       bool enableInversion=true;
       if(childs->size()<=1) continue;
       for(auto polynId: *childs){
            auto currPolyn=pMgr->getPolynomialLeadTerms(polynId);
            auto newPolyn=invertVariableWithinPolynomial(currPolyn, var);
            if(newPolyn->size()>=currPolyn->size()){
                enableInversion=false;
                break;
            }
        }
        if(enableInversion){
            for(auto polynId: *childs){
                auto currPolyn=pMgr->getPolynomialLeadTerms(polynId);
                auto newPolyn=invertVariableWithinPolynomial(currPolyn, var);          
                eraseFromMonomialTable(polynId);
                currPolyn->swap(*newPolyn);
                addToMonomialTable(polynId);
            }
            auto leadTerms=pMgr->getPolynomialLeadTerms(var);
            for(auto term: *leadTerms){
                term->first*=-1;
            }
            auto monl1=pMgr->allocateMonomial();
            auto term1=pMgr->allocateTerm(1, monl1);
            pMgr->insertTerm(leadTerms, term1);
        }
    }
    renewMonomialTable( );
    //if(fVerbose>0)
    //    pMgr->printIdeal(stdout, fVerbose); 
   return; 
}

void PreprocessMgr::refineDepVarsXORs(){

    _removedTable->clear();
    pMonomial keepedVars=pMgr->allocateMonomial();
    for (auto polynId_it: *pMgr->_ideal){
        auto polynId=polynId_it.first;
        auto polynData=polynId_it.second;
        if(isXORPolynomial(polynId)){
            keepedVars->insert(polynId);
            for(auto var: *polynData->inVars){
                if(!pMgr->isPrimaryInput(var)){
                    keepedVars->insert(var);
                }
            }
        }
    }
    
    updateFaninsTable();
    for (auto table_it: *_fanInsTable){
        auto polynIds=table_it.second;
        auto inVar=table_it.first;
        if (polynIds->size()<=1) continue;
            
        for(auto var: *polynIds){
            keepedVars->insert(var);
        }
    }
    auto erasedVars=getDifferenceVariables( pMgr->_depVars, keepedVars);
    
    for(auto var: erasedVars){
        markEliminatedVariables(var);
    }
    
   return; 
}


pPolynomial PreprocessMgr::inferSimplerMonomials(pMonomial pSet, pMonomial inVars){
    
    pPolynomial redPolyn=pMgr->allocatePolynomial();
    auto diff=getDifferenceVariables(pSet, inVars);
    auto intersVars= getIntersectedVariables(pSet, inVars);
    
    while(pMgr->isPrimaryInput(*diff.begin())){
        intersVars.insert(*diff.begin());
        diff.erase(diff.begin());
    }
    if(!diff.empty()){
        auto polynTerms=pMgr->getPolynomialLeadTerms(*diff.begin());
        //redPolyn->insert(polynTerms->begin(), polynTerms->end());
        pMgr->insertPolyn(redPolyn, polynTerms);

        auto inputMonl= copytoNewMonomial(intersVars);
        auto inputTerm=pMgr->allocateTerm(1, inputMonl);
        redPolyn=multiplyPolynWithMonl(inputTerm, redPolyn);

        for(auto var:diff){
            if(var==*diff.begin()) continue;
            pPolynomial varPolyn= pMgr->getPolynomialLeadTerms(var);
            redPolyn=multiplyTwoPolynomials(redPolyn, varPolyn);
        }
        
        if(redPolyn->empty()){
            _inferenceAnalysis.nonIntersected++;
            return redPolyn;
        }
        if(redPolyn->size()==1){
            auto firstTerm=*redPolyn->begin();
            auto firstMonl=firstTerm->second;
            if(isSubmonomial(firstMonl, pSet)
                    ||firstMonl->empty()
                    ||pMgr->isPrimaryInputMonomial(firstMonl)){
                _inferenceAnalysis.subsumed++;
                return redPolyn;
            }
        }

        auto sortedRedPolyn=copytoSortedPolynomial(redPolyn);
        auto lastTerm=*std::prev(sortedRedPolyn->end());
        pMonomial lastMonomial=lastTerm->second;
        auto lastMonomial_it= pMgr->_monlTable->find(*lastMonomial);
        if(lastMonomial_it!=pMgr->_monlTable->end()){
            auto potentialPolyn=lastMonomial_it->second->inPolynIds;
        
            for(auto var:potentialPolyn){
                auto polynTerms=pMgr->getPolynomialLeadTerms(var);
                auto newPolynomial= pMgr->allocatePolynomial();
                //newPolynomial->insert(redPolyn->begin(), redPolyn->end());
                pMgr->insertPolyn(newPolynomial, redPolyn);
                for(auto term: *polynTerms){
                    auto newTerm=copyNewTerm(term);
                    newTerm->first=newTerm->first*-1;
                    //newPolynomial->insert(newTerm);
                    pMgr->insertTerm(newPolynomial,newTerm);
                }
                if(newPolynomial->empty()){
                    _inferenceAnalysis.subsumed++;
                    auto newMonl= pMgr->allocateMonomial(var);
                    assert(!isSubmonomial( newMonl, inVars));
                    auto newTerm=pMgr->allocateTerm(1,newMonl);
                    //newPolynomial->insert(newTerm);
                    pMgr->insertTerm(newPolynomial,newTerm);
                    return newPolynomial;
                }
            }
        }
    
        if(redPolyn->size()<2*maxSize){
            applyInferenceRules(redPolyn); //recurrence Inference
            if(redPolyn->empty()){
                _inferenceAnalysis.deep++;
                return redPolyn;
            }
        }
        
        redPolyn->clear();
    }
    auto newTerm=pMgr->allocateTerm(1,pSet);
    //redPolyn->insert(newTerm);
    pMgr->insertTerm(redPolyn,newTerm);
    return redPolyn;
}

pPolynomial PreprocessMgr::inferSimplerMonomialsfromCuts(pMonomial pSet, pMonomial inVars){
    
    pPolynomial redPolyn=pMgr->allocatePolynomial();
    auto diff=getDifferenceVariables(pSet, inVars);
    auto intersVars= getIntersectedVariables(pSet, inVars);
    
    while(pMgr->isPrimaryInput(*diff.begin())){
        intersVars.insert(*diff.begin());
        diff.erase(diff.begin());
    }
    Monomial extraVars;
    pMonomial newFanins=pMgr->allocateMonomial();
    for(auto var:diff){
        auto fanins=pMgr->getPolynomialFanins(var);
        auto deepDiff= getDifferenceVariables(fanins, inVars);
        while(!deepDiff.empty()){
            newFanins->clear();
            while(pMgr->isPrimaryInput(*deepDiff.begin())){
                deepDiff.erase(deepDiff.begin());
            }
            extraVars.insert(deepDiff.begin(),deepDiff.end());
            for(auto deVar: deepDiff){
                auto inFanins=pMgr->getPolynomialFanins(deVar);
               newFanins->insert(inFanins->begin(),inFanins->end());
            }
            deepDiff= getDifferenceVariables(newFanins, inVars);
        }
    }
    /*printSet(pSet);
    printSet(inVars);
    printSet(&extraVars);
    printf("\n\n");*/
    diff.insert(extraVars.begin(),extraVars.end());
    if(!diff.empty()&&extraVars.size()<6){
        auto inputMonl= copytoNewMonomial(*pSet);
        auto inputTerm=pMgr->allocateTerm(1, inputMonl);
        pMgr->insertTerm(redPolyn, inputTerm);
        pPolynomial newPolyn=pMgr->allocatePolynomial();
        pPolynomial product;

      while (!diff.empty()){
        auto firstElement_it=std::prev(diff.end());
        auto elimVar=*firstElement_it;
        //printf(" var %llu\n", elimVar);
        diff.erase(firstElement_it);
        
        auto subsPolyn=pMgr->getPolynomialLeadTerms(elimVar);
        
        for(auto currTerm: *redPolyn){
            auto currCoeff=std::get<0>(*currTerm); 
            auto currMonl=std::get<1>(*currTerm);

            auto newMonl= pMgr->allocateMonomial();
            newMonl->insert(currMonl->begin(), currMonl->end());
            auto newTerm = pMgr->allocateTerm(currCoeff, newMonl);

            if(currMonl->empty()){ pMgr->insertTerm(newPolyn,newTerm); continue; }

            if(isInPMonomial(currMonl, elimVar)){
                 newMonl->erase(elimVar);
            }
            else{   
                pMgr->insertTerm(newPolyn,newTerm); 
                continue; 
            }
           
            product=multiplyPolynWithMonl(newTerm, subsPolyn);
            
            assert(product!=NULL);

            pMgr->insertPolyn(newPolyn, product);
        }
        
        redPolyn->swap(*newPolyn);
        newPolyn->clear();
    }
        
        if(redPolyn->empty()){
            _inferenceAnalysis.nonIntersected++;
            return redPolyn;
        }
        if(redPolyn->size()==1){
            auto firstTerm=*redPolyn->begin();
            auto firstMonl=firstTerm->second;
            if(isSubmonomial(firstMonl, pSet)
                    ||firstMonl->empty()
                    ||pMgr->isPrimaryInputMonomial(firstMonl)){
                _inferenceAnalysis.subsumed++;
                return redPolyn;
            }
        }

        auto sortedRedPolyn=copytoSortedPolynomial(redPolyn);
        auto lastTerm=*std::prev(sortedRedPolyn->end());
        pMonomial lastMonomial=lastTerm->second;
        auto lastMonomial_it= pMgr->_monlTable->find(*lastMonomial);
        if(lastMonomial_it!=pMgr->_monlTable->end()){
            auto potentialPolyn=lastMonomial_it->second->inPolynIds;
        
            for(auto var:potentialPolyn){
                if(var==1904) continue;
                auto polynTerms=pMgr->getPolynomialLeadTerms(var);
                auto newPolynomial= pMgr->allocatePolynomial();
                //newPolynomial->insert(redPolyn->begin(), redPolyn->end());
                pMgr->insertPolyn(newPolynomial, redPolyn);
                for(auto term: *polynTerms){
                    auto newTerm=copyNewTerm(term);
                    newTerm->first=newTerm->first*-1;
                    //newPolynomial->insert(newTerm);
                    pMgr->insertTerm(newPolynomial,newTerm);
                }
                if(newPolynomial->empty()){
                    _inferenceAnalysis.subsumed++;
                    auto newMonl= pMgr->allocateMonomial(var);
                    assert(!isSubmonomial( newMonl, inVars));
                    auto newTerm=pMgr->allocateTerm(1,newMonl);
                    //newPolynomial->insert(newTerm);
                    pMgr->insertTerm(newPolynomial,newTerm);
                    return newPolynomial;
                }
            }
        }
        
        redPolyn->clear();
    }
    auto newTerm=pMgr->allocateTerm(1,pSet);
    //redPolyn->insert(newTerm);
    pMgr->insertTerm(redPolyn,newTerm);
    return redPolyn;
}

void PreprocessMgr::insertSimplifiedPolynomial (const Monomial & set, pMonomial invars,  
                                                pSubstitutionTable subsumedTable){
   
    
    auto pSet=copytoNewMonomial(set);

///    auto reducedPolyn=inferSimplerMonomialsfromCuts(pSet, invars);
    auto reducedPolyn=inferSimplerMonomials(pSet, invars);
    
    if(reducedPolyn->empty()){
        subsumedTable->insert(std::make_pair(set, reducedPolyn));
    }
    else if(reducedPolyn->size()==1){
        auto newMonl=(*reducedPolyn->begin())->second;
        if(newMonl->empty()){
            reducedPolyn->clear();
            subsumedTable->insert(std::make_pair(set, reducedPolyn));
        }
        else if (*newMonl!=*pSet){
            subsumedTable->insert(std::make_pair(set, reducedPolyn));
        }

    }
    return;
}

pSubstitutionTable PreprocessMgr::getSharedSupportingVariables( pMonomial faninVars){
    pSubstitutionTable subsumedTable= (pSubstitutionTable)new SubstitutionTable;

    if(faninVars->empty()) return subsumedTable;
    auto lastVar_it=std::prev(faninVars->end());
    
    while(lastVar_it!=faninVars->begin()){
        if(pMgr->isPrimaryInput(*lastVar_it)) break;
        auto inputsLastVar= pMgr->getPolynomialFanins(*lastVar_it);
        if(inputsLastVar->size()>inferenceDepth){ 
            lastVar_it=std::prev(lastVar_it);
            continue;
        }
        pMonomial remainVars=pMgr->allocateMonomial();
        remainVars->insert(faninVars->begin(), lastVar_it);
        auto intersVars= getIntersectedVariables(remainVars, inputsLastVar);
        
        Monomial subsumedSupported;
        Monomial totalIntersVars;
        if(!intersVars.empty()){
            subsumedSupported.insert(intersVars.begin(), intersVars.end());
            totalIntersVars.insert(intersVars.begin(), intersVars.end());
            auto sharedVars= copytoNewMonomial(intersVars);
            subsumedSupported.insert(*lastVar_it);
            insertSimplifiedPolynomial(subsumedSupported, sharedVars, subsumedTable);            
        }
        else{
            subsumedSupported.insert(*lastVar_it);
        }
 

        for (auto it =faninVars->begin(); it!=lastVar_it; it=std::next(it)){
            if(pMgr->isPrimaryInput(*it)) continue;
            auto inputs= pMgr->getPolynomialFanins(*it);
            if(inputs->size()>inferenceDepth) continue;
            auto intersVars= getIntersectedVariables(inputs, inputsLastVar);
            if(intersVars.size()>1){
                Monomial subsumedPair;
                subsumedPair.insert(*lastVar_it);
                subsumedPair.insert(*it);
                auto sharedVars=copytoNewMonomial(intersVars);
                insertSimplifiedPolynomial(subsumedPair,sharedVars, subsumedTable);
                
                subsumedSupported.insert(*it);
                totalIntersVars.insert(intersVars.begin(), intersVars.end());
                auto totalSharedVars= copytoNewMonomial(totalIntersVars);
                insertSimplifiedPolynomial(subsumedSupported, totalSharedVars, subsumedTable);
            }
        }
        
        lastVar_it=std::prev(lastVar_it);
    }
    
    return subsumedTable;
}

pSubstitutionTable PreprocessMgr::getSharedCutsForInference( pMonomial faninVars){
    pSubstitutionTable subsumedTable= (pSubstitutionTable)new SubstitutionTable;

    if(faninVars->empty()) return subsumedTable;
    auto lastVar_it=std::prev(faninVars->end());
    
    while(lastVar_it!=faninVars->begin()){
        if(pMgr->isPrimaryInput(*lastVar_it)) break;
        auto inputsLastVar= pMgr->getPolynomialFanins(*lastVar_it);
        if(inputsLastVar->size()>6){ 
            lastVar_it=std::prev(lastVar_it);
            continue;
        }
        auto cutsVar=getVariableCuts(*lastVar_it);
        if (cutsVar->empty()){
            lastVar_it=std::prev(lastVar_it);
            continue;
        } 
        pMonomial remainVars=pMgr->allocateMonomial();
        remainVars->insert(faninVars->begin(), lastVar_it);
        pMonomial totalSharedVars= pMgr->allocateMonomial();
        pMonomial totalCuts= pMgr->allocateMonomial();
        Monomial subsumedSupported;
        subsumedSupported.insert(*lastVar_it);
        for(auto brotherVar: *remainVars){
            ppMonomialsSet brotherCuts;
            if(pMgr->isPrimaryInput(brotherVar)){
                auto selfCut=pMgr->allocateMonomial(brotherVar);
                brotherCuts=(ppMonomialsSet ) new pMonomialsSet;
                brotherCuts->insert(selfCut);
            }
            else{
                auto inputs= pMgr->getPolynomialFanins(brotherVar);
                if(inputs->size()>6) continue;
                brotherCuts= getVariableCuts(brotherVar);
            }
            pMonomial bestBroCut=NULL;
            pMonomial bestCut=NULL;
            for(auto cut:*cutsVar){
                for (auto broCut: *brotherCuts){
                    auto intersVars= getIntersectedVariables(broCut, cut);
                    if(intersVars==*broCut){
                        bestBroCut=broCut;
                        if(bestCut==NULL||*std::prev(bestCut->end())>*std::prev(cut->end()))
                            bestCut=cut;
                        break;
                    }
                }                
            }
            if(bestBroCut!=NULL){
                Monomial subsumedPair;
                subsumedPair.insert(*lastVar_it);
                subsumedPair.insert(brotherVar);
                auto sharedVars=copytoNewMonomial(*bestCut);
                sharedVars->insert(bestBroCut->begin(),bestBroCut->end());
                if(bestCut->size()-bestBroCut->size()<3)
                insertSimplifiedPolynomial(subsumedPair,sharedVars, subsumedTable);
                
                subsumedSupported.insert(brotherVar);
                totalSharedVars->insert(bestBroCut->begin(), bestBroCut->end());

                if(subsumedSupported.size()>2&&bestCut->size()-totalSharedVars->size()<3){
                    totalCuts->insert(totalSharedVars->begin(), totalSharedVars->end());
                    totalCuts->insert(bestCut->begin(), bestCut->end());
                    insertSimplifiedPolynomial(subsumedSupported, totalCuts, subsumedTable); 
                    totalCuts->clear();
                }
            }
        }
        
        lastVar_it=std::prev(lastVar_it);
    }
    
    return subsumedTable;
}

SetsTable PreprocessMgr::groupVariablesBasedFanin( pMonomial faninVars){
    SetsTable subsumedGroups;

    auto lastVar_it=std::prev(faninVars->end());
    
    while(lastVar_it!=faninVars->begin()){
        if(pMgr->isPrimaryInput(*lastVar_it)) break;
        auto inputsLastVar= pMgr->getPolynomialFanins(*lastVar_it);
        pMonomial remainVars=pMgr->allocateMonomial();
        remainVars->insert(faninVars->begin(), lastVar_it);
        auto intersVars= getIntersectedVariables(remainVars, inputsLastVar);
        
        Monomial subsumedSupported;
        if(intersVars.size()>1){
            subsumedSupported.insert(intersVars.begin(), intersVars.end());
            auto sharedVars= copytoNewMonomial(intersVars);
            subsumedSupported.insert(*lastVar_it);
            subsumedGroups.insert(std::make_pair(subsumedSupported, sharedVars));            
        }
        
        lastVar_it=std::prev(lastVar_it);
    }
    
    return subsumedGroups;
}


void PreprocessMgr::replaceWithSimplerMonomials(pPolynomial leadTerms,
                                                  pSubstitutionTable simplerMonomials ){
    pPolynomial newTerms=pMgr->allocatePolynomial();
    for(auto set: *simplerMonomials){
        auto pSet=copytoNewMonomial(set.first);
        pPolynomial reducedPolyn= set.second;
        //printSet(pSet);
        //pMgr->printPolynomial(stdout, reducedPolyn);
        Polynomial erasedTerms; 
       for(auto term: *leadTerms){
           auto monl=term->second;
           auto coeff=term->first;
           if(monl->size()<=1||coeff==0) continue;
           auto intersVars= getIntersectedVariables(pSet, monl);
           if(intersVars.size()!=pSet->size()) continue;

            if(reducedPolyn->empty()){
                term->first=0;
                erasedTerms.insert(term);
            }
            else if(reducedPolyn->size()==1){
                auto newMonl=(*reducedPolyn->begin())->second;
                if (*newMonl!=*pSet){
                    erasedTerms.insert(term);
                    auto diffVars=getDifferenceVariables(monl, pSet);
                    auto insertMonl= copytoNewMonomial(diffVars);
                    insertMonl->insert(newMonl->begin(), newMonl->end());
                    auto newterm=pMgr->allocateTerm(term->first, insertMonl);
                    pMgr->insertTerm(newTerms, newterm);
                }
            }                           
        }
        for(auto eraseTerm: erasedTerms){
            leadTerms->erase(eraseTerm);
        }
        //addSimilarMonomials(leadTerms);
    } 
    pMgr->insertPolyn(leadTerms, newTerms);
    return;
}


void PreprocessMgr::applyInferenceRules(pPolynomial leadTerms ){

  // abctime clk_infer = Abc_Clock();
    
   if(currDepth>inferenceDepth) return;
   else currDepth++;

   auto faninVars=pMgr->allocateMonomial();
   for(auto term: *leadTerms){
       auto monl=term->second;
       for(auto var:*monl){
           faninVars->insert(var);
       }
   }
    
   abctime clk_shared = Abc_Clock();
   auto subsumedGroups= getSharedSupportingVariables(faninVars);
   //auto subsumedGroups= getSharedCutsForInference(faninVars);
   timeAnalysis.shared+= Abc_Clock() - clk_shared;
   abctime clk_replace = Abc_Clock();
   replaceWithSimplerMonomials(leadTerms, subsumedGroups);
   timeAnalysis.replace+= Abc_Clock() - clk_replace;
   //addSimilarMonomials(leadTerms);
   
   //timeAnalysis.inference+= Abc_Clock() - clk_infer;
   return;    
}

void PreprocessMgr::applyInferenceRules(const variableId& polynId){
       
   auto polynData= pMgr->getPolynomialData(polynId);
   auto faninVars=polynData->inVars;
   
   abctime clk_shared = Abc_Clock();
   auto subsumedGroups= getSharedSupportingVariables(faninVars);
   timeAnalysis.shared+= Abc_Clock() - clk_shared;
   abctime clk_replace = Abc_Clock();
   replaceWithSimplerMonomials(polynData->leadTerms, subsumedGroups);
   timeAnalysis.replace+= Abc_Clock() - clk_replace;
   //addSimilarMonomials(polynData->leadTerms);


    /*if(fVerbose>0){
        printf("simplified Polynomial: ");
        printPolynomial(stdout, polynData->leadTerms, polynId);
    }*/
    //assert(polynData->leadTerms->size()<4096);
    return;    
}

void PreprocessMgr::applyModulo(const variableId& polynId){
    
       auto polynData= pMgr->getPolynomialData(polynId);
       auto leadTerms= polynData->leadTerms;
       auto modulo=polynData->modulo;
       if(modulo==0||modulo>64) return;
       auto erasedTerms=pMgr->allocatePolynomial();
       for(auto term:*leadTerms){
           auto coeff=term->first;
           while(absCoeff(coeff)>=modulo){
               if(coeff>0) coeff-=modulo;
               else coeff+=modulo;
           }
           if(coeff==0){
               erasedTerms->insert(term);
           }
       }
       for(auto term: *erasedTerms){
           leadTerms->erase(term);
       }
}

pPolynomial PreprocessMgr::invertVariableWithinPolynomial(pPolynomial currPolyn,
                                                      const variableId& elimVar ){
    
    pPolynomial newPolyn=pMgr->allocatePolynomial();
    pPolynomial product;

    pPolynomial subsPolyn=pMgr->allocatePolynomial();
    auto monl1=pMgr->allocateMonomial();
    auto monlv=pMgr->allocateMonomial(elimVar);
    auto term1=pMgr->allocateTerm(1, monl1);
    auto termv=pMgr->allocateTerm(-1, monlv);
    pMgr->insertTerm(subsPolyn, term1);
    pMgr->insertTerm(subsPolyn, termv);
    

    for(auto currTerm: *currPolyn){
        auto currCoeff=std::get<0>(*currTerm); 
        auto currMonl=std::get<1>(*currTerm);
        
        auto newMonl= pMgr->allocateMonomial();
        newMonl->insert(currMonl->begin(), currMonl->end());
        auto newTerm = pMgr->allocateTerm(currCoeff, newMonl);
        
        if(currMonl->empty()){ pMgr->insertTerm(newPolyn, newTerm); continue; }
        
        if(isInPMonomial(currMonl, elimVar)){
             newMonl->erase(elimVar);
        }
        else{   
            pMgr->insertTerm(newPolyn,newTerm); 
            continue; 
        }
           
        product=multiplyPolynWithMonl(newTerm, subsPolyn);
        assert(product!=NULL);
        
        pMgr->insertPolyn(newPolyn, product);
    }

    return newPolyn;
}


void PreprocessMgr::rewriteOnePolynomial(const variableId & polynId,
                                                        pMonomial removedVars ){
    
    pPolynomial newPolyn=pMgr->allocatePolynomial();
    pPolynomial product;

    auto currPolyn=pMgr->getPolynomialLeadTerms(polynId);

    for(auto currTerm: *currPolyn){
        auto currCoeff=std::get<0>(*currTerm); 
        auto currMonl=std::get<1>(*currTerm);
        
        auto newMonl= pMgr->allocateMonomial();
        newMonl->insert(currMonl->begin(), currMonl->end());
        auto newTerm = pMgr->allocateTerm(currCoeff, newMonl);
        
        if(currMonl->empty()){ pMgr->insertTerm(newPolyn, newTerm); continue; }
        
        auto intersectVars=getIntersectedVariables(removedVars, currMonl);
        
        if(intersectVars.empty()) { pMgr->insertTerm(newPolyn, newTerm); continue; }
        

        for (auto var: intersectVars){
            newMonl->erase(var);
        }
        
        if(intersectVars.size()==1){
            auto subsPolyn=pMgr->getPolynomialLeadTerms(*intersectVars.begin());
            product=multiplyPolynWithMonl(newTerm, subsPolyn);
        }
        else{
            auto subsPolyn_it=_substituteTable->find(intersectVars);
            assert(subsPolyn_it!=_substituteTable->end());
            auto subsPolyn=subsPolyn_it->second;
            if(subsPolyn->empty()) continue; // zero Polynomial
            product=multiplyPolynWithMonl(newTerm, subsPolyn);
        }
        assert(product!=NULL);
        
        pMgr->insertPolyn(newPolyn, product);
    }
    //addSimilarMonomials(newPolyn);
    currPolyn->swap(*newPolyn);
    
    return;
}


pSortedSizeTuples PreprocessMgr::buildSortedSizedSet(pMonomial remvVars){
    auto sortedSet = (pSortedSizeTuples) new sortedSizeTuples;
    for(auto var: *remvVars){
        auto varD=pMgr->getVariableData(var);
        auto totalSize=varD->fanoutSize*varD->substSize;
        sizeTuple newTuple= sizeTuple(var, totalSize);
        sortedSet->insert(newTuple);
    }
    return sortedSet;
}

void PreprocessMgr::rewritePolynomialWithInference(const variableId & polynId,
                                pMonomial removedVars, const bool & enableInference ){
    
    pPolynomial newPolyn=pMgr->allocatePolynomial();
    pPolynomial product;
    auto currPolyn= pMgr->getPolynomialLeadTerms(polynId);
    

   
    /*if(fVerbose>0){
        printf("Polynomial: ");
        printPolynomial(stdout, currPolyn, polynId);
    }*/

   for(auto currTerm: *currPolyn){
        auto currCoeff=std::get<0>(*currTerm); 
        auto currMonl=std::get<1>(*currTerm);

        auto newMonl= pMgr->allocateMonomial();
        newMonl->insert(currMonl->begin(), currMonl->end());
        auto newTerm = pMgr->allocateTerm(currCoeff, newMonl);
        
        if(currMonl->size()!=1) { pMgr->insertTerm(newPolyn,newTerm); continue; }
        auto intersectVars=getIntersectedVariables(removedVars, currMonl);

        if(intersectVars.empty()) { pMgr->insertTerm(newPolyn,newTerm); continue; }


        for (auto var: intersectVars){
            newMonl->erase(var);
        }

        if(intersectVars.size()==1){
            auto subsPolyn=pMgr->getPolynomialLeadTerms(*intersectVars.begin());
            product=multiplyPolynWithMonl(newTerm, subsPolyn);
        }
        else{
            assert(0);
        }
        assert(product!=NULL);
        pMgr->insertPolyn(newPolyn, product);
    }
    currPolyn->swap(*newPolyn);
    newPolyn->clear();

    auto currRemvVars=buildSortedSizedSet(removedVars);

    //pMgr->printPolynomial(stdout,currPolyn);
    // eliminate nonlinear terms 
    while (!currRemvVars->empty()){
        auto firstElement_it=currRemvVars->begin();
        auto elimVar=firstElement_it->first;
        //printf(" var %llu\n", elimVar);
        currRemvVars->erase(firstElement_it);
        
        auto subsPolyn=pMgr->getPolynomialLeadTerms(elimVar);
        
        for(auto currTerm: *currPolyn){
            auto currCoeff=std::get<0>(*currTerm); 
            auto currMonl=std::get<1>(*currTerm);

            auto newMonl= pMgr->allocateMonomial();
            newMonl->insert(currMonl->begin(), currMonl->end());
            auto newTerm = pMgr->allocateTerm(currCoeff, newMonl);

            if(currMonl->empty()){ pMgr->insertTerm(newPolyn,newTerm); continue; }

            //auto intersectVars=getIntersectedVariables(removedVars, currMonl);

            if(isInPMonomial(currMonl, elimVar)){
                 newMonl->erase(elimVar);
            }
            else{   
                pMgr->insertTerm(newPolyn,newTerm); 
                continue; 
            }
           
            product=multiplyPolynWithMonl(newTerm, subsPolyn);
            
            assert(product!=NULL);

            pMgr->insertPolyn(newPolyn, product);
            if(newPolyn->size()>maxSize) break;
        }
        
        if(newPolyn->size()>maxSize){
            for(auto var: *removedVars){
                pMgr->_depVars->insert(var);
                _eliminatedVars->erase(var);
            }
            removedVars->clear();
            newPolyn->clear();
            return;
        }
        currPolyn->swap(*newPolyn);
        newPolyn->clear();
        if(enableInference){
            currDepth=0;
            abctime clk_infer = Abc_Clock();
            applyInferenceRules(currPolyn);
            timeAnalysis.inference+= Abc_Clock() - clk_infer;
        }
    }
    
    return;
}

void PreprocessMgr::eraseFromCutsTable( const variableId & polynId ){
   
    return;
}


void PreprocessMgr::updateCutsTable( const variableId & polynId ){
    const int k=6;
    auto polynFanins=pMgr->getPolynomialFanins(polynId);
    auto newCuts= findVariableCuts(k,polynId, polynFanins);
    _cutsTable->erase(polynId);
    _cutsTable->insert(std::make_pair(polynId,newCuts));
    return;
}

void PreprocessMgr::eraseFromMonomialTable( const variableId & polynId ){
   
    
    auto polynData=pMgr->getPolynomialData(polynId);
    auto monlTable= pMgr->_monlTable;
    
    for(auto term: *polynData->leadTerms){
        auto monl=term->second;
        
        if(monl->size()<=1) continue;

        auto monl_it = monlTable->find(*monl);
        if(monl_it!=monlTable->end()){
            auto monlD=monl_it->second;
            auto polynId_it=monlD->inPolynIds.find(polynId);
    
            if(polynId_it!=monlD->inPolynIds.end()){
                monlD->inPolynIds.erase(polynId_it);
            }
            
            if(monlD->inPolynIds.empty()) monlTable->erase(monl_it);
        }
        for(auto var: *monl){
            auto varD=pMgr->getVariableData(var);
            assert(varD->fanoutSize!=0);
            varD->fanoutSize--;
        }
       
    }
   
    return;
}

void PreprocessMgr::addToMonomialTable( const variableId & polynId ){
    
    auto polynData=pMgr->getPolynomialData(polynId);
    
    auto monlTable= pMgr->_monlTable;
    
    for(auto term: *polynData->leadTerms){
      
        auto monl=term->second;
        if(monl->size()<=1) continue;

        auto monl_it = monlTable->find(*monl);
        if(monl_it==monlTable->end()){
            auto monlD=pMgr->allocateMonomialData();
            monlD->inPolynIds.insert(polynId);
            monlTable->insert(std::make_pair(*monl,monlD));
        }
        else{
            auto monlD=monl_it->second;
            monlD->inPolynIds.insert(polynId);
        }
       
        for(auto var: *monl){
            auto varD=pMgr->getVariableData(var);
            varD->fanoutSize++;
        }
    }
   
    return;
}

void PreprocessMgr::updateVariableData(pMonomial vars, const variableId & oldId, 
                                                    const variableId & newId ){
    for(auto var: *vars){
        auto var_it = pMgr->_varTable->find(var);
        if(var_it==pMgr->_varTable->end()){
            auto varD= pMgr->allocateVariableData();
            varD->inPolynIds->insert(newId);
            pMgr->_varTable->insert(std::make_pair(var,varD));
        }
        else{
            auto varD=var_it->second;
            auto oldId_it=varD->inPolynIds->find(oldId);
            if(oldId_it!=varD->inPolynIds->end()){ 
                varD->inPolynIds->erase(oldId_it);
            }
            varD->inPolynIds->insert(newId);
        }
    }
    return;
}

void PreprocessMgr::renewMonomialTable( ){
    
    auto monlTable= pMgr->_monlTable;
    auto varTable=pMgr->_varTable;
    monlTable->clear();
    varTable->clear();
    
    for(auto polyn_it: *pMgr->_ideal){
        auto id= polyn_it.first;
        auto leadTerms=polyn_it.second->leadTerms;
        auto inVars=polyn_it.second->inVars;
        inVars->clear();
        for(auto term: *leadTerms){
            auto monl=term->second;
            
            for(auto var :*monl){
                inVars->insert(var);
                auto var_it = varTable->find(var);
                if(var_it==varTable->end()){
                    auto varD= pMgr->allocateVariableData();
                    varD->inPolynIds->insert(id);
                    varD->fanoutSize++;
                    varTable->insert(std::make_pair(var,varD));
                }
                else{
                    auto varD=var_it->second;
                    varD->inPolynIds->insert(id);
                    varD->fanoutSize++;
                }
            }
            
            if(monl->size()<=1){
                continue;
            }

            auto monl_it = monlTable->find(*monl);
            if(monl_it==monlTable->end()){
                auto monlD=pMgr->allocateMonomialData();
                monlD->inPolynIds.insert(id);
                monlTable->insert(std::make_pair(*monl,monlD));
            }
            else{
                auto monlD=monl_it->second;
                monlD->inPolynIds.insert(id);
            }
        }
        pMgr->updateVariableSubstitutionSize(id, leadTerms->size());
    }
    
    return;
}

void PreprocessMgr::updatePolynomialData( const variableId &polynId,
                                                        pMonomial removedVars ){

    auto polynData=pMgr->getPolynomialData(polynId);
    auto leadTerms=polynData->leadTerms;
    
    /*if(fVerbose>0){
        printf("rewritten Polynomial: ");
        printPolynomial(stdout, polynData->leadTerms, polynId);
    }*/
    polynData->inVars->clear();
    for (auto term: *leadTerms){
        auto monl=term->second;
        polynData->inVars->insert(monl->begin(), monl->end());
    }
    for(auto var: *removedVars){
       //polynData->inVars->erase(var);
       auto newFanin=pMgr->getPolynomialFanins(var);
       //polynData->inVars->insert(newFanin->begin(), newFanin->end());
       updateVariableData(newFanin, var, polynId);
    }
    
    if(polynData->inVars->size()<4&&  polynData->lut!=NULL){
        polynData->lut->clear();
        for (auto i=0; i<std::exp2(polynData->inVars->size()); i++){
            std::bitset<8> assign (i);
            if(simulatePolynomial(polynData, assign)){   
                polynData->lut->insert(assign);
            }
        }
    }
    else polynData->lut=NULL;
    
    auto varD=pMgr->getVariableData(polynId);
    varD->substSize=polynData->leadTerms->size();
    
    return;
}

void PreprocessMgr::rewriteIdeal( const bool & enableInference){
    if(_removedTable->empty()) return;
    
    for(auto remvTable_it: *_removedTable){
        auto rewritePolynId = remvTable_it.first;
        auto removedVars = remvTable_it.second;
        
        //refineRemovedVariables(removedVars);

        
        abctime clk_rewr = Abc_Clock();
        eraseFromMonomialTable(rewritePolynId);
        //updateSubstitutionTable(rewritePolynId, removedVars);
        //rewriteOnePolynomial(rewritePolynId , removedVars);
        rewritePolynomialWithInference(rewritePolynId , removedVars, enableInference);
        if(isMostOutput(rewritePolynId)){
            applyModulo(rewritePolynId);
        }
        timeAnalysis.rewrite+= Abc_Clock() - clk_rewr;
        abctime clk_updateD = Abc_Clock();
        updatePolynomialData(rewritePolynId , removedVars);
        timeAnalysis.update+= Abc_Clock() - clk_updateD;
        abctime clk_infer = Abc_Clock();
        //applyInferenceRules(rewritePolynId);
        timeAnalysis.inference+= Abc_Clock() - clk_infer;
        
        addToMonomialTable(rewritePolynId);
//        updateCutsTable(rewritePolynId);
    }
    
    for(auto var: *_eliminatedVars){
        eraseFromMonomialTable(var);
        pMgr->_ideal->erase(var);
        pMgr->_varTable->erase(var);
  //      _cutsTable->erase(var);
    }
    
    renewMonomialTable( );
    _removedTable->clear();
    _substituteTable->clear();
    _eliminatedVars->clear();
    
    //if(fVerbose>0)
    //pMgr->printIdeal(stdout, fVerbose);
    return;
}

void PreprocessMgr::refineIdeal( const bool & enableInference){
    for(auto var: *_eliminatedVarsL){
        updateRemovedTable(var);
        pMgr->_depVars->erase(var);
        _eliminatedVars->insert(var);
        //printf("elemL:%llu\n",var);
    }
    
    //rewriteIdeal(enableInference);
    _eliminatedVarsL->clear();
    for(auto var: *_eliminatedVarsH){
        updateRemovedTable(var);
        pMgr->_depVars->erase(var);
        _eliminatedVars->insert(var);
        //printf("elemH:%llu\n",var);   
    }
    rewriteIdeal(enableInference);
    _eliminatedVarsH->clear();
}

PreprocessMgr* preprocessGroebnerIdeal(PolynMgr * pMgr, const int &verbose){
    abctime clk = Abc_Clock();
    PreprocessMgr* prepMgr= new PreprocessMgr(pMgr);
    prepMgr->fVerbose=verbose;

   fprintf(pMgr->logFile,"***c Preprocessing the Model \n\n");
    
   prepMgr->analyzeSpecPolynomial();
//   prepMgr->findAllCuts(2);
   
    if(verbose>0)
        fprintf(pMgr->logFile,"***c   Reveal XORs\n\n");
    fprintf(stdout,"***c   Reveal XORs\n\n");
    
    abctime clk_Revealing = Abc_Clock();
    prepMgr->refineDepVarsFanIns();
    prepMgr->refineIdeal(true);
   // printf("new elements\n\n");
   // prepMgr->refineDepVarsCuts(2);
   // prepMgr->refineIdeal(true);
    //exit(0);
    if(verbose>0)
        printSC4AVTime(pMgr->logFile, "***c XOR-Revealing Time", 
                                                Abc_Clock() - clk_Revealing );

    if(verbose>0)
        fprintf(pMgr->logFile,"***c   Polynomials Factorization\n\n");
    fprintf(stdout,"***c   Polynomials Factorization\n\n");
    
    abctime clk_Factorization = Abc_Clock();
    prepMgr->polynomialFactorization();
   
    if(verbose>0)
         printSC4AVTime(pMgr->logFile, "***c Factorization Time", 
                                                 Abc_Clock() - clk_Factorization);
    if(verbose>0)
        fprintf(pMgr->logFile,"***c   XOR Common Rewritting\n\n");
    fprintf(stdout,"***c   XOR Common Rewritting\n\n");
    abctime clk_XORCommon = Abc_Clock();
    abctime inference_XORCommon =prepMgr->timeAnalysis.inference;

    prepMgr->refineDepVarsXORs();
    prepMgr->refineIdeal(true);

    prepMgr->refineDepVarsSingular();
    prepMgr->refineIdeal(true);
    
    inference_XORCommon =prepMgr->timeAnalysis.inference-inference_XORCommon;
    if(verbose>0)
        printSC4AVTime(pMgr->logFile, "***c XOR-Common-Rewriting Time", 
                                                Abc_Clock() - clk_XORCommon -inference_XORCommon);
    if(verbose>0)
        fprintf(pMgr->logFile,"***c   Coupling Rewritting\n\n");
    fprintf(stdout,"***c   Coupling Rewritting\n\n");
    
    abctime clk_Coupling = Abc_Clock();
    abctime inference_Coupling =prepMgr->timeAnalysis.inference;
    //pMgr->printIdeal(stdout,0);
    prepMgr->refineDepVarsCoupling();
    prepMgr->refineIdeal(true);
    prepMgr->refineDepVarsCoupling();
    prepMgr->rewriteIdeal(true);
    //prepMgr->refineDepCutsCoupling();
    //prepMgr->refineIdeal(true);


    inference_Coupling =prepMgr->timeAnalysis.inference-inference_Coupling;
    if(verbose>0)
        printSC4AVTime(pMgr->logFile, "***c Coupling-Rewriting Time", 
                              Abc_Clock() - clk_Coupling- inference_Coupling );
    //if(verbose>0)
       // prepMgr->printMonomialTable();
    
 
    if(verbose>0){
        printSC4AVTime(pMgr->logFile, "\n\n***c Inference Time",  prepMgr->timeAnalysis.inference);
        fprintf(pMgr->logFile, "***c Number of Terms removed by Subsumed Inference: %d\n", prepMgr->_inferenceAnalysis.subsumed);
        fprintf(pMgr->logFile, "***c Number of Terms removed by Nonintersected Inference: %d\n", prepMgr->_inferenceAnalysis.nonIntersected);
        printSC4AVTime(pMgr->logFile, "\n\n***c Preprocess Time", Abc_Clock() - clk );
    }
    return prepMgr;
}
