/**CPPFile****************************************************************

  FileName    [polynMgr.cpp]

  SystemName  [SC4AV.]

  PackageName [prover]

  Synopsis    [Construct and Destruct polynMgr.]

  Author      [Amr Sayed Ahmed]
  
  Affiliation [NTU]

  Date        [Ver. 1.0. Started - MAY, 2017.]

  Revision    []

***********************************************************************/

#include"INTprover/polyn.h"
#include "misc/util/abc_global.h"


    PolynMgr::PolynMgr( ){
        logFile=NULL;
        _varTable = (pVariableTable) new VariableTable;
        _monlTable = (pMonomialTable) new MonomialTable;
        _ideal = (pIdeal) new Ideal;
        _depVars = (pMonomial) new Monomial;
        _inputVars = (pMonomial ) new Monomial ;
        _outputVars = (pMonomial) new Monomial;
        _polynId2Aig = (pIdTable) new IdTable;
        _specPolyn = (pEXPPolynomial) new EXPPolynomial;
     }


    PolynMgr::~PolynMgr( ){
        closeLogFile();
    }

    
    pMonomial PolynMgr::allocateMonomial(){
        return (pMonomial) new Monomial;
    }
    
    pMonomial PolynMgr::allocateMonomial(const variableId& v){
        pMonomial m= (pMonomial) new Monomial;
        m->insert(v);
        return m;
    }
    
    pMonomial PolynMgr::allocateMonomial(const variableId& v1, const variableId& v2){
        pMonomial m= (pMonomial) new Monomial;
        m->insert(v1);
        m->insert(v2);
        return m;
    }
    
    
    void PolynMgr::printTerm(FILE* &out,  pTerm t)
    {
        auto coeff=t->first;
        auto m=t->second;
        if(coeff>=0) 
            fprintf(out, "+%ld", coeff);
        else 
            fprintf(out, "%ld", coeff);
        
        if (!m->empty())
            fprintf(out, "*", coeff);
        else    
            return;
        
        size_t i =0;
        for ( auto& var : *m){
                i++;
                if(i!=m->size())
                        fprintf(out,"v_%llu*", var);
                else
                        fprintf(out,"v_%llu", var);
        }
        return;
    }
    
        void PolynMgr::printEXPTerm(FILE* &out,  pEXPTerm t)
    {
        auto coeff=t->first;
        auto m=t->second;

        if(coeff>=0){
            gmp_fprintf(out, "+%Zd", coeff);
        }
        else{
            EXPCoefficient minusOne;
            mpz_set_si(minusOne.get_mpz_t(),-1);
            minusOne*=coeff;
            gmp_fprintf(out, "-%Zd", minusOne);
        }
       
        if (!m->empty()){
            gmp_fprintf(out, "*");
        }
        else return;
        size_t i =0;
        for ( auto& var : *m){
                i++;
                if(i!=m->size())
                        fprintf(out,"v_%llu*", var);
                else
                        fprintf(out,"v_%llu", var);
        }
        return;
    }

    void PolynMgr::printPolynomial(FILE* &out, pPolynomial p )
    {
            if(p->empty()){
                    fprintf(out,"Zero Polynomial\n");
                    return;
            }
            size_t i=0;
            for ( auto& term : *p){
                    i++;
                    printTerm(out, term);
                   // if( i!=p->size()){ fprintf(out, "+"); }
            }
            fprintf(out, "=0;");
            return;
    }
    
        void PolynMgr::printEXPPolynomial(FILE* &out, pEXPPolynomial p )
    {
            if(p->empty()){
                    fprintf(out,"Zero Polynomial\n");
                    return;
            }
            size_t i=0;
            for ( auto& term : *p){
                    i++;
                    printEXPTerm(out, term);
                   //if( i!=p->size()){ fprintf(out, "+"); }
            }
            fprintf(out, "=0;");
            return;
    }
    
        void PolynMgr::printPrimaryInputs(FILE* &out, Monomial* m )
    {
            if(m->empty()){
                    fprintf(out,"empty primary input vector\n");
                    return;
            }
            size_t i=0;
            fprintf(out, "\t{in: ");
            for ( auto& v : *m){
                    i++;
                    fprintf(out,"v_%llu", v);
                    if( i!=m->size()){ fprintf(out, ","); }
            }
            fprintf(out, "}");
            return;
    }

    void PolynMgr::printIdeal(FILE *&out, const int& fVerbose)
    {
        if(_ideal->empty()){
            fprintf(out, "Ideal is empty\n");
            return;
        }
        for ( auto& it : *_ideal){
            fprintf(out,"-1*v_%llu ", it.first);
            printPolynomial(out, it.second->leadTerms);
            if(fVerbose>0) 
                printPrimaryInputs(out, &it.second->primaryInVars);
            fprintf(out, "\n\n");
        }
        return;
    }

    void PolynMgr::openLogFile(vecStr* fileName){
        if(fileName->size()==0){
            logFile=stdout;
            return;
        }
        logFile=fopen(fileName->c_str(),"w");
        assert(logFile!=NULL);
    }

    void PolynMgr::closeLogFile(){
        if(logFile!=NULL)
            fclose(logFile);
    }      
    
    void PolynMgr::updateMonomialVariableTables(pMonomial pmonl, const variableId& id){
        auto monl=*pmonl; 
        auto monl_it = _monlTable->find(monl);
        if(monl_it==_monlTable->end()){
            auto monlD= allocateMonomialData();
            monlD->inPolynIds.insert(id);
            _monlTable->insert(std::make_pair(monl,monlD));
        }
        else{
            auto monlD=monl_it->second;
            monlD->inPolynIds.insert(id);
        }
        
        for(auto var :monl){
            auto var_it = _varTable->find(var);
            if(var_it==_varTable->end()){
                auto varD= allocateVariableData();
                varD->inPolynIds->insert(id);
                varD->fanoutSize++;
                _varTable->insert(std::make_pair(var,varD));
            }
            else{
                auto varD=var_it->second;
                varD->inPolynIds->insert(id);
                varD->fanoutSize++;
            }
        }
    }
    
    void PolynMgr::addPolyn2Ideal(const variableId & key, pPolynomialData p){
        
        updatePrimaryInVariables(p);    
        _ideal->insert(std::make_pair(key, p));
    }

    void PolynMgr::updatePrimaryInVariables(pPolynomialData p){
        
        assert(!p->inVars->empty());
        
        for(auto var: *p->inVars){
            if(isPrimaryInput(var)){
                p->primaryInVars.insert(var);
                continue;
            }
            auto var_it = _ideal->find(var);
            assert(var_it!=_ideal->end());
            assert(!var_it->second->primaryInVars.empty());
            auto var_fanin= var_it->second->primaryInVars;
            p->primaryInVars.insert(var_fanin.begin(), var_fanin.end());
        }
        
    }
    
    void PolynMgr::updateVariableSubstitutionSize(const variableId& var , const size_t& size){
        auto var_it = _varTable->find(var);
        if(var_it==_varTable->end()){
            auto varD= allocateVariableData();
            varD->substSize=size;
            _varTable->insert(std::make_pair(var,varD));
        }
        else{
            auto varD=var_it->second;
            varD->substSize=size;
        }
        
    }