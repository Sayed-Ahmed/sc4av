module miter ( a, b, lhs, rhs );

 input wire  [63:0] a, b;

 wire [31:0] al, ah, bl, bh;
 wire [63:0] a0, a1, b0, b1;
 wire [127:0] p0, p1, p2, p3, t1, t2;

 output wire [127:0] lhs, rhs;

 assign al = a[31:0];
 assign ah = a[63:32];

 assign bl = b[31:0];
 assign bh = b[63:32];

 assign a0 = {32'h0, al};
 assign b0 = {32'h0, bl};

 assign a1 = {ah, 32'h0};
 assign b1 = {bh, 32'h0};

 assign p0 = a0 * b0;
 assign p1 = a1 * b0;
 assign p2 = a0 * b1;
 assign p3 = a1 * b1;

 assign t1  = p0 + p1;
 assign t2  = t1 + p2;
 assign lhs = t2 + p3;

 assign rhs = a * b;


endmodule
