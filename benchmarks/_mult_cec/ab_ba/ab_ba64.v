module miter ( a, b, t1, t2 );

 input wire  [63:0] a, b;

 output wire [127:0] t1, t2;

 assign t1 = a * b;
 assign t2 = b * a;

endmodule
