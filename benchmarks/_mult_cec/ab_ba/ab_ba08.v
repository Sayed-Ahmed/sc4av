module miter ( a, b, m );

 input wire  [7:0] a, b;

 output wire [15:0] t1, t2;


 assign t1 = a * b;
 assign t2 = b * a;


endmodule
