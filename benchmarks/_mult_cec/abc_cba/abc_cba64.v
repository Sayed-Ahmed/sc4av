module miter ( a, b, c, t1, t2 );

 input wire  [63:0] a, b, c;

 output wire [191:0] t1, t2;

 wire [191:0] s1, s2;

 assign s1 = a * b;
 assign t1 = s1 * c;

 assign s2 = c * b;
 assign t2 = s2 * a;

endmodule
