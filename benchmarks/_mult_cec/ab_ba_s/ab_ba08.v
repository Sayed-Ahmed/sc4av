module miter ( a, b, t1, t2 );

 input signed [7:0] a, b;

 output wire signed [15:0] t1, t2;

 assign t1 = a * b;
 assign t2 = b * a;


endmodule
