module miter ( a, b, t1, t2 );

 input wire  [7:0] a, b;

 output wire [15:0] t1, t2;
 
 wire [2:0] part1 = a[2:0];
 wire [7:4] part2 = a[7:4];
 wire [7:0] ai = {part2, 1'b0, part1};

 assign t1 = ai * b;
 assign t2 = b * ai;

endmodule
