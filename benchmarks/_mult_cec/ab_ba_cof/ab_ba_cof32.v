module miter ( a, b, t1, t2 );

 input wire  [31:0] a, b;

 output wire [63:0] t1, t2;
 
 wire [12:0] part1 = a[12:0];
 wire [31:14] part2 = a[31:14];
 wire [31:0] ai = {part2, 1'b0, part1};

 assign t1 = ai * b;
 assign t2 = b * ai;

endmodule
