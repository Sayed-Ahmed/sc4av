module miter ( a, b, lhs, rhs );

 input wire  [15:0] a, b;

 wire [7:0] al, ah, bl, bh;
 wire [15:0] a0, a1, b0, b1;
 wire [31:0] p0, p1, p2, p3, t1, t2;

 output wire [31:0] lhs, rhs;


 assign al = a[7:0];
 assign ah = a[15:8];

 assign bl = b[7:0];
 assign bh = b[15:8];

 assign a0 = {8'h0, al};
 assign b0 = {8'h0, bl};

 assign a1 = {ah, 8'h0};
 assign b1 = {bh, 8'h0};

 assign p0 = a0 * b0;
 assign p1 = b0 * a1;
 assign p2 = a0 * b1;
 assign p3 = b1 * a1;

 assign t1  = p0 + p1;
 assign t2  = p2 + t1;
 assign lhs = t2 + p3;

 assign rhs = a * b;


endmodule
