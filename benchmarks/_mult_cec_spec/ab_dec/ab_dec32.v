module miter ( a, b, lhs, rhs );

 input wire  [31:0] a, b;

 wire [15:0] al, ah, bl, bh;
 wire [31:0] a0, a1, b0, b1;
 wire [63:0] p0, p1, p2, p3, t1, t2;

 output wire [63:0]  lhs, rhs;

 assign al = a[15:0];
 assign ah = a[31:16];

 assign bl = b[15:0];
 assign bh = b[31:16];

 assign a0 = {16'h0, al};
 assign b0 = {16'h0, bl};

 assign a1 = {ah, 16'h0};
 assign b1 = {bh, 16'h0};

 assign p0 = a0 * b0;
 assign p1 = a1 * b0;
 assign p2 = a0 * b1;
 assign p3 = a1 * b1;

 assign t1  = p0 + p1;
 assign t2  = t1 + p2;
 assign lhs = t2 + p3;

 assign rhs = a * b;

endmodule
