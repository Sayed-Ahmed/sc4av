module miter ( a, b, t1, t2 );

 input wire  [63:0] a, b;

 output wire [127:0] t1, t2;
 
 wire [30:0] part1 = a[30:0];
 wire [63:32] part2 = a[63:32];
 wire [63:0] ai = {part2, 1'b0, part1};

 assign t1 = ai * b;
 assign t2 = b * ai;


endmodule
