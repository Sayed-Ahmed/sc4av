module miter ( a, b, t1, t2 );

 input wire  [15:0] a, b;

 output wire [31:0] t1, t2;
 
 
 wire [7:0] part1 = a[7:0];
 wire [15:9] part2 = a[15:9];
 wire [15:0] ai = {part2, 1'b0, part1};

 assign t1 = ai * b;
 assign t2 = b * ai;


endmodule
